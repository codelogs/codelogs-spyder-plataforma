/*
 * The MIT License (MIT)
 * =========================================================================================================
 *
 * Copyright (c) 10/10/16 12:30 Modulo  Proyecto rexcode-sync  Autor [rfcardenas]
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY
 * , WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ec.edu.utpl.datalab.codelogs.spyder.sync;

import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.google.inject.Singleton;
import ec.edu.utpl.datalab.codelogs.spyder.core.env.EnvironmentProperties;
import ec.edu.utpl.datalab.codelogs.spyder.core.persistencia.RepositoryService;
import ec.edu.utpl.datalab.codelogs.spyder.core.synchronization.RestService;
import ec.edu.utpl.datalab.codelogs.spyder.sync.network.DefaultNetworkProvider;
import ec.edu.utpl.datalab.codelogs.spyder.sync.network.NetworkProvider;
import ec.edu.utpl.datalab.codelogs.spyder.sync.network.NetworkService;
import ec.edu.utpl.datalab.codelogs.spyder.sync.network.RestServiceBaseImpl;
import ec.edu.utpl.datalab.codelogs.spyder.sync.tasks.TaskBook;

import java.util.Objects;

/**
 * * Created by rfcardenas
 */
public class InjectionSystem extends AbstractModule {
    private EnvironmentProperties configurationSync;
    private RepositoryService repositoryService;

    public InjectionSystem(EnvironmentProperties environmentProperties, RepositoryService repositoryService) {
        this.configurationSync = Objects.requireNonNull(environmentProperties, "se requiere networkmodule");
        this.repositoryService = Objects.requireNonNull(repositoryService, "Se requiere repo module");
    }

    @Override
    protected void configure() {
        // bind(new TypeLiteral<AbstractPushTask<FileCode>>(){}).to(CreateFileTask.class).in(new SingletonScope());
        // bind(new TypeLiteral<AbstractPushTask<Project>>(){}).to(CreateProjectTask.class).in(new SingletonScope());
        // bind(new TypeLiteral<AbstractPushTask<PulseCode>>(){}).to(CreatePulseTask.class).in(new SingletonScope());
        // bind(new TypeLiteral<AbstractPushTask<WorkSession>>(){}).to(CreateSessionTask.class).in(new SingletonScope());
        bind(TaskBook.class).in(Singleton.class);

        //Link implementacion de las interfaces del codigo base


        bind(RestService.class).to(RestServiceBaseImpl.class);

        bind(StrategyOnDemand.class);
        bind(StrategyFull.class);
        /**bind(CreateProjectTask.class).in(new SingletonScope());
         bind(CreatePulseTask.class).in(new SingletonScope());
         bind(CreateSessionTask.class).in(new SingletonScope());
         bind(CreateFileTask.class).in(new SingletonScope());
         bind(FixDataGrantedTask.class).in(new SingletonScope());
         bind(SyncOnDemand.class).in(new SingletonScope());**/

    }

    @Provides
    @Singleton
    EnvironmentProperties environmentProperties() {
        return environmentProperties();
    }

    @Provides
    @Singleton
    NetworkService networkService() {
        NetworkProvider retro = new DefaultNetworkProvider(configurationSync);
        return retro.network();
    }

    @Provides
    RepositoryService repositoryService() {
        return repositoryService;
    }


}
