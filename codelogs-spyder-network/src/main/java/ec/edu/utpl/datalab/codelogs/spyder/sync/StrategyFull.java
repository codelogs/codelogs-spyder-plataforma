/*
 * The MIT License (MIT)
 * =========================================================================================================
 *
 * Copyright (c) 10/10/16 12:30 Modulo  Proyecto rexcode-sync  Autor [rfcardenas]
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY
 * , WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ec.edu.utpl.datalab.codelogs.spyder.sync;


import ec.edu.utpl.datalab.codelogs.spyder.core.persistencia.RepositoryService;
import ec.edu.utpl.datalab.codelogs.spyder.core.synchronization.Sync;
import ec.edu.utpl.datalab.codelogs.spyder.libs.ia.behaviortree.BehaviorTree;
import ec.edu.utpl.datalab.codelogs.spyder.libs.ia.behaviortree.branch.Selector;
import ec.edu.utpl.datalab.codelogs.spyder.libs.ia.behaviortree.branch.Sequence;
import ec.edu.utpl.datalab.codelogs.spyder.sync.tasks.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;

/**
 * * Created by rfcardenas
 */
public final class StrategyFull extends BehaviorTree<DataBoard> implements Sync {
    private static final Logger log = LoggerFactory.getLogger(AbstractPushTask.class);
    private TaskBook taskBook;

    @Inject
    public StrategyFull(TaskBook taskBook, DataBoard dataBoard) {
        this.taskBook = taskBook;
        this.setObject(dataBoard);
        initTree();
    }

    private void initTree() {
        CreatePulseTask pulseTask = taskBook.createPulseTask(() -> {
            RepositoryService repo = getObject().getRepositoryService();
            return repo.pulseCodeRepository().getAllUnSyncEntries();
        });

        CreateProjectTask projectTask = taskBook.createProjectTask(() -> {
            RepositoryService repo = getObject().getRepositoryService();
            return repo.projectRepository().getAllUnSyncEntries();
        });

        CreateSessionTask sessionTask = taskBook.createSessionTask(() -> {
            RepositoryService repo = getObject().getRepositoryService();
            return repo.worksessionRepository().getAllUnSyncEntries();
        });

        CreateFileTask fileTask = taskBook.createFileTask(() -> {
            RepositoryService repo = getObject().getRepositoryService();
            return repo.fileCodeRepository().getAllUnSyncEntries();
        });

        // Selector de acciones
        Selector<DataBoard> selector = new Selector<>();
        // Secuencia de acciones cuando cuando no esta un archivo en server
        Sequence<DataBoard> sequence1 = new Sequence<>();
        sequence1.addChild(fileTask);
        // sequence1.addChild(taskBook.createPulseTask());

        /**
         * Secuencia de acciones cuando no esta la session en el server
         */
        Sequence<DataBoard> sequence2 = new Sequence<>();
        sequence2.addChild(sessionTask);
        sequence2.addChild(pulseTask);


        /**
         * Tarea costosa ejecutada al final si las otras fallan
         * Secuencia final replicar todo
         */
        Sequence<DataBoard> sequence4 = new Sequence<>();
        sequence4.addChild(projectTask);
        sequence4.addChild(sessionTask);
        sequence4.addChild(fileTask);
        sequence4.addChild(pulseTask);


        Sequence<DataBoard> sequence3 = new Sequence<>();
        // sequence3.addChild(taskBook.fixDataGranted(AbstractPushTask.TYPE.FULL));


        //Tarea ligera
        //   selector.addChild(taskBook.createPulseTask());
        //TArea costo nivel 1
        selector.addChild(sequence1);
        //TArea costo nivel 2
        //selector.addChild(sequence2);
        //TArea costo nivel 4
        //  selector.addChild(sequence4);
        // tarea ultimo recurso [TRABAJO INTEGRIDAD DE DATOS ]
        // selector.addChild(sequence3);


        super.addChild(selector);
    }


    @Override
    public void execute() {
        step();
    }
}
