/*
 * The MIT License (MIT)
 * =========================================================================================================
 *
 * Copyright (c) 10/10/16 12:30 Modulo  Proyecto rexcode-sync  Autor [rfcardenas]
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY
 * , WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ec.edu.utpl.datalab.codelogs.spyder.sync.network;


import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import ec.edu.utpl.datalab.codelogs.spyder.core.env.EnvironmentProperties;
import ec.edu.utpl.datalab.codelogs.spyder.core.env.PropertiesDefault;
import okhttp3.ConnectionPool;
import okhttp3.Dispatcher;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.jackson.JacksonConverterFactory;
import rx.Observable;
import rx.Single;
import rx.subjects.PublishSubject;

import javax.inject.Inject;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * * Created by rfcardenas
 */

public class DefaultNetworkProvider implements NetworkProvider {
    private static final Logger log = LoggerFactory.getLogger(DefaultNetworkProvider.class);
    private PublishSubject<Object> publishSubject = PublishSubject.create();
    private Retrofit retrofit;
    private NetworkService networkService;

    @Inject
    public DefaultNetworkProvider(EnvironmentProperties configurationSync) {
        init(configurationSync);
    }

    private void init(EnvironmentProperties configurationSync) {
        boolean debugEnable = configurationSync.getAsBolean(PropertiesDefault.MONITOR_DEBUG, true);
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.findAndRegisterModules();
        objectMapper.configure(DeserializationFeature.FAIL_ON_IGNORED_PROPERTIES, false);
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);


        Dispatcher dispatcher = new Dispatcher(Executors.newSingleThreadExecutor());
        dispatcher.setMaxRequests(1);
        dispatcher.setMaxRequestsPerHost(1);

        OkHttpClient.Builder httpClientBuilder = new OkHttpClient.Builder()
            .connectTimeout(15, TimeUnit.SECONDS)
            .readTimeout(15, TimeUnit.SECONDS)
            .connectionPool(new ConnectionPool(5, 10, TimeUnit.SECONDS))
            .dispatcher(dispatcher)
            .addInterceptor(chain -> {
                String protocol = configurationSync.getProperty(PropertiesDefault.SYNC_PROTOCOL, "");
                String apikey = configurationSync.getProperty(PropertiesDefault.SYNC_APIKEY, "");
                if (debugEnable) {
                    log.info("Authorization {} {}", protocol, apikey);
                    log.info(chain.request().toString());
                }
                Request request = chain.request()
                    .newBuilder().addHeader("Authorization", protocol + " " + apikey)
                    .addHeader("Content-Type", "application/json")
                    .build();

                return chain.proceed(request);
            });
        if (debugEnable) {
            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            httpClientBuilder.addInterceptor(interceptor);
        }
        OkHttpClient okHttpClient = httpClientBuilder.build();

        retrofit = new Retrofit.Builder()
            .baseUrl(configurationSync.getProperty(PropertiesDefault.SYNC_SERVER))
            .client(okHttpClient)
            .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
            .addConverterFactory(JacksonConverterFactory.create(objectMapper))
            .build();
        networkService = retrofit.create(NetworkService.class);
    }

    private <T> void configure(Single<T> single, Object dto) {
        single.doOnError(throwable -> {
            publishSubject.onNext(NetworkBeat.createFail(dto, throwable));
        }).subscribe(projectDto -> {
            publishSubject.onNext(NetworkBeat.createSuccess(projectDto));
        }, Throwable::printStackTrace);

    }

    public Observable observable() {
        return publishSubject.asObservable();
    }

    @Override
    public NetworkService network() {
        return networkService;
    }
}
