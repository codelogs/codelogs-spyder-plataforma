/*
 * The MIT License (MIT)
 * =========================================================================================================
 *
 * Copyright (c) 10/10/16 12:30 Modulo  Proyecto rexcode-sync  Autor [rfcardenas]
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY
 * , WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ec.edu.utpl.datalab.codelogs.spyder.sync.network;

import ec.edu.utpl.datalab.codelogs.spyder.libs.transfers.*;
import retrofit2.http.Body;
import retrofit2.http.POST;
import rx.Single;

/**
 * Interfaz de conexión con el servidor,
 * proporciona los metodos básicos para interactuar Spyder -> Codelogs Server
 * el servidor retorna los contextos y el spyder envia la data contextualizada
 * minimizando la carga del servidor.
 * * Created by rfcardenas
 */
public interface NetworkService {

    String API_ANALYSYS     = "/api/v1/spyder/code_analysis";
    String API_FILES        = "/api/v1/spyder/node/file";
    String API_PROJECTS     = "/api/v1/spyder/node/project";
    String API_PULSES       = "/api/v1/spyder/heartbeatcode";
    String API_SESSIONS     = "/api/v1/spyder/node/work";
    String API_COMPILATIONS = "/api/v1/spyder/beat_run_compile";

    /**
     * ------------------------NETWORK SERVICE------------------------
     * Registra el resultado de analisis de código
     * @param data @link {@link CodeAnalysisPack}
     * @return GrantedContextPack @link {@link GrantedContextPack}
     * @implSpec la implementacion por defecto envia los datos al servidor
     */
    @POST(API_ANALYSYS)
    Single<GrantedContextPack> create(@Body CodeAnalysisPack data);

    /**
     * Crea un nodo de tipo archivo en servidor
     * @param data
     * @return
     */
    @POST(API_FILES)
    Single<GrantedContextPack> create(@Body FileCodePack data);

    /**
     * Crea un nodo de tipo proyecto en el servidor
     * @param data
     * @return
     */
    @POST(API_PROJECTS)
    Single<GrantedContextPack> create(@Body ProjectPack data);

    /**
     * crea un pulso de actividad de progración
     * @param data
     * @return
     */
    @POST(API_PULSES)
    Single<GrantedContextPack> create(@Body HeartBeatCodePack data);

    /**
     * Crea una session de trabajo
     * @param data
     * @return
     */
    @POST(API_SESSIONS)
    Single<GrantedContextPack> create(@Body WorkSessionPack data);

    /**
     * Crea los resultados de compilaciones o ejecuciones de un programa
     * ver java tool transformer pack, programa que trabaja a nivel de bytecode
     * para capturar los problemas y tiempos en contrucción y ejecución
     * @param data
     * @return
     */
    @POST(API_COMPILATIONS)
    Single<GrantedContextPack> create(@Body HeartBeatBuildRunPack data);

}
