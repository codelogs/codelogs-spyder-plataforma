/*
 * The MIT License (MIT)
 * =========================================================================================================
 *
 * Copyright (c) 10/10/16 12:30 Modulo  Proyecto rexcode-sync  Autor [rfcardenas]
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY
 * , WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ec.edu.utpl.datalab.codelogs.spyder.sync.tasks;

import ec.edu.utpl.datalab.codelogs.spyder.core.models.FileCode;
import ec.edu.utpl.datalab.codelogs.spyder.libs.transfers.GrantedContextPack;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;
import java.util.Optional;

/**
 * * Created by rfcardenas
 */
public class CreateFileTask extends AbstractPushTask<FileCode> {
    private static final Logger log = LoggerFactory.getLogger(CreateFileTask.class);

    public CreateFileTask(NetworkPush networkPush, Collector<Collection<FileCode>> collector) {
        super(networkPush, collector);
    }


    @Override
    boolean isValidTrama(FileCode data) {
        if (data.getProject() == null) {
            return false;
        }
        return true;
    }

    @Override
    public void onSuccess(FileCode entity, Optional<GrantedContextPack> data) {
        data.ifPresent(key -> {
            entity.setUuid(key.getUuid());
            entity.setSync(true);
            getObject().saveData(entity);
        });
    }


}
