/*
 * The MIT License (MIT)
 * =========================================================================================================
 *
 * Copyright (c) 10/10/16 12:30 Modulo  Proyecto rexcode-sync  Autor [rfcardenas]
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY
 * , WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ec.edu.utpl.datalab.codelogs.spyder.sync.tasks;

import ec.edu.utpl.datalab.codelogs.spyder.libs.transfers.GrantedContextPack;
import ec.edu.utpl.datalab.codelogs.spyder.core.models.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;
import java.util.Optional;

/**
 * Esta clase trata con conflictos de tipo:
 * EL SERVIDOR NO TIENE LA DATA
 * * Created by rfcardenas
 */
public class FixDataGrantedTask extends AbstractPushTask<EntityVisitable> {
    private static final Logger log = LoggerFactory.getLogger(CreateSessionTask.class);

    private boolean shouldRun = true;

    public FixDataGrantedTask(NetworkPush networkPush, Collector<Collection<EntityVisitable>> collector) {
        super(networkPush, collector);
    }


    /**
     * @Override public void execute() {
     * log.info("FIXXXXXXX");
     * shouldRun = true;
     * Stack<EntityVisitable> dataConfl = getObject().getFailedElements();
     * while(shouldRun && !dataConfl.isEmpty()){
     * EntityVisitable entityVisitable = dataConfl.pop();
     * entityVisitable.applyFunction(new FixDataGranted());
     * }
     * // Si no tiene que correr entonces borre la data temporal
     * if(!shouldRun) getObject().getFailedElements().clear();
     * }
     **/

    @Override
    boolean isValidTrama(EntityVisitable data) {
        return true;
    }

    @Override
    public void onSuccess(EntityVisitable entity, Optional<GrantedContextPack> data) {
        shouldRun = true;
    }

    @Override
    public void onError(EntityVisitable entity) {
        shouldRun = false;
    }

    private class FixDataGranted implements EntityVisitor {

        @Override
        public void function(Project project) {
            log.warn("Corrigiendo Data granted server node Project{}", project.getName());
            getObject().saveData(project);
        }

        @Override
        public void function(WorkSession session) {
            log.warn("Corrigiendo Data granted server node WorkSession{}", session.getId());
            getObject().saveData(session);
        }

        @Override
        public void function(HeartBeatCode heartBeatCode) {
            Project project = heartBeatCode.getFileCode().getProject();
            FileCode fileCode = heartBeatCode.getFileCode();
            WorkSession workSession = heartBeatCode.getWorkSession();

            push(project, data -> {
                if (data.status == DataStatus.SUCCESS) {
                    GrantedContextPack dataserver = data.dataserver.get();
                    project.setUuid(dataserver.getUuid());
                    getObject().saveData(project);
                    function(project);
                }
            });
            push(fileCode, data -> {
                if (data.status == DataStatus.SUCCESS) {
                    GrantedContextPack dataserver = data.dataserver.get();
                    fileCode.setUuid(dataserver.getUuid());
                    getObject().saveData(fileCode);
                    function(fileCode);
                }
            });

            push(workSession, data -> {
                if (data.status == DataStatus.SUCCESS) {
                    GrantedContextPack dataserver = data.dataserver.get();
                    workSession.setUuid(dataserver.getUuid());
                    function(workSession);
                } else {
                    // Si falla luego de actualizar la data en el server indicarle que no continue
                    shouldRun = false;
                }
            });

        }

        @Override
        public void function(FileCode fileCode) {
            log.warn("Corrigiendo Data granted server node FileCode {}", fileCode.getName());
            getObject().saveData(fileCode);
        }

        @Override
        public void function(HeartBeatBuildRun data) {
        }


    }
}
