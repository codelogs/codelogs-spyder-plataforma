package ec.edu.utpl.datalab.codelogs.spyder.sync.network;

/**
 * * Created by rfcardenas
 */
public interface NetworkProvider {
    NetworkService network();
}
