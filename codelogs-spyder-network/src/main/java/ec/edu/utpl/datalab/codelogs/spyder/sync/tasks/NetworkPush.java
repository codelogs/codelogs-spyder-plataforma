/*
 * The MIT License (MIT)
 * =========================================================================================================
 *
 * Copyright (c) 10/10/16 12:30 Modulo  Proyecto rexcode-sync  Autor [rfcardenas]
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY
 * , WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ec.edu.utpl.datalab.codelogs.spyder.sync.tasks;


import ec.edu.utpl.datalab.codelogs.spyder.core.mappers.MinerMap;
import ec.edu.utpl.datalab.codelogs.spyder.core.models.*;
import ec.edu.utpl.datalab.codelogs.spyder.core.synchronization.RestService;
import ec.edu.utpl.datalab.codelogs.spyder.libs.transfers.*;

import javax.inject.Inject;
import java.util.Optional;
import java.util.function.Consumer;

/**
 * Clase principal interactua con el servidor, notifica cuando existe un exito o fracaso
 * * Created by rfcardenas
 */
public class NetworkPush {

    private RestService networkService;

    @Inject
    public NetworkPush(RestService networkService) {
        this.networkService = networkService;
    }


    public <T extends EntityVisitable> void push(T entityVisitable, Consumer<DataConsumer<T>> consumer) {
        entityVisitable.applyFunction(new Pusher(consumer));
    }

    private class Pusher implements EntityVisitor {
        Consumer<DataConsumer> consumer;

        public Pusher(Consumer consumer) {
            this.consumer = consumer;
        }

        @Override
        public void function(Project project) {
            ProjectPack dto = MinerMap.project.map(project);
            networkService
                .create(dto)
                .subscribe(data -> {
                    notifyResult(project, DataStatus.SUCCESS, Optional.ofNullable(data));
                }, throwable -> {
                    notifyResult(project, DataStatus.FAIL, Optional.empty());
                });
        }

        @Override
        public void function(WorkSession session) {
            WorkSessionPack dto = MinerMap.worksession.map(session);
            networkService
                .create(dto)
                .subscribe(data -> {
                    notifyResult(session, DataStatus.SUCCESS, Optional.ofNullable(data));
                }, throwable -> {
                    notifyResult(session, DataStatus.FAIL, Optional.empty());
                });
        }

        @Override
        public void function(HeartBeatCode heartBeatCode) {
            HeartBeatCodePack dto = MinerMap.pulse.map(heartBeatCode);
            networkService
                .create(dto)
                .subscribe(data -> {
                    notifyResult(heartBeatCode, DataStatus.SUCCESS, Optional.ofNullable(data));
                }, throwable -> {
                    notifyResult(heartBeatCode, DataStatus.FAIL, Optional.empty());
                });
        }


        @Override
        public void function(FileCode fileCode) {
            FileCodePack dto = MinerMap.filecode.map(fileCode);
            networkService
                .create(dto)
                .subscribe(data -> {
                    notifyResult(fileCode, DataStatus.SUCCESS, Optional.ofNullable(data));
                }, throwable -> {
                    notifyResult(fileCode, DataStatus.FAIL, Optional.empty());
                });
        }

        @Override
        public void function(HeartBeatBuildRun comprun) {
            HeartBeatBuildRunPack dto = MinerMap.compilationResultMapper.map(comprun);
            networkService
                .create(dto)
                .subscribe(data -> {
                    notifyResult(comprun, DataStatus.SUCCESS, Optional.ofNullable(data));
                }, throwable -> {
                    notifyResult(comprun, DataStatus.FAIL, Optional.empty());
                });
        }

        void notifyResult(EntityVisitable entity, DataStatus status, Optional<GrantedContextPack> data) {
            consumer.accept(new DataConsumer(entity, data, status));
        }


    }
}
