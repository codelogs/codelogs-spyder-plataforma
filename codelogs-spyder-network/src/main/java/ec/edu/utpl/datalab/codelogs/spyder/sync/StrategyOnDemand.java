package ec.edu.utpl.datalab.codelogs.spyder.sync;

import ec.edu.utpl.datalab.codelogs.spyder.core.models.HeartBeatCode;
import ec.edu.utpl.datalab.codelogs.spyder.core.persistencia.RepositoryService;
import ec.edu.utpl.datalab.codelogs.spyder.core.synchronization.Sync;
import ec.edu.utpl.datalab.codelogs.spyder.libs.ia.behaviortree.BehaviorTree;
import ec.edu.utpl.datalab.codelogs.spyder.libs.ia.behaviortree.branch.Selector;
import ec.edu.utpl.datalab.codelogs.spyder.libs.ia.behaviortree.branch.Sequence;
import ec.edu.utpl.datalab.codelogs.spyder.sync.tasks.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.util.Collection;

/**
 * * Created by rfcardenas
 */
public class StrategyOnDemand extends BehaviorTree<DataBoard> implements Sync {
    private static final Logger log = LoggerFactory.getLogger(StrategyOnDemand.class);
    private TaskBook taskBook;

    @Inject
    public StrategyOnDemand(TaskBook taskBook, DataBoard dataBoard) {
        this.taskBook = taskBook;
        this.setObject(dataBoard);
        initTree();
    }

    private void initTree() {
        Selector<DataBoard> selector = new Selector<>();

        RepositoryService repositoryService = getObject().getRepositoryService();
        Collection<HeartBeatCode> entry = repositoryService.pulseCodeRepository().getLastUnsyncEntry();

        CreatePulseTask pulseTask = taskBook.createPulseTask(() -> {
            log.info("Recolectando ultimos cambios [Pulse]");
            RepositoryService repo = getObject().getRepositoryService();
            return repo.pulseCodeRepository().getLastUnsyncEntry();
        });
        CreateComRunTask comRunTask = taskBook.createComRunTask(() -> {
            log.info("Recolectando ultimos cambios [ComRun]");
            RepositoryService repo = getObject().getRepositoryService();
            return repo.runCompileRepository().getLastUnsyncEntry();
        });


        CreateProjectTask projectTask = taskBook.createProjectTask(() -> {
            log.info("Recolectando ultimos cambios [Project]");
            RepositoryService repo = getObject().getRepositoryService();
            return repo.projectRepository().getAllUnSyncEntries();
        });

        CreateSessionTask sessionTask = taskBook.createSessionTask(() -> {
            log.info("Recolectando ultimos cambios [WorkSession]");
            RepositoryService repo = getObject().getRepositoryService();
            return repo.worksessionRepository().getAllUnSyncEntries();
        });

        CreateFileTask fileTask = taskBook.createFileTask(() -> {
            log.info("Recolectando ultimos cambios [file]");
            RepositoryService repo = getObject().getRepositoryService();
            return repo.fileCodeRepository().getAllUnSyncEntries();
        });

        Sequence<DataBoard> secuencia = new Sequence<>();
        secuencia.addChild(projectTask);
        secuencia.addChild(sessionTask);
        secuencia.addChild(fileTask);
        secuencia.addChild(pulseTask);


        Sequence<DataBoard> seqMain = new Sequence<>();
        seqMain.addChild(pulseTask);
        seqMain.addChild(comRunTask);
        /**
         * la accion principal es la recoleccion del ultimo pulso
         */

        //selector.addChild(pulseTask);
        secuencia.addChild(seqMain);
        /**
         * Accion Full sync las dependencias
         */
        selector.addChild(secuencia);


        super.addChild(selector);
    }


    @Override
    public void execute() {

        this.step();
    }
}
