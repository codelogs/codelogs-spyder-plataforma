/*
 * The MIT License (MIT)
 * =========================================================================================================
 *
 * Copyright (c) 10/10/16 12:30 Modulo  Proyecto rexcode-sync  Autor [rfcardenas]
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY
 * , WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ec.edu.utpl.datalab.codelogs.spyder.sync.tasks;

import ec.edu.utpl.datalab.codelogs.spyder.core.models.*;
import ec.edu.utpl.datalab.codelogs.spyder.core.persistencia.RepositoryService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.util.Collection;
import java.util.Stack;

/**
 * Databoar , trata de minizar el envio de parametros desde un unico punto de acceso
 * de datos , el databoar esta ligado al repositorio de datos para obtener los datos
 * que aun no estan sincroniados
 * * Created by rfcardenas
 */
public class DataBoard {
    private static final Logger log = LoggerFactory.getLogger(DataBoard.class);
    private Stack<EntityVisitable> failedElements = new Stack<>();

    private RepositoryService repositoryService;

    @Inject
    public DataBoard(RepositoryService repositoryService) {
        this.repositoryService = repositoryService;
    }

    public Collection<HeartBeatCode> loadPulses() {
        return repositoryService.pulseCodeRepository().getAllUnSyncEntries();
    }

    public Collection<Project> loadProjects() {
        return repositoryService.projectRepository().getAllUnSyncEntries();
    }

    public Collection<FileCode> loadFiles() {
        return repositoryService.fileCodeRepository().getAllUnSyncEntries();
    }

    public Collection<WorkSession> loadSessions() {
        return repositoryService.worksessionRepository().getAllUnSyncEntries();
    }

    public void pushFailEntity(EntityVisitable entityVisitable) {
        failedElements.push(entityVisitable);
    }

    public Stack<EntityVisitable> getFailedElements() {
        return failedElements;
    }

    public RepositoryService getRepositoryService() {
        return repositoryService;
    }

    public void saveData(EntityVisitable entity) {
        entity.applyFunction(new UpdateLocal());
    }

    public void removeData(EntityVisitable entity) {
        entity.applyFunction(new removeData());
    }

    private class removeData implements EntityVisitor {

        @Override
        public void function(Project project) {

        }

        @Override
        public void function(WorkSession session) {

        }

        @Override
        public void function(HeartBeatCode heartBeatCode) {
            log.info("Eliminado pulso syncronizado de la bd");
            repositoryService.pulseCodeRepository().delete(heartBeatCode);
        }

        @Override
        public void function(FileCode fileCode) {

        }

        @Override
        public void function(HeartBeatBuildRun data) {
            repositoryService.runCompileRepository().delete(data);
        }
    }

    private class UpdateLocal implements EntityVisitor {

        @Override
        public void function(Project project) {
            log.info("Actualizando proyecto");
            repositoryService.projectRepository().update(project);
        }

        @Override
        public void function(WorkSession session) {
            log.info("Actualizando Session");
            repositoryService.worksessionRepository().update(session);
        }

        @Override
        public void function(HeartBeatCode heartBeatCode) {
            log.info("Actualizando Pulse code");
            repositoryService.pulseCodeRepository().update(heartBeatCode);
        }

        @Override
        public void function(FileCode fileCode) {
            log.info("Actualiznado FileCode");
            repositoryService.fileCodeRepository().update(fileCode);
        }

        @Override
        public void function(HeartBeatBuildRun data) {
            log.info("Actualiznado Compilation result");
            repositoryService.runCompileRepository().update(data);
        }
    }
}
