/*
 * The MIT License (MIT)
 * =========================================================================================================
 *
 * Copyright (c) 10/10/16 12:30 Modulo  Proyecto rexcode-sync  Autor [rfcardenas]
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY
 * , WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ec.edu.utpl.datalab.codelogs.spyder.sync;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Singleton;
import ec.edu.utpl.datalab.codelogs.spyder.core.env.EnvironmentProperties;
import ec.edu.utpl.datalab.codelogs.spyder.core.persistencia.RepositoryService;
import ec.edu.utpl.datalab.codelogs.spyder.core.synchronization.ModuloSynchronization;
import ec.edu.utpl.datalab.codelogs.spyder.core.synchronization.RestService;
import ec.edu.utpl.datalab.codelogs.spyder.core.synchronization.STRATEGY_SYNC;

import javax.inject.Inject;
import java.util.Objects;

/**
 * * Created by rfcardenas
 */
@Singleton
public class ModuleSyncBeta extends ModuloSynchronization {
    private RepositoryService repositoryService;
    private EnvironmentProperties environmentProperties;
    private Injector injector;

    @Inject
    public ModuleSyncBeta(EnvironmentProperties environmentProperties, RepositoryService repositoryService) {
        //super(environmentProperties);
        this.repositoryService = Objects.requireNonNull(repositoryService, "Require modulo repo");
        this.environmentProperties = Objects.requireNonNull(environmentProperties, "Requiere environment cfg");
        this.injector = Guice.createInjector(new InjectionSystem(environmentProperties, repositoryService));
    }

    @Override
    public void onStart() {
        StrategyFull tree = injector.getInstance(StrategyFull.class);
        tree.step();
    }


    @Override
    public void onStop() {

    }

    @Override
    public void execute(STRATEGY_SYNC strategy_sync) {
        if (strategy_sync == STRATEGY_SYNC.FULL_CHANGES) {
            //injector.getInstance(StrategyFull.class).
            //      step();
        } else if (strategy_sync == STRATEGY_SYNC.LAST_CHANGES) {
            synchronize();
            injector.getInstance(StrategyOnDemand.class)
                .step();
            available();
        }
    }

    @Override
    public RestService restService() {
        return injector.getInstance(RestService.class);
    }
}
