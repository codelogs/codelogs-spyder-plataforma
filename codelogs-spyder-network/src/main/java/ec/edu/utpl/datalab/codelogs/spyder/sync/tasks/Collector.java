package ec.edu.utpl.datalab.codelogs.spyder.sync.tasks;

/**
 * * Created by rfcardenas
 */
public interface Collector<T> {
    T collector();
}
