/*
 * The MIT License (MIT)
 * =========================================================================================================
 *
 * Copyright (c) 10/10/16 12:30 Modulo  Proyecto rexcode-sync  Autor [rfcardenas]
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY
 * , WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ec.edu.utpl.datalab.codelogs.spyder.sync.tasks;


import ec.edu.utpl.datalab.codelogs.spyder.core.models.*;

import javax.inject.Inject;
import java.util.Collection;

/**
 * * Created by rfcardenas
 */
public class TaskBook {
    private NetworkPush mod;

    @Inject
    public TaskBook(NetworkPush mod) {
        this.mod = mod;
    }

    public CreateProjectTask createProjectTask(Collector<Collection<Project>> collector) {
        return new CreateProjectTask(mod, collector);
    }

    public CreatePulseTask createPulseTask(Collector<Collection<HeartBeatCode>> collector) {
        return new CreatePulseTask(mod, collector);
    }
    public CreateComRunTask createComRunTask(Collector<Collection<HeartBeatBuildRun>> collector) {
        return new CreateComRunTask(mod, collector);
    }

    public CreateSessionTask createSessionTask(Collector<Collection<WorkSession>> collector) {
        return new CreateSessionTask(mod, collector);
    }

    public CreateFileTask createFileTask(Collector<Collection<FileCode>> collector) {
        return new CreateFileTask(mod, collector);
    }
    /**public FixDataGrantedTask fixDataGranted(AbstractPushTask.TYPE type)
     {
     return new FixDataGrantedTask(type,mod);
     }**/

}
