/*
 * The MIT License (MIT)
 * =========================================================================================================
 *
 * Copyright (c) 10/10/16 12:30 Modulo  Proyecto rexcode-sync  Autor [rfcardenas]
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY
 * , WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ec.edu.utpl.datalab.codelogs.spyder.sync.tasks;


import ec.edu.utpl.datalab.codelogs.spyder.core.models.EntityVisitable;
import ec.edu.utpl.datalab.codelogs.spyder.libs.ia.behaviortree.LeaftTask2;
import ec.edu.utpl.datalab.codelogs.spyder.libs.ia.behaviortree.Task;
import ec.edu.utpl.datalab.codelogs.spyder.libs.transfers.GrantedContextPack;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;
import java.util.Optional;
import java.util.function.Consumer;

/**
 * * Created by rfcardenas
 */
public abstract class AbstractPushTask<T extends EntityVisitable> extends LeaftTask2<DataBoard> {
    private static final Logger log = LoggerFactory.getLogger(AbstractPushTask.class);
    private final NetworkPush networkPush;
    private boolean allowNext = true;
    private boolean onDemandActive = false;
    private Collector<Collection<T>> collector;

    public AbstractPushTask(NetworkPush networkPush, Collector<Collection<T>> collector) {
        this.networkPush = networkPush;
        this.collector = collector;
    }


    public void setModeFullSync() {
        onDemandActive = false;
    }

    public void setModeOnDemand() {
        onDemandActive = true;
    }

    @Override
    public void run() {
        execute();
    }

    /**
     * Metodo funcion vital, no override
     *
     * @param data
     */
    private final void consumerResult(DataConsumer<T> data) {
        allowNext = data.status != DataStatus.FAIL;
        if (data.status == DataStatus.SUCCESS) {
            onSuccess(data.entity, data.dataserver);
        } else {
            onError(data.entity);
        }
        notifyStatusTask();
    }

    public final void push(Collection<T> dataCollection) {
        if (dataCollection.isEmpty()) {
            log.info("nada que sincronizar ... collection size {} from  {}", dataCollection.size(), this.getClass().getSimpleName());
        }
        for (T data : dataCollection) {
            log.info("Enviando {}" , data.toString());
            push(data, this::consumerResult);
            if (!allowNext) break;
            try {
                // intervalos de 400 milisegundos para no saturar la red ni el servidor
                Thread.sleep(400);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        notifyStatusTask();
        reset();
    }

    public void push(T data) {
        networkPush.push(data, this::consumerResult);
    }

    public void push(T data, Consumer<DataConsumer<T>> consumer) {
        if (onDemandActive && !isValidTrama(data)) {
            log.warn("La trama no es valida");
            allowNext = false;
            return;
        }
        try {
            networkPush.push(data, consumer);
        } catch (Exception e) {
            log.error("Error {}, {}", e.getMessage(), " es posible que sea algo con el server");
            e.printStackTrace();
        }


    }

    private void notifyStatusTask() {
        if (allowNext) {
            success();
        } else {
            fail();
        }
    }

    public void reset() {
        allowNext = true;
    }

    // acciones ejecutar
    public final void execute() {
        Collection<T> tramas = collector.collector();
        push(tramas);
    }


    abstract boolean isValidTrama(T data);

    /**
     * Implementar el tipo de accion por ejemplo :
     * Cuando el registro se ha sincronizado que se tiene que hacer?
     * - Elimnarlo    (En caso de datos sin contexto)
     * - Actualizarlo (En caso de keyGranted [Datos con un contexto])
     *
     * @param entity
     * @param data
     */
    public abstract void onSuccess(T entity, Optional<GrantedContextPack> data);

    public void onError(T entity) {
        log.error("No se pudo sincronizar la trama {} ", entity);
    }

    @Override
    protected Task<DataBoard> copyTo(Task<DataBoard> task) {
        return null;
    }


    /**public static class Builder<T extends EntityVisitable>{
     private  BiConsumer<T,Data> onSuccess;
     private  Provider provider;
     private  NetworkPush networkPush;

     public Builder(Provider provider, NetworkPush networkPush) {
     this.provider = provider;
     this.networkPush = networkPush;
     }
     public Builder withPresaveAction(BiConsumer<T,Data> onSuccess){
     this.onSuccess = onSuccess;
     return this;
     }
     public AbstractPushTask<T> make(){
     return new AbstractPushTask<T>() {
     }
     }
     }
     public static<T extends EntityVisitable> Builder<T> builder(Class<T> entityTarget, NetworkPush networkPush, Supplier<T> provider){
     return new Builder<T>(provider,networkPush);
     }**/
}
