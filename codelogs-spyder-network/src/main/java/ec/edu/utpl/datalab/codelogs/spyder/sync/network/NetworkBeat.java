/*
 * The MIT License (MIT)
 * =========================================================================================================
 *
 * Copyright (c) 10/10/16 12:30 Modulo  Proyecto rexcode-sync  Autor [rfcardenas]
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY
 * , WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ec.edu.utpl.datalab.codelogs.spyder.sync.network;

import java.util.Optional;

/**
 * Representa el resultado tras la ejecucion de una peticion al servidor
 * * Created by rfcardenas
 */
public class NetworkBeat {
    private static int SUCCESS = 0;
    private static int FAIL = 1;
    private final Object dto;
    private final int status;
    private final Optional<Throwable> throwable;

    private NetworkBeat(Object dto, int status, Throwable throwable) {
        this.dto = dto;
        this.status = status;
        this.throwable = Optional.ofNullable(throwable);
    }

    private NetworkBeat(Object dto, int status) {
        this.dto = dto;
        this.status = status;
        this.throwable = Optional.empty();
    }

    public static NetworkBeat createSuccess(Object dto) {
        return new NetworkBeat(dto, SUCCESS);
    }

    public static NetworkBeat createFail(Object dto, Throwable throwable) {
        return new NetworkBeat(dto, FAIL, throwable);
    }

    public Object getDto() {
        return dto;
    }

    public int getStatus() {
        return status;
    }

    public Optional<Throwable> getThrowable() {
        return throwable;
    }

    public boolean isEqual(Class target) {
        return this.getDto().getClass().isInstance(target);
    }
}
