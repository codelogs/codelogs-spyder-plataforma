/*
 * The MIT License (MIT)
 * =========================================================================================================
 *
 * Copyright (c) 10/10/16 12:30 Modulo  Proyecto rexcode-sync  Autor [rfcardenas]
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY
 * , WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ec.edu.utpl.datalab.codelogs.spyder.sync.network;

import ec.edu.utpl.datalab.codelogs.spyder.core.synchronization.RestService;
import ec.edu.utpl.datalab.codelogs.spyder.libs.transfers.*;
import rx.Single;

import javax.inject.Inject;

/**
 * * Created by rfcardenas
 */
public class RestServiceBaseImpl implements RestService {

    private NetworkService networkService;

    @Inject
    public RestServiceBaseImpl(NetworkService networkService) {
        this.networkService = networkService;
    }

    public void execute() {

    }

    public Single<GrantedContextPack> create(HeartBeatBuildRunPack data) {
        return networkService.create(data);
    }

    @Override
    public Single<GrantedContextPack> create(ProjectPack data) {
        return networkService.create(data);
    }

    @Override
    public Single<GrantedContextPack> create(WorkSessionPack data) {
        return networkService.create(data);
    }

    @Override
    public Single<GrantedContextPack> create(FileCodePack data) {
        return networkService.create(data);
    }

    @Override
    public Single<GrantedContextPack> create(HeartBeatCodePack data) {
        return networkService.create(data);
    }

    @Override
    public Single<GrantedContextPack> create(CodeAnalysisPack data) {
        return networkService.create(data);
    }
}
