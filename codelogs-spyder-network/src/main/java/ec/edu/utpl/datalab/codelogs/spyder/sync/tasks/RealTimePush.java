package ec.edu.utpl.datalab.codelogs.spyder.sync.tasks;

import ec.edu.utpl.datalab.codelogs.spyder.core.models.*;
import ec.edu.utpl.datalab.codelogs.spyder.core.persistencia.RepositoryService;

/**
 * * Created by rfcardenas
 */
public class RealTimePush implements EntityVisitor {
    private RepositoryService repository;
    private NetworkPush networkPush;

    public RealTimePush(RepositoryService repository, NetworkPush networkPush) {
        this.repository = repository;
        this.networkPush = networkPush;
    }

    @Override
    public void function(Project project) {
        networkPush.push(project, projectDataConsumer -> {
            DataConsumer<Project> data = projectDataConsumer;
            if (data.status == DataStatus.FAIL) {
                repository.projectRepository().save(project);
            } else {
                project.setUuid(data.dataserver.get().getUuid());
            }
        });
    }

    @Override
    public void function(WorkSession session) {
        networkPush.push(session, projectDataConsumer -> {
            DataConsumer<WorkSession> data = projectDataConsumer;
            if (data.status == DataStatus.FAIL) {
                repository.worksessionRepository().save(session);
            } else {
                session.setUuid(data.dataserver.get().getUuid());
            }
        });
    }

    @Override
    public void function(HeartBeatCode heartBeatCode) {
        networkPush.push(heartBeatCode, projectDataConsumer -> {
            DataConsumer<HeartBeatCode> data = projectDataConsumer;
            if (data.status == DataStatus.FAIL) {
                repository.pulseCodeRepository().save(heartBeatCode);
            } else {
                heartBeatCode.setUuid(data.dataserver.get().getUuid());
            }
        });
    }

    @Override
    public void function(FileCode fileCode) {
        networkPush.push(fileCode, projectDataConsumer -> {
            DataConsumer<FileCode> data = projectDataConsumer;
            if (data.status == DataStatus.FAIL) {
                repository.fileCodeRepository().save(fileCode);
            } else {
                fileCode.setUuid(data.dataserver.get().getUuid());
            }
        });
    }

    @Override
    public void function(HeartBeatBuildRun data) {
        networkPush.push(data,compilationRunResultDataConsumer -> {
            DataConsumer<HeartBeatBuildRun> cos = compilationRunResultDataConsumer;
            if (cos.status == DataStatus.FAIL) {
                repository.runCompileRepository().save(data);
            } else {
                data.setUuid(cos.dataserver.get().getUuid());
            }
        });
    }
}
