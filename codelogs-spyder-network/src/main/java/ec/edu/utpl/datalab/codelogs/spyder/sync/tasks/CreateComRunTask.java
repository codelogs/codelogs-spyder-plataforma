package ec.edu.utpl.datalab.codelogs.spyder.sync.tasks;

import ec.edu.utpl.datalab.codelogs.spyder.core.models.HeartBeatBuildRun;
import ec.edu.utpl.datalab.codelogs.spyder.libs.transfers.GrantedContextPack;

import java.util.Collection;
import java.util.Optional;

/**
 * * Created by rfcardenas
 */
public class CreateComRunTask extends AbstractPushTask<HeartBeatBuildRun> {

    public CreateComRunTask(NetworkPush networkPush, Collector<Collection<HeartBeatBuildRun>> collector) {
        super(networkPush, collector);
    }

    @Override
    boolean isValidTrama(HeartBeatBuildRun data) {
        if (data.getWorkSession() == null) {
            return false;
        }
        return true;
    }

    @Override
    public void onSuccess(HeartBeatBuildRun entity, Optional<GrantedContextPack> data) {
        data.ifPresent(key -> {
            entity.setUuid(key.getUuid());
            entity.setSync(true);
            getObject().saveData(entity);
        });
    }
}
