package ec.edu.utpl.datalab.codelogs.spyder.core.os;

/**
 * Template , metodos de conexion con un monitor
 * * Created by rfcardenas
 */
public abstract class AbstractConnectionProcess {

    /**
     * Metodo conexion
     */
    public final void connect() {
        try {
            connectMonitor();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public final void send(String command) {
        sendCommandMonitor(command);
    }

    public final void disconnect() {
        disconnectMonitor();
    }

    public final boolean isConnected() {
        return isMonitorConnected();
    }

    /**
     * Metodo conecta a un monitor existente
     */
    protected abstract void connectMonitor() throws Exception;

    /**
     * Metodo envia un comando
     *
     * @param command
     * @return
     */
    protected abstract void sendCommandMonitor(String command);

    /**
     * Desconecta el cliente del monitor
     *
     * @return
     */
    protected abstract void disconnectMonitor();

    /**
     * Retorna un valor boolean que indica si un monitor esta conectado o no
     *
     * @return
     */
    protected abstract boolean isMonitorConnected();


}
