/*
 * The MIT License (MIT)
 * =========================================================================================================
 *
 * Copyright (c) 10/10/16 12:30 Modulo  Proyecto rexcode-base  Autor [rfcardenas]
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY
 * , WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ec.edu.utpl.datalab.codelogs.spyder.core;

import ec.edu.utpl.datalab.codelogs.spyder.core.env.EnvironmentProperties;
import ec.edu.utpl.datalab.codelogs.spyder.core.env.EnvironmentPropertiesImpl;
import ec.edu.utpl.datalab.codelogs.spyder.core.env.FileEnvironmentPropertiesImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by rfcardenas
 */
public abstract class SimpleApplication implements Application, Configurable {
    private static final Logger log = LoggerFactory.getLogger(FileEnvironmentPropertiesImpl.class);
    protected EnvironmentProperties environmentProperties = new EnvironmentPropertiesImpl();

    /**
     * Inject
     *
     * @param environmentProperties
     */
    @Override
    public final void injectEnvironment(EnvironmentProperties environmentProperties) {
        this.environmentProperties = environmentProperties;
    }

    @Override
    public final void start() {
        log.info("Iniciando demonio..");
        onStart();
    }

    @Override
    public final void stop() {

    }

    public abstract void onStart();

    public abstract void onStop();
}
