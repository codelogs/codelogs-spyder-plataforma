package ec.edu.utpl.datalab.codelogs.spyder.core.logging.log4j2;

import org.apache.logging.log4j.core.LogEvent;
import org.apache.logging.log4j.core.config.plugins.Plugin;
import org.apache.logging.log4j.core.pattern.ConverterKeys;
import org.apache.logging.log4j.core.pattern.PatternConverter;
import org.apache.logging.log4j.core.pattern.ThrowablePatternConverter;


@Plugin(name = "WhitespaceThrowablePatternConverter", category = PatternConverter.CATEGORY)
@ConverterKeys({"wEx", "wThrowable", "wException"})
public final class WhitespaceThrowablePatternConverter extends ThrowablePatternConverter {

    private WhitespaceThrowablePatternConverter(String[] options) {
        super("WhitespaceThrowable", "throwable", options);
    }

    public static WhitespaceThrowablePatternConverter newInstance(String[] options) {
        return new WhitespaceThrowablePatternConverter(options);
    }

    @Override
    public void format(LogEvent event, StringBuilder buffer) {
        if (event.getThrown() != null) {
            buffer.append(this.options.getSeparator());
            super.format(event, buffer);
            buffer.append(this.options.getSeparator());
        }
    }

}
