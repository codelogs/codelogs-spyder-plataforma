package ec.edu.utpl.datalab.codelogs.spyder.core.util;

import ec.edu.utpl.datalab.codelogs.spyder.core.env.EnvironmentCkecker;
import ec.edu.utpl.datalab.codelogs.spyder.core.env.EnvironmentProperties;
import ec.edu.utpl.datalab.codelogs.spyder.core.env.PropertiesDefault;
import ec.edu.utpl.datalab.codelogs.spyder.core.filesystem.EnvironmentAppNotFound;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * * Created by rfcardenas
 */
public class HomeValidatorArgs implements EnvironmentCkecker {
    private static final Logger log = LoggerFactory.getLogger(HomeValidatorArgs.class);

    @Override
    public void check(EnvironmentProperties properties) throws EnvironmentAppNotFound {
        if(!properties.hasProperty(PropertiesDefault.SPYDER_HOME)){
            log.error("la propidad {} no esta configurada",PropertiesDefault.SPYDER_HOME);
            log.info("se requiere indicar el direcotio de trabajo para ejecutar el jar");
//            throw new EnvironmentAppNotFound(String.format("la propiedad %s no es configurada critica",PropertiesDefault.SPYDER_HOME));
        }
    }
}
