/*
 * The MIT License (MIT)
 * =========================================================================================================
 *
 * Copyright (c) 10/10/16 12:30 Modulo  Proyecto rexcode-base  Autor [rfcardenas]
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY
 * , WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ec.edu.utpl.datalab.codelogs.spyder.core.util.pipes;

import java.util.Stack;
import java.util.StringJoiner;

/**
 * Created by rfcardenas
 */
public class DataPipe {
    private final String name;
    private final Object object;
    private Stack<String> pathPipe = new Stack<>();

    public DataPipe(String name, Object object) {
        this.name = name;
        this.object = object;
    }

    public Stack<String> getPathPipe() {
        return pathPipe;
    }

    public String getName() {
        return name;
    }

    public Object getObject() {
        return object;
    }

    public void markSource(AbstractPipe2 pipeSource) {
        pathPipe.push(pipeSource.getName());
    }

    public boolean dataisOfType(Class a) {
        return object.getClass() == a;
    }

    /**
     * imprime la ruta del paquete en un punto determinado
     */
    public void printTraceRoute() {
        StringJoiner stringJoiner = new StringJoiner(" | ");
        pathPipe.forEach(stringJoiner::add);
        System.out.println(stringJoiner.toString());
    }
}
