package ec.edu.utpl.datalab.codelogs.spyder.core.synchronization;

/**
 * Created by rfcardenas
 */
public enum STRATEGY_SYNC {
    FULL_CHANGES,
    LAST_CHANGES
}
