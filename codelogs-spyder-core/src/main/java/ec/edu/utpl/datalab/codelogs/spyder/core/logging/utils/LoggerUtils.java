package ec.edu.utpl.datalab.codelogs.spyder.core.logging.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class LoggerUtils {
    public static void logIfNull(Class acClass, Object value, String message, Object... objects) {
        Logger log = LoggerFactory.getLogger(acClass);
        if (value == null) {
            log.info(message, objects);
        }
    }

    public static void logIfNull(Object value, String message, Object... objects) {
        Logger log = LoggerFactory.getLogger(LoggerUtils.class);
        if (value == null) {
            log.info(message, objects);
        }
    }
}
