/*
 * The MIT License (MIT)
 * =========================================================================================================
 *
 * Copyright (c) 10/10/16 12:30 Modulo  Proyecto rexcode-base  Autor [rfcardenas]
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY
 * , WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ec.edu.utpl.datalab.codelogs.spyder.core.filesystem;

import org.apache.commons.vfs2.FileObject;
import org.apache.commons.vfs2.VFS;

import java.io.*;
import java.util.StringJoiner;

/**
 * Created by rfcardenas
 */
public class FileSystemCommon {
    /**
     * Retorna la primera linea de un archivo
     *
     * @param fileObject
     * @return
     */
    public static String readBufferLimit(FileObject fileObject, int limit) {
        try {
            InputStream inputStream = fileObject.getContent().getInputStream();
            InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
            int c = inputStream.read();
            StringBuilder cmmand = new StringBuilder();
            while (c != -1 && cmmand.length() < limit) {
                cmmand.append((char) c);
                c = inputStreamReader.read();
            }
            inputStream.close();
            inputStreamReader.close();

            return cmmand.length() > 0
                ? cmmand.toString()
                : null;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "";
    }

    public static String readAndRemoveFirstEntry(FileObject fileObject) {
        try {
            Reader reader = new FileReader(fileObject.getName().getPath());
            BufferedReader bufferedReader = new BufferedReader(reader);
            String line = bufferedReader.readLine();
            fileObject.setWritable(true, true);
            OutputStream outputStream = fileObject.getContent().getOutputStream();
            outputStream.write(("").getBytes());
            outputStream.close();
            return line;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "";
    }

    public static void removeFirstLine(FileObject fileObject) {
        try {
            fileObject.setWritable(true, true);
            OutputStream outputStream = fileObject.getContent().getOutputStream();
            outputStream.write(("").getBytes());
            outputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Bloque la escritura de un archivo
     *
     * @param fileObject
     * @return
     */
    public static boolean lock(FileObject fileObject) {
        try {
            return fileObject.setWritable(false, true);
        } catch (org.apache.commons.vfs2.FileSystemException e) {
            e.printStackTrace();
        }
        return false;
    }

    /**
     * Libera el segunro de un archivo
     *
     * @param fileObject
     * @return
     */
    public static boolean unlock(FileObject fileObject) {
        try {
            return fileObject.setWritable(true, true);
        } catch (org.apache.commons.vfs2.FileSystemException e) {
            e.printStackTrace();
        }
        return false;
    }

    /**
     * Unifica paths segun el orden en el cual son pasados
     *
     * @param paths
     * @return
     */
    public static String createPath(String... paths) {
        StringJoiner stringJoiner = new StringJoiner("" + File.separator);
        for (String path : paths) {
            stringJoiner.add(path);
        }
        return stringJoiner.toString();
    }

    public static String createPathifNotExit(String... paths) {
        StringJoiner stringJoiner = new StringJoiner("" + File.separator);
        for (String path : paths) {
            try {
                FileObject fileObject = VFS.getManager().resolveFile(path);
                if (!fileObject.exists()) {
                    fileObject.createFile();
                    fileObject.close();
                }
            } catch (org.apache.commons.vfs2.FileSystemException e) {
                e.printStackTrace();
            }
            stringJoiner.add(path);
        }
        return stringJoiner.toString();
    }

}
