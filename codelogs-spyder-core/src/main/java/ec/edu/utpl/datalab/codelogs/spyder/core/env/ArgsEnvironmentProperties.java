/*
 * The MIT License (MIT)
 * =========================================================================================================
 *
 * Copyright (c) 10/10/16 12:30 Modulo  Proyecto rexcode-base  Autor [rfcardenas]
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY
 * , WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ec.edu.utpl.datalab.codelogs.spyder.core.env;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

/**
 * Carga las propiedades pasadas como parametricas
 * Tiene mayor prioridad que las propiedades definidas en un archivo
 * Created by rfcardenas
 */
public class ArgsEnvironmentProperties extends EnvironmentPropertiesImpl {
    private static final Logger log = LoggerFactory.getLogger(ArgsEnvironmentProperties.class);

    public ArgsEnvironmentProperties(String[] args) {
        map.putAll(mapArgs(args));
    }

    public Map<Object, Object> mapArgs(String[] args) {
        Map<Object, Object> mapdata = new HashMap<>();
        for (String arg : args) {
            String[] propert = arg.split("=");
            if(propert.length==2){
                String key = propert[0];
                String value = propert[1];
                if (!key.isEmpty() && !value.isEmpty()) {
                    mapdata.put(key, value);
                }
            }else{
                log.error("Argumento mal definido {}",arg);
            }

        }
        return mapdata;
    }
}
