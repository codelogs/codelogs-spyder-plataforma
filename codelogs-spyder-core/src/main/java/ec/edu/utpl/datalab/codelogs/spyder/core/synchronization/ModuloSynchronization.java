/*
 * The MIT License (MIT)
 * =========================================================================================================
 *
 * Copyright (c) 10/10/16 12:30 Modulo  Proyecto rexcode-base  Autor [rfcardenas]
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY
 * , WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ec.edu.utpl.datalab.codelogs.spyder.core.synchronization;

import ec.edu.utpl.datalab.codelogs.spyder.core.Application;
import ec.edu.utpl.datalab.codelogs.spyder.core.env.EnvironmentProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import rx.Observer;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by rfcardenas
 */
public abstract class ModuloSynchronization implements Application, Observer<STRATEGY_SYNC>, Runnable {
    private static final Logger log = LoggerFactory.getLogger(ModuloSynchronization.class);
    private STATUS_SYNC status = STATUS_SYNC.INICIADA;
    private boolean shouldRun = true;
    //20 maximo numero de request sync, cuando existe cambios raros en el sistema de archivos
    // la aplicacion tiene que bloquearse, por ejemplo copiar toda una serie de carpetas
    private ArrayBlockingQueue<STRATEGY_SYNC> request = new ArrayBlockingQueue<>(256);
    private ExecutorService service;
    private RestService restService;

    public ModuloSynchronization(RestService restService) {
        this.restService = restService;
    }

    public ModuloSynchronization() {
        service = Executors.newFixedThreadPool(1, r -> {
            Thread t = Executors.defaultThreadFactory().newThread(r);
            t.setName("Sync-Thread");
            t.setPriority(Thread.MIN_PRIORITY);
            t.setDaemon(true);
            return t;
        });

    }

    /**
     * Idica que el proceso esta sincronizandose
     */
    protected final void synchronize() {
        this.status = STATUS_SYNC.SINCRONIZANDO;
    }

    /**
     * Idica que el proceso esta sincronizandose
     */
    protected final void available() {
        this.status = STATUS_SYNC.DISPONIBLE;
    }

    @Override
    public void run() {
        try {
            while (!Thread.currentThread().isInterrupted() && shouldRun) {
                STRATEGY_SYNC take = request.take();
                log.info("Sync Thread take request {}", take);
                try {
                    execute(take);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (InterruptedException e) {
            shouldRun = false;
        }
        log.info("End Sync Thread");
    }

    @Override
    public final void start() {
        service.execute(this);
    }

    @Override
    public final void stop() {
        shouldRun = false;
    }

    public abstract void execute(STRATEGY_SYNC strategy_sync);

    public abstract RestService restService();

    @Override
    public void onCompleted() {

    }

    @Override
    public void onError(Throwable e) {
        e.printStackTrace();
    }

    @Override
    public void onNext(STRATEGY_SYNC type) {
        log.info("Peticion en cola de tipo {}", type);
        request.add(type);
    }

    @Override
    public void injectEnvironment(EnvironmentProperties environmentProperties) {

    }

    public enum STATUS_SYNC {
        INICIADA, // El proceso esta en lista
        SINCRONIZANDO, // El proceso esta ocupado
        DISPONIBLE, // El proceso ESTA DISPONIBLE
    }
}
