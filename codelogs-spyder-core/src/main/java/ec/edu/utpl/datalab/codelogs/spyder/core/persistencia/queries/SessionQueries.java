/*
 * The MIT License (MIT)
 * =========================================================================================================
 *
 * Copyright (c) 10/10/16 12:30 Modulo  Proyecto rexcode-base  Autor [rfcardenas]
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY
 * , WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ec.edu.utpl.datalab.codelogs.spyder.core.persistencia.queries;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.QueryBuilder;
import ec.edu.utpl.datalab.codelogs.spyder.core.models.Project;
import ec.edu.utpl.datalab.codelogs.spyder.core.models.WorkSession;
import ec.edu.utpl.datalab.codelogs.spyder.libs.simpledata.QueryWrapper;
import ec.edu.utpl.datalab.codelogs.spyder.libs.simpledata.queryset.ComplexQuery;

/**
 * Created by rfcardenas
 */
public class SessionQueries {
    public static final String _findAllSessionUnsyncFromProject = "_findAllSessionUnsyncFromProject";


    public static QueryWrapper findAllSessionUnsyncFrom =
        new QueryWrapper((ComplexQuery) (val, daos) -> {
            Dao<WorkSession, Integer> dao = daos.loadService(WorkSession.class);
            QueryBuilder<WorkSession, Integer> sesqlb = dao.queryBuilder();
            return sesqlb.
                where().eq(WorkSession.COL_PROJETFK, val.get(Project._ID))
                .query();
        }, _findAllSessionUnsyncFromProject);

    /**
     public static QueryWrapper findAllSessionUnsyncFrom() {
     return new QueryWrapper(new ComplexQuery() {
    @Override public Collection<WorkSession> call(Map<String, Object> val,
    RexSimpleData.Service daos) throws Exception
    {
    Dao<WorkSession, Integer> dao = daos.loadService(WorkSession.class);
    QueryBuilder<WorkSession, Integer> sesqlb = dao.queryBuilder();
    return sesqlb.
    where().eq(WorkSession.COL_PROJETFK, val.get(Project._ID))
    .query();
    }
    }, _findAllSessionUnsyncFromProject);
     }**/
}
