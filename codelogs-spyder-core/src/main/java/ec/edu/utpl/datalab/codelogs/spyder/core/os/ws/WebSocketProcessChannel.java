/*
 * The MIT License (MIT)
 * =========================================================================================================
 *
 * Copyright (c) 10/10/16 12:30 Modulo  Proyecto rexcode-base  Autor [rfcardenas]
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY
 * , WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package ec.edu.utpl.datalab.codelogs.spyder.core.os.ws;

import ec.edu.utpl.datalab.codelogs.spyder.core.os.AbstractProcessChanel;
import ec.edu.utpl.datalab.codelogs.spyder.core.os.ProcessRedirectStreams;
import ec.edu.utpl.datalab.codelogs.spyder.core.os.files.FileProcessChanel;
import ec.edu.utpl.datalab.codelogs.spyder.libs.common.Network;
import org.java_websocket.WebSocket;
import org.java_websocket.handshake.ClientHandshake;
import org.java_websocket.server.WebSocketServer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import rx.Observer;

import java.io.IOException;
import java.io.PipedInputStream;
import java.net.InetSocketAddress;
import java.util.*;

/**
 * Created by rfcardenas
 */
public class WebSocketProcessChannel extends AbstractProcessChanel {

    private static final Logger log = LoggerFactory.getLogger(FileProcessChanel.class);
    PipedInputStream inputStream;
    private ServerWs serverWs;
    private HashMap<WebSocket, Thread> clients = new HashMap<>();

    public WebSocketProcessChannel(int port) {

        if (!Network.isAvaiblePort(port)) {
            log.error("El puerto esta ocupado host:{} ", port);
            log.error("Exit monitor...");
            System.exit(-1);
        }
        serverWs = new ServerWs(new InetSocketAddress(port), 2);

        ProcessRedirectStreams processRedirectStreams = new ProcessRedirectStreams(System.out, System.err);
        processRedirectStreams.observable().subscribe(bytes -> {
            new ReaderDataProc().onNext(bytes);
        }, Throwable::printStackTrace);
        processRedirectStreams.connect();
    }

    @Override
    public void start() {
        log.info("Process ws start");
        serverWs.start();
    }

    @Override
    public void stop() {
        log.info("Process ws stop");
        try {
            serverWs.stop();
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
    }

    class ServerWs extends WebSocketServer {

        Set<WebSocket> webSockets = Collections.synchronizedSet(new HashSet<>());
        Iterator<WebSocket> iter = webSockets.iterator();

        public ServerWs(InetSocketAddress address) {
            super(address);
        }

        public ServerWs(InetSocketAddress address, int decoders) {
            super(address, decoders);
        }

        @Override
        public void onOpen(WebSocket conn, ClientHandshake handshake) {
            webSockets.add(conn);
            conn.send("Connectado " + webSockets.size());
            log.info("Cliente monitor conectado ");

        }

        @Override
        public void onClose(WebSocket conn, int code, String reason, boolean remote) {
            log.info("Cliente monitor desconectado");
            webSockets.remove(conn);
        }

        @Override
        public void onMessage(WebSocket conn, String message) {
            log.info("comando " + message);
            publish(message);
        }

        @Override
        public void onError(WebSocket conn, Exception ex) {
            log.info("Error " + ex.getMessage());
            ex.printStackTrace();
        }

        public void sendData(String data) {
            iter = webSockets.iterator();
            if (!data.isEmpty()) {
                while (iter.hasNext()) {
                    WebSocket webSocket = iter.next();
                    try {
                        webSocket.send(data);
                    } catch (Exception e) {
                        log.error("No se pudo enviar los datos");
                    }
                }

            }
        }
    }

    class ReaderDataProc implements Observer<String> {

        @Override
        public void onCompleted() {
            System.out.println("Tarea compleata");
        }

        @Override
        public void onError(Throwable e) {
            e.printStackTrace();
        }

        @Override
        public void onNext(String bytes) {
            serverWs.sendData(bytes);
        }
    }
}
