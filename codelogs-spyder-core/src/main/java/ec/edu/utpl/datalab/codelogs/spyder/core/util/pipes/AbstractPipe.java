/*
 * The MIT License (MIT)
 * =========================================================================================================
 *
 * Copyright (c) 10/10/16 12:30 Modulo  Proyecto rexcode-base  Autor [rfcardenas]
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY
 * , WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ec.edu.utpl.datalab.codelogs.spyder.core.util.pipes;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import rx.Observable;
import rx.Observer;
import rx.subjects.PublishSubject;

import java.util.Optional;
import java.util.function.Consumer;

/**
 * Created by rfcardenas
 */
public abstract class AbstractPipe<I, O> implements Observer<I> {
    private static final Logger log = LoggerFactory.getLogger(AbstractPipe.class);
    private final String name = this.getClass().getName();
    private PublishSubject<O> notify = PublishSubject.create();
    private Optional<Consumer<O>> prenotify = Optional.empty();
    private Optional<Consumer<O>> postNotify = Optional.empty();

    public Observable<O> observable() {
        return notify.asObservable();
    }

    protected void preNotify(Consumer<O> action) {
        this.prenotify = Optional.ofNullable(action);
    }

    protected void postNotify(Consumer<O> action) {
        this.postNotify = Optional.ofNullable(action);
    }

    public void notify(O data) {
        prenotify.ifPresent(evtConsumer -> {
            log.info("PRE-NOTIFY : INTERCEPTOR > exec");
            evtConsumer.accept(data);
        });
        notify.onNext(data);
        postNotify.ifPresent(evtConsumer -> {
            log.info("POST-NOTIFY : INTERCEPTOR > exec");
            evtConsumer.accept(data);
        });
        reset();
    }

    public void reset() {
        prenotify = Optional.empty();
        postNotify = Optional.empty();
    }

    @Override
    public void onCompleted() {
        log.info("La subscripcion termino correctamente");
    }

    @Override
    public void onError(Throwable e) {
        log.error("Error {}", e.getMessage());
        e.printStackTrace();
    }

    public String getName() {
        return name;
    }
}
