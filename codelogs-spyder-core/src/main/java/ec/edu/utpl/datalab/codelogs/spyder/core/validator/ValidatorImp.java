package ec.edu.utpl.datalab.codelogs.spyder.core.validator;

import org.glassfish.hk2.api.AnnotationLiteral;

/**
 * Normalmente una anotacion no tiene que implementarse, esto es realizado para hk2 pueda buscar servicios
 * que implementen esta anotacion
 * Created by rfcardenas
 */
public class ValidatorImp extends AnnotationLiteral<Validator> implements Validator {
    private String name;

    public ValidatorImp(String name) {
        this.name = name;
    }

    @Override
    public String value() {
        return name;
    }

}
