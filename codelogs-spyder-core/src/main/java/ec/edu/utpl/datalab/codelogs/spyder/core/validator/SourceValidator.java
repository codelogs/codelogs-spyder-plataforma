package ec.edu.utpl.datalab.codelogs.spyder.core.validator;

import org.apache.commons.vfs2.FileObject;
import org.jvnet.hk2.annotations.Contract;

/**
 * Created by rfcardenas
 */
@Contract
public interface SourceValidator {
    boolean isValid(FileObject fileObject) throws Exception;
}
