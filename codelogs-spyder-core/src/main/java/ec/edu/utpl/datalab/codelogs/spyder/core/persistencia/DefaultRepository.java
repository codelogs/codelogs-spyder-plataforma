/*
 * The MIT License (MIT)
 * =========================================================================================================
 *
 * Copyright (c) 10/10/16 12:30 Modulo  Proyecto rexcode-base  Autor [rfcardenas]
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY
 * , WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ec.edu.utpl.datalab.codelogs.spyder.core.persistencia;

import ec.edu.utpl.datalab.codelogs.spyder.core.env.EnvironmentProperties;
import ec.edu.utpl.datalab.codelogs.spyder.core.env.PropertiesDefault;
import ec.edu.utpl.datalab.codelogs.spyder.core.filesystem.FileSystemCommon;
import ec.edu.utpl.datalab.codelogs.spyder.core.models.*;
import ec.edu.utpl.datalab.codelogs.spyder.core.persistencia.queries.FileCodeQueries;
import ec.edu.utpl.datalab.codelogs.spyder.core.persistencia.queries.ProjectQueries;
import ec.edu.utpl.datalab.codelogs.spyder.core.persistencia.queries.SessionQueries;
import ec.edu.utpl.datalab.codelogs.spyder.core.persistencia.queries.SharedQuery;
import ec.edu.utpl.datalab.codelogs.spyder.libs.simpledata.Connection;
import ec.edu.utpl.datalab.codelogs.spyder.libs.simpledata.GenerationType;
import ec.edu.utpl.datalab.codelogs.spyder.libs.simpledata.RexSimpleData;
import ec.edu.utpl.datalab.codelogs.spyder.libs.simpledata.ServiceRepository;
import ec.edu.utpl.datalab.codelogs.spyder.core.persistencia.repos.*;

/**
 * Created by rfcardenas
 */
public class DefaultRepository implements RepositoryService {
    private Connection connection;
    private WorkSessionRepository workSessionSimpleDataRepo;
    private FileCodeRepository fileCodeRepositoryDataRepo;
    private ProjectRepository projectSimpleDataRepo;
    private ProgrammerRepository programmerSimpleDataRepo;
    private PulseCodeRepository pulseCodeSimpleData;
    private RunCompileRepository runCompileRepository;

    public DefaultRepository(EnvironmentProperties environmentProperties) {
        String pathstore = environmentProperties.getProperty(PropertiesDefault.SPYDER_HOME_DB_PATH, "db");
        FileSystemCommon.createPathifNotExit(pathstore);
        Connection connection = Connection.newConnection(pathstore);
        build(connection);
    }

    public DefaultRepository(String pathDb) {
        FileSystemCommon.createPathifNotExit(pathDb);
        Connection connection = Connection.newConnection(pathDb);
        build(connection);
    }

    private void build(Connection connection) {
        //  Connection connection  = Connection.newConnection("demo");
        RexSimpleData rexSimpleData = RexSimpleData.builder()
            .addServiceRepository(ServiceRepository.builder(connection, WorkSession.class, Integer.class)
                .addProvider(SharedQuery.findUnSynclast)
                .addProvider(SharedQuery.findUnSyncEntries)
                .addProvider(SessionQueries.findAllSessionUnsyncFrom)
                .generationType(GenerationType.CREATE_TABLES_IFNOTEXIST)
                .build())
            .addServiceRepository(ServiceRepository.builder(connection, Project.class, Integer.class)
                .addProvider(SharedQuery.findUnSynclast)
                .addProvider(SharedQuery.findUnSyncEntries)
                .addProvider(ProjectQueries.findOneByPath)
                .generationType(GenerationType.CREATE_TABLES_IFNOTEXIST)
                .build())
            .addServiceRepository(ServiceRepository.builder(connection, HeartBeatCode.class, Integer.class)
                .addProvider(SharedQuery.findUnSynclast)
                .addProvider(SharedQuery.findUnSyncEntries)
                .generationType(GenerationType.CREATE_TABLES_IFNOTEXIST)
                .build())
            .addServiceRepository(ServiceRepository.builder(connection, FileCode.class, Integer.class)
                .addProvider(SharedQuery.findUnSynclast)
                .addProvider(SharedQuery.findUnSyncEntries)
                .addProvider(FileCodeQueries.findByPath)
                .generationType(GenerationType.CREATE_TABLES_IFNOTEXIST)
                .build())
            .addServiceRepository(ServiceRepository.builder(connection, Programmer.class, Integer.class)
                .addProvider(SharedQuery.findUnSynclast)
                .addProvider(SharedQuery.findUnSyncEntries)
                .generationType(GenerationType.CREATE_TABLES_IFNOTEXIST)
                .build())
            .addServiceRepository(ServiceRepository.builder(connection, HeartBeatBuildRun.class, Integer.class)
                .addProvider(SharedQuery.findUnSynclast)
                .addProvider(SharedQuery.findUnSyncEntries)
                .generationType(GenerationType.CREATE_TABLES_IFNOTEXIST)
                .build())
            .build();
        // creando reposiorios
        this.fileCodeRepositoryDataRepo = rexSimpleData.createRepository(FileCodeRepository.class);
        this.workSessionSimpleDataRepo = rexSimpleData.createRepository(WorkSessionRepository.class);
        this.programmerSimpleDataRepo = rexSimpleData.createRepository(ProgrammerRepository.class);
        this.pulseCodeSimpleData = rexSimpleData.createRepository(PulseCodeRepository.class);
        this.projectSimpleDataRepo = rexSimpleData.createRepository(ProjectRepository.class);
        this.runCompileRepository = rexSimpleData.createRepository(RunCompileRepository.class);
    }

    @Override
    public WorkSessionRepository worksessionRepository() {
        return workSessionSimpleDataRepo;
    }

    @Override
    public ProjectRepository projectRepository() {
        return projectSimpleDataRepo;
    }

    @Override
    public PulseCodeRepository pulseCodeRepository() {
        return pulseCodeSimpleData;
    }

    @Override
    public FileCodeRepository fileCodeRepository() {
        return fileCodeRepositoryDataRepo;
    }

    @Override
    public RunCompileRepository runCompileRepository() {
        return runCompileRepository;
    }


}
