/*
 * The MIT License (MIT)
 * =========================================================================================================
 *
 * Copyright (c) 10/10/16 12:30 Modulo  Proyecto rexcode-base  Autor [rfcardenas]
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY
 * , WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ec.edu.utpl.datalab.codelogs.spyder.core.persistencia.repos;


import ec.edu.utpl.datalab.codelogs.spyder.core.models.Project;
import ec.edu.utpl.datalab.codelogs.spyder.core.models.WorkSession;
import ec.edu.utpl.datalab.codelogs.spyder.core.persistencia.Repository;
import ec.edu.utpl.datalab.codelogs.spyder.core.persistencia.queries.SessionQueries;
import ec.edu.utpl.datalab.codelogs.spyder.core.persistencia.queries.SharedQuery;
import ec.edu.utpl.datalab.codelogs.spyder.libs.simpledata.annotations.QueryID;
import ec.edu.utpl.datalab.codelogs.spyder.libs.simpledata.annotations.QueryParam;

import java.util.Collection;

/**
 * Created by rfcardenas
 */
public interface WorkSessionRepository extends Repository<WorkSession, Integer> {

    /**
     * Retorna todas las entradas no syncronizadas tomando como referencia
     * una consula compatrida,
     * dao.queryBuilder()
     * .where()
     * .eq("sync",false) // El campo Sync esta presente en esta tabla Worksession
     * .query();
     *
     * @return
     */
    @QueryID(SharedQuery._findUnSyncData)
    Collection<WorkSession> getAllUnSyncEntries();

    /**
     * Encontrar todos los sessines de un proyecto con un ID X
     * OJO @QueryParam tiene que ser una identificador unico, y este debe ser usado en el
     * reverso la implemtacion de la consula
     *
     * @param id
     * @return
     */
    @QueryID(SessionQueries._findAllSessionUnsyncFromProject)
    Collection<WorkSession> findAllUnSyncByProjectID(@QueryParam(Project._ID) int id);
}
