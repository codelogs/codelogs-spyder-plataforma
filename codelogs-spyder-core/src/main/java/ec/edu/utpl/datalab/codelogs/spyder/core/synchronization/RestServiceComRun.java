package ec.edu.utpl.datalab.codelogs.spyder.core.synchronization;

import ec.edu.utpl.datalab.codelogs.spyder.libs.transfers.GrantedContextPack;
import ec.edu.utpl.datalab.codelogs.spyder.libs.transfers.HeartBeatBuildRunPack;
import rx.Single;

/**
 * * Created by rfcardenas
 */
public interface RestServiceComRun
{
    Single<GrantedContextPack> create(HeartBeatBuildRunPack data);
}
