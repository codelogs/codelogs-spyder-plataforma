package ec.edu.utpl.datalab.codelogs.spyder.core.models;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import ec.edu.utpl.datalab.codelogs.spyder.core.models.enumeration.TYPE_INSTRUCTION;

import java.sql.Date;

/**
 * Modele para el almacenamiento del resultado de una compilacion (exito o fracaso),
 * y otros datos utiles como el tiempo que ha invertido un programador
 * * Created by rfcardenas
 */
@DatabaseTable(tableName = "heartbeat_build_run")
public class HeartBeatBuildRun implements EntityVisitable {
    private static final long serialVersionUID = 1L;

    @DatabaseField(dataType = DataType.LONG, generatedId = true)
    private long id;
    @DatabaseField
    private String uuid;
    @DatabaseField
    private boolean success;
    @DatabaseField
    private String messageL1 = "ejecución existosa";
    @DatabaseField
    private String messageL2 = "";
    @DatabaseField
    private String messageL3 = "";
    @DatabaseField
    private long time;
    @DatabaseField
    private Date capturate_at;
    @DatabaseField
    private String pathContext;


    @DatabaseField(dataType = DataType.ENUM_STRING)
    private TYPE_INSTRUCTION instruction;

    @DatabaseField(dataType = DataType.BOOLEAN)
    private boolean sync;

    @DatabaseField(foreign = true, foreignAutoRefresh = true)
    private WorkSession workSession;

    public HeartBeatBuildRun() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessageL1() {
        return messageL1;
    }

    public void setMessageL1(String messageL1) {
        this.messageL1 = messageL1;
    }

    public String getMessageL2() {
        return messageL2;
    }

    public void setMessageL2(String messageL2) {
        this.messageL2 = messageL2;
    }

    public String getMessageL3() {
        return messageL3;
    }

    public void setMessageL3(String messageL3) {
        this.messageL3 = messageL3;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public Date getCapturate_at() {
        return capturate_at;
    }

    public void setCapturate_at(Date capturate_at) {
        this.capturate_at = capturate_at;
    }

    public TYPE_INSTRUCTION getInstruction() {
        return instruction;
    }

    public void setInstruction(TYPE_INSTRUCTION instruction) {
        this.instruction = instruction;
    }

    public boolean isSync() {
        return sync;
    }

    public void setSync(boolean sync) {
        this.sync = sync;
    }

    public WorkSession getWorkSession() {
        return workSession;
    }

    public void setWorkSession(WorkSession workSession) {
        this.workSession = workSession;
    }

    public String getPathContext() {
        return pathContext;
    }

    public void setPathContext(String pathContext) {
        this.pathContext = pathContext;
    }

    @Override
    public String toString() {
        return "HeartBeatBuildRun{" +
            "id=" + id +
            ", uuid='" + uuid + '\'' +
            ", success=" + success +
            ", messageL1='" + messageL1 + '\'' +
            ", messageL2='" + messageL2 + '\'' +
            ", messageL3='" + messageL3 + '\'' +
            ", time=" + time +
            ", capturate_at=" + capturate_at +
            ", pathContext='" + pathContext + '\'' +
            ", instruction=" + instruction +
            ", sync=" + sync +
            ", workSession=" + workSession +
            '}';
    }

    @Override
    public boolean isValidRepresentation() {
        return id > 0;
    }

    @Override
    public void applyFunction(EntityVisitor entityVisitor) {
        entityVisitor.function(this);
    }
}
