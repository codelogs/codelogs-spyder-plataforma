/*
 * The MIT License (MIT)
 * =========================================================================================================
 *
 * Copyright (c) 10/10/16 12:30 Modulo  Proyecto rexcode-base  Autor [rfcardenas]
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY
 * , WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ec.edu.utpl.datalab.codelogs.spyder.core.env;

import ec.edu.utpl.datalab.codelogs.spyder.core.logging.utils.LoggerUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

/**
 * Created by rfcardenas
 */
public class EnvironmentPropertiesImpl implements EnvironmentProperties {
    private static final Logger log = LoggerFactory.getLogger(FileEnvironmentPropertiesImpl.class);
    protected Map<Object, Object> map = new HashMap<>();

    public EnvironmentPropertiesImpl(Map<Object, Object> map) {
        this.map = map;
    }

    public EnvironmentPropertiesImpl() {
    }

    @Override
    public boolean containsProperty(String key) {
        return map.containsKey(key);
    }

    @Override
    public String getProperty(String key) {
        String value = (String) map.get(key);
        LoggerUtils.logIfNull(value, "La Propiedad es nula props [{}] ", key);
        return value;
    }

    @Override
    public String getProperty(String key, String defaultValue) {
        String value = (String) map.get(key);
        return value == null
            ? defaultValue
            : value;
    }

    @Override
    public <T> T getProperty(String key, Class<T> targetType) {
        Object value = map.get(key);
        LoggerUtils.logIfNull(value, "La Propiedad es nula props [{}] ", key);
        return (T) value;
    }

    @Override
    public <T> T getProperty(String key, Class<T> targetType, T defaultValue) {
        Object value = map.get(key);
        return value == null
            ? defaultValue
            : (T) value;
    }

    @Override
    public String getRequiredProperty(String key) throws IllegalStateException {
        Object value = map.get(key);
        if (value == null) {
            throw new IllegalStateException("No se puede continuar sin la propiedad" + key);
        }
        return (String) value;
    }

    @Override
    public <T> T getRequiredProperty(String key, Class<T> targetType) throws IllegalStateException {
        Object value = map.get(key);
        if (value == null) {
            throw new IllegalStateException("No se puede continuar sin la propiedad" + key + " de tipo " + targetType);
        }
        return (T) value;
    }

    /**
     * Retorna una coleecion no modificable de entradas
     *
     * @return
     */
    @Override
    public Set<Map.Entry<Object, Object>> asSet() {
        return Collections.unmodifiableSet(map.entrySet());
    }

    @Override
    public <T> Set<T> getAsSet(String key, Class<T> target) {
        Set<T> properties = new HashSet<T>();
        T data = (T) map.get(key);
        if (data instanceof String) {
            properties.add(data);
        }
        if (data instanceof List) {
            ((List) data).forEach(o -> properties.add((T) o));
        }
        return properties;
    }

    @Override
    public <T> Set<T> getAsSetWithDefaults(String key, Class<T> target, T... values) {
        Set<T> set = getAsSet(key, target);
        for (T value : values) {
            set.add(value);
        }
        return set;
    }

    @Override
    public void putValue(Object key, Object value) {
        map.put(key, value);
    }

    @Override
    public void overrideWith(EnvironmentProperties environmentProperties) {
        log.info("Override Environment properties");
        for (Map.Entry<Object, Object> objectObjectEntry : environmentProperties.asSet()) {
            log.info("Override property {}", objectObjectEntry.getKey().toString());
            putValue(objectObjectEntry.getKey(), objectObjectEntry.getValue());
        }
    }

    @Override
    public boolean getAsBolean(String key, boolean defaultvalue) {
        Objects.requireNonNull(defaultvalue, "Se requiere un valor en caso de error");

        try {
            //String value  = getProperty(PropertiesDefault.MONITOR_DEBUG,"false");
            Boolean bolean = Boolean.valueOf((String) map.get(key));
            return bolean;
        } catch (Exception e) {
            // x que tratar
        }
        return defaultvalue;
    }

    @Override
    public int getAsInt(String key) {
        return getAsInt(key, 0);
    }

    @Override
    public int getAsInt(String key, int defaultValue) {
        int value = defaultValue;
        Objects.requireNonNull(defaultValue, "Se requiere un valor en caso de error");
        try{
            if(containsKey(key)){
                value = Integer.valueOf(String.valueOf(map.get(key)));
            }
        }catch (ClassCastException exception){
            throw new BadArgumentException("Se espero un numero ",exception);
        }

        return value;
    }

    @Override
    public boolean hasProperty(String key) {
        Object value = map.get(key);
        return !Objects.isNull(value);
    }

    private boolean containsKey(String key){
        return map.containsKey(key);
    }
}
