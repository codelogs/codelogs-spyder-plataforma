/*
 * The MIT License (MIT)
 * =========================================================================================================
 *
 * Copyright (c) 10/10/16 12:30 Modulo  Proyecto rexcode-base  Autor [rfcardenas]
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY
 * , WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.utpl.datalab.codelogs.spyder.core.support.ide.netbeans;

import ec.edu.utpl.datalab.codelogs.spyder.core.support.ide.ResultRunError;
import ec.edu.utpl.datalab.codelogs.spyder.core.support.ide.ResultType;
import org.apache.commons.io.IOUtils;
import org.apache.commons.vfs2.FileObject;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Time;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Procesador de Logs
 *
 * @author ronald
 */
public class JavaLog {

    private static int LINE_ERR = 0;
    private static int LINE_PACK = 0;

    public static Optional<? extends ResultRunError> getErrorObject(FileObject fileObject) throws IOException {
        InputStream stream = fileObject.getContent().getInputStream();
        ResultRunError rgeneric = null;
        String content = IOUtils.toString(stream, "UTF-8");
        if (content.length() > 0 && content.contains(" Exception ")) {
            ResultRunError resultRuntime = new ResultRunError(Date.from(Instant.now()));
            String[] lineas = content.split("\n");
            initSettings(lineas);
            //Procesar Solamente primera linea (informacion critica)
            String[] toksL1 = lineas[0].split(" ");
            resultRuntime.setMain(getTypeErr(lineas[0]));
            resultRuntime.setLevel1(getReason(lineas[0]));

            //procesar Solamente desde la segunda linea (Traza)
            //por ahora solo se tomara el primer paquete luego
            resultRuntime.setLevel2(getSource(lineas[LINE_PACK]));
            resultRuntime.setLevel3(getPack(lineas[LINE_PACK]));
            resultRuntime.setResultType(ResultType.RUN_ERR);
            rgeneric = resultRuntime;
        }
        if (content.contains("javac")) {
            ResultRunError result = null;
            result = new ResultRunError(Time.from(Instant.now()));
            String[] lineas = content.split("\n");
            String err = "";
            int deep = 0;
            for (String linea : lineas) {

                if (linea.contains("error")) {
                    String[] arr = linea.split("error: ");
                    if (arr.length >= 2) {
                        err = arr[1];

                        if (deep == 0) {
                            result.setMain(err);
                        }
                        if (deep == 1) {
                            result.setLevel1(err);
                        }
                        if (deep == 2) {
                            result.setLevel2(err);
                        }
                        if (deep == 3) {
                            result.setLevel3(err);
                        }
                    }

                    deep++;
                }

            }

            result.setResultType(ResultType.COMPILATION_ERR);
            rgeneric = result;
        }
        stream.close();
        return Optional.ofNullable(rgeneric);
    }

    private static String getTypeErr(String line) {
        String result = null;
        try {
            int init = line.indexOf("\" ");
            int last = line.indexOf(":");
            if (last < 0) {
                line = line.substring(init + 1, line.length());
            } else {
                line = line.substring(init + 1, last);
            }
            result = line;
        } catch (Exception e) {
            e.printStackTrace();
            result = "Sin datos";
        } finally {
            return result;
        }
    }

    private static String getReason(String line) {
        Pattern pattern = Pattern.compile(".");
        int last = line.indexOf(": ");
        line = line.substring(last + 1, line.length());
        return line;
    }

    private static String getSource(String line) {

        Pattern pattern = Pattern.compile("(\\(.*?)\\)");
        Matcher matcher = pattern.matcher(line);
        List<String> listMatches = new ArrayList<String>();
        while (matcher.find()) {
            listMatches.add(matcher.group());
        }
        if (listMatches.size() > 0) {
            return listMatches.get(0);
        } else {
            return "Sin datos";
        }
    }

    private static String getPack(String line) {
        line = line.replaceAll("at", "");
        line = line.replace(getSource(line), "");
        return line;
    }

    private static void initSettings(String[] lines) {
        LINE_ERR = 0;
        LINE_PACK = lines.length - 1;
    }
}
