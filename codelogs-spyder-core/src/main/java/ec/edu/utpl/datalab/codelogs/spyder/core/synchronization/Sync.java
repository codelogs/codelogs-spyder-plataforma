package ec.edu.utpl.datalab.codelogs.spyder.core.synchronization;

/**
 * Created by rfcardenas
 */
public interface Sync {
    void execute();
}
