/*
 * The MIT License (MIT)
 * =========================================================================================================
 *
 * Copyright (c) 10/10/16 12:30 Modulo  Proyecto rexcode-base  Autor [rfcardenas]
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY
 * , WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ec.edu.utpl.datalab.codelogs.spyder.core.persistencia;

import ec.edu.utpl.datalab.codelogs.spyder.libs.common.ObjectUtils;
import ec.edu.utpl.datalab.codelogs.spyder.core.models.*;
import ec.edu.utpl.datalab.codelogs.spyder.core.persistencia.repos.*;

import java.util.Objects;

/**
 * Almacena una entidad  independientemente del tipo
 * que sea Ejm > - new UpdaterLocalData(X).save(filecode);
 * - new UpdaterLocalData(X).save(pulsecode);
 * - new UpdaterLocalData(X).save(project);
 * Created by rfcardenas
 */
public class SaverLocalData {
    private RepositoryService repositoryService;

    public SaverLocalData(RepositoryService repositoryService) {
        Objects.requireNonNull(repositoryService, "El repositorio no puede ser nulo");
        this.repositoryService = repositoryService;
    }

    public void save(EntityVisitable data) {
        data.applyFunction(new Vistor());
    }

    /**
     * Persite los datos en la base de datos, os objetos y las propiedaes
     * son pasados por referencia asi que estos se mantendran actualizados
     * siempre y cuando no se utilice objetos inmodificables
     */
    private class Vistor implements EntityVisitor {

        @Override
        public void function(Project project) {
            ProjectRepository projectRepository = repositoryService.projectRepository();
            if (!ObjectUtils.isEmpty(repositoryService)) {
                projectRepository.save(project);
            }
        }

        @Override
        public void function(WorkSession session) {
            WorkSessionRepository workSessionRepository = repositoryService.worksessionRepository();
            if (!ObjectUtils.isEmpty(repositoryService)) {
                workSessionRepository.save(session);
            }
        }

        @Override
        public void function(HeartBeatCode heartBeatCode) {
            PulseCodeRepository pulseCodeRepository = repositoryService.pulseCodeRepository();
            if (!ObjectUtils.isEmpty(repositoryService)) {
                pulseCodeRepository.save(heartBeatCode);
            }
        }

        @Override
        public void function(FileCode fileCode) {
            FileCodeRepository fileCodeRepository = repositoryService.fileCodeRepository();
            if (!ObjectUtils.isEmpty(repositoryService)) {
                fileCodeRepository.save(fileCode);
            }
        }

        @Override
        public void function(HeartBeatBuildRun data) {
            RunCompileRepository repository = repositoryService.runCompileRepository();
            if (!ObjectUtils.isEmpty(repository)) {
                repository.save(data);
            }
        }
    }
}
