/*
 * The MIT License (MIT)
 * =========================================================================================================
 *
 * Copyright (c) 10/10/16 12:30 Modulo  Proyecto rexcode-base  Autor [rfcardenas]
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY
 * , WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ec.edu.utpl.datalab.codelogs.spyder.core.models;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import org.apache.commons.vfs2.FileObject;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Objects;

/**
 * A FileCode.
 */
@DatabaseTable
public class FileCode implements EntityVisitable, Serializable {
    public static final String _PATH = "pathFile";

    private static final long serialVersionUID = 1L;

    private FileObject fileObject;

    @DatabaseField(dataType = DataType.LONG, generatedId = true)
    private long id;

    @DatabaseField
    private String uuid;

    @DatabaseField(unique = true)
    private String name;

    @DatabaseField
    private String pathFile;

    @DatabaseField
    private String extension;

    @DatabaseField
    private Date createDate;

    @DatabaseField
    private Date updateDate;

    @DatabaseField
    private Integer codeLines;

    @DatabaseField
    private Boolean available;

    @DatabaseField
    private Integer updateCounter;

    @DatabaseField
    private long fixedTimeactivity;

    @DatabaseField
    private Timestamp timestamp;

    @DatabaseField(foreign = true, foreignAutoRefresh = true)
    private Project project;

    @DatabaseField(dataType = DataType.BOOLEAN)
    private boolean sync;

    @DatabaseField(dataType = DataType.BOOLEAN)
    private boolean keyGranted = false;

    public FileObject getFileObject() {
        return fileObject;
    }

    public void setFileObject(FileObject fileObject) {
        this.fileObject = fileObject;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPathFile() {
        return pathFile;
    }

    public void setPathFile(String pathFile) {
        this.pathFile = pathFile;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public Integer getCodeLines() {
        return codeLines;
    }

    public void setCodeLines(Integer codeLines) {
        this.codeLines = codeLines;
    }

    public Boolean getAvailable() {
        return available;
    }

    public void setAvailable(Boolean available) {
        this.available = available;
    }

    public Integer getUpdateCounter() {
        return updateCounter;
    }

    public void setUpdateCounter(Integer updateCounter) {
        this.updateCounter = updateCounter;
    }

    public long getFixedTimeactivity() {
        return fixedTimeactivity;
    }

    public void setFixedTimeactivity(long fixedTimeactivity) {
        this.fixedTimeactivity = fixedTimeactivity;
    }

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }

    public boolean isSync() {
        return sync;
    }

    public void setSync(boolean sync) {
        this.sync = sync;
    }

    public boolean isKeyGranted() {
        return keyGranted;
    }

    public void setKeyGranted(boolean keyGranted) {
        this.keyGranted = keyGranted;
    }

    public Timestamp getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Timestamp timestamp) {
        this.timestamp = timestamp;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        FileCode fileCode = (FileCode) o;
        /**if(fileCode.id == null || id == null) {
         return false;
         }**/
        return Objects.equals(id, fileCode.id);
    }


    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "FileCode{" +
            "id=" + id +
            ", name='" + name + "'" +
            ", pathFile='" + pathFile + "'" +
            ", extension='" + extension + "'" +
            ", createDate='" + createDate + "'" +
            ", updateDate='" + updateDate + "'" +
            ", codeLines='" + codeLines + "'" +
            ", available='" + available + "'" +
            ", updateCounter='" + updateCounter + "'" +
            ", fixedTimeactivity='" + fixedTimeactivity + "'" +
            '}';
    }

    @Override
    public boolean isValidRepresentation() {
        return id > 0;
    }

    @Override
    public void applyFunction(EntityVisitor entityVisitor) {
        entityVisitor.function(this);
    }
}
