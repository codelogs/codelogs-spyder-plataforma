package ec.edu.utpl.datalab.codelogs.spyder.core.mappers;

import ec.edu.utpl.datalab.codelogs.spyder.core.models.HeartBeatBuildRun;
import ec.edu.utpl.datalab.codelogs.spyder.libs.transfers.HeartBeatBuildRunPack;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.ReportingPolicy;

/**
 * * Created by rfcardenas
 */
@Mapper(componentModel = "default",
    uses = {GrantedContextMapper.class, GrantedContextRct.class,TypeInstructionMapper.class},
    unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface HeartBeatBuildRunMapper {
    @Mappings({
        @Mapping(source = "capturate_at",target = "capturate_at", ignore = true)
    })
    HeartBeatBuildRunPack map(HeartBeatBuildRun source);

    @Mappings({
        @Mapping(source = "capturate_at",target = "capturate_at", ignore = true)

    })
    HeartBeatBuildRun map(HeartBeatBuildRunPack source);
}
