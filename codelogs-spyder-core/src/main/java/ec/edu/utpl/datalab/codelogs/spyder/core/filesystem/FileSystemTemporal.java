/*
 * The MIT License (MIT)
 * =========================================================================================================
 *
 * Copyright (c) 10/10/16 12:30 Modulo  Proyecto rexcode-base  Autor [rfcardenas]
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY
 * , WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ec.edu.utpl.datalab.codelogs.spyder.core.filesystem;

import org.apache.commons.vfs2.FileObject;
import org.apache.commons.vfs2.FileSystemException;
import org.apache.commons.vfs2.NameScope;
import org.apache.commons.vfs2.VFS;

import java.io.IOException;

/**
 * Created by rfcardenas
 */
public class FileSystemTemporal {
    public static final String PATH_LOGS = "logs";
    public static final String PATH_METRICS = "metrics";
    public static final String PATH_HISTORY = "history";
    private static final String PATH_TEMPORAL = System.getProperty("java.io.tmpdir");
    public static final String PATH_ROOT = FileSystemCommon.createPath(PATH_TEMPORAL, "rxcode");

    public static FileObject getRootFolder() {
        FileObject fileObject = null;
        try {
            fileObject = VFS.getManager().resolveFile(PATH_ROOT);
            if (!fileObject.exists()) {
                fileObject.createFolder();
            }
        } catch (FileSystemException e) {
            e.printStackTrace();
        }

        return fileObject;
    }

    /**
     * Retorna el objeto compartido de comunicacion del proceso
     *
     * @return NULL objeto virtual si este archivo no ha sido creado por el proceso
     */
    public static FileObject getProcFile() {
        String pathProc = FileSystemCommon.createPath(FileSystemTemporal.PATH_ROOT, "rxproc.proc");
        FileObject fileObject = null;
        try {
            fileObject = getRootFolder().resolveFile(pathProc, NameScope.DESCENDENT);
        } catch (FileSystemException e) {
            e.printStackTrace();
        }
        return fileObject;
    }

    private static FileObject resolveProjectLogs(String pathRelativeProject) {
        FileObject fileObject = null;
        try {
            fileObject = getRootFolder().resolveFile(pathRelativeProject, NameScope.DESCENDENT);
            if (!fileObject.exists()) {
                fileObject.createFolder();
            }
        } catch (FileSystemException e) {
            e.printStackTrace();
        }
        return fileObject;
    }

    public static void createStructure(String project) throws IOException {
        FileObject root = getRootFolder();
        FileObject project_folder = root.resolveFile(project, NameScope.CHILD);
        FileObject project_logs = project_folder.resolveFile(PATH_LOGS, NameScope.CHILD);
        FileObject project_metrics = project_folder.resolveFile(PATH_METRICS, NameScope.CHILD);
        FileObject project_history = project_folder.resolveFile(PATH_HISTORY, NameScope.CHILD);
        if (!project_folder.exists()) {
            project_folder.createFolder();
        }
        if (!project_logs.exists()) {
            project_logs.createFolder();
        }
        if (!project_metrics.exists()) {
            project_metrics.createFolder();
        }
        if (!project_history.exists()) {
            project_history.createFolder();
        }
    }

}
