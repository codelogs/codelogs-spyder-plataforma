package ec.edu.utpl.datalab.codelogs.spyder.core.support.ide.netbeans;

import ec.edu.utpl.datalab.codelogs.spyder.core.support.ide.ResultRunError;
import org.apache.commons.vfs2.FileObject;
import org.apache.commons.vfs2.VFS;

import java.io.IOException;
import java.util.Optional;

/**
 * * Created by rfcardenas
 */
public class test {
    public static void main(String[] args) throws IOException {
        FileObject fileObject = VFS.getManager().resolveFile("/home/rfcardenas/NetBeansProjects/Test/logs/log.rxlog");
        Optional<? extends ResultRunError> rerror = JavaLog.getErrorObject(fileObject);
        System.out.println(rerror.get().toString());

    }
}
