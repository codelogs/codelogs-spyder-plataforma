/*
 * The MIT License (MIT)
 * =========================================================================================================
 *
 * Copyright (c) 10/10/16 12:30 Modulo  Proyecto rexcode-base  Autor [rfcardenas]
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY
 * , WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ec.edu.utpl.datalab.codelogs.spyder.core.ansi;

/**
 * {@link AnsiElement Ansi} background colors.
 */
public enum AnsiBackground implements AnsiElement {

    DEFAULT("49"),

    BLACK("40"),

    RED("41"),

    GREEN("42"),

    YELLOW("43"),

    BLUE("44"),

    MAGENTA("45"),

    CYAN("46"),

    WHITE("47"),

    BRIGHT_BLACK("100"),

    BRIGHT_RED("101"),

    BRIGHT_GREEN("102"),

    BRIGHT_YELLOW("103"),

    BRIGHT_BLUE("104"),

    BRIGHT_MAGENTA("105"),

    BRIGHT_CYAN("106"),

    BRIGHT_WHITE("107");

    private String code;

    AnsiBackground(String code) {
        this.code = code;
    }

    @Override
    public String toString() {
        return this.code;
    }

}
