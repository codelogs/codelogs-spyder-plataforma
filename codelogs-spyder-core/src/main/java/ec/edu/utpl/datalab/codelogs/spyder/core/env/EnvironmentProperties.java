/*
 * The MIT License (MIT)
 * =========================================================================================================
 *
 * Copyright (c) 10/10/16 12:30 Modulo  Proyecto rexcode-base  Autor [rfcardenas]
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY
 * , WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ec.edu.utpl.datalab.codelogs.spyder.core.env;

import java.util.Map;
import java.util.Set;

/**
 * Propiedades del sistema existen tres implementaciones
 * Nivel 1 ---> Propiedades cargadas como parametricas java -jar x x x
 * Nivel 2 ---> Propiedades cargadas desde un archivo externo
 * Nivel 3 ---> Propiedades cargadas desde el mapa por defecto
 * <p>
 * Created by rfcardenas
 */
public interface EnvironmentProperties {
    /**
     * Determina si los argumentos contiene una propiedad X
     *
     * @param key
     * @return
     */
    boolean containsProperty(String key);

    /**
     * Retorna un propiedad de tipo string sin un valor alterno
     *
     * @param key
     * @return
     */
    String getProperty(String key);

    /**
     * Retorna un valor String, indicando que retorne un valor alterno
     * en caso de que no exista en los argumentos
     *
     * @param key
     * @param defaultValue
     * @return
     */

    String getProperty(String key, String defaultValue);

    /**
     * Retorna una propiedad pasando la clase a la que corresponde
     *
     * @param key
     * @param targetType
     * @param <T>
     * @return
     */

    <T> T getProperty(String key, Class<T> targetType);

    /**
     * Retorna una valor segun la clase y el valor por defecto
     *
     * @param key
     * @param targetType
     * @param defaultValue
     * @param <T>
     * @return
     */
    <T> T getProperty(String key, Class<T> targetType, T defaultValue);

    /**
     * Retorna un propiedad critica
     *
     * @param key
     * @return
     * @throws IllegalStateException
     */

    String getRequiredProperty(String key) throws IllegalStateException;

    /**
     * Retorna un propiedad critica
     *
     * @param key
     * @param targetType
     * @param <T>
     * @return
     * @throws IllegalStateException
     */
    <T> T getRequiredProperty(String key, Class<T> targetType) throws IllegalStateException;

    /**
     * Retorna un cojunto de elementos
     *
     * @return
     */

    Set<Map.Entry<Object, Object>> asSet();

    /**
     * retorna un conjunto de elementos segun el tipo
     *
     * @param key
     * @param target
     * @param <T>
     * @return
     */
    <T> Set<T> getAsSet(String key, Class<T> target);

    /**
     * Retorna un conjunto con valores por defecto
     *
     * @param key
     * @param target
     * @param values
     * @param <T>
     * @return
     */

    <T> Set<T> getAsSetWithDefaults(String key, Class<T> target, T... values);

    /**
     * Añade una propiedad al ambiente
     *
     * @param key
     * @param value
     */

    void putValue(Object key, Object value);

    /**
     * OVerride de una propiedad
     *
     * @param environmentProperties
     */

    void overrideWith(EnvironmentProperties environmentProperties);

    /**
     * Retorna un valor como boolean
     *
     * @param key
     * @param defaultvalue
     * @return
     */

    boolean getAsBolean(String key, boolean defaultvalue);

    /**
     * Retorna un propiedad como un entero
     *
     * @param key
     * @return
     */
    int getAsInt(String key);

    /**
     * Retorna un propiedad como un entor con un valor por defecto
     *
     * @param key
     * @param defaultValue
     * @return
     */
    int getAsInt(String key, int defaultValue);

    /**
     * determina si el ambiente tiene una propieadad
     * @param key
     * @return
     */
    boolean hasProperty(String key);
}
