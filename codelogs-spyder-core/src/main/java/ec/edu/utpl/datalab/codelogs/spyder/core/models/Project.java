/*
 * The MIT License (MIT)
 * =========================================================================================================
 *
 * Copyright (c) 10/10/16 12:30 Modulo  Proyecto rexcode-base  Autor [rfcardenas]
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY
 * , WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ec.edu.utpl.datalab.codelogs.spyder.core.models;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;
import org.hibernate.validator.constraints.NotEmpty;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Collection;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * Modelos de  datos del proyecto
 * Entidad de datos
 * A Project.
 */
@DatabaseTable
public class Project implements EntityVisitable, Serializable {

    public static final String _ID = "id";
    public static final String _UUID = "uuid";
    public static final String _NAME = "name";
    public static final String _PATH = "path";
    private static final long serialVersionUID = 1L;
    @DatabaseField(dataType = DataType.LONG, generatedId = true)
    private long id;

    @DatabaseField
    private String uuid;

    @DatabaseField
    private String name;

    @NotEmpty
    @DatabaseField(unique = true)
    private String path;

    @DatabaseField(dataType = DataType.SERIALIZABLE)
    private ZonedDateTime createDate;

    @DatabaseField
    private String git;

    @DatabaseField(foreign = true)
    private Programmer programmer;

    @ForeignCollectionField(eager = false)
    private Collection<WorkSession> sessions = new HashSet<>();

    @ForeignCollectionField(eager = false)
    private Collection<FileCode> files = new HashSet<>();

    @DatabaseField(dataType = DataType.BOOLEAN)
    private boolean sync;

    @DatabaseField(dataType = DataType.BOOLEAN)
    private boolean keyGranted = false;

    private String ide;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public ZonedDateTime getCreateDate() {
        return createDate;
    }

    public void setCreateDate(ZonedDateTime createDate) {
        this.createDate = createDate;
    }

    public String getGit() {
        return git;
    }

    public void setGit(String git) {
        this.git = git;
    }

    public Programmer getProgrammer() {
        return programmer;
    }

    public void setProgrammer(Programmer programmer) {
        this.programmer = programmer;
    }

    public Collection<WorkSession> getSessions() {
        return sessions;
    }

    public void setSessions(Set<WorkSession> workSessions) {
        this.sessions = workSessions;
    }

    public Collection<FileCode> getFiles() {
        return files;
    }

    public void setFiles(Set<FileCode> fileCodes) {
        this.files = fileCodes;
    }

    public boolean isSync() {
        return sync;
    }

    public void setSync(boolean sync) {
        this.sync = sync;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public boolean isKeyGranted() {
        return keyGranted;
    }

    public void setKeyGranted(boolean keyGranted) {
        this.keyGranted = keyGranted;
    }

    public String getIde() {
        return ide;
    }

    public void setIde(String ide) {
        this.ide = ide;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Project project = (Project) o;
        /**if(project.id == null || id == null) {
         return false;
         }**/
        return Objects.equals(id, project.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "Project{" +
            "id=" + id +
            ", uuid='" + uuid + "'" +
            ", name='" + name + "'" +
            ", path='" + path + "'" +
            ", createDate='" + createDate + "'" +
            ", tawReferente='" + git + "'" +
            '}';
    }

    @Override
    public boolean isValidRepresentation() {
        return id > 0;
    }

    @Override
    public void applyFunction(EntityVisitor entityVisitor) {
        entityVisitor.function(this);
    }
}
