package ec.edu.utpl.datalab.codelogs.spyder.core.util;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;
import java.io.IOException;
import java.util.Optional;

/**
 * * Created by rfcardenas
 */
public class JsonMapper {
    private static ObjectMapper objectMapper = new ObjectMapper();

    public static void writter(Object data, File file) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.writeValue(file, data);
    }

    public static String writte(Object data) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(data);
    }

    public static <T> Optional<T> read(Class<T> target, String data) {
        ObjectMapper objectMapper = new ObjectMapper();
        Object result = null;
        try {
            result = objectMapper.readValue(data, target);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return Optional.ofNullable((T) result);
    }

    public ObjectMapper getObjectMapper() {
        return objectMapper;
    }

    public void autoInstallModules() {
        objectMapper.findAndRegisterModules();
    }
}
