/*
 * The MIT License (MIT)
 * =========================================================================================================
 *
 * Copyright (c) 10/10/16 12:30 Modulo  Proyecto rexcode-base  Autor [rfcardenas]
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY
 * , WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ec.edu.utpl.datalab.codelogs.spyder.core;

import ec.edu.utpl.datalab.codelogs.spyder.core.env.EnvironmentProperties;
import ec.edu.utpl.datalab.codelogs.spyder.core.os.AbstractProcessChanel;

import java.util.HashSet;
import java.util.Set;
import java.util.function.Consumer;

/**
 * Created by rfcardenas
 */
public class DeamonFactory {
    private EnvironmentProperties environmentProperties;
    private Set<AbstractProcessChanel> processChanels = new HashSet<>();
    private Consumer<String> commandProcessor;

    public DeamonFactory withProcessChannel(AbstractProcessChanel listener) {
        processChanels.add(listener);
        return this;
    }

    public DeamonFactory withProperties(EnvironmentProperties properties) {
        this.environmentProperties = properties;
        return this;
    }

    public DeamonFactory withCommandProcess(Consumer<String> commandProcessor) {
        this.commandProcessor = commandProcessor;
        return this;
    }

    public ApplicationDaemon build(ApplicationDaemon applicationDaemon) {
        applicationDaemon.setEnvironmentProperties(environmentProperties);
        applicationDaemon.setProcessChanels(processChanels);
        applicationDaemon.setCmdProcessor(commandProcessor);
        applicationDaemon.configure();
        return applicationDaemon;
    }
}
