/*
 * The MIT License (MIT)
 * =========================================================================================================
 *
 * Copyright (c) 10/10/16 12:30 Modulo  Proyecto rexcode-base  Autor [rfcardenas]
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY
 * , WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ec.edu.utpl.datalab.codelogs.spyder.core.models;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Objects;

/**
 * A WorkSession.
 */
@DatabaseTable
public class WorkSession implements EntityVisitable, Serializable {

    public static final String COL_PROJETFK = "project_id";
    private static final long serialVersionUID = 1L;
    @DatabaseField(dataType = DataType.LONG, generatedId = true)
    private long id;

    @DatabaseField(dataType = DataType.STRING)
    private String uuid;

    @DatabaseField(dataType = DataType.SERIALIZABLE)
    private ZonedDateTime sessionStart;

    @DatabaseField(dataType = DataType.SERIALIZABLE)
    private ZonedDateTime sessionEnd;

    @DatabaseField(columnName = "editor")
    private String editor;

    @DatabaseField
    private int fixedTimeCodeAct;

    @DatabaseField(columnName = "terminal_uptime")
    private Long terminalUptime;

    @DatabaseField(columnName = "terminal_architecture")
    private String terminalArchitecture;

    @DatabaseField(columnName = "terminal_os")
    private String terminalOs;

    @DatabaseField(columnName = "terminal_ip")
    private String terminalIp;

    @DatabaseField(columnName = "terminal_mac")
    private String terminalMac;

    @DatabaseField(columnName = "terminal_name")
    private String terminalName;

    @DatabaseField(columnName = "terminal_cpu")
    private Integer terminalCpu;

    @DatabaseField(columnName = "terminal_temperature")
    private Integer terminalTemperature;

    @DatabaseField(columnName = "terminal_cores")
    private Integer terminalCores;

    @DatabaseField(columnName = "terminal_ram")
    private Long terminalRam;

    @DatabaseField(columnName = "terminal_ram_av")
    private Long terminalRamAv;

    @NotNull(message = "Este campo tiene que estar ligado a un proyecto")
    @Valid
    @DatabaseField(foreign = true, foreignAutoCreate = false, foreignAutoRefresh = true, columnName = COL_PROJETFK)
    private Project project;

    @ForeignCollectionField(eager = false)
    private Collection<HeartBeatBuildRun> compilationRunBlock = new ArrayList<>();

    @ForeignCollectionField(eager = false)
    private Collection<HeartBeatCode> pulses = new HashSet<>();

    @DatabaseField(dataType = DataType.BOOLEAN)
    private boolean sync;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public ZonedDateTime getSessionStart() {
        return sessionStart;
    }

    public void setSessionStart(ZonedDateTime sessionStart) {
        this.sessionStart = sessionStart;
    }

    public ZonedDateTime getSessionEnd() {
        return sessionEnd;
    }

    public void setSessionEnd(ZonedDateTime sessionEnd) {
        this.sessionEnd = sessionEnd;
    }

    public String getEditor() {
        return editor;
    }

    public void setEditor(String editor) {
        this.editor = editor;
    }

    public int getFixedTimeCodeAct() {
        return fixedTimeCodeAct;
    }

    public void setFixedTimeCodeAct(int fixedTimeCodeAct) {
        this.fixedTimeCodeAct = fixedTimeCodeAct;
    }

    public Long getTerminalUptime() {
        return terminalUptime;
    }

    public void setTerminalUptime(Long terminalUptime) {
        this.terminalUptime = terminalUptime;
    }

    public String getTerminalArchitecture() {
        return terminalArchitecture;
    }

    public void setTerminalArchitecture(String terminalArchitecture) {
        this.terminalArchitecture = terminalArchitecture;
    }

    public String getTerminalOs() {
        return terminalOs;
    }

    public void setTerminalOs(String terminalOs) {
        this.terminalOs = terminalOs;
    }

    public String getTerminalIp() {
        return terminalIp;
    }

    public void setTerminalIp(String terminalIp) {
        this.terminalIp = terminalIp;
    }

    public String getTerminalName() {
        return terminalName;
    }

    public void setTerminalName(String terminalName) {
        this.terminalName = terminalName;
    }

    public Integer getTerminalCpu() {
        return terminalCpu;
    }

    public void setTerminalCpu(Integer terminalCpu) {
        this.terminalCpu = terminalCpu;
    }

    public Integer getTerminalTemperature() {
        return terminalTemperature;
    }

    public void setTerminalTemperature(Integer terminalTemperature) {
        this.terminalTemperature = terminalTemperature;
    }

    public Integer getTerminalCores() {
        return terminalCores;
    }

    public void setTerminalCores(Integer terminalCores) {
        this.terminalCores = terminalCores;
    }

    public Long getTerminalRam() {
        return terminalRam;
    }

    public void setTerminalRam(Long terminalRam) {
        this.terminalRam = terminalRam;
    }

    public Long getTerminalRamAv() {
        return terminalRamAv;
    }

    public void setTerminalRamAv(Long terminalRamAv) {
        this.terminalRamAv = terminalRamAv;
    }

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }

    public Collection<HeartBeatBuildRun> getCompilationRunBlock() {
        return compilationRunBlock;
    }

    public void setCompilationRunBlock(Collection<HeartBeatBuildRun> compilationRunBlock) {
        this.compilationRunBlock = compilationRunBlock;
    }

    public Collection<HeartBeatCode> getPulses() {
        return pulses;
    }

    public void setPulses(Collection<HeartBeatCode> pulses) {
        this.pulses = pulses;
    }

    public boolean isSync() {
        return sync;
    }

    public void setSync(boolean sync) {
        this.sync = sync;
    }

    public String getTerminalMac() {
        return terminalMac;
    }

    public void setTerminalMac(String terminalMac) {
        this.terminalMac = terminalMac;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        WorkSession workSession = (WorkSession) o;
        /** if(workSession.id == null || id == null) {
         return false;
         }**/
        return Objects.equals(id, workSession.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "WorkSession{" +
            "id=" + id +
            ", uuid=" + uuid +
            ", start='" + sessionStart + "'" +
            ", end='" + sessionEnd + "'" +
            ", editor='" + editor + "'" +
            ", fixedTimeCodeAct='" + fixedTimeCodeAct + "'" +
            ", terminalOs='" + terminalOs + "'" +
            ", terminalArchitecture='" + terminalArchitecture + "'" +
            ", terminalIp='" + terminalIp + "'" +
            ", terminalName='" + terminalName + "'" +
            ", terminalCpu='" + terminalCpu + "'" +
            ", terminalCores='" + terminalCores + "'" +
            ", terminalRam='" + terminalRam + "'" +
            ", terminalRamAv='" + terminalRamAv + "'" +
            ", terminalUptime='" + terminalUptime + "'" +
            ", project='" + ((project != null) ? project : "") + "'" +
            '}';
    }

    @Override
    public boolean isValidRepresentation() {
        return id > 0;
    }

    @Override
    public void applyFunction(EntityVisitor entityVisitor) {
        entityVisitor.function(this);
    }
}
