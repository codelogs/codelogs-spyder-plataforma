package ec.edu.utpl.datalab.codelogs.spyder.core.os.ws;

import ec.edu.utpl.datalab.codelogs.spyder.core.os.AbstractConnectionProcess;
import ec.edu.utpl.datalab.codelogs.spyder.core.os.SocketPack;
import org.java_websocket.client.WebSocketClient;
import org.java_websocket.handshake.ServerHandshake;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import rx.subjects.PublishSubject;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

/**
 * * Created by rfcardenas
 */
public class SimpleConnectionSocket extends AbstractConnectionProcess {
    private static final Logger log = LoggerFactory.getLogger(SimpleConnectionSocket.class);

    private PublishSubject<SocketPack> channel = PublishSubject.create();
    private WebSocketCli socket = null;
    private int port;
    private int attemps = 5;
    private int timeoutByAttemp = 5;
    private TimeUnit timeUnit = TimeUnit.SECONDS;
    public SimpleConnectionSocket(int port) {
        this.port = Objects.requireNonNull(port, "El puerto no debe ser nulo");
    }

    public SimpleConnectionSocket(int port, int attemps, int timeoutByAttemp, TimeUnit timeUnit) {
        this(port);
        this.attemps = (attemps < 0) ? 1 : attemps;
        this.timeoutByAttemp = timeoutByAttemp;
        this.timeUnit = timeUnit;
    }

    @Override
    protected void connectMonitor() throws Exception {
        log.info("Simple connection");
        long mill = TimeUnit.MILLISECONDS.convert(timeoutByAttemp, timeUnit);
        for (int i = 0; i < attemps; i++) {
            connectMethod();
            Thread.sleep(mill);
            if (isConnected()) {
                break;
            }
        }
    }

    @Override
    protected void sendCommandMonitor(String command) {
        if (isConnected()) {
            log.info("Canal activo enviando comando ");
            socket.send(command);
        } else {
            log.error("Ningun canal activo, no se puede enviar comando");
        }
    }

    @Override
    protected void disconnectMonitor() {
        if (isConnected()) {
            socket.close();
        }
    }

    @Override
    protected boolean isMonitorConnected() {
        if (socket == null) {
            return false;
        }
        if (socket.getConnection().isClosed()) {
            return false;
        }
        return socket.getConnection().isOpen();
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public int getTimeoutByAttemp() {
        return timeoutByAttemp;
    }

    public void setTimeoutByAttemp(int timeoutByAttemp) {
        this.timeoutByAttemp = timeoutByAttemp;
    }

    public TimeUnit getTimeUnit() {
        return timeUnit;
    }

    public void setTimeUnit(TimeUnit timeUnit) {
        this.timeUnit = timeUnit;
    }

    public PublishSubject<SocketPack> getChannel() {
        return channel;
    }

    protected void connectMethod() throws URISyntaxException {
        URI uri = new URI("http://localhost:" + ((port < 0 || port > 65000) ? 8484 : port));
        if (!isConnected()) {
            socket = new WebSocketCli(uri);
            socket.connect();
        }
    }

    class WebSocketCli extends WebSocketClient {
        public WebSocketCli(URI serverURI) {
            super(serverURI);
        }

        @Override
        public void onOpen(ServerHandshake handshakedata) {
            channel.onNext(SocketPack.create(SocketPack.PACK.CONNECT_SUCCESS));
            log.info("Cliente conectado, puerto {}", port);

        }

        @Override
        public void onMessage(String message) {
            //System.out.println(message);
            channel.onNext(SocketPack.create(SocketPack.PACK.MESSAGE, message));
        }

        @Override
        public void onClose(int code, String reason, boolean remote) {
            channel.onNext(SocketPack.create(SocketPack.PACK.CLOSE));
            //log.info("conexión cerrada, puerto {}", port);
        }

        @Override
        public void onError(Exception ex) {
            channel.onNext(SocketPack.create(SocketPack.PACK.CONNECT_ERROR, ex.getMessage()));
        }
    }
}
