package ec.edu.utpl.datalab.codelogs.spyder.core.support.ide;

/**
 * * Created by rfcardenas
 */
public class PathException extends RuntimeException {
    public PathException() {
        super();
    }

    public PathException(String message) {
        super(message);
    }

    public PathException(String message, Throwable cause) {
        super(message, cause);
    }

    public PathException(Throwable cause) {
        super(cause);
    }

    protected PathException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
