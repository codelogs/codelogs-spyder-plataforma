package ec.edu.utpl.datalab.codelogs.spyder.core.persistencia.repos;

import ec.edu.utpl.datalab.codelogs.spyder.core.models.HeartBeatBuildRun;
import ec.edu.utpl.datalab.codelogs.spyder.core.persistencia.Repository;
import ec.edu.utpl.datalab.codelogs.spyder.core.persistencia.queries.SharedQuery;
import ec.edu.utpl.datalab.codelogs.spyder.libs.simpledata.annotations.QueryID;

import java.util.Collection;

/**
 * * Created by rfcardenas
 */
public interface RunCompileRepository extends Repository<HeartBeatBuildRun,Integer> {
    @QueryID(SharedQuery._findUnSyncDataLast)
    Collection<HeartBeatBuildRun> getLastUnsyncEntry();
}
