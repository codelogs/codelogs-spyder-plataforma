/*
 * The MIT License (MIT)
 * =========================================================================================================
 *
 * Copyright (c) 10/10/16 12:30 Modulo  Proyecto rexcode-base  Autor [rfcardenas]
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY
 * , WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ec.edu.utpl.datalab.codelogs.spyder.core.util;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.ValidatorFactory;
import java.util.Set;

/**
 * Utlidad para el procesamiento de las entidades
 * Created by rfcardenas
 */
public class ValidateEntity {
    private static ValidatorFactory factory = Validation.buildDefaultValidatorFactory();

    /**
     * Valida una entidad
     *
     * @param object
     * @param <T>
     * @return
     */
    public static <T> Set<ConstraintViolation<T>> isValid(T object) {
        return factory.getValidator().validate(object);
    }

    /**
     * Valida una entidad
     *
     * @param object
     * @param groups
     * @param <T>
     * @return
     */
    public static <T> Set<ConstraintViolation<T>> isValid(T object, Class<?> groups) {
        return factory.getValidator().validate(object, groups);
    }

    /**
     * Valida la propiedad de un objecto
     *
     * @param object
     * @param propertyname
     * @param <T>
     * @return
     */
    public static <T> Set<ConstraintViolation<T>> isValidProperty(T object, String propertyname) {
        return factory.getValidator().validateProperty(object, propertyname);
    }

    /**
     * Valida el valor de una propiedad
     *
     * @param beanType
     * @param propertyname
     * @param value
     * @param <T>
     * @return
     */
    public static <T> Set<ConstraintViolation<T>> isValidValue(Class<T> beanType, String propertyname, Object value) {
        return factory.getValidator().validateValue(beanType, propertyname, value);
    }

}
