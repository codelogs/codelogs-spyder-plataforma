package ec.edu.utpl.datalab.codelogs.spyder.core.support.ide;

import org.apache.commons.vfs2.FileObject;
import org.apache.commons.vfs2.FileSystemException;
import org.apache.commons.vfs2.VFS;

import java.io.File;

/**
 * * Created by rfcardenas
 */
public abstract class TemplatePatcher implements IdePatch {
    public static final String NAME_LOG = "rx.log";
    private String targetFile;
    private String outputRootFolder;

    public TemplatePatcher(String targetFile, String outputRootFolder) {
        this.targetFile = targetFile;
        this.outputRootFolder = outputRootFolder;
    }

    public void runPatch() throws FileSystemException {
        //1 Crear un directorio codelogs si no existe
        stepStructure();
        //2 Parchear los archivos

    }

    private void stepStructure() throws FileSystemException {
        FileObject targetFo = VFS.getManager().resolveFile(targetFile);
        FileObject outFo = VFS.getManager().resolveFile(outputRootFolder);

        if (targetFo.exists()) {
            throw new PathException("No se puede parchar el archivo no existe");
        }
        if (!outFo.exists()) {
            outFo.createFolder();
        }

        FileObject resultObject = VFS.getManager().resolveFile(outputRootFolder + File.separatorChar);

        patchTask(targetFo, outFo);
    }

    private void stepCreateFileStatus(boolean status) {
        //FileWriter fileWriter = new FileWriter();

    }
}
