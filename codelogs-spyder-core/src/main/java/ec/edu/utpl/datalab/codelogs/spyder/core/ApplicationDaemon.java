/*
 * The MIT License (MIT)
 * =========================================================================================================
 *
 * Copyright (c) 10/10/16 12:30 Modulo  Proyecto rexcode-base  Autor [rfcardenas]
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY
 * , WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ec.edu.utpl.datalab.codelogs.spyder.core;

import ec.edu.utpl.datalab.codelogs.spyder.core.env.EnvironmentProperties;
import ec.edu.utpl.datalab.codelogs.spyder.core.os.AbstractProcessChanel;
import ec.edu.utpl.datalab.codelogs.spyder.core.os.files.CommandReader;
import rx.Observer;

import java.util.Set;
import java.util.function.Consumer;

/**
 * Aplicacion que escucha un directorio especificado y responde a esos eventos
 * sadsad sadasddsaasdsadsdfsdfsdfdsfdsfsadasdsadasdsdfsdf
 * Created by rfcardenas
 */
public abstract class ApplicationDaemon implements Application {
    private Set<AbstractProcessChanel> processChanels;
    private EnvironmentProperties environmentProperties;
    private CommandReader commandReader;
    private Consumer<String> cmdProcessor;

    /**
     * @param processChanels        canal del proceso
     * @param environmentProperties //Propiedades de la aplicacion
     * @param cmdProcessor          //
     */
    public ApplicationDaemon(Set<AbstractProcessChanel> processChanels, EnvironmentProperties environmentProperties, Consumer<String> cmdProcessor) {
        this.processChanels = processChanels;
        this.environmentProperties = environmentProperties;
        this.cmdProcessor = cmdProcessor;
        this.commandReader = new CommandReaderImpl();
        configure();
    }

    public ApplicationDaemon() {
        this.commandReader = new CommandReaderImpl();
    }

    public EnvironmentProperties getEnvironmentProperties() {
        return environmentProperties;
    }

    public void setEnvironmentProperties(EnvironmentProperties environmentProperties) {
        this.environmentProperties = environmentProperties;
    }

    protected void configure() {
        for (AbstractProcessChanel processChanel : processChanels) {
            processChanel.acceptListener(commandReader);
        }
    }

    @Override
    public void start() {
        for (AbstractProcessChanel processChanel : processChanels) {
            processChanel.start();
        }
        onStart();
    }

    @Override
    public void stop() {
        for (AbstractProcessChanel processChanel : processChanels) {
            processChanel.stop();
        }
    }

    public void setProcessChanels(Set<AbstractProcessChanel> processChanels) {
        this.processChanels = processChanels;
    }

    public void setCommandReader(CommandReader commandReader) {
        this.commandReader = commandReader;
    }

    public void setCmdProcessor(Consumer<String> cmdProcessor) {
        this.cmdProcessor = cmdProcessor;
    }

    @Override
    public void injectEnvironment(EnvironmentProperties environmentProperties) {

    }

    private class CommandReaderImpl implements CommandReader {
        private Observer<String> observer;

        public CommandReaderImpl() {
            this.observer = new Observer<String>() {
                @Override
                public void onCompleted() {

                }

                @Override
                public void onError(Throwable e) {
                    e.printStackTrace();
                }

                @Override
                public void onNext(String s) {
                    cmdProcessor.accept(s);
                }
            };
        }

        @Override
        public Observer<String> listener() {
            return observer;
        }
    }
}
