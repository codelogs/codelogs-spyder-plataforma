package ec.edu.utpl.datalab.codelogs.spyder.core.os;

/**
 * * Created by rfcardenas
 */
public class SocketPack {
    private PACK pack;
    private String data;
    private SocketPack(PACK pack) {
        this.pack = pack;
    }

    private SocketPack(PACK pack, String data) {
        this.pack = pack;
        this.data = data;
    }

    public static SocketPack create(PACK pack, String data) {
        return new SocketPack(pack, data);
    }

    public static SocketPack create(PACK pack) {
        return new SocketPack(pack, "");
    }

    public PACK getPack() {
        return pack;
    }

    public String getData() {
        return data;
    }

    public enum PACK {
        MESSAGE,
        CONNECT_ERROR,
        CONNECT_SUCCESS,
        CLOSE
    }
}
