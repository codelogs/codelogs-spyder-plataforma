/*
 * The MIT License (MIT)
 * =========================================================================================================
 *
 * Copyright (c) 10/10/16 15:20 Modulo  Proyecto shell-monitor  Autor [rfcardenas]
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY
 * , WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ec.edu.utpl.datalab.codelogs.spyder.core.os.ws;

import com.google.inject.Singleton;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URISyntaxException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;

/**
 * * Created by rfcardenas
 */
@Singleton
public class AutoConnectionSocket extends SimpleConnectionSocket implements Runnable {
    private static final Logger log = LoggerFactory.getLogger(AutoConnectionSocket.class);
    private ExecutorService service;

    public AutoConnectionSocket(int port) {
        super(port);
        this.service = Executors.newSingleThreadExecutor(factoryThread());
    }

    public AutoConnectionSocket(int port, int attemps, int timeoutByAttemp, TimeUnit timeUnit) {
        super(port, attemps, timeoutByAttemp, timeUnit);
        this.service = Executors.newSingleThreadExecutor(factoryThread());
    }

    /**
     * Fabrica de threads
     *
     * @return
     */
    private ThreadFactory factoryThread() {
        return r -> {
            Thread thread = new Thread(r);
            thread.setPriority(Thread.MIN_PRIORITY);
            thread.setDaemon(true);
            thread.setName("System-socket");
            return thread;
        };
    }

    private void connectLoop() throws InterruptedException {
        long mill = TimeUnit.MILLISECONDS.convert(getTimeoutByAttemp(), getTimeUnit());
        while (!Thread.currentThread().isInterrupted()) try {
            connectMethod();
            Thread.sleep(mill);
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void connectMonitor() throws Exception {
        log.info("Autoconexion puerto {}", getPort());
        service.execute(this);
    }

    @Override
    protected void disconnectMonitor() {
        super.disconnectMonitor();
        service.shutdownNow();
    }

    @Override
    public void run() {
        try {
            connectLoop();
        } catch (InterruptedException e) {
            log.warn("connectLoop terminado");
        }
    }

    public void forceConnectionMonitor(int port) {
        log.info("Forzando conexión en el puerto {}", port);
        if (service != null) service.shutdownNow();
        this.setPort(port);
        this.service = Executors.newSingleThreadExecutor(factoryThread());
        service.execute(this);
    }

}

