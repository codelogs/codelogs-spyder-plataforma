/*
 * The MIT License (MIT)
 * =========================================================================================================
 *
 * Copyright (c) 10/10/16 12:30 Modulo  Proyecto rexcode-base  Autor [rfcardenas]
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY
 * , WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ec.edu.utpl.datalab.codelogs.spyder.core.os.rmi;

import ec.edu.utpl.datalab.codelogs.spyder.core.os.AbstractProcessChanel;
import ec.edu.utpl.datalab.codelogs.spyder.core.os.files.FileProcessChanel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.rmi.AlreadyBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

/**
 * Created by rfcardenas
 */
public class RmiServer extends AbstractProcessChanel {
    private static final Logger log = LoggerFactory.getLogger(FileProcessChanel.class);
    private Registry registry;
    private String registro;
    private int port;

    public RmiServer(String registro, int port) throws RemoteException {
        this.port = port <= 0 || port > 65535 ? 9090 : port;
        this.registro = registro;
    }

    private void createServer() {
        if (registry == null) {
            try {
                this.registry = LocateRegistry.createRegistry(port);
                registry.bind(registro, new DefaultRemoteExecutor(this));
            } catch (RemoteException | AlreadyBoundException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void start() {
        log.info("Servicio   :Start registro [{}] en el puerto [{}]", registro, port);
        createServer();
    }

    @Override
    public void stop() {
        log.info("Servicio   :Stop");

    }
}
