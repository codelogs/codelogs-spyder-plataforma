/*
 * The MIT License (MIT)
 * =========================================================================================================
 *
 * Copyright (c) 10/10/16 12:30 Modulo  Proyecto rexcode-base  Autor [rfcardenas]
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY
 * , WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ec.edu.utpl.datalab.codelogs.spyder.core.env;

import ec.edu.utpl.datalab.codelogs.spyder.core.filesystem.EnvironmentAppNotFound;
import org.apache.commons.vfs2.FileObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;
import java.util.Properties;

/**
 * Created by rfcardenas
 */
public class FileEnvironmentPropertiesImpl extends EnvironmentPropertiesImpl {
    private static final Logger log = LoggerFactory.getLogger(FileEnvironmentPropertiesImpl.class);

    private FileObject fileObject;


    public FileEnvironmentPropertiesImpl(FileObject fileObject, Map propertiesDefault) {
        this.fileObject = fileObject;
        try {
            log.info("Cargado propiedades por defecto " + fileObject);
            map.putAll(propertiesDefault);
            log.info("Sobreescribiendo propiedades externas");
            init();
        } catch (EnvironmentAppNotFound environmentAppNotFound) {
            log.error("No se pudo recuperar la configuracion del archivo");
            environmentAppNotFound.printStackTrace();
        }
    }

    /**
     * Carga las configuraciones buscando en el directorio de inslatacion del demonio
     *
     * @throws EnvironmentAppNotFound
     * @throws UnloadablePropertiesException
     */
    protected void init() throws EnvironmentAppNotFound {
        Properties properties = new Properties();
        PropertyLoader propertyLoader = new FileLoaderProperties();
        Map propeties = null;
        try {
            propeties = propertyLoader.process(fileObject);
        } catch (UnloadablePropertiesException e) {
            e.printStackTrace();
        }
        map.putAll(propeties);
    }

}
