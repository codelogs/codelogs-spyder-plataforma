package ec.edu.utpl.datalab.codelogs.spyder.core.logging.log4j2;

import org.apache.logging.log4j.core.LogEvent;
import org.apache.logging.log4j.core.config.plugins.Plugin;
import org.apache.logging.log4j.core.pattern.ConverterKeys;
import org.apache.logging.log4j.core.pattern.ExtendedThrowablePatternConverter;
import org.apache.logging.log4j.core.pattern.PatternConverter;
import org.apache.logging.log4j.core.pattern.ThrowablePatternConverter;


@Plugin(name = "ExtendedWhitespaceThrowablePatternConverter", category = PatternConverter.CATEGORY)
@ConverterKeys({"xwEx", "xwThrowable", "xwException"})
public final class ExtendedWhitespaceThrowablePatternConverter
    extends ThrowablePatternConverter {

    private final ExtendedThrowablePatternConverter delegate;

    private ExtendedWhitespaceThrowablePatternConverter(String[] options) {
        super("WhitespaceExtendedThrowable", "throwable", options);
        this.delegate = ExtendedThrowablePatternConverter.newInstance(options);
    }

    public static ExtendedWhitespaceThrowablePatternConverter newInstance(
        String[] options) {
        return new ExtendedWhitespaceThrowablePatternConverter(options);
    }

    @Override
    public void format(LogEvent event, StringBuilder buffer) {
        if (event.getThrown() != null) {
            buffer.append(this.options.getSeparator());
            this.delegate.format(event, buffer);
            buffer.append(this.options.getSeparator());
        }
    }

}
