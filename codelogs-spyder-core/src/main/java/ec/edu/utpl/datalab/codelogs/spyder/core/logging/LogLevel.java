package ec.edu.utpl.datalab.codelogs.spyder.core.logging;


public enum LogLevel {

    TRACE, DEBUG, INFO, WARN, ERROR, FATAL, OFF

}
