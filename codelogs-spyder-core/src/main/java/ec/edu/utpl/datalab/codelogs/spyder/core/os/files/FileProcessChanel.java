/*
 * The MIT License (MIT)
 * =========================================================================================================
 *
 * Copyright (c) 10/10/16 12:30 Modulo  Proyecto rexcode-base  Autor [rfcardenas]
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY
 * , WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ec.edu.utpl.datalab.codelogs.spyder.core.os.files;

import ec.edu.utpl.datalab.codelogs.spyder.core.filesystem.FileSystemCommon;
import ec.edu.utpl.datalab.codelogs.spyder.core.os.AbstractProcessChanel;
import org.apache.commons.vfs2.FileChangeEvent;
import org.apache.commons.vfs2.FileListener;
import org.apache.commons.vfs2.FileObject;
import org.apache.commons.vfs2.FileSystemException;
import org.apache.commons.vfs2.impl.DefaultFileMonitor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by rfcardenas
 */
public class FileProcessChanel extends AbstractProcessChanel {
    private static final Logger log = LoggerFactory.getLogger(FileProcessChanel.class);
    private FileObject fileObject;
    private ExecutorService service;

    public FileProcessChanel(FileObject fileObject) {
        this.fileObject = fileObject;
    }

    private void watch() throws IOException {
        if (!fileObject.exists()) fileObject.createFile();
        FileListener fileListener = new FileListener() {
            @Override
            public void fileCreated(FileChangeEvent event) throws Exception {
                loadCommand();
            }

            @Override
            public void fileDeleted(FileChangeEvent event) throws Exception {

            }

            @Override
            public void fileChanged(FileChangeEvent event) throws Exception {
                loadCommand();
            }
        };
        DefaultFileMonitor fileMonitor = new DefaultFileMonitor(fileListener);
        service = Executors.newFixedThreadPool(1);
        service.execute(() -> {
            fileMonitor.addFile(fileObject);
            fileMonitor.start();
        });
    }

    public String loadCommand() {
        String command = "";
        try {
            if (!fileObject.exists()) fileObject.createFile();
            FileSystemCommon.lock(fileObject);
            command = FileSystemCommon.readBufferLimit(fileObject, 200);
            if (command != null) {
                FileSystemCommon.removeFirstLine(fileObject);
                log.info("Publicando comando " + command);
                publish(command);
            }
            FileSystemCommon.unlock(fileObject);

        } catch (FileSystemException e) {
            e.printStackTrace();
        }

        return command;
    }


    @Override
    public void start() {
        log.info("Servicio   :Start");
        try {
            watch();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void stop() {
        log.info("Servicio   :Stop ");
        if (service != null) service.shutdownNow();
    }
}
