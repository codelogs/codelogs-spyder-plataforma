package ec.edu.utpl.datalab.codelogs.spyder.core.env;

/**
 * * Created by rfcardenas
 */
public class BadArgumentException extends RuntimeException {
    public BadArgumentException() {
        super("Argumento mal especificado");
    }

    public BadArgumentException(String message) {
        super(message);
    }

    public BadArgumentException(String message, Throwable cause) {
        super(message, cause);
    }

    public BadArgumentException(Throwable cause) {
        super(cause);
    }

    public BadArgumentException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
