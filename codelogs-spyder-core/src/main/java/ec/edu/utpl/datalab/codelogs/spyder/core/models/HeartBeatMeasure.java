/*
 * The MIT License (MIT)
 * =========================================================================================================
 *
 * Copyright (c) 10/10/16 12:30 Modulo  Proyecto rexcode-base  Autor [rfcardenas]
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY
 * , WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ec.edu.utpl.datalab.codelogs.spyder.core.models;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import ec.edu.utpl.datalab.codelogs.spyder.core.models.enumeration.TYPE_INSTRUCTION;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Objects;

/**
 * A Measure.
 */
@DatabaseTable(tableName = "heartbeat_measure")
public class HeartBeatMeasure implements Serializable {

    private static final long serialVersionUID = 1L;

    @DatabaseField(dataType = DataType.LONG, generatedId = true)
    private long id;
    private String uuid;
    @DatabaseField(dataType = DataType.DOUBLE)
    private double value;
    @DatabaseField(dataType = DataType.ENUM_STRING)
    private TYPE_INSTRUCTION instruction;
    @DatabaseField(foreign = true)
    private ProjectAnalysis projectAnalysis;

    private String metricUUID;

    @DatabaseField(dataType = DataType.BOOLEAN)
    private boolean sync;

    @DatabaseField
    private Timestamp timestamp;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public TYPE_INSTRUCTION getInstruction() {
        return instruction;
    }

    public void setInstruction(TYPE_INSTRUCTION instruction) {
        this.instruction = instruction;
    }

    public ProjectAnalysis getProjectAnalysis() {
        return projectAnalysis;
    }

    public void setProjectAnalysis(ProjectAnalysis projectAnalysis) {
        this.projectAnalysis = projectAnalysis;
    }

    public String getMetricUUID() {
        return metricUUID;
    }

    public void setMetricUUID(String metricUUID) {
        this.metricUUID = metricUUID;
    }

    public boolean isSync() {
        return sync;
    }

    public void setSync(boolean sync) {
        this.sync = sync;
    }

    public Timestamp getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Timestamp timestamp) {
        this.timestamp = timestamp;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "Measure{" +
            "id=" + id +
            ", value='" + value + "'" +
            ", metricuuid='" + metricUUID + "'" +
            '}';
    }
}
