package ec.edu.utpl.datalab.codelogs.spyder.core.validator;

/**
 * Created by rfcardenas
 */

import javax.inject.Qualifier;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Validador de codigo fuente, extension
 */
@Qualifier
@Retention(RUNTIME)
@Target({TYPE, METHOD, FIELD, PARAMETER})
public @interface Validator {
    /**
     * Extension del archivo
     *
     * @return
     */
    String value();
}
