package ec.edu.utpl.datalab.codelogs.spyder.core.env;

import ec.edu.utpl.datalab.codelogs.spyder.core.filesystem.EnvironmentAppNotFound;

/**
 * Reglas para definir que propiedades son criticas
 * * Created by rfcardenas
 */
public interface EnvironmentCkecker {
    /**
     * Si una propiedad no se encuentra se tiene que dispara una exception
     * indicando la propiedad no definida
     * @param properties
     * @throws EnvironmentAppNotFound
     */
    void check(EnvironmentProperties properties) throws EnvironmentAppNotFound;
}
