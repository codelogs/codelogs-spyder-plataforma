/*
 * The MIT License (MIT)
 * =========================================================================================================
 *
 * Copyright (c) 10/10/16 12:30 Modulo  Proyecto rexcode-base  Autor [rfcardenas]
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY
 * , WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ec.edu.utpl.datalab.codelogs.spyder.core.synchronization;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import rx.Observer;
import rx.subjects.PublishSubject;

/**
 * Created by rfcardenas
 */
public abstract class AbstractSyncronizerRunnner implements SyncronizationRunner {
    private static final Logger log = LoggerFactory.getLogger(AbstractSyncService.class);
    private final Observer<Object> anyEvent;
    private final PublishSubject<StatusSync> endSubject;
    private StatusSync statusSync = StatusSync.UNKNOW;

    public AbstractSyncronizerRunnner() {
        this.anyEvent = new ObserverPulses();
        this.endSubject = PublishSubject.create();
    }

    private class ObserverPulses implements Observer<Object> {

        @Override
        public void onCompleted() {
            log.info("Sub finalizada");
        }

        @Override
        public void onError(Throwable e) {
            log.error("Error " + e.getMessage());
        }

        @Override
        public void onNext(Object pulseCode) {
            log.info("Evento icm");
        }
    }
}
