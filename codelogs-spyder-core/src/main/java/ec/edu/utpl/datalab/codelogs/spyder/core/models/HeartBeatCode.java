/*
 * The MIT License (MIT)
 * =========================================================================================================
 *
 * Copyright (c) 10/10/16 12:30 Modulo  Proyecto rexcode-base  Autor [rfcardenas]
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY
 * , WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ec.edu.utpl.datalab.codelogs.spyder.core.models;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import ec.edu.utpl.datalab.codelogs.spyder.core.models.enumeration.TYPE_INSTRUCTION;
import org.hibernate.validator.constraints.NotEmpty;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

/**
 * A PulseCode.
 */
@DatabaseTable(tableName = "heartbeat_code")
public class HeartBeatCode implements EntityVisitable, Serializable {
    public static final String COL_ID = "id";
    public static final String COL_WORK = "workSession";
    private static final long serialVersionUID = 1L;

    @DatabaseField(dataType = DataType.LONG, generatedId = true)
    private long id;

    @DatabaseField
    private String uuid;

    @DatabaseField(dataType = DataType.DATE)
    private Date captureDate;

    @DatabaseField
    private String pathFile;

    @DatabaseField
    private int linesAdd;

    @DatabaseField
    private int linesDel;

    @DatabaseField(dataType = DataType.LONG)
    private long levenshtein;

    @DatabaseField(dataType = DataType.LONG)
    private long timeFixed;

    @DatabaseField
    private String blockMap;

    @DatabaseField
    private String brachname;

    @DatabaseField
    private String lang;

    @DatabaseField(dataType = DataType.ENUM_STRING)
    private TYPE_INSTRUCTION type_instruction = TYPE_INSTRUCTION.HEART_BEAT_CODE_ACTIVITY;

    @DatabaseField(foreign = true, foreignAutoRefresh = true)
    private WorkSession workSession;

    @DatabaseField(foreign = true, foreignAutoRefresh = true)
    private FileCode fileCode;

    @DatabaseField(dataType = DataType.BOOLEAN)
    private boolean sync;

    @DatabaseField(dataType = DataType.BOOLEAN)
    private boolean keyGranted = false;

    @NotEmpty
    @DatabaseField(dataType = DataType.STRING)
    private String typeBeat;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public Date getCaptureDate() {
        return captureDate;
    }

    public void setCaptureDate(Date captureDate) {
        this.captureDate = captureDate;
    }

    public String getPathFile() {
        return pathFile;
    }

    public void setPathFile(String pathFile) {
        this.pathFile = pathFile;
    }

    public int getLinesAdd() {
        return linesAdd;
    }

    public void setLinesAdd(int linesAdd) {
        this.linesAdd = linesAdd;
    }

    public int getLinesDel() {
        return linesDel;
    }

    public void setLinesDel(int linesDel) {
        this.linesDel = linesDel;
    }

    public long getLevenshtein() {
        return levenshtein;
    }

    public void setLevenshtein(long levenshtein) {
        this.levenshtein = levenshtein;
    }

    public long getTimeFixed() {
        return timeFixed;
    }

    public void setTimeFixed(long timeFixed) {
        this.timeFixed = timeFixed;
    }

    public String getBlockMap() {
        return blockMap;
    }

    public void setBlockMap(String blockMap) {
        this.blockMap = blockMap;
    }

    public String getBrachname() {
        return brachname;
    }

    public void setBrachname(String brachname) {
        this.brachname = brachname;
    }

    public WorkSession getWorkSession() {
        return workSession;
    }

    public void setWorkSession(WorkSession workSession) {
        this.workSession = workSession;
    }

    public FileCode getFileCode() {
        return fileCode;
    }

    public void setFileCode(FileCode fileCode) {
        this.fileCode = fileCode;
    }

    public boolean isSync() {
        return sync;
    }

    public void setSync(boolean sync) {
        this.sync = sync;
    }

    public boolean isKeyGranted() {
        return keyGranted;
    }

    public void setKeyGranted(boolean keyGranted) {
        this.keyGranted = keyGranted;
    }

    public String getTypeBeat() {
        return typeBeat;
    }

    public void setTypeBeat(String typeBeat) {
        this.typeBeat = typeBeat;
    }

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    public TYPE_INSTRUCTION getType_instruction() {
        return type_instruction;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        HeartBeatCode heartBeatCode = (HeartBeatCode) o;
        /**if(pulseCode.id == null || id == null) {
         return false;
         }**/
        return Objects.equals(id, heartBeatCode.id);
    }


    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "HeartBeatCode{" +
            "id=" + id +
            ", uuid='" + uuid + '\'' +
            ", captureDate=" + captureDate +
            ", pathFile='" + pathFile + '\'' +
            ", linesAdd=" + linesAdd +
            ", linesDel=" + linesDel +
            ", levenshtein=" + levenshtein +
            ", timeFixed=" + timeFixed +
            ", blockMap='" + blockMap + '\'' +
            ", brachname='" + brachname + '\'' +
            ", lang='" + lang + '\'' +
            ", type_instruction=" + type_instruction +
            ", workSession=" + workSession +
            ", fileCode=" + fileCode +
            ", sync=" + sync +
            ", keyGranted=" + keyGranted +
            ", typeBeat='" + typeBeat + '\'' +
            '}';
    }

    @Override
    public boolean isValidRepresentation() {
        return id > 0;
    }

    @Override
    public void applyFunction(EntityVisitor entityVisitor) {
        entityVisitor.function(this);
    }
}
