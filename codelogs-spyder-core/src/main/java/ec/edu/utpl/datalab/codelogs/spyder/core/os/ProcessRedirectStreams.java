/*
 * The MIT License (MIT)
 * =========================================================================================================
 *
 * Copyright (c) 10/10/16 12:43 Modulo  Proyecto rexcode-base  Autor [rfcardenas]
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY
 * , WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ec.edu.utpl.datalab.codelogs.spyder.core.os;

import ec.edu.utpl.datalab.codelogs.spyder.core.os.files.FileProcessChanel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import rx.Observable;
import rx.subjects.PublishSubject;

import java.io.OutputStream;
import java.io.PrintStream;
import java.util.Objects;

import static java.lang.System.err;
import static java.lang.System.out;

/**
 * Created by rfcardenas
 */
public class ProcessRedirectStreams {
    private static final Logger log = LoggerFactory.getLogger(FileProcessChanel.class);
    private PrintStream originalOutput;
    private PrintStream originalError;
    private PublishSubject<String> publishSubject = PublishSubject.create();
    private PublishSubject<String> errorSubject = PublishSubject.create();

    public ProcessRedirectStreams(PrintStream out, PrintStream err) {
        this.originalOutput = Objects.requireNonNull(out, "Out no debe ser null");
        this.originalError = Objects.requireNonNull(err, "Error no debe ser null");
    }

    public void connect() {
        log.info("Redirect proces stream start.....");
        System.setOut(new Out(out));
        System.setErr(new Error(err));
    }

    public void finish() {
        log.info("Redirect proces stream end.....");
        System.setOut(this.originalOutput);
        System.setErr(this.originalError);
    }

    public Observable<String> observable() {
        return publishSubject.asObservable();
    }

    public Observable<String> errorObservable() {
        return errorSubject.asObservable();
    }

    private class Out extends PrintStream {
        private Out(OutputStream out) {
            super(out);
        }

        @Override
        public void write(byte[] buf, int off, int len) {
            originalOutput.write(buf, off, len);
            String s = new String(buf, off, len);
            publishSubject.onNext(s);
        }
    }

    private class Error extends PrintStream {
        private Error(OutputStream out) {
            super(out);
        }

        @Override
        public void write(byte[] buf, int off, int len) {
            originalError.write(buf, off, len);
            String s = new String(buf, off, len);
            publishSubject.onNext(s);
            errorSubject.onNext(s);
        }
    }


}
