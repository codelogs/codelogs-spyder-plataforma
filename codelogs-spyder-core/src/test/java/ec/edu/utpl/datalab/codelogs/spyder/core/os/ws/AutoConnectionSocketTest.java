package ec.edu.utpl.datalab.codelogs.spyder.core.os.ws;

import ec.edu.utpl.datalab.codelogs.spyder.core.os.AbstractConnectionProcess;
import junit.framework.TestCase;
import org.junit.Ignore;
import org.junit.Test;

/**
 * * Created by rfcardenas
 */
@Ignore
public class AutoConnectionSocketTest extends TestCase {
    AbstractConnectionProcess autoConnectionSocket = new AutoConnectionSocket(8484);

    @Test
    public void test() throws Exception {
        testConnectMonitor();
        testDisconnectMonitor();
    }

    public void testConnectMonitor() throws Exception {
        System.out.println();
        System.out.println("Test Monitor");
        autoConnectionSocket.connect();
        Thread.sleep(5000);
    }

    public void testDisconnectMonitor() throws Exception {
        autoConnectionSocket.disconnect();
    }

}
