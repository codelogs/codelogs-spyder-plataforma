package ec.edu.utpl.datalab.codelogs.spyder.core.os.ws;

import ec.edu.utpl.datalab.codelogs.spyder.core.os.AbstractConnectionProcess;
import junit.framework.TestCase;
import org.junit.Ignore;
import org.junit.Test;

import java.util.concurrent.TimeUnit;

/**
 * * Created by rfcardenas
 */
@Ignore
public class SimpleConnectionSocketTest extends TestCase {
    AbstractConnectionProcess abstractConnectionProcess = new SimpleConnectionSocket(8484, 2, 1, TimeUnit.SECONDS);

    @Test
    public void test() {
        testConnection();
        testDiscnn();
    }

    public void testConnection() {
        abstractConnectionProcess.connect();
        abstractConnectionProcess.send("hello");
    }

    public void testDiscnn() {
        abstractConnectionProcess.disconnect();
    }
}
