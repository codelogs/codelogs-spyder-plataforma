package ec.edu.utpl.datalab.codelogs.spyder.core.env;

import org.apache.commons.vfs2.FileObject;
import org.apache.commons.vfs2.VFS;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;

/**
 * * Created by rfcardenas
 */
public class FileEnvironmentPropertiesImplTest {
    @Test
    public void init() throws Exception {
        FileObject fileObject = VFS.getManager().toFileObject(new File("src/test/resources/monitor_daemon.properties"));
        FileEnvironmentPropertiesImpl fileEnvironmentProperties = new FileEnvironmentPropertiesImpl(fileObject, PropertiesDefault.map);
        String protocol = fileEnvironmentProperties.getProperty(PropertiesDefault.SYNC_PROTOCOL, String.class);
        String server = fileEnvironmentProperties.getProperty(PropertiesDefault.SYNC_SERVER, String.class);
        System.out.println(protocol);
        System.out.println(server);
        Assert.assertEquals("KWT-Bearer", protocol);
        Assert.assertEquals("http://localhost:8080", server);
    }

}
