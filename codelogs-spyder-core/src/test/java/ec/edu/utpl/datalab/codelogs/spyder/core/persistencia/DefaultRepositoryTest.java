package ec.edu.utpl.datalab.codelogs.spyder.core.persistencia;

import ec.edu.utpl.datalab.codelogs.spyder.core.models.HeartBeatBuildRun;
import ec.edu.utpl.datalab.codelogs.spyder.core.models.WorkSession;
import org.junit.Test;

import java.io.File;

/**
 * * Created by rfcardenas
 */
public class DefaultRepositoryTest {
    @Test
    public void init(){
        String tmp = System.getProperty("java.io.tmpdir");
        RepositoryService repositoryService = new DefaultRepository(tmp+ File.separatorChar+"demo");

        WorkSession workSession = new WorkSession();
        workSession.setFixedTimeCodeAct(100);

        repositoryService.worksessionRepository().save(workSession);

        HeartBeatBuildRun heartBeatBuildRun = new HeartBeatBuildRun();
        heartBeatBuildRun.setMessageL2("adsasda");
        heartBeatBuildRun.setPathContext("asdad");

        heartBeatBuildRun.setWorkSession(workSession);

        repositoryService.runCompileRepository().save(heartBeatBuildRun);
    }
}
