package ec.edu.utpl.datalab.codelogs.spyder.core.system;

import ec.edu.utpl.datalab.codelogs.spyder.libs.terminal.SystemTerminal;
import ec.edu.utpl.datalab.codelogs.spyder.libs.terminal.TerminalBuilder;
import org.junit.Test;

/**
 * * Created by rfcardenas
 */
public class TerminalBuilderTest {
    @Test
    public void commit() throws Exception {
        SystemTerminal sstemTerminal = TerminalBuilder.instance()
            .addCommand(new CommandHelloWorld())
            .commit();
        sstemTerminal.process(new String[]{"-hello Ronald", "-mess cardenas"});
    }

}
