package ec.edu.utpl.datalab.codelogs.spyder.core.env;

import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

/**
 * * Created by rfcardenas
 */
public class EnvironmentPropertiesImplTest {
    EnvironmentPropertiesImpl environmentProperties;

    Map<Object,Object> map = new HashMap<>();

    @Before
    public void setTup() {
        map.put("entero",8484);

        environmentProperties = new EnvironmentPropertiesImpl(map);
    }

    @Test
    public void containsProperty() throws Exception {

    }

    @Test
    public void getProperty() throws Exception {

    }

    @Test
    public void getProperty1() throws Exception {

    }

    @Test
    public void getProperty2() throws Exception {

    }

    @Test
    public void getProperty3() throws Exception {

    }

    @Test
    public void getRequiredProperty() throws Exception {

    }

    @Test
    public void getRequiredProperty1() throws Exception {

    }

    @Test
    public void asSet() throws Exception {

    }

    @Test
    public void getAsSet() throws Exception {

    }

    @Test
    public void getAsSetWithDefaults() throws Exception {

    }

    @Test
    public void putValue() throws Exception {

    }

    @Test
    public void overrideWith() throws Exception {

    }

    @Test
    public void getAsBolean() throws Exception {

    }

    @Test
    public void getAsInt() throws Exception {
        int a  = environmentProperties.getAsInt("entero");

        assert a==8484;
    }

    @Test
    public void getAsInt1() throws Exception {
        int a = environmentProperties.getAsInt("entero",8484);
        assert a==8484;
    }

    @Test
    public void hasProperty() throws Exception {

    }

}
