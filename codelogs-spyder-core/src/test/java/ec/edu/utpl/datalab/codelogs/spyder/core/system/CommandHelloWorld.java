package ec.edu.utpl.datalab.codelogs.spyder.core.system;

import ec.edu.utpl.datalab.codelogs.spyder.libs.terminal.SystemCommand;
import ec.edu.utpl.datalab.codelogs.spyder.libs.terminal.SystemCommandProvider;
import org.apache.commons.cli.Option;

/**
 * * Created by rfcardenas
 */
public class CommandHelloWorld implements SystemCommandProvider {
    @Override
    public SystemCommand command() {
        Option openOption = Option.builder("hello")
            .hasArg()
            .desc("monitorea el proyecto")
            .build();
        Option argument = Option.builder("mess")
            .hasArg()
            .desc("Message")
            .build();

        return SystemCommand.builder(openOption)
            .addOptions(argument)
            .setAction(args -> {
                System.out.println("Apellido " + args.get("mess"));
                System.out.printf("Hello from terminal %s\n", args.get("hello"));
            })
            .build();
    }
}
