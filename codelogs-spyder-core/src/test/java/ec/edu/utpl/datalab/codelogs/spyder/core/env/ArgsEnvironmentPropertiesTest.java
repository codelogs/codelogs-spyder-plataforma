package ec.edu.utpl.datalab.codelogs.spyder.core.env;

import org.junit.Assert;
import org.junit.Test;

/**
 * * Created by rfcardenas
 */
public class ArgsEnvironmentPropertiesTest {
    @Test
    public void mapArgs() throws Exception {
        String[] args = new String[]{PropertiesDefault.MONITOR_PROC_CMD_PORT+"=9898", "monitor.cpu.step=112"};
        ArgsEnvironmentProperties argsEnvironmentProperties = new ArgsEnvironmentProperties(args);

        String port = argsEnvironmentProperties.getProperty(PropertiesDefault.MONITOR_PROC_CMD_PORT, String.class);

        Assert.assertEquals("9898", port);
    }

}
