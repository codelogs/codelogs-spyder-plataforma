package ec.edu.utpl.datalab.codelogs.spyder.libs.terminal;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.*;

/**
 * Representa un comando, puede estarcomuesta por varias entradas o argumentos
 * * Created by rfcardenas
 */

public class Command {
    @JsonProperty("commands")
    private List<CommandEntry> commandEntries = new ArrayList<>();

    public Command() {
    }

    public Command(Collection<CommandEntry> commandEntries) {
        this.commandEntries = new ArrayList<>(commandEntries);
    }

    public List<CommandEntry> getCommandEntries() {
        return commandEntries;
    }

    public void setCommandEntries(Collection<CommandEntry> commandEntries) {
        this.commandEntries = new ArrayList<>(commandEntries);
    }

    @Override
    public String toString() {
        return "CommandBuilder{" +
            "commands=" + commandEntries +
            '}';
    }

    public static CommandMake createCommand(String command, String argument){
        return new CommandMake(command,argument);
    }

    public static class CommandMake{
        private Set<CommandEntry> commandEntries = new HashSet<>();

        public CommandMake(String command, String argument) {
            addCommand(command,argument);
        }

        public CommandMake addCommand(String command, String argument){
            Objects.requireNonNull(command,"Se requiere un comando");
            commandEntries.add(new CommandEntry(command,argument));
            return this;
        }
        public CommandMake addCommand(String command){
            Objects.requireNonNull(command,"Se requiere un comando");
            commandEntries.add(new CommandEntry(command,""));
            return this;
        }

        public Command build(){
            return new Command(commandEntries);
        }
    }
}
