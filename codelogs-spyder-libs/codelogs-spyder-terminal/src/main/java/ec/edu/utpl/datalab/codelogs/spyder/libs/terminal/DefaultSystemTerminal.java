/*
 * The MIT License (MIT)
 * =========================================================================================================
 *
 * Copyright (c) 10/10/16 12:30 Modulo  Proyecto rexcode-base  Autor [rfcardenas]
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY
 * , WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ec.edu.utpl.datalab.codelogs.spyder.libs.terminal;


import ec.edu.utpl.datalab.codelogs.spyder.libs.common.StringUtils;
import org.apache.commons.cli.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

/**
 * Created by rfcardenas
 */
final class DefaultSystemTerminal implements SystemTerminal {
    private static final Logger log = LoggerFactory.getLogger(DefaultSystemTerminal.class);

    private CommandLineParser parser = new GnuParser();
    private Options options = new Options();
    private Set<SystemCommand> comandos = new HashSet<>();

    public static void main(String[] parameters) {
        CommandLine commandLine;
        Option option_A = Option.builder("A")
            .hasArg()
            .build();
        Option option_r = Option.builder("r")
            .hasArg()
            .build();
        Option option_S = OptionBuilder.withArgName("opt2").hasArg().withDescription("The S option").create("S");
        Option option_test = new Option("TestServe", "The TestServe option");
        Options options = new Options();
        CommandLineParser parser = new GnuParser();

        String[] testArgs =
            {"-r", "opt1", "-S", "opt2", "arg1", "arg2",
                "arg3", "arg4", "--TestServe", "-A", "opt3",};

        options.addOption(option_A);
        options.addOption(option_r);
        options.addOption(option_S);
        options.addOption(option_test);

        try {
            commandLine = parser.parse(options, testArgs);

            if (commandLine.hasOption("A")) {
                System.out.print("Option A is present.  The value is: ");
                System.out.println(commandLine.getOptionValue("A"));
            }

            if (commandLine.hasOption("r")) {
                System.out.print("Option r is present.  The value is: ");
                System.out.println(commandLine.getOptionValue("r"));
            }

            if (commandLine.hasOption("S")) {
                System.out.print("Option S is present.  The value is: ");
                System.out.println(commandLine.getOptionValue("S"));
            }

            if (commandLine.hasOption("TestServe")) {
                System.out.println("Option TestServe is present.  This is a flag option.");
            }

            {
                String[] remainder = commandLine.getArgs();
                System.out.print("Remaining arguments: ");
                for (String argument : remainder) {
                    System.out.print(argument);
                    System.out.print(" ");
                }

                System.out.println();
            }

        } catch (ParseException exception) {
            System.out.print("Parse error: ");
            System.out.println(exception.getMessage());
        }
    }

    public void initConsole() {
        for (SystemCommand systemCommand : comandos) {
            options.addOption(systemCommand.desencadenador);
        }
    }

    /**
     * identifica los comandos y procesa las acciones en caso de estar
     * registraddas
     *
     * @param args formato []={"-cmd1 algo en","-cmd2 ewdasd"}
     * @return
     */
    @Override
    public void process(String[] args) {
        try {
            CommandLine commandLine = parser.parse(options, parseCommand(args));
            Map<String, String> argumentos = new HashMap<>();
            int counterRef = 0;
            for (SystemCommand systemCommand : comandos) {
                Option desencadenador = systemCommand.desencadenador;
                // Si la linea tiene el comando desencadenador
                if (desencadenador.getOpt() != null && commandLine.hasOption(desencadenador.getOpt())) {
                    argumentos.put(desencadenador.getOpt(), commandLine.getOptionValue(desencadenador.getOpt()));
                    for (Option option : systemCommand.options) {
                        if (commandLine.hasOption(option.getOpt())) {
                            argumentos.put(option.getOpt(), commandLine.getOptionValue(option.getOpt()));
                        }
                    }
                    /**systemCommand.options.stream().filter(option -> commandLine.hasOption(option.getOpt())).forEach(option -> {
                     argumentos.put(option.getOpt(), commandLine.getOptionValue(option.getOpt()));
                     });**/
                    ++counterRef;
                    systemCommand.commandAction.execute(argumentos);
                    break;
                }
            }
            if (counterRef == 0) {
                log.warn("Desencadenador no tiene opciones o argumentos ");
            }

        } catch (UnrecognizedOptionException e) {
            String mess = String.format("No se conoce el comando " + StringUtils.stringFrom(args));
            throw new UnsupportedCommandException(mess, e);
        } catch (ParseException | InterruptedException e) {
            e.printStackTrace();
        }
        log.info("End proces...[]");
    }

    final void add(SystemCommandProvider command) {
        try {
            SystemCommand commanBuild = command.command();
            if (commanBuild.desencadenador != null) {
                this.comandos.add(commanBuild);
                options.addOption(commanBuild.desencadenador);
                commanBuild.options.forEach(options::addOption);
            }
        } catch (ConcurrentModificationException e) {
            e.printStackTrace();
        }
    }

    /**
     * Preprocesa un comando, en caso de cuerpos con espacios en blanco
     *
     * @param args formato []={"-open algo en -otro ewdasd}
     * @return
     */
    private String[] parseCommand(String[] args) {
        List<String> argsList = new ArrayList<>();
        // Temporal
        for (String arg : args) {
            //"-open algo en" -> {"-open","algo","en"}
            String cmd = arg.replaceAll(" .*", "");
            String body = arg.replaceAll("-[a-z]* +", "");
            argsList.add(cmd);
            argsList.add(body);
        }
        String[] argproc = new String[argsList.size()];
        argsList.toArray(argproc);
        return argproc;
    }

}
