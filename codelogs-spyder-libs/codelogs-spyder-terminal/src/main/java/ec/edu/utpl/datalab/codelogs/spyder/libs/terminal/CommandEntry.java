package ec.edu.utpl.datalab.codelogs.spyder.libs.terminal;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Represeta un comando
 * * Created by rfcardenas
 */
public class CommandEntry {

    @JsonProperty("instruction")
    private  String instruction;

    @JsonProperty("value")
    private  String argument;

    public CommandEntry() {
    }

    public CommandEntry(String instruction, String argument) {
        this.instruction = instruction;
        this.argument = argument;
    }

    public String getInstruction() {
        return instruction;
    }

    public void setInstruction(String instruction) {
        this.instruction = instruction;
    }

    public String getArgument() {
        return argument;
    }

    public void setArgument(String argument) {
        this.argument = argument;
    }

    @Override
    public String toString() {
        return "Command{" +
            "instruction='" + instruction + '\'' +
            ", argument='" + argument + '\'' +
            '}';
    }
}
