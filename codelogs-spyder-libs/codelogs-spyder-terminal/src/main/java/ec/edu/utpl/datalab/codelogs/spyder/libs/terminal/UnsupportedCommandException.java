package ec.edu.utpl.datalab.codelogs.spyder.libs.terminal;

/**
 * * Created by rfcardenas
 */
public class UnsupportedCommandException extends UnsupportedOperationException {
    public UnsupportedCommandException() {
        super();
    }

    public UnsupportedCommandException(String message) {
        super(message);
    }

    public UnsupportedCommandException(String message, Throwable cause) {
        super(message, cause);
    }

    public UnsupportedCommandException(Throwable cause) {
        super(cause);
    }
}
