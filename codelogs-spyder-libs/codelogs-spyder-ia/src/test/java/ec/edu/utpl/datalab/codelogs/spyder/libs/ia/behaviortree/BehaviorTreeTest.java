package ec.edu.utpl.datalab.codelogs.spyder.libs.ia.behaviortree;

import ec.edu.utpl.datalab.codelogs.spyder.libs.ia.behaviortree.branch.Sequence;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * * Created by rfcardenas
 */
public class BehaviorTreeTest {
    @Test
    public void step() throws Exception {
        ArrayList<Integer> testList = new ArrayList<>(Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9));
        BehaviorTree<ArrayList> doubleBehaviorTree = new BehaviorTree<>();
        Sequence<ArrayList> doubleSequence = new Sequence<>();
        doubleSequence.addChild(new TaskAdd10Elements());
        doubleSequence.addChild(new TaskRemoveLast3());

        System.out.println("Operaciones en sequencia a ejecutar " + doubleSequence.getChildCount());

        doubleBehaviorTree.setObject(testList);
        doubleBehaviorTree.addChild(doubleSequence);
        System.out.println("Antes de la sequencia " + testList.size());
        doubleBehaviorTree.step();
        System.out.println("Despues de la sequencia " + testList.size());

        Assert.assertEquals(16, testList.size());
    }
}
