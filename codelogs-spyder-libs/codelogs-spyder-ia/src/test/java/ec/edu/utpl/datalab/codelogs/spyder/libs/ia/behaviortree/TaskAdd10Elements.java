package ec.edu.utpl.datalab.codelogs.spyder.libs.ia.behaviortree;

import java.util.ArrayList;

/**
 * * Created by rfcardenas
 */
public class TaskAdd10Elements extends LeaftTask2<ArrayList> {
    @Override
    public void run() {
        for (int i = 0; i < 10; i++) {
            getObject().add(i * 10);
        }
        success();
    }
}
