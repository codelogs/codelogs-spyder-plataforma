package ec.edu.utpl.datalab.codelogs.spyder.libs.ia.behaviortree;

import java.util.ArrayList;

/**
 * * Created by rfcardenas
 */
public class TaskRemoveLast3 extends LeaftTask2<ArrayList> {
    @Override
    public void run() {
        getObject().remove(getObject().size() - 1);
        getObject().remove(getObject().size() - 1);
        getObject().remove(getObject().size() - 1);
        success();
    }
}
