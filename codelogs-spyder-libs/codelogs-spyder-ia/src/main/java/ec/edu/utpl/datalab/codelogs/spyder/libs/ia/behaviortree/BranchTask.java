/*
 * The MIT License (MIT)
 * =========================================================================================================
 *
 * Copyright (c) 10/10/16 12:30 Modulo  Proyecto rexcode-iastructures  Autor [rfcardenas]
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY
 * , WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ec.edu.utpl.datalab.codelogs.spyder.libs.ia.behaviortree;


import java.util.ArrayList;
import java.util.List;


public abstract class BranchTask<E> extends Task<E> {


    protected List<Task<E>> children;


    public BranchTask() {
        this(new ArrayList<Task<E>>());
    }


    public BranchTask(List<Task<E>> tasks) {
        this.children = tasks;
    }

    @Override
    protected int addChildToTask(Task<E> child) {
        children.add(child);
        return children.size() - 1;
    }

    @Override
    public int getChildCount() {
        return children.size();
    }

    @Override
    public Task<E> getChild(int i) {
        return children.get(i);
    }

    @Override
    protected Task<E> copyTo(Task<E> task) {
        BranchTask<E> branch = (BranchTask<E>) task;
        if (children != null) {
            for (int i = 0; i < children.size(); i++) {
                branch.children.add(children.get(i).cloneTask());
            }
        }
        return task;
    }
}
