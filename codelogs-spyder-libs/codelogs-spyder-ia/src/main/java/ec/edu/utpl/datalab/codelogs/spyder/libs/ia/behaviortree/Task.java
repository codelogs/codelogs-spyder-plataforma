/*
 * The MIT License (MIT)
 * =========================================================================================================
 *
 * Copyright (c) 10/10/16 12:30 Modulo  Proyecto rexcode-iastructures  Autor [rfcardenas]
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY
 * , WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ec.edu.utpl.datalab.codelogs.spyder.libs.ia.behaviortree;


import rx.subjects.PublishSubject;

import java.util.Optional;

public abstract class Task<E> {
    public static TaskCloner TASK_CLONER = null;
    protected Status status = Status.FRESH;
    protected Task<E> control;
    protected BehaviorTree<E> tree;
    protected Task<E> guard;
    private Optional<PublishSubject<Status>> publishSubject = Optional.empty();

    /**
     * Añade una tarea a la lista
     *
     * @param child
     * @return
     */
    public final int addChild(Task<E> child) {
        int index = addChildToTask(child);
        if (tree != null && tree.listeners != null) tree.notifyChildAdded(this, index);
        return index;
    }

    /**
     * @param child
     * @return
     */
    protected abstract int addChildToTask(Task<E> child);

    /**
     * @return
     */
    public abstract int getChildCount();

    public abstract Task<E> getChild(int i);

    /**
     * Retorna el tablero del arbol
     *
     * @return
     */
    public E getObject() {
        if (tree == null) throw new IllegalStateException("Esta tarea no se ejecutara el arbol es nulo");
        return tree.getObject();
    }

    public Task<E> getGuard() {
        return guard;
    }

    public void setGuard(Task<E> guard) {
        this.guard = guard;
    }

    public final Status getStatus() {
        return status;
    }

    public final void setControl(Task<E> control) {
        this.control = control;
        this.tree = control.tree;
    }

    /**
     * check guardian
     *
     * @param control
     * @return
     */
    public boolean checkGuard(Task<E> control) {

        if (guard == null) return true;

        if (!guard.checkGuard(control)) return false;

        guard.setControl(control.tree.guardEvaluator);
        guard.start();
        guard.run();
        switch (guard.getStatus()) {
            case SUCCEEDED:
                return true;
            case FAILED:
                return false;
            default:
                throw new IllegalStateException("Illegal guard status '" + guard.getStatus() + "'. Guards must either succeed or fail in one step.");
        }
    }

    public void start() {
    }

    public void end() {
    }

    public abstract void run();

    public final void running() {
        Status previousStatus = status;
        status = Status.RUNNING;
        if (tree.listeners != null && tree.listeners.size() > 0) tree.notifyStatusUpdated(this, previousStatus);
        if (control != null) control.childRunning(this, this);
    }

    public final void success() {
        Status previousStatus = status;
        status = Status.SUCCEEDED;
        publishSubject.ifPresent(sob -> sob.onNext(status));
        if (tree.listeners != null && tree.listeners.size() > 0) tree.notifyStatusUpdated(this, previousStatus);
        end();
        if (control != null) control.childSuccess(this);

    }

    public final void fail() {
        Status previousStatus = status;
        status = Status.FAILED;
        publishSubject.ifPresent(sob -> sob.onNext(status));
        if (tree.listeners != null && tree.listeners.size() > 0) tree.notifyStatusUpdated(this, previousStatus);
        end();
        if (control != null) control.childFail(this);

    }

    public abstract void childSuccess(Task<E> task);

    public abstract void childFail(Task<E> task);

    public abstract void childRunning(Task<E> runningTask, Task<E> reporter);

    public final void cancel() {
        cancelRunningChildren(0);
        Status previousStatus = status;
        status = Status.CANCELLED;
        if (tree.listeners != null && tree.listeners.size() > 0) tree.notifyStatusUpdated(this, previousStatus);
        end();
    }

    protected void cancelRunningChildren(int startIndex) {
        for (int i = startIndex, n = getChildCount(); i < n; i++) {
            Task<E> child = getChild(i);
            if (child.status == Status.RUNNING) child.cancel();
        }
    }

    public void reset() {

        if (status == Status.RUNNING) cancel();
        for (int i = 0, n = getChildCount(); i < n; i++) {
            getChild(i).reset();
        }
        status = Status.FRESH;
        tree = null;
        control = null;
    }

    @SuppressWarnings("unchecked")
    public Task<E> cloneTask() {
        if (TASK_CLONER != null) {
            try {
                return TASK_CLONER.cloneTask(this);
            } catch (Throwable t) {
                throw new TaskCloneException(t);
            }
        }

        return null;
    }

    public PublishSubject<Status> getObservable() {
        if (!publishSubject.isPresent()) {
            publishSubject = Optional.of(PublishSubject.create());
        }
        return publishSubject.get();
    }

    protected abstract Task<E> copyTo(Task<E> task);

    /**
     * Resultado de la tarea
     */
    public enum Status {

        FRESH,

        RUNNING,

        FAILED,

        SUCCEEDED,

        CANCELLED;
    }

}
