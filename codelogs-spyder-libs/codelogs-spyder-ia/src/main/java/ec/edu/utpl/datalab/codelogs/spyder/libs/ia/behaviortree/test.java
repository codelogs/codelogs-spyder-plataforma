/*
 * The MIT License (MIT)
 * =========================================================================================================
 *
 * Copyright (c) 10/10/16 12:30 Modulo  Proyecto rexcode-iastructures  Autor [rfcardenas]
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY
 * , WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ec.edu.utpl.datalab.codelogs.spyder.libs.ia.behaviortree;


import ec.edu.utpl.datalab.codelogs.spyder.libs.ia.behaviortree.branch.Selector;
import ec.edu.utpl.datalab.codelogs.spyder.libs.ia.behaviortree.branch.Sequence;
import ec.edu.utpl.datalab.codelogs.spyder.libs.ia.behaviortree.decorator.Include;
import ec.edu.utpl.datalab.codelogs.spyder.libs.ia.behaviortree.decorator.Include2;


public class test {
    public static void main(String[] args) {
        BehaviorTree<Integer> behaviorTree1 = new BehaviorTree<>();
        BehaviorTree<Integer> behaviorTree2 = new BehaviorTree<>();
        behaviorTree2.addChild(new Sequence<>(new C()));
        behaviorTree1.setObject(212);


        B b = new B();

        Sequence<Integer> sq = new Sequence<>();
        sq.addChild(new A());
        sq.addChild(new B());
        sq.addChild(new Include2<>(behaviorTree2));
        sq.addChild(new Selector<>(new C()));


        behaviorTree1.addChild(sq);
        behaviorTree1.step();

    }
}

class S extends Selector<Integer> {

}

class A extends Task<Integer> {

    @Override
    protected int addChildToTask(Task<Integer> child) {
        return 0;
    }

    @Override
    public int getChildCount() {
        return 0;
    }

    @Override
    public Task<Integer> getChild(int i) {
        return null;
    }

    @Override
    public void run() {
        System.out.println("RUN A");
        success();
    }

    @Override
    public void childSuccess(Task<Integer> task) {
        System.out.println("EXISTO");
    }

    @Override
    public void childFail(Task<Integer> task) {

    }

    @Override
    public void childRunning(Task<Integer> runningTask, Task<Integer> reporter) {

    }

    @Override
    protected Task<Integer> copyTo(Task<Integer> task) {
        return null;
    }
}

class B extends Task<Integer> {

    @Override
    protected int addChildToTask(Task<Integer> child) {
        return 0;
    }

    @Override
    public int getChildCount() {
        return 0;
    }

    @Override
    public Task<Integer> getChild(int i) {
        return null;
    }

    @Override
    public void run() {
        System.out.println("RUN B");
        success();
    }

    @Override
    public void childSuccess(Task<Integer> task) {
        System.out.println("Exito");
    }

    @Override
    public void childFail(Task<Integer> task) {
        System.out.println("Fallo " + task);
    }

    @Override
    public void childRunning(Task<Integer> runningTask, Task<Integer> reporter) {
        System.out.println("RUN");
    }

    @Override
    protected Task<Integer> copyTo(Task<Integer> task) {
        return null;
    }
}

class C extends Task<Integer> {
    @Override
    protected int addChildToTask(Task<Integer> child) {
        return 0;
    }

    @Override
    public int getChildCount() {
        return 0;
    }

    @Override
    public Task<Integer> getChild(int i) {
        return null;
    }

    @Override
    public void start() {
        super.start();
        System.out.println("start C");
    }

    @Override
    public void run() {
        if (getObject().equals(12)) {
            success();
        } else {
            fail();
            System.out.println("RUN C");

        }
        success();
    }

    @Override
    public void childSuccess(Task<Integer> task) {
        System.out.println("Existo");
    }

    @Override
    public void childFail(Task<Integer> task) {
        System.out.println("Fallo");
    }

    @Override
    public void childRunning(Task<Integer> runningTask, Task<Integer> reporter) {

    }

    @Override
    protected Task<Integer> copyTo(Task<Integer> task) {
        return null;
    }
}

class dec extends Include<Integer> {

}
