/*
 * The MIT License (MIT)
 * =========================================================================================================
 *
 * Copyright (c) 10/10/16 12:30 Modulo  Proyecto rexcode-iastructures  Autor [rfcardenas]
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY
 * , WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ec.edu.utpl.datalab.codelogs.spyder.libs.ia.behaviortree.branch;


import ec.edu.utpl.datalab.codelogs.spyder.libs.ia.behaviortree.SingleRunningChildBranch;
import ec.edu.utpl.datalab.codelogs.spyder.libs.ia.behaviortree.Task;

import java.util.Arrays;
import java.util.List;


public class Sequence<E> extends SingleRunningChildBranch<E> {


    public Sequence() {
        super();
    }


    public Sequence(List<Task<E>> tasks) {
        super(tasks);
    }


    public Sequence(Task<E>... tasks) {
        super(Arrays.asList(tasks));
    }

    @Override
    public void childSuccess(Task<E> runningTask) {
        super.childSuccess(runningTask);
        if (++currentChildIndex < children.size()) {
            run(); // Run next child
        } else {
            success(); // All children processed, return success status
        }
    }

    @Override
    public void childFail(Task<E> runningTask) {
        super.childFail(runningTask);
        fail(); // Return failure status when a child says it failed
    }

}
