/*
 * The MIT License (MIT)
 * =========================================================================================================
 *
 * Copyright (c) 10/10/16 12:30 Modulo  Proyecto rexcode-iastructures  Autor [rfcardenas]
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY
 * , WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ec.edu.utpl.datalab.codelogs.spyder.libs.ia.behaviortree;


import java.util.ArrayList;
import java.util.List;

/**
 * Arbol de comportamiento
 *
 * @param <E>
 */
public class BehaviorTree<E> extends Task<E> {

    public List<Listener<E>> listeners;
    GuardEvaluator<E> guardEvaluator;
    private Task<E> rootTask;
    private E object;


    public BehaviorTree() {
        this(null, null);
    }


    public BehaviorTree(Task<E> rootTask) {
        this(rootTask, null);
    }


    public BehaviorTree(Task<E> rootTask, E object) {
        this.rootTask = rootTask;
        this.object = object;
        this.tree = this;
        this.guardEvaluator = new GuardEvaluator<E>(this);
    }


    @Override
    public E getObject() {
        return object;
    }


    public void setObject(E object) {
        this.object = object;
    }


    @Override
    protected int addChildToTask(Task<E> child) {
        if (this.rootTask != null)
            throw new IllegalStateException("A behavior tree cannot have more than one root tasks");
        this.rootTask = child;
        return 0;
    }

    @Override
    public int getChildCount() {
        return rootTask == null ? 0 : 1;
    }

    @Override
    public Task<E> getChild(int i) {
        if (i == 0 && rootTask != null) return rootTask;
        throw new IndexOutOfBoundsException("index can't be >= size: " + i + " >= " + getChildCount());
    }

    @Override
    public void childRunning(Task<E> runningTask, Task<E> reporter) {
        running();
    }

    @Override
    public void childFail(Task<E> runningTask) {
        fail();
    }

    @Override
    public void childSuccess(Task<E> runningTask) {
        success();
    }


    public void step() {
        if (rootTask.status == Status.RUNNING) {
            rootTask.run();
        } else {
            rootTask.setControl(this);
            rootTask.start();
            if (rootTask.checkGuard(this))
                rootTask.run();
            else
                rootTask.fail();
        }
    }

    @Override
    public void run() {
    }

    @Override
    public void reset() {
        super.reset();
        tree = this;
    }

    @Override
    protected Task<E> copyTo(Task<E> task) {
        BehaviorTree<E> tree = (BehaviorTree<E>) task;
        tree.rootTask = rootTask.cloneTask();

        return task;
    }

    public void addListener(Listener<E> listener) {
        if (listeners == null) listeners = new ArrayList<>();
        listeners.add(listener);
    }

    public void removeListener(Listener<E> listener) {
        if (listeners != null) listeners.remove(listener);
    }

    public void removeListeners() {
        if (listeners != null) listeners.clear();
    }


    public void notifyStatusUpdated(Task<E> task, Status previousStatus) {
        for (Listener<E> listener : listeners) {
            listener.statusUpdated(task, previousStatus);
        }
    }

    public void notifyChildAdded(Task<E> task, int index) {
        for (Listener<E> listener : listeners) {
            listener.childAdded(task, index);
        }
    }

    public Task<E> getRootTask() {
        return rootTask;
    }

    public interface Listener<E> {


        public void statusUpdated(Task<E> task, Status previousStatus);


        public void childAdded(Task<E> task, int index);
    }

    private static final class GuardEvaluator<E> extends Task<E> {

        // No argument constructor useful for Kryo serialization
        @SuppressWarnings("unused")
        public GuardEvaluator() {
        }

        public GuardEvaluator(BehaviorTree<E> tree) {
            this.tree = tree;
        }

        @Override
        protected int addChildToTask(Task<E> child) {
            return 0;
        }

        @Override
        public int getChildCount() {
            return 0;
        }

        @Override
        public Task<E> getChild(int i) {
            return null;
        }

        @Override
        public void run() {
        }

        @Override
        public void childSuccess(Task<E> task) {
        }

        @Override
        public void childFail(Task<E> task) {
        }

        @Override
        public void childRunning(Task<E> runningTask, Task<E> reporter) {
        }

        @Override
        protected Task<E> copyTo(Task<E> task) {
            return null;
        }

    }
}
