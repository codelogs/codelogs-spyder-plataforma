/*
 * The MIT License (MIT)
 * =========================================================================================================
 *
 * Copyright (c) 10/10/16 12:30 Modulo  Proyecto rexcode-iastructures  Autor [rfcardenas]
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY
 * , WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ec.edu.utpl.datalab.codelogs.spyder.libs.ia.behaviortree.decorator;


import ec.edu.utpl.datalab.codelogs.spyder.libs.ia.behaviortree.Decorator;
import ec.edu.utpl.datalab.codelogs.spyder.libs.ia.behaviortree.Task;
import ec.edu.utpl.datalab.codelogs.spyder.libs.ia.behaviortree.TaskCloneException;


public class Include<E> extends Decorator<E> {


    public String subtree;


    public boolean lazy;


    public Include() {
    }


    public Include(String subtree) {
        this.subtree = subtree;
    }


    public Include(String subtree, boolean lazy) {
        this.subtree = subtree;
        this.lazy = lazy;
    }


    @Override
    public void start() {
        if (!lazy)
            throw new UnsupportedOperationException("A non-lazy " + Include.class.getSimpleName() + " isn't meant to be run!");

        if (child == null) {
            // Lazy include is grafted at run-time
            addChild(createSubtreeRootTask());
        }
    }


    @Override
    public Task<E> cloneTask() {
        if (lazy) return super.cloneTask();

        // Non lazy include is grafted at clone-time
        return createSubtreeRootTask();
    }


    @Override
    protected Task<E> copyTo(Task<E> task) {
        if (!lazy)
            throw new TaskCloneException("A non-lazy " + getClass().getSimpleName() + " should never be copied.");

        Include<E> include = (Include<E>) task;
        include.subtree = subtree;
        include.lazy = lazy;

        return task;
    }

    private Task<E> createSubtreeRootTask() {
        //return BehaviorTreeLibraryManager.getInstance().createRootTask(subtree)
        return null;
    }
}
