/*
 * The MIT License (MIT)
 * =========================================================================================================
 *
 * Copyright (c) 10/10/16 12:30 Modulo  Proyecto rexcode-iastructures  Autor [rfcardenas]
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY
 * , WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ec.edu.utpl.datalab.codelogs.spyder.libs.ia.behaviortree.branch;


import ec.edu.utpl.datalab.codelogs.spyder.libs.ia.behaviortree.BranchTask;
import ec.edu.utpl.datalab.codelogs.spyder.libs.ia.behaviortree.Task;

import java.util.Arrays;
import java.util.List;


public class DynamicGuardSelector<E> extends BranchTask<E> {


    protected Task<E> runningChild;


    public DynamicGuardSelector() {
        super();
    }


    public DynamicGuardSelector(Task<E>... tasks) {
        super(Arrays.asList(tasks));
    }


    public DynamicGuardSelector(List<Task<E>> tasks) {
        super(tasks);
    }

    @Override
    public void childRunning(Task<E> task, Task<E> reporter) {
        runningChild = task;
        running(); // Return a running status when a child says it's running
    }

    @Override
    public void childSuccess(Task<E> task) {
        this.runningChild = null;
        success();
    }

    @Override
    public void childFail(Task<E> task) {
        this.runningChild = null;
        fail();
    }

    @Override
    public void run() {
        // Check guards
        Task<E> childToRun = null;
        for (int i = 0, n = children.size(); i < n; i++) {
            Task<E> child = children.get(i);
            if (child.checkGuard(this)) {
                childToRun = child;
                break;
            }
        }

        if (runningChild != null && runningChild != childToRun) {
            runningChild.cancel();
            runningChild = null;
        }
        if (childToRun == null) {
            fail();
        } else {
            if (runningChild == null) {
                runningChild = childToRun;
                runningChild.setControl(this);
                runningChild.start();
            }
            runningChild.run();
        }
    }

    @Override
    public void reset() {
        super.reset();
        this.runningChild = null;
    }

    @Override
    protected Task<E> copyTo(Task<E> task) {
        DynamicGuardSelector<E> branch = (DynamicGuardSelector<E>) task;
        branch.runningChild = null;

        return super.copyTo(task);
    }

}
