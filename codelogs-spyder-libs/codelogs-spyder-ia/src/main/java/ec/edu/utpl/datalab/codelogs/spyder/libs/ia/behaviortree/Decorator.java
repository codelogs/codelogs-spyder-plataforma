/*
 * The MIT License (MIT)
 * =========================================================================================================
 *
 * Copyright (c) 10/10/16 12:30 Modulo  Proyecto rexcode-iastructures  Autor [rfcardenas]
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY
 * , WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ec.edu.utpl.datalab.codelogs.spyder.libs.ia.behaviortree;


/**
 * Decorador utilizado para cambiar el resultado de una tarea, añadir funcionalidad etc.
 *
 * @param <E>
 */
public abstract class Decorator<E> extends Task<E> {

    /**
     * Tarea
     */
    protected Task<E> child;


    public Decorator() {
    }

    /**
     * Target a decorar
     *
     * @param child
     */
    public Decorator(Task<E> child) {
        this.child = child;
    }

    /**
     * Añade un decorador, si un decorador fue contruido por contructor
     * entonces este metodo no puede ser llamado cuando exista un decorador configurado
     *
     * @param child
     * @return
     */
    @Override
    protected int addChildToTask(Task<E> child) {
        if (this.child != null) throw new IllegalStateException("Decorador solo puede tener un hijo");
        this.child = child;
        return 0;
    }

    /**
     * retorna 0 o 1
     *
     * @return
     */
    @Override
    public int getChildCount() {
        return child == null ? 0 : 1;
    }

    /**
     * Retorna el hijo
     *
     * @param i
     * @return
     */
    @Override
    public Task<E> getChild(int i) {
        if (i == 0 && child != null) return child;
        throw new IndexOutOfBoundsException("index no permitido >= size: " + i + " >= " + getChildCount());
    }

    /**
     * Ejecuta
     */
    @Override
    public void run() {
        if (child.status == Status.RUNNING) {
            child.run();
        } else {
            child.setControl(this);
            child.start();
            if (child.checkGuard(this)) {
                child.run();
            } else {
                child.fail();
            }
        }
    }

    /**
     * Ejecucion de la tarea inner (decorada)
     *
     * @param runningTask
     * @param reporter
     */
    @Override
    public void childRunning(Task<E> runningTask, Task<E> reporter) {
        running();
    }

    /**
     * Notificar el fallo de la tarea hijo
     *
     * @param runningTask
     */
    @Override
    public void childFail(Task<E> runningTask) {
        fail();
    }

    /**
     * Notifica el existo de la tarea hijo
     *
     * @param runningTask
     */
    @Override
    public void childSuccess(Task<E> runningTask) {
        success();
    }

    @Override
    protected Task<E> copyTo(Task<E> task) {
        if (this.child != null) {
            Decorator<E> decorator = (Decorator<E>) task;
            decorator.child = this.child.cloneTask();
        }

        return task;
    }

}
