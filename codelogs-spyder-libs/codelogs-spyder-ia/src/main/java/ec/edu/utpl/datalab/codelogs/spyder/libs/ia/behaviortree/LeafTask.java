/*
 * The MIT License (MIT)
 * =========================================================================================================
 *
 * Copyright (c) 10/10/16 12:30 Modulo  Proyecto rexcode-iastructures  Autor [rfcardenas]
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY
 * , WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ec.edu.utpl.datalab.codelogs.spyder.libs.ia.behaviortree;


public abstract class LeafTask<E> extends Task<E> {


    public LeafTask() {
    }


    public abstract Status execute();


    @Override
    public final void run() {
        Status result = execute();
        if (result == null) throw new IllegalStateException("Invalid status 'null' returned by the execute method");
        switch (result) {
            case SUCCEEDED:
                success();
                return;
            case FAILED:
                fail();
                return;
            case RUNNING:
                running();
                return;
            default:
                throw new IllegalStateException("Invalid status '" + result.name() + "' returned by the execute method");
        }
    }


    @Override
    protected int addChildToTask(Task<E> child) {
        throw new IllegalStateException("A leaf tasks cannot have any children");
    }

    @Override
    public int getChildCount() {
        return 0;
    }

    @Override
    public Task<E> getChild(int i) {
        throw new IndexOutOfBoundsException("A leaf tasks can not have any child");
    }

    @Override
    public final void childRunning(Task<E> runningTask, Task<E> reporter) {
    }

    @Override
    public final void childFail(Task<E> runningTask) {
    }

    @Override
    public final void childSuccess(Task<E> runningTask) {
    }

}
