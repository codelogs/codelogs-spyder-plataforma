/*
 * The MIT License (MIT)
 * =========================================================================================================
 *
 * Copyright (c) 10/10/16 12:30 Modulo  Proyecto rexcode-iastructures  Autor [rfcardenas]
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY
 * , WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ec.edu.utpl.datalab.codelogs.spyder.libs.ia.behaviortree;


import java.util.List;


public abstract class SingleRunningChildBranch<E> extends BranchTask<E> {


    protected Task<E> runningChild;


    protected int currentChildIndex;


    protected Task<E>[] randomChildren;


    public SingleRunningChildBranch() {
        super();
    }


    public SingleRunningChildBranch(List<Task<E>> tasks) {
        super(tasks);
    }

    @Override
    public void childRunning(Task<E> task, Task<E> reporter) {
        runningChild = task;
        running(); // Return a running status when a child says it's running
    }

    @Override
    public void childSuccess(Task<E> task) {
        this.runningChild = null;
    }

    @Override
    public void childFail(Task<E> task) {
        this.runningChild = null;
    }

    @Override
    public void run() {
        if (runningChild != null) {
            runningChild.run();
        } else {
            if (currentChildIndex < children.size()) {
                if (randomChildren != null) {
                    int last = children.size() - 1;
                    if (currentChildIndex < last) {
                        // Random swap
                        int otherChildIndex = (int) (Math.random() * last);
                        Task<E> tmp = randomChildren[currentChildIndex];
                        randomChildren[currentChildIndex] = randomChildren[otherChildIndex];
                        randomChildren[otherChildIndex] = tmp;
                    }
                    runningChild = randomChildren[currentChildIndex];
                } else {
                    runningChild = children.get(currentChildIndex);
                }
                runningChild.setControl(this);
                runningChild.start();
                if (!runningChild.checkGuard(this))
                    runningChild.fail();
                else
                    run();
            } else {
                // Should never happen; this case must be handled by subclasses in childXXX methods
            }
        }
    }

    @Override
    public void start() {
        this.currentChildIndex = 0;
        runningChild = null;
    }

    @Override
    protected void cancelRunningChildren(int startIndex) {
        super.cancelRunningChildren(startIndex);
        runningChild = null;
    }

    @Override
    public void reset() {
        super.reset();
        this.currentChildIndex = 0;
        this.runningChild = null;
        this.randomChildren = null;
    }

    @Override
    protected Task<E> copyTo(Task<E> task) {
        SingleRunningChildBranch<E> branch = (SingleRunningChildBranch<E>) task;
        branch.randomChildren = null;

        return super.copyTo(task);
    }

    @SuppressWarnings("unchecked")
    protected Task<E>[] createRandomChildren() {
        Task<E>[] rndChildren = new Task[children.size()];
        System.arraycopy(children, 0, rndChildren, 0, children.size());
        return rndChildren;
    }

}
