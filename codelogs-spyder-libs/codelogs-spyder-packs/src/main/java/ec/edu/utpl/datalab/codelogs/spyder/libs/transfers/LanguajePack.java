package ec.edu.utpl.datalab.codelogs.spyder.libs.transfers;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Objects;

/**
 * A Languaje.
 */
public class LanguajePack implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long resource_id;
    private String uuid;
    private String nombre;
    private String extension;
    private ZonedDateTime createDate;

    public Long getResource_id() {
        return resource_id;
    }

    public void setResource_id(Long resource_id) {
        this.resource_id = resource_id;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    public ZonedDateTime getCreateDate() {
        return createDate;
    }

    public void setCreateDate(ZonedDateTime createDate) {
        this.createDate = createDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        LanguajePack languaje = (LanguajePack) o;
        if(languaje.resource_id == null || resource_id == null) {
            return false;
        }
        return Objects.equals(resource_id, languaje.resource_id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(resource_id);
    }

    @Override
    public String toString() {
        return "Languaje{" +
            "resource_id=" + resource_id +
            ", uuid='" + uuid + "'" +
            ", nombre='" + nombre + "'" +
            ", extension='" + extension + "'" +
            ", createDate='" + createDate + "'" +
            '}';
    }
}
