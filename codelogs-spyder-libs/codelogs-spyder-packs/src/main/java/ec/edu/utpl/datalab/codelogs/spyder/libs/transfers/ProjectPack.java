package ec.edu.utpl.datalab.codelogs.spyder.libs.transfers;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Objects;

/**
 * A Project.
 */
public class ProjectPack implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long resource_id;
    private String uuid;
    private String name;
    private String path;
    private ZonedDateTime createDate;
    private ZonedDateTime lastAccess;
    private String git;

    public Long getResource_id() {
        return resource_id;
    }

    public void setResource_id(Long resource_id) {
        this.resource_id = resource_id;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public ZonedDateTime getCreateDate() {
        return createDate;
    }

    public void setCreateDate(ZonedDateTime createDate) {
        this.createDate = createDate;
    }

    public ZonedDateTime getLastAccess() {
        return lastAccess;
    }

    public void setLastAccess(ZonedDateTime lastAccess) {
        this.lastAccess = lastAccess;
    }

    public String getGit() {
        return git;
    }

    public void setGit(String git) {
        this.git = git;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ProjectPack project = (ProjectPack) o;
        if(project.resource_id == null || resource_id == null) {
            return false;
        }
        return Objects.equals(resource_id, project.resource_id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(resource_id);
    }

    @Override
    public String toString() {
        return "Project{" +
            "resource_id=" + resource_id +
            ", uuid='" + uuid + "'" +
            ", name='" + name + "'" +
            ", path='" + path + "'" +
            ", createDate='" + createDate + "'" +
            ", lastAccess='" + lastAccess + "'" +
            ", git='" + git + "'" +
            '}';
    }
}
