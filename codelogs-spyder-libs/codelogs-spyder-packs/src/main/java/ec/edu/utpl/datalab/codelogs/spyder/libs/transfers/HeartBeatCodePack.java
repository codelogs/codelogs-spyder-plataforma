package ec.edu.utpl.datalab.codelogs.spyder.libs.transfers;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import ec.edu.utpl.datalab.codelogs.spyder.libs.serial.TypeInstructionDeserialzer;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Objects;

/**
 * A PulseCode.
 */
public class HeartBeatCodePack implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long resource_id;

    private String uuid;

    private ZonedDateTime captureDate;

    private String pathFile;

    private Long linesAdd;

    private Long linesDel;

    private Long levenshtein;

    private Long timeFixed;

    private String blockMap;

    private String brachname;

    private String lang;

    @JsonDeserialize(using = TypeInstructionDeserialzer.class)
    private TYPE_INSTRUCTION_PACK instruction;

    private FileCodeGrantedContextPack fileCode;

    private GrantedContextPack workSession;

    public Long getResource_id() {
        return resource_id;
    }

    public void setResource_id(Long resource_id) {
        this.resource_id = resource_id;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public ZonedDateTime getCaptureDate() {
        return captureDate;
    }

    public void setCaptureDate(ZonedDateTime captureDate) {
        this.captureDate = captureDate;
    }

    public String getPathFile() {
        return pathFile;
    }

    public void setPathFile(String pathFile) {
        this.pathFile = pathFile;
    }

    public Long getLinesAdd() {
        return linesAdd;
    }

    public void setLinesAdd(Long linesAdd) {
        this.linesAdd = linesAdd;
    }

    public Long getLinesDel() {
        return linesDel;
    }

    public void setLinesDel(Long linesDel) {
        this.linesDel = linesDel;
    }

    public Long getLevenshtein() {
        return levenshtein;
    }

    public void setLevenshtein(Long levenshtein) {
        this.levenshtein = levenshtein;
    }

    public Long getTimeFixed() {
        return timeFixed;
    }

    public void setTimeFixed(Long timeFixed) {
        this.timeFixed = timeFixed;
    }

    public String getBlockMap() {
        return blockMap;
    }

    public void setBlockMap(String blockMap) {
        this.blockMap = blockMap;
    }

    public String getBrachname() {
        return brachname;
    }

    public void setBrachname(String brachname) {
        this.brachname = brachname;
    }

    public TYPE_INSTRUCTION_PACK getInstruction() {
        return instruction;
    }

    public void setInstruction(TYPE_INSTRUCTION_PACK instruction) {
        this.instruction = instruction;
    }

    public FileCodeGrantedContextPack getFileCode() {
        return fileCode;
    }

    public void setFileCode(FileCodeGrantedContextPack fileCode) {
        this.fileCode = fileCode;
    }

    public GrantedContextPack getWorkSession() {
        return workSession;
    }

    public void setWorkSession(GrantedContextPack workSession) {
        this.workSession = workSession;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        HeartBeatCodePack heartBeatCode = (HeartBeatCodePack) o;
        if(heartBeatCode.resource_id == null || resource_id == null) {
            return false;
        }
        return Objects.equals(resource_id, heartBeatCode.resource_id);
    }

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(resource_id);
    }

    @Override
    public String toString() {
        return "HeartBeatCodePack{" +
            "resource_id=" + resource_id +
            ", uuid='" + uuid + '\'' +
            ", captureDate=" + captureDate +
            ", pathFile='" + pathFile + '\'' +
            ", linesAdd=" + linesAdd +
            ", linesDel=" + linesDel +
            ", levenshtein=" + levenshtein +
            ", timeFixed=" + timeFixed +
            ", blockMap='" + blockMap + '\'' +
            ", brachname='" + brachname + '\'' +
            ", lang='" + lang + '\'' +
            ", instruction=" + instruction +
            ", fileCode=" + fileCode +
            ", workSession=" + workSession +
            '}';
    }
}
