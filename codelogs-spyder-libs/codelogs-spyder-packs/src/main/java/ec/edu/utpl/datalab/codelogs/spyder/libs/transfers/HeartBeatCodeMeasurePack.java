package ec.edu.utpl.datalab.codelogs.spyder.libs.transfers;


import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import ec.edu.utpl.datalab.codelogs.spyder.libs.serial.TypeInstructionDeserialzer;

import java.util.Objects;

/**
 * * Created by rfcardenas
 */
public class HeartBeatCodeMeasurePack {
    private static final long serialVersionUID = 1L;

    private Long resource_id;
    private String uuid;
    private Double value;
    @JsonDeserialize(using = TypeInstructionDeserialzer.class)
    private TYPE_INSTRUCTION_PACK instruction;
    private GrantedContextPack metric;

    public Long getResource_id() {
        return resource_id;
    }

    public void setResource_id(Long resource_id) {
        this.resource_id = resource_id;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }

    public TYPE_INSTRUCTION_PACK getInstruction() {
        return instruction;
    }

    public void setInstruction(TYPE_INSTRUCTION_PACK instruction) {
        this.instruction = instruction;
    }

    public GrantedContextPack getMetric() {
        return metric;
    }

    public void setMetric(GrantedContextPack metric) {
        this.metric = metric;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        HeartBeatCodeMeasurePack beat = (HeartBeatCodeMeasurePack) o;
        if(beat.resource_id == null || resource_id == null) {
            return false;
        }
        return Objects.equals(resource_id, beat.resource_id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(resource_id);
    }

    @Override
    public String toString() {
        return "HeartBeatCodeMeasureDto{" +
            "resource_id=" + resource_id +
            ", uuid='" + uuid + '\'' +
            ", value=" + value +
            ", instruction=" + instruction +
            ", metric=" + metric +
            '}';
    }
}
