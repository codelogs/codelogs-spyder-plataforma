package ec.edu.utpl.datalab.codelogs.spyder.libs.transfers;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import ec.edu.utpl.datalab.codelogs.spyder.libs.serial.TypeInstructionDeserialzer;

import java.time.ZonedDateTime;
import java.util.EnumSet;
import java.util.Objects;

/**
 * * Created by rfcardenas
 */
public class HeartBeatBuildRunPack {
    private static final EnumSet enumSet = EnumSet.of(
        TYPE_INSTRUCTION_PACK.HEART_BEAT_RUN_FAILED,
        TYPE_INSTRUCTION_PACK.HEART_BEAT_RUN_SUCCESS,
        TYPE_INSTRUCTION_PACK.HEART_BEAT_COMPILE_SUCCESS,
        TYPE_INSTRUCTION_PACK.HEART_BEAT_COMPILE_FAILED
    );
    private static final long serialVersionUID = 1L;

    private Long resource_id;
    private String uuid;
    private boolean success;
    private String messageL1 = "";
    private String messageL2 = "";
    private String messageL3 = "";
    private long time;
    private ZonedDateTime capturate_at;
    @JsonDeserialize(using = TypeInstructionDeserialzer.class)
    private TYPE_INSTRUCTION_PACK instruction;
    private String pathContext;
    private GrantedContextPack workSession;

    public Long getResource_id() {
        return resource_id;
    }

    public void setResource_id(Long resource_id) {
        this.resource_id = resource_id;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessageL1() {
        return messageL1;
    }

    public void setMessageL1(String messageL1) {
        this.messageL1 = messageL1;
    }

    public String getMessageL2() {
        return messageL2;
    }

    public void setMessageL2(String messageL2) {
        this.messageL2 = messageL2;
    }

    public String getMessageL3() {
        return messageL3;
    }

    public void setMessageL3(String messageL3) {
        this.messageL3 = messageL3;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public ZonedDateTime getCapturate_at() {
        return capturate_at;
    }

    public void setCapturate_at(ZonedDateTime capturate_at) {
        this.capturate_at = capturate_at;
    }

    public TYPE_INSTRUCTION_PACK getInstruction() {
        return instruction;
    }

    public void setInstruction(TYPE_INSTRUCTION_PACK instruction) {
        this.instruction = instruction;
    }

    public GrantedContextPack getWorkSession() {
        return workSession;
    }

    public void setWorkSession(GrantedContextPack workSession) {
        this.workSession = workSession;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        HeartBeatBuildRunPack heartBeatRun = (HeartBeatBuildRunPack) o;
        if(heartBeatRun.resource_id == null || resource_id == null) {
            return false;
        }
        return Objects.equals(resource_id, heartBeatRun.resource_id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(resource_id);
    }


    public String getPathContext() {
        return pathContext;
    }

    public void setPathContext(String pathContext) {
        this.pathContext = pathContext;
    }

    @Override
    public String toString() {
        return "HeartBeatBuildRunPack{" +
            "resource_id=" + resource_id +
            ", uuid='" + uuid + '\'' +
            ", success=" + success +
            ", messageL1='" + messageL1 + '\'' +
            ", messageL2='" + messageL2 + '\'' +
            ", messageL3='" + messageL3 + '\'' +
            ", time=" + time +
            ", capturate_at=" + capturate_at +
            ", instruction=" + instruction +
            ", pathContext='" + pathContext + '\'' +
            ", workSession=" + workSession +
            '}';
    }
}
