package ec.edu.utpl.datalab.codelogs.spyder.libs.transfers;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * * Created by rfcardenas
 */
@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum TYPE_INSTRUCTION_PACK {
    HEART_BEAT_COMPILE_SUCCESS(1, "public-beat:compile-success"),
    HEART_BEAT_COMPILE_FAILED(2, "public-beat:compile-failed"),
    HEART_BEAT_RUN_SUCCESS(3, "public-beat:run-success"),
    HEART_BEAT_RUN_FAILED(4, "public-beat:run-failed"),
    HEART_BEAT_CODE_ACTIVITY(5, "public-beat:code-activity"),
    HEART_BEAT_CODE_METRIC(6, "public-beat:code-metric"),
    HEART_BEAT_CODE_ANALYSIS(7, "public-beat:code-analysis");
    @JsonProperty("id")
    private int id;
    @JsonProperty("status")
    private String status;

    TYPE_INSTRUCTION_PACK(int id, String status) {
        this.id = id;
        this.status = status;
    }

    public String getStatus() {
        return this.status;
    }

    public String toString() {
        return this.status;
    }

    /**
     * Gets a MyEnumType from id or <tt>null</tt> if the requested type doesn't exist.
     *
     * @param id String
     * @return MyEnumType
     */
    public static TYPE_INSTRUCTION_PACK fromId(final int id) {
        for (TYPE_INSTRUCTION_PACK type : TYPE_INSTRUCTION_PACK.values()) {
            if (type.id == id) {
                return type;
            }
        }
        return null;
    }
}
