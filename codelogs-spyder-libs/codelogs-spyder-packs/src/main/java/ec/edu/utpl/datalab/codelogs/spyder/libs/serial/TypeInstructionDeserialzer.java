package ec.edu.utpl.datalab.codelogs.spyder.libs.serial;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import ec.edu.utpl.datalab.codelogs.spyder.libs.transfers.TYPE_INSTRUCTION_PACK;

import java.io.IOException;

/**
 * * Created by rfcardenas
 */
public class TypeInstructionDeserialzer extends JsonDeserializer
{
    @Override
    public TYPE_INSTRUCTION_PACK deserialize(JsonParser jp, DeserializationContext dc) throws IOException, JsonProcessingException {
        JsonNode node = jp.getCodec().readTree(jp);
        System.out.println(node.size());
        System.out.println(node.toString());
        System.out.println(node);
        TYPE_INSTRUCTION_PACK type = TYPE_INSTRUCTION_PACK.fromId(node.get("id").asInt());
        if (type != null) {
            return type;
        }
        throw new JsonMappingException("invalid value for type, must be 'one' or 'two'");
    }
}
