package ec.edu.utpl.datalab.codelogs.spyder.libs.transfers;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * A ProjectAnalysis.
 */
public class CodeAnalysisPack implements Serializable {

    private static final long serialVersionUID = 1L;
    private Long resource_id;
    private String uuid;
    private ZonedDateTime createDate;
    private ZonedDateTime lastUpdateDate;
    private Long timeExecution;
    private Set<HeartBeatCodeMeasurePack> heartBeatCodeMeasures = new HashSet<>();
    private String pathContext;
    private GrantedContextPack project;

    public Long getId() {
        return resource_id;
    }

    public void setId(Long resource_id) {
        this.resource_id = resource_id;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public ZonedDateTime getCreateDate() {
        return createDate;
    }

    public void setCreateDate(ZonedDateTime createDate) {
        this.createDate = createDate;
    }

    public ZonedDateTime getLastUpdateDate() {
        return lastUpdateDate;
    }

    public void setLastUpdateDate(ZonedDateTime lastUpdateDate) {
        this.lastUpdateDate = lastUpdateDate;
    }

    public Long getTimeExecution() {
        return timeExecution;
    }

    public void setTimeExecution(Long timeExecution) {
        this.timeExecution = timeExecution;
    }

    public Set<HeartBeatCodeMeasurePack> getHeartBeatCodeMeasures() {
        return heartBeatCodeMeasures;
    }

    public void setHeartBeatCodeMeasures(Set<HeartBeatCodeMeasurePack> heartBeatCodeMeasures) {
        this.heartBeatCodeMeasures = heartBeatCodeMeasures;
    }

    public GrantedContextPack getProject() {
        return project;
    }

    public void setProject(GrantedContextPack project) {
        this.project = project;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        CodeAnalysisPack codeAnalysis = (CodeAnalysisPack) o;
        if(codeAnalysis.resource_id == null || resource_id == null) {
            return false;
        }
        return Objects.equals(resource_id, codeAnalysis.resource_id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(resource_id);
    }

    public String getPathContext() {
        return pathContext;
    }

    public void setPathContext(String pathContext) {
        this.pathContext = pathContext;
    }

    @Override
    public String toString() {
        return "CodeAnalysisPack{" +
            "resource_id=" + resource_id +
            ", uuid='" + uuid + '\'' +
            ", createDate=" + createDate +
            ", lastUpdateDate=" + lastUpdateDate +
            ", timeExecution=" + timeExecution +
            ", heartBeatCodeMeasures=" + heartBeatCodeMeasures +
            ", pathContext='" + pathContext + '\'' +
            ", project=" + project +
            '}';
    }
}
