package ec.edu.utpl.datalab.codelogs.spyder.libs.transfers;


/**
 * * Created by rfcardenas
 */
public class FileCodeGrantedContextPack extends GrantedContextPack {
    private String pathFile;

    public String getPathFile() {
        return pathFile;
    }

    public void setPathFile(String pathFile) {
        this.pathFile = pathFile;
    }
}
