# CODELOGS FILESYSTEM

Este modulo proporciona utilidad para :
 - Busquedas en sistema de archivos
 - Escucha de eventos a nivel de sistema de archivos
 - TimeOuts de tiempos de edicion
 - Soporte de ficheros
 
 Todo el código similar o nuevas funciones relacionadas
 con el tratamiento de archivos (Busquedas, Eventos)
 debe ser añadido en este nivel