/*
 * The MIT License (MIT)
 * =========================================================================================================
 *
 * Copyright (c) 10/10/16 12:30 Modulo  Proyecto rexcode-filesystem  Autor [rfcardenas]
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY
 * , WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ec.edu.utpl.datalab.codelogs.spyder.libs.filesystem.code_event2;

import ec.edu.utpl.datalab.codelogs.spyder.libs.filesystem.code_event2.file.EventSysteminherited;
import ec.edu.utpl.datalab.codelogs.spyder.libs.filesystem.code_event2.support.DefaultSupportFileRegistry;
import ec.edu.utpl.datalab.codelogs.spyder.libs.filesystem.code_event2.support.FileSupport;
import ec.edu.utpl.datalab.codelogs.spyder.libs.filesystem.code_event2.support.SupportFileRegistry;
import org.apache.commons.vfs2.FileObject;
import org.apache.commons.vfs2.FileSystemException;
import org.apache.commons.vfs2.VFS;

import java.util.concurrent.Executors;

/**
 * * Created by rfcardenas
 */
public class test {
    public static void main(String[] args) throws FileSystemException {
        FileObject fileObject = VFS.getManager().resolveFile("/home/rfcardenas/Escritorio/behavior-trees-master");

        SupportFileRegistry supportFileRegistry = new DefaultSupportFileRegistry();
        supportFileRegistry.register(new FileSupport("Java", "java"));

        EventSystem eventSystem = EventSystem
            .builder()
            .withSourceEvents(EventSysteminherited
                .builder()
                .addSupport(new FileSupport("java", "java")).build())
            .addSupport(new FileSupport("Java", "java"))
            .build();

        eventSystem.watch(fileObject);
        eventSystem.observable().subscribe(event -> {
            System.out.println("Cualquir notificacion " + event.getEventType());
        });
        Executors.newFixedThreadPool(1)
            .submit(eventSystem);

    }
}
