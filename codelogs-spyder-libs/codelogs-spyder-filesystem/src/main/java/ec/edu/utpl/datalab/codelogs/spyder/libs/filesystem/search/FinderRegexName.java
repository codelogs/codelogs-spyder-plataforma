package ec.edu.utpl.datalab.codelogs.spyder.libs.filesystem.search;

import org.apache.commons.vfs2.FileSelectInfo;

import java.util.Objects;

/**
 * * Created by rfcardenas
 */
public class FinderRegexName extends FinderDecorator {
    private String regex;

    public FinderRegexName(String regex) {
        this.regex = Objects.requireNonNull(regex,"regex no puede ser null");
    }

    @Override
    public boolean includeFile(FileSelectInfo fileInfo) throws Exception {
        return fileInfo.getFile().getName().getBaseName().contains(regex);
    }
}
