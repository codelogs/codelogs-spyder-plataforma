package ec.edu.utpl.datalab.codelogs.spyder.libs.filesystem.code_event2.file;

import ec.edu.utpl.datalab.codelogs.spyder.libs.filesystem.code_event2.packs.Event;
import org.apache.commons.vfs2.FileObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import rx.Observable;
import rx.subjects.PublishSubject;

/**
 * Implementacion de la interface event source, util para reutilizar
 * eventos extornos como por ejemplo las API de los editores existetnes
 * * Created by rfcardenas
 */
public class EventSystemExternal implements EventFileSystem {
    private static final Logger log = LoggerFactory.getLogger(EventSystemExternal.class);
    private PublishSubject<Event> publishSubject = PublishSubject.create();

    /**
     * Evento externo
     *
     * @param event
     */
    public void event(Event event) {
        log.info("Publicando evento externo {}", event.getFileObject().getName());
        publishSubject.onNext(event);
    }

    /**
     * Observable
     *
     * @return
     */
    @Override
    public Observable<Event> observable() {
        return publishSubject;
    }

    @Override
    public void start() {
        // nada que hacer la fuente es externa
    }

    @Override
    public void stop() {
        // nada que hacer la fuente es externa
    }

    @Override
    public void followEvents(FileObject fileObject) {
        // nada que hacer la fuente es externa
    }

    @Override
    public void unfollowEvents(FileObject fileObject) {
        // nada que hacer la fuente es externa
    }
}
