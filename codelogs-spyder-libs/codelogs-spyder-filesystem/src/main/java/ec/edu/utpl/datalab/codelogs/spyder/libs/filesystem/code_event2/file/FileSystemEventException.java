package ec.edu.utpl.datalab.codelogs.spyder.libs.filesystem.code_event2.file;

/**
 * * Created by rfcardenas
 */
public class FileSystemEventException extends RuntimeException {
    public FileSystemEventException() {
        super();
    }

    public FileSystemEventException(String message) {
        super(message);
    }

    public FileSystemEventException(String message, Throwable cause) {
        super(message, cause);
    }

    public FileSystemEventException(Throwable cause) {
        super(cause);
    }

    protected FileSystemEventException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
