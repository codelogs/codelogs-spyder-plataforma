package ec.edu.utpl.datalab.codelogs.spyder.libs.filesystem.code_event2.file;

import org.apache.commons.vfs2.FileObject;

/**
 * * Created by rfcardenas
 */
public interface EventFileSystem extends EventSource {
    void start();

    void stop();

    void followEvents(FileObject fileObject);

    void unfollowEvents(FileObject fileObject);
}
