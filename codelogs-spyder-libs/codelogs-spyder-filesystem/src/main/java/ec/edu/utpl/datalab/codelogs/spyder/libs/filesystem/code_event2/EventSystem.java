/*
 * The MIT License (MIT)
 * =========================================================================================================
 *
 * Copyright (c) 10/10/16 12:30 Modulo  Proyecto rexcode-filesystem  Autor [rfcardenas]
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY
 * , WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ec.edu.utpl.datalab.codelogs.spyder.libs.filesystem.code_event2;

import ec.edu.utpl.datalab.codelogs.spyder.libs.filesystem.code_event2.file.EventFileSystem;
import ec.edu.utpl.datalab.codelogs.spyder.libs.filesystem.code_event2.file.EventSource;
import ec.edu.utpl.datalab.codelogs.spyder.libs.filesystem.code_event2.packs.Event;
import ec.edu.utpl.datalab.codelogs.spyder.libs.filesystem.code_event2.packs.EventType;
import ec.edu.utpl.datalab.codelogs.spyder.libs.filesystem.code_event2.packs.FileEvent;
import ec.edu.utpl.datalab.codelogs.spyder.libs.filesystem.code_event2.packs.TimeEvent;
import ec.edu.utpl.datalab.codelogs.spyder.libs.filesystem.code_event2.support.DefaultSupportFileRegistry;
import ec.edu.utpl.datalab.codelogs.spyder.libs.filesystem.code_event2.support.FileSupport;
import ec.edu.utpl.datalab.codelogs.spyder.libs.filesystem.code_event2.support.SupportFileRegistry;
import ec.edu.utpl.datalab.codelogs.spyder.libs.filesystem.code_event2.time.TimeManager;
import org.apache.commons.vfs2.FileObject;
import org.apache.commons.vfs2.FileSystemException;
import org.apache.commons.vfs2.VFS;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import rx.Observable;
import rx.Observer;
import rx.subjects.PublishSubject;

import java.util.HashSet;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.function.BiConsumer;

/**
 * * Created by rfcardenas
 */
public final class EventSystem implements Runnable, EventObserver, EventSource {
    private static final Logger LOG = LoggerFactory.getLogger(EventSystem.class);
    private final PublishSubject<Event> subject = PublishSubject.create();
    private final Optional<TimeManager> managerOptional;
    private final EventFileSystem eventFileSystem;

    /**
     * Contructor privado, pattern Builder
     *
     * @param buidlerService //Datos para contruir el objeto
     */
    private EventSystem(BuidlerService buidlerService) {
        this.eventFileSystem = Objects.requireNonNull(buidlerService.eventSource, "Se requiere de una fuente de eventos");
        this.managerOptional = buidlerService.timeManager;
        internalConfig();
    }

    public static BuidlerService builder() {
        return new BuidlerService();
    }

    private void internalConfig() {
        /**
         * El gestor de tiempo esta subscrito a los paquetes de tipo File
         * cuando le llega uno a su entrada este lo gestiona para recalcular tiempos
         * y reenvios de eventos
         */
        this.eventFileSystem
            .observable()
            .subscribe(event -> {
                System.out.println(event);
                subject.onNext(event);
            });
        System.out.println(managerOptional.isPresent());
        managerOptional.ifPresent(timeManager -> {
            this.eventFileSystem.observable()
                .subscribe(fileEvent -> {
                    timeManager.manage(fileEvent.getFileObject());
                }, Throwable::printStackTrace);
            timeManager.observable()
                .subscribe(subject::onNext, Throwable::printStackTrace);
        });
    }

    /**
     * Monitorear un directorio
     *
     * @param path
     */
    public void watch(String path) {
        try {
            FileObject fileObject = VFS.getManager().resolveFile(path);
            watch(fileObject);
        } catch (FileSystemException e) {
            e.printStackTrace();
        }
    }

    /**
     * Deja de monitorear un proyecto
     *
     * @param path
     */
    public void stopWatch(String path) {
        try {
            FileObject fileObject = VFS.getManager().resolveFile(path);
            this.eventFileSystem.unfollowEvents(fileObject);
            LOG.info("Se ha dejado de seguir los eventos de {}", path);
        } catch (FileSystemException e) {
            e.printStackTrace();
        }
    }

    public void watch(FileObject reference) {
        this.eventFileSystem.followEvents(reference);
        LOG.info("Se ha comenzado de seguir los eventos de {}", reference.getName());
    }

    private void makeFilePack(FileObject reference, EventType eventType) {
        FileEvent fileEvent = new FileEvent(reference, eventType);
        subject.onNext(fileEvent);
    }

    private void makeTimePack(FileObject reference, long timeAcum) {
        TimeEvent timeEvent = new TimeEvent(reference, timeAcum);
        subject.onNext(timeEvent);
    }

    @Override
    public Observer<FileObject> observer() {
        return new Observer<FileObject>() {
            @Override
            public void onCompleted() {
                LOG.debug("Complete sub");
            }

            @Override
            public void onError(Throwable e) {
                LOG.error("Problema ", e);
                e.printStackTrace();
            }

            @Override
            public void onNext(FileObject fileObject) {

            }
        };
    }

    @Override
    public Observable<Event> observable() {
        return subject.asObservable();
    }

    public void start() {
        this.subject.doOnError(throwable -> {
            System.out.println("Error poco frencuente");
        });
        this.eventFileSystem.start();
        managerOptional.ifPresent(TimeManager::start);
    }

    public void stop() {
        this.subject.onCompleted();
        this.eventFileSystem.stop();
        managerOptional.ifPresent(TimeManager::stop);
    }

    @Override
    public void run() {
        start();
    }

    /**
     * Constructor del servicio de eventos
     */
    public static class BuidlerService {
        private Set<FileSupport> fileSupportHashSet = new HashSet<>();
        private SupportFileRegistry supportFileRegistry = new DefaultSupportFileRegistry();
        private Optional<TimeManager> timeManager = Optional.empty();
        private EventFileSystem eventSource = null;
        private boolean enableTimer = false;
        private int delayMonitor;

        public BuidlerService addSupport(FileSupport fileSupport) {
            fileSupportHashSet.add(fileSupport);
            return this;
        }

        public BuidlerService addSupport(Set<? extends FileSupport> fileSupport) {
            fileSupportHashSet.addAll(fileSupport);
            return this;
        }

        public BuidlerService withSourceEvents(EventFileSystem sourceEvents) {
            eventSource = Objects.requireNonNull(sourceEvents, "Se requiere de un fuente de eventos");
            return this;
        }

        public BuidlerService withTimerManager(TimeManager timer) {
            timeManager = Optional.ofNullable(timer);
            return this;
        }

        public BuidlerService withDelay(int delay) {
            this.delayMonitor = delay;
            return this;
        }

        public EventSystem build() {
            fileSupportHashSet.forEach(supportFileRegistry::register);
            return new EventSystem(this);
        }
    }

    /**
     * Accesso local, creacion de paquetes de tiempo y envio a subject
     */
    private class ConsumerTimeEvent implements BiConsumer<FileObject, Long> {
        // empaqueta los datos recolectados en nuevo mensaje inmutable para los observadores
        @Override
        public void accept(FileObject fileObject, Long aLong) {
            LOG.info("Creando paquete de tipo tiempo para {} ", fileObject);
            makeTimePack(fileObject, aLong);
        }
    }

    /**
     *
     */
    private class ConsumerFileEvent implements BiConsumer<FileObject, EventType> {
        @Override
        public void accept(FileObject fileObject, EventType typeEvent) {
            LOG.info("Creando paquete de tipo crud file para {} ", fileObject.getName());
            makeFilePack(fileObject, typeEvent);
        }
    }
}
