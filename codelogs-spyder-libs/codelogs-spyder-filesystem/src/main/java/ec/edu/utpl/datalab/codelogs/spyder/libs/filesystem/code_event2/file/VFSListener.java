/*
 * The MIT License (MIT)
 * =========================================================================================================
 *
 * Copyright (c) 10/10/16 12:30 Modulo  Proyecto rexcode-filesystem  Autor [rfcardenas]
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY
 * , WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ec.edu.utpl.datalab.codelogs.spyder.libs.filesystem.code_event2.file;

import ec.edu.utpl.datalab.codelogs.spyder.libs.filesystem.code_event2.packs.EventType;
import org.apache.commons.vfs2.FileChangeEvent;
import org.apache.commons.vfs2.FileListener;
import org.apache.commons.vfs2.FileObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.function.BiConsumer;

final class VFSListener implements FileListener {
    private static final Logger log = LoggerFactory.getLogger(CustomMonitor.class);
    private BiConsumer<FileObject, EventType> consumer;

    public VFSListener(BiConsumer<FileObject, EventType> consumer) {
        this.consumer = consumer;
    }

    @Override
    public void fileCreated(FileChangeEvent event) throws Exception {
        log.warn("Fire Create");
        FileObject reference = event.getFile();
        if (reference.isFile()) {
            consumer.accept(reference, EventType.FILE_CREATE);
        } else if (reference.isFolder()) {
            consumer.accept(reference, EventType.FOLDER_CREATE);
        }

    }

    @Override
    public void fileDeleted(FileChangeEvent event) throws Exception {
        log.warn("Fire delete");
        FileObject reference = event.getFile();
        if (reference.isFile()) {
            consumer.accept(reference, EventType.FILE_DELETE);
        } else if (reference.isFolder()) {
            consumer.accept(reference, EventType.FOLDER_DELETE);
        }
    }

    @Override
    public void fileChanged(FileChangeEvent event) throws Exception {
        log.warn("Fire Update");
        FileObject reference = event.getFile();
        if (reference.isFile()) {
            consumer.accept(reference, EventType.FILE_UPDATE);
        } else if (reference.isFolder()) {
            consumer.accept(reference, EventType.FOLDER_UPDATE);
        }
    }

}
