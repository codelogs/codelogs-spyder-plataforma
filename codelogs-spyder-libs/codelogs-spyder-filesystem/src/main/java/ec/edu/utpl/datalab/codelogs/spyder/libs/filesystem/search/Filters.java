/*
 * The MIT License (MIT)
 * =========================================================================================================
 *
 * Copyright (c) 10/10/16 12:30 Modulo  Proyecto rexcode-filesystem  Autor [rfcardenas]
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY
 * , WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ec.edu.utpl.datalab.codelogs.spyder.libs.filesystem.search;

import org.apache.commons.vfs2.FileSelectInfo;

/**
 * * Created by rfcardenas
 */
final class Filters {
    private Filters() {
    }

    static class FilterByName extends FinderDecorator {
        private String name;

        public FilterByName(String name) {
            this.name = name;
        }

        @Override
        public boolean includeFile(FileSelectInfo fileInfo) throws Exception {
            return getComponent().includeFile(fileInfo) && fileInfo.getFile().getName().getBaseName().equals(name);
        }
    }

    static class FilterByExt extends FinderDecorator {
        private String ext;

        public FilterByExt(String ext) {
            this.ext = ext;
        }

        @Override
        public boolean includeFile(FileSelectInfo fileInfo) throws Exception {
            return fileInfo.getFile().getName().getExtension().equals(ext);
        }
    }

}
