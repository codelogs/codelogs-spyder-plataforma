/*
 * The MIT License (MIT)
 * =========================================================================================================
 *
 * Copyright (c) 10/10/16 12:30 Modulo  Proyecto rexcode-filesystem  Autor [rfcardenas]
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY
 * , WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ec.edu.utpl.datalab.codelogs.spyder.libs.filesystem.code_event2.support;

import org.apache.commons.vfs2.FileName;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.ConcurrentHashMap;

public final class DefaultSupportFileRegistry implements SupportFileRegistry {

    private final Logger log = LoggerFactory.getLogger(DefaultSupportFileRegistry.class);
    public ConcurrentHashMap<String, FileSupport> supportHashMap;

    public DefaultSupportFileRegistry() {
        this.supportHashMap = new ConcurrentHashMap<>();
    }

    @Override
    public synchronized void register(FileSupport fileSupport) {
        log.info("Instalanando soporte para archivo  " + fileSupport.getLanguaje());
        supportHashMap.put(fileSupport.getExtension(), fileSupport);
    }

    @Override
    public synchronized boolean isSupported(FileName reference) {
        return (reference != null && supportHashMap.containsKey(reference.getExtension()));
    }
}
