/*
 * The MIT License (MIT)
 * =========================================================================================================
 *
 * Copyright (c) 10/10/16 12:30 Modulo  Proyecto rexcode-filesystem  Autor [rfcardenas]
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY
 * , WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ec.edu.utpl.datalab.codelogs.spyder.libs.filesystem.code_event2.packs;

import org.apache.commons.vfs2.FileObject;

import java.util.Arrays;
import java.util.EnumSet;

/**
 * * Created by rfcardenas
 */
public class Event {
    private final FileObject fileObject;
    private final EventType eventType;

    public Event(FileObject fileObject, EventType eventType) {
        this.fileObject = fileObject;
        this.eventType = eventType;
    }

    public FileObject getFileObject() {
        return fileObject;
    }

    public EventType getEventType() {
        return eventType;
    }

    /**
     * indica si este evento corresponde a uno del conjunto de argumento propocionado
     *
     * @param types
     * @return
     */
    public boolean isOfType(EventType... types) {
        EnumSet e = EnumSet.copyOf(Arrays.asList(types));
        return e.contains(eventType);
    }

    @Override
    public String toString() {
        return "{Evento de tipo" + eventType.toString() + "}";
    }
}
