/*
 * The MIT License (MIT)
 * =========================================================================================================
 *
 * Copyright (c) 10/10/16 12:30 Modulo  Proyecto rexcode-filesystem  Autor [rfcardenas]
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY
 * , WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ec.edu.utpl.datalab.codelogs.spyder.libs.filesystem.code_event2.file;

import ec.edu.utpl.datalab.codelogs.spyder.libs.filesystem.code_event2.packs.Event;
import ec.edu.utpl.datalab.codelogs.spyder.libs.filesystem.code_event2.packs.EventType;
import ec.edu.utpl.datalab.codelogs.spyder.libs.filesystem.code_event2.support.DefaultSupportFileRegistry;
import ec.edu.utpl.datalab.codelogs.spyder.libs.filesystem.code_event2.support.FileSupport;
import ec.edu.utpl.datalab.codelogs.spyder.libs.filesystem.code_event2.support.SupportFileRegistry;
import ec.edu.utpl.datalab.codelogs.spyder.libs.filesystem.code_event2.time.TimeManager;
import org.apache.commons.vfs2.FileListener;
import org.apache.commons.vfs2.FileObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import rx.Observable;
import rx.subjects.PublishSubject;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.function.BiConsumer;

/**
 * * Created by rfcardenas
 */
public class EventSysteminherited implements EventFileSystem {
    private static final Logger log = LoggerFactory.getLogger(EventSysteminherited.class);
    private BiConsumer<FileObject, EventType> consumer;
    private FileListener fileListener;
    private SupportFileRegistry supportFileRegistry;
    private CustomMonitor customMonitor;
    private PublishSubject<Event> publishSubject = PublishSubject.create();

    private EventSysteminherited(BuidlerService buidlerService) {
        this.supportFileRegistry = buidlerService.supportFileRegistry;
        this.consumer = new Consumer();
        this.fileListener = new VFSListener(consumer);
        this.customMonitor = new CustomMonitor(fileListener, supportFileRegistry);
        this.customMonitor.setDelay(buidlerService.delayMonitor);
        init();
    }

    private EventSysteminherited(SupportFileRegistry supportFileRegistry, int delay) {
        this.supportFileRegistry = supportFileRegistry;
        this.consumer = new Consumer();
        this.fileListener = new VFSListener(consumer);
        this.customMonitor = new CustomMonitor(fileListener, supportFileRegistry);
        this.customMonitor.setDelay(delay);
        init();
    }

    /**
     * Constructor de servicio de eventos heredado
     */


    public static BuidlerService builder() {
        return new BuidlerService();
    }

    private void init() {
        this.customMonitor.setRecursive(true);
        start();
    }

    @Override
    public void start() {
        customMonitor.start();
    }

    @Override
    public void stop() {
        this.customMonitor.stop();
    }

    @Override
    public Observable<Event> observable() {
        return publishSubject;
    }

    @Override
    public void followEvents(FileObject fileObject) {
        if (customMonitor.isRunning()) {
            throw new FileSystemEventException("Aun no se ha invocado el metodo start");
        }
        this.customMonitor.addFile(fileObject);
    }

    @Override
    public void unfollowEvents(FileObject fileObject) {
        this.customMonitor.removeRecursive(fileObject);
    }

    public static class BuidlerService {

        private Set<FileSupport> fileSupportHashSet = new HashSet<>();
        private SupportFileRegistry supportFileRegistry = new DefaultSupportFileRegistry();
        private Optional<TimeManager> timeManager = Optional.empty();
        private EventFileSystem eventSource = null;
        private boolean enableTimer = false;
        private int delayMonitor;

        public BuidlerService addSupport(FileSupport fileSupport) {
            fileSupportHashSet.add(fileSupport);
            return this;
        }

        public BuidlerService addSupport(Set<? extends FileSupport> fileSupport) {
            fileSupportHashSet.addAll(fileSupport);
            return this;
        }

        public BuidlerService withDelay(int delay) {
            this.delayMonitor = delay;
            return this;
        }

        public EventSysteminherited build() {
            fileSupportHashSet.forEach(supportFileRegistry::register);
            return new EventSysteminherited(this);
        }
    }

    /**
     * Clase priva utilizada por listener interno de FSM
     */
    private class Consumer implements BiConsumer<FileObject, EventType> {
        @Override
        public void accept(FileObject fileObject, EventType eventType) {
            Event event = new Event(fileObject, eventType);
            publishSubject.onNext(event);
        }
    }


}
