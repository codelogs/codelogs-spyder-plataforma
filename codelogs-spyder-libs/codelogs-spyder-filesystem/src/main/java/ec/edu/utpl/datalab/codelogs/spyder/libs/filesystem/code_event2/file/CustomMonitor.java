/*
 * The MIT License (MIT)
 * =========================================================================================================
 *
 * Copyright (c) 10/10/16 12:30 Modulo  Proyecto rexcode-filesystem  Autor [rfcardenas]
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY
 * , WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ec.edu.utpl.datalab.codelogs.spyder.libs.filesystem.code_event2.file;

import ec.edu.utpl.datalab.codelogs.spyder.libs.filesystem.code_event2.support.SupportFileRegistry;
import org.apache.commons.vfs2.*;
import org.apache.commons.vfs2.provider.AbstractFileSystem;
import org.jvnet.hk2.annotations.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Stack;
import java.util.logging.Level;

@Service
final class CustomMonitor implements Runnable, FileMonitor {
    private static final Logger LOG = LoggerFactory.getLogger(CustomMonitor.class);
    private static final long DEFAULT_DELAY = 1000;
    private static final int DEFAULT_MAX_FILES = 1000;
    private final Map<FileName, FileMonitorAgent> monitorMap = new HashMap<FileName, FileMonitorAgent>();
    private final Stack<FileObject> deleteStack = new Stack<FileObject>();
    private final Stack<FileObject> addStack = new Stack<FileObject>();
    public SupportFileRegistry layerFileSupport;
    protected FileListener fileListener;
    int lastSIZE = 0;
    LinkedList fileNames = new LinkedList();
    private volatile boolean shouldRun = true; // used for inter-thread communication
    private long delay = DEFAULT_DELAY;
    private int checksPerRun = DEFAULT_MAX_FILES;
    private boolean recursive;
    private Thread monitorThread;

    @Inject
    public CustomMonitor(FileListener listener, SupportFileRegistry fileSupport) {
        this.fileListener = listener;
        this.layerFileSupport = fileSupport;
    }

    public boolean isRecursive() {
        return this.recursive;
    }

    public void setRecursive(final boolean newRecursive) {
        this.recursive = newRecursive;
    }

    FileListener getFileListener() {
        return this.fileListener;
    }

    public void addSpecifict(FileObject file) {
        doAddFile(file);
    }

    @Override
    public void addFile(final FileObject file) {
        //System.out.println("---> Add File");
        try {
            if (layerFileSupport.isSupported(file.getName()) || file.getType().equals(FileType.FOLDER)) {
                // System.out.println("ISourceParsed añadido " );
                doAddFile(file);
                try {
                    // add all direct children too
                    if (file.getType().hasChildren()) {
                        // Traverse the children
                        final FileObject[] children = file.getChildren();
                        for (int i = 0; i < children.length; i++) {
                            if (layerFileSupport.isSupported(children[i].getName()) || children[i].getType().equals(FileType.FOLDER)) {
                                doAddFile(children[i]);
                            } else {
                                LOG.warn("Exclusion " + children[i].getName().getBaseName());
                            }
                        }
                    }
                } catch (FileSystemException fse) {
                    LOG.error(fse.getLocalizedMessage(), fse);
                }
            }

        } catch (FileSystemException ex) {
            java.util.logging.Logger.getLogger(CustomMonitor.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void doAddFile(final FileObject file) {
        synchronized (this.monitorMap) {
            if (this.monitorMap.get(file.getName()) == null) {
                this.monitorMap.put(file.getName(), new FileMonitorAgent(this, file, layerFileSupport));
                try {
                    if (this.fileListener != null) {
                        file.getFileSystem().addListener(file, this.fileListener);
                    }
                    if (file.getType().hasChildren() && this.recursive) {
                        // Traverse the children
                        final FileObject[] children = file.getChildren();
                        for (int i = 0; i < children.length; i++) {
                            this.addFile(children[i]); // Add depth first
                        }
                    }
                } catch (FileSystemException fse) {
                    LOG.error(fse.getLocalizedMessage(), fse);
                }

            }
        }
    }

    @Override
    public void removeFile(final FileObject file) {
        LOG.info("Eliminando file track {}", file.getName());
        synchronized (this.monitorMap) {
            FileName fn = file.getName();
            if (this.monitorMap.get(fn) != null) {
                FileObject parent;
                try {
                    parent = file.getParent();
                } catch (FileSystemException fse) {
                    parent = null;
                }

                this.monitorMap.remove(fn);

                if (parent != null) { // Not the root
                    FileMonitorAgent parentAgent
                        = this.monitorMap.get(parent.getName());
                    if (parentAgent != null) {
                        parentAgent.resetChildrenList();
                    }
                }
            }
        }
    }

    public void removeRecursive(FileObject fileObject) {
        try {
            if (fileObject.getType().hasChildren()) {
                FileObject[] children = fileObject.getChildren();
                for (FileObject child : children) {
                    removeRecursive(child);
                }
            }
        } catch (FileSystemException e) {
            e.printStackTrace();
        }
        removeFile(fileObject);
        fileObject.getFileSystem().removeListener(fileObject, fileListener);
    }

    protected void queueRemoveFile(final FileObject file) {
        this.deleteStack.push(file);
    }

    public long getDelay() {
        return delay;
    }

    public void setDelay(long delay) {
        if (delay > 0) {
            this.delay = delay;
        } else {
            this.delay = DEFAULT_DELAY;
        }
    }

    public int getChecksPerRun() {
        return checksPerRun;
    }

    public void setChecksPerRun(int checksPerRun) {
        this.checksPerRun = checksPerRun;
    }

    protected void queueAddFile(final FileObject file) {
        this.addStack.push(file);
    }

    public void start() {
        if (this.monitorThread == null) {
            this.monitorThread = new Thread(this);
            this.monitorThread.setName("Deamon FileSystem");
            this.monitorThread.setDaemon(true);
            this.monitorThread.setPriority(Thread.MIN_PRIORITY);
        }

        LOG.info("MONITOR START A ::::::::::::::::::::::::::::::::::::::::: {}", monitorThread.getState());
        if (this.monitorThread.getState() == Thread.State.NEW)
            this.monitorThread.start();
        LOG.info("MONITOR START D ::::::::::::::::::::::::::::::::::::::::: {}", monitorThread.getState());
    }

    public void stop() {
        this.shouldRun = false;
    }

    public boolean isRunning() {
        if (this.monitorThread == null) {
            LOG.warn("El monitor thread no ha sido inicializado");
            return false;
        }
        if (this.monitorThread.getState() == Thread.State.TERMINATED) {
            LOG.warn("El monitor thread ha terminado");
            return false;
        }
        if (this.monitorThread.isInterrupted()) {
            LOG.warn("El monitor thread ha sido interrumpido");
            return false;
        }
        return false;
    }

    @Override
    public void run() {
        mainloop:
        while (!monitorThread.isInterrupted() && this.shouldRun) {
            //System.out.println("---- beat [FS]");
            /**
             * Borrar Carpetas al final
             */
            if (!this.deleteStack.isEmpty()) {
                System.out.println("Sort");
                for (int i = 0; i < deleteStack.size(); i++) {
                    if (!deleteStack.get(i).getName().getExtension().isEmpty()) {
                        FileObject fo = deleteStack.remove(i);
                        deleteStack.push(fo);
                    }
                }
                while (!this.deleteStack.empty()) {
                    FileObject ref = this.deleteStack.pop();
                    ((AbstractFileSystem) ref.getFileSystem()).fireFileDeleted(ref);
                    this.removeFile(ref);
                    if (this.getFileListener() != null) {
                        ref.getFileSystem().removeListener(ref,
                            this.getFileListener());
                    }

                }
            }
            // For each entry in the map
            Object[] tmp;

            synchronized (this.monitorMap) {
                tmp = this.monitorMap.keySet().toArray();
                /// Ejeucucion Solo cuando es necesaria,
                if (lastSIZE != tmp.length) {
                    LOG.info("!!!!!!!!!!!!!!!!!!!!!!!");
                    lastSIZE = tmp.length;
                    for (Object fileName : tmp) {
                        FileName fileName_ = (FileName) fileName;
                        if (layerFileSupport.isSupported(fileName_)) {
                            fileNames.addLast(fileName_);
                        } else {
                            fileNames.addFirst(fileName_);
                        }
                    }
                } else {

                }
            }

            for (int iterFileNames = 0; iterFileNames < fileNames.size();
                 iterFileNames++) {
                FileName fileName = (FileName) fileNames.get(iterFileNames);
                FileMonitorAgent agent;
                synchronized (this.monitorMap) {
                    agent = this.monitorMap.get(fileName);
                }
                if (agent != null) {
                    agent.check();
                }

                if (getChecksPerRun() > 0) {
                    if ((iterFileNames % getChecksPerRun()) == 0) {
                        try {
                            Thread.sleep(getDelay());
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                            // Woke up.
                        }
                    }
                }

                if (monitorThread.isInterrupted() || !this.shouldRun) {
                    continue mainloop;
                }
            }

            while (!this.addStack.empty()) {
                this.addFile(this.addStack.pop());
            }

            try {
                Thread.sleep(getDelay());
            } catch (InterruptedException e) {
                continue;
            }
        }

        this.shouldRun = true;
    }

    private static final class FileMonitorAgent {
        private final FileObject file;
        private final CustomMonitor fm;
        private boolean exists;
        private long timestamp;
        private SupportFileRegistry fileSupport;

        private Map<FileName, Object> children;

        private FileMonitorAgent(CustomMonitor fm, FileObject file, SupportFileRegistry support) {
            this.fm = fm;
            this.file = file;
            this.fileSupport = support;
            this.refresh();
            this.resetChildrenList();

            try {
                this.exists = this.file.exists();
            } catch (FileSystemException fse) {
                this.exists = false;
                this.timestamp = -1;
            }

            if (this.exists) {
                try {
                    this.timestamp = this.file.getContent().getLastModifiedTime();
                } catch (FileSystemException fse) {
                    this.timestamp = -1;
                }
            }
        }

        private void resetChildrenList() {
            try {
                if (this.file.getType().hasChildren()) {
                    this.children = new HashMap<FileName, Object>();
                    FileObject[] childrenList = this.file.getChildren();
                    for (int i = 0; i < childrenList.length; i++) {
                        this.children.put(childrenList[i].getName(), new Object()); // null?
                    }
                }
            } catch (FileSystemException fse) {
                this.children = null;
            }
        }

        private void refresh() {
            try {
                this.file.refresh();
            } catch (FileSystemException fse) {
                LOG.error(fse.getLocalizedMessage(), fse);
            }
        }

        private void fireAllCreate(FileObject child) throws FileSystemException {
            if (!fileSupport.isSupported(child.getName()) && !child.getType().equals(FileType.FOLDER)) {
                LOG.warn("RETURN PARA Archivo {}", child.getName());
                return;
            }
            // Add listener so that it can be triggered
            if (this.fm.getFileListener() != null) {
                child.getFileSystem().addListener(child, this.fm.getFileListener());
            }

            ((AbstractFileSystem) child.getFileSystem()).fireFileCreated(child);

            // Remove it because pojo listener is added in the queueAddFile
            if (this.fm.getFileListener() != null) {
                child.getFileSystem().removeListener(child,
                    this.fm.getFileListener());
            }

            this.fm.queueAddFile(child); // Add

            try {

                if (this.fm.isRecursive()) {
                    if (child.getType().hasChildren()) {
                        FileObject[] newChildren = child.getChildren();
                        for (int i = 0; i < newChildren.length; i++) {
                            fireAllCreate(newChildren[i]);
                        }
                    }
                }

            } catch (FileSystemException fse) {
                LOG.error(fse.getLocalizedMessage(), fse);
            }
        }

        private void checkForNewChildren() {
            try {
                if (this.file.getType().hasChildren()) {
                    FileObject[] newChildren = this.file.getChildren();
                    if (this.children != null) {
                        // See which new children are not listed in the current children map.
                        Map<FileName, Object> newChildrenMap = new HashMap<FileName, Object>();
                        Stack<FileObject> missingChildren = new Stack<FileObject>();

                        for (int i = 0; i < newChildren.length; i++) {
                            newChildrenMap.put(newChildren[i].getName(), new Object()); // null ?
                            // If the child's not there
                            if (!this.children.containsKey(newChildren[i].getName())) {
                                missingChildren.push(newChildren[i]);
                            }
                        }

                        this.children = newChildrenMap;

                        // If there were missing children
                        if (!missingChildren.empty()) {

                            while (!missingChildren.empty()) {
                                FileObject child = missingChildren.pop();
                                this.fireAllCreate(child);
                            }
                        }

                    } else {
                        // First set of children - Break out the cigars
                        if (newChildren.length > 0) {
                            this.children = new HashMap<FileName, Object>();
                        }
                        for (int i = 0; i < newChildren.length; i++) {
                            this.children.put(newChildren[i].getName(), new Object()); // null?
                            this.fireAllCreate(newChildren[i]);
                        }
                    }
                }
            } catch (FileSystemException fse) {
                LOG.error(fse.getLocalizedMessage(), fse);
            }
        }

        private void check() {
            this.refresh();

            try {
                // If the clase existed and now doesn'sessionLoader
                if (this.exists && !this.file.exists()) {
                    this.exists = this.file.exists();
                    this.timestamp = -1;
                    // Fire delete event
                    ///((AbstractFileSystem) this.clase.getFileSystem()).fireFileDeleted(this.clase);
                    // Remove listener in case clase is re-created. Don'sessionLoader want to fire twice.
                    // Remove from map
                    this.fm.queueRemoveFile(this.file);
                } else if (this.exists && this.file.exists()) {
                    // Check the timestamp to see if it has been modified
                    if (this.timestamp != this.file.getContent().getLastModifiedTime()) {
                        this.timestamp = this.file.getContent().getLastModifiedTime();
                        // Fire change event

                        // Don'sessionLoader fire if it's pojo folder because new clase children
                        // and deleted sources in pojo folder have their own event triggered.
                        if (!this.file.getType().hasChildren()) {
                            ((AbstractFileSystem) this.file.getFileSystem()).fireFileChanged(this.file);
                        }
                    }

                    //                } else if (!this.exists && this.clase.exists()) {
                    //                    this.exists = this.clase.exists();
                    //                    this.timestamp = this.clase.getContent().getLastModifiedTime();
                    //                    // Don'sessionLoader fire if it's pojo folder because new clase children
                    //                    // and deleted sources in pojo folder have their own event triggered.
                    //                    if (!this.clase.getType().hasChildren()) {
                    //                        ((AbstractFileSystem) this.clase.getFileSystem()).fireFileCreated(this.clase);
                    //                    }

                }
                this.checkForNewChildren();
            } catch (FileSystemException fse) {
                fse.printStackTrace();
                LOG.error(fse.getLocalizedMessage(), fse);
            }
        }

    }

}
