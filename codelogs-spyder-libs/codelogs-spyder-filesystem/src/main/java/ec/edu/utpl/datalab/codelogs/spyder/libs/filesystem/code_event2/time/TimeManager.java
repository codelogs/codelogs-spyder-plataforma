/*
 * The MIT License (MIT)
 * =========================================================================================================
 *
 * Copyright (c) 10/10/16 12:30 Modulo  Proyecto rexcode-filesystem  Autor [rfcardenas]
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY
 * , WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.utpl.datalab.codelogs.spyder.libs.filesystem.code_event2.time;


import ec.edu.utpl.datalab.codelogs.spyder.libs.filesystem.code_event2.packs.TimeEvent;
import org.apache.commons.vfs2.FileObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import rx.Observable;
import rx.subjects.PublishSubject;

import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.*;


/**
 * @author ronald
 */
public final class TimeManager {
    private static final Logger log = LoggerFactory.getLogger(TimeManager.class);
    private final TimeUnit timeUnit;
    PublishSubject<TimeEvent> subject;
    private int time = 5;
    private ConcurrentHashMap<String, UnitTime> map;
    private ScheduledExecutorService service;

    public TimeManager(int timeout, TimeUnit timeUnit) {
        this.subject = PublishSubject.create();
        this.time = timeout;
        this.timeUnit = timeUnit;
        this.service = new ScheduledThreadPoolExecutor(1);
    }

    /**
     * Crea o actualiza un gestor de timpo
     *
     * @param <T>
     * @param gnode
     */
    public void manage(FileObject file) {
        String path = file.getName().getPath();
        log.info("Time out Enable para {}", file.getName());
        if (map == null) {
            map = new ConcurrentHashMap<>();
            checker();
        }
        if (map.containsKey(path)) {
            UnitTime unit = map.get(path);
            unit.updateModify();
        }
        if (!map.containsKey(path)) {
            TimePackModificable timePackModificable = new TimePackModificable(file);
            UnitTime man = new UnitTime(timePackModificable);
            man.updateModify();
            map.put(path, man);
        }
    }

    public void start() {
        if (service == null) {
            service = new ScheduledThreadPoolExecutor(1);
        }
        log.info("Time manager Start..");
    }

    public void stop() {
        log.info("Time manager Stop..");
        service.shutdownNow();
    }

    public ScheduledExecutorService getService() {
        return service;
    }

    public <T extends FileObject> void remove(T node) {
        UnitTime unit = map.get(node);
        boolean status = unit.getFuture().cancel(true);
        map.remove(node);
    }

    public void checker() {
        Runnable runnable = () -> {
            for (Iterator<Map.Entry<String, UnitTime>> it = map.entrySet().iterator();
                 it.hasNext(); ) {
                Map.Entry<String, UnitTime> entry = it.next();
                UnitTime n = entry.getValue();
                if (n.getFuture() == null) {
                    it.remove();
                } else if (n.getFuture().getDelay(TimeUnit.SECONDS) <= 0) {
                    it.remove();
                }
            }
        };
        //service.scheduleAtFixedRate(runnable, 5, 45, TimeUnit.SECONDS);
    }

    /**
     * Detenerminar si puede existir deadlook [status NO]
     *
     * @param timeevent
     */
    private synchronized void propagate(UnitTime timeevent) {
        log.info("TimeOut Event tiempo acumulado {}", timeevent.getAcumulador());
        TimePackModificable node = timeevent.getAttachFile();
        node.incrementTime(timeevent.acumulador);
    }

    public Observable<TimeEvent> observable() {
        return subject.asObservable();
    }

    public boolean isRunning() {
        return (!service.isTerminated());
    }

    public void setMaxTImeWait(int timeWait) {
        time = timeWait;
    }

    /**
     * Private pack time
     */
    private final class UnitTime implements Runnable {

        private long start;
        private long lastMod;
        private long delay;
        private long acumulador = 0;
        private ScheduledFuture<?> future;
        private TimePackModificable attachFile;

        public UnitTime(TimePackModificable af) {
            start = System.currentTimeMillis();
            this.attachFile = af;
        }

        public long calcElapsedTime() {
            return (lastMod - start);
        }

        public void updateModify() {

            if (future == null) {
                future = service.schedule(this, time, timeUnit);
            } else {
                future.cancel(true);
                future = service.schedule(this, time, timeUnit);
            }
            if (start == 0) {
                start = System.currentTimeMillis();
            }
            this.lastMod = System.currentTimeMillis();
            delay = calcElapsedTime();
            acumulador += delay;
            start = lastMod;

        }

        public TimePackModificable getAttachFile() {
            return attachFile;
        }

        public long getStart() {
            return start;
        }

        public long getUpdate() {
            return lastMod;
        }

        public void restart() {
        }

        public long getAcumulador() {
            return acumulador;
        }

        @Override
        public void run() {
            start = 0;
            lastMod = 0;
            propagate(this);
        }

        public ScheduledFuture<?> getFuture() {
            return future;
        }
    }
}
