/*
 * The MIT License (MIT)
 * =========================================================================================================
 *
 * Copyright (c) 10/10/16 12:30 Modulo  Proyecto rexcode-filesystem  Autor [rfcardenas]
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY
 * , WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ec.edu.utpl.datalab.codelogs.spyder.libs.filesystem.search;

import org.apache.commons.vfs2.FileObject;
import org.apache.commons.vfs2.FileSelectInfo;
import org.apache.commons.vfs2.FileSystemException;

import java.util.Objects;

/**
 * * Created by rfcardenas
 */
public final class SearchFileSystem implements FinderExecutor {
    private int deep = 0;
    private Finder filter;

    private SearchFileSystem(Builder builder) {
        this.deep = builder.deep;
        this.filter = builder.finder;
    }

    public static Builder newSearch(int deep) {
        return new Builder(deep);
    }

    @Override
    public FileObject[] executeFinder(FileObject context) {
        try {
            FileObject[] result = context.findFiles(filter);
            return (Objects.isNull(result)) ? new FileObject[0] : result;
        } catch (FileSystemException e) {
            e.printStackTrace();
        }
        return new FileObject[0];
    }

    public static class Builder {
        private int deep;
        private Finder finder;

        private Builder(int deep) {
            this.deep = deep;
            this.finder = new Finder() {
                @Override
                public boolean includeFile(FileSelectInfo fileInfo) throws Exception {
                    return true;
                }
            };
        }

        public Builder withFilter(FinderDecorator finderDecorator) {
            finderDecorator.decorateWith(finder);
            finder = finderDecorator;
            finder.setDeep(deep);
            return this;
        }

        public SearchFileSystem build() {
            return new SearchFileSystem(this);
        }
    }
}
