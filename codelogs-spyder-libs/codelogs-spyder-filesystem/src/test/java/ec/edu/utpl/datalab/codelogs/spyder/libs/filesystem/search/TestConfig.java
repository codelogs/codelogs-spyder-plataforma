package ec.edu.utpl.datalab.codelogs.spyder.libs.filesystem.search;

import org.apache.commons.vfs2.FileObject;
import org.apache.commons.vfs2.FileSystemException;
import org.apache.commons.vfs2.VFS;

import java.io.File;
import java.util.Optional;

/**
 * * Created by rfcardenas
 */
public class TestConfig {
    public static Optional<FileObject> loadApp() {
        FileObject fileObject = null;
        try {
            File file = new File("src/test/resources/app");
            fileObject = VFS.getManager().toFileObject(file);
        } catch (FileSystemException e) {
            e.printStackTrace();
        }
        return Optional.ofNullable(fileObject);
    }
}
