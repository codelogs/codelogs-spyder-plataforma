package ec.edu.utpl.datalab.codelogs.spyder.libs.filesystem.search;

import org.apache.commons.vfs2.FileObject;
import org.junit.Test;

import java.util.Optional;

import static org.junit.Assert.assertTrue;

/**
 * * Created by rfcardenas
 */
public class SearchFileSystemTest {
    @Test
    public void executeFinder() throws Exception {
        Optional<FileObject> rootFile = TestConfig.loadApp();
        if (rootFile.isPresent()) {
            /**
             * Busque de archivos en con una profundidad maxima de 5 niveles
             */
            System.out.println("===================Filter By Name==========================");
            SearchFileSystem searchFileSystem = SearchFileSystem.newSearch(5)
                .withFilter(FilterSet.filterOnlyFiles())
                .withFilter(FilterSet.filterByName("HelloUTPL.java"))
                .build();
            FileObject[] results = searchFileSystem.executeFinder(rootFile.get());
            for (FileObject result : results) {
                System.out.println("Find File " + result.getName());
            }
            System.out.println("Total Results = " + results.length);
            assertTrue(results.length > 0);

            System.out.println("===================Filter By ext===========================");

            SearchFileSystem searchFileSystem2 = SearchFileSystem.newSearch(5)
                .withFilter(FilterSet.filterOnlyFiles())
                .withFilter(FilterSet.filterByExtension("java"))
                .build();
            FileObject[] results2 = searchFileSystem2.executeFinder(rootFile.get());
            for (FileObject result : results2) {
                System.out.println("Find File " + result.getName());
            }
            assertTrue(results2.length == 2);
            System.out.println("===================Filter By Folder=======================");

            SearchFileSystem searchFileSystem3 = SearchFileSystem.newSearch(5)
                .withFilter(FilterSet.filterOnlyFolders())
                .build();
            FileObject[] results3 = searchFileSystem3.executeFinder(rootFile.get());
            for (FileObject result : results3) {
                System.out.println("Find File " + result.getName());
                System.out.println("Folder ?   " + result.isFolder());
            }
            assertTrue(results3.length == 3);
        }
    }

}
