# CODELOGS COMMON

Este modulo proporciona una coleccion de utilidades
para el proyecto CODELOGS, contiene utilidades a nivel
de :
 - Clases
 - Archivos
 - Reflexión
 - Barras de progreso en cosola
 - Streams
 - Strings
 - Mapas
 - Red
 - Colecciones
 
 En este modulo se debe ir agregando nuevas utilidades,
 para los niveles descritos