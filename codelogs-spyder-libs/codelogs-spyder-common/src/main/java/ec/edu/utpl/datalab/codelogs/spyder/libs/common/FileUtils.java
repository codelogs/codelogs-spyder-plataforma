package ec.edu.utpl.datalab.codelogs.spyder.libs.common;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * * Created by rfcardenas
 */
public class FileUtils {
    public static String formatUniversal(String url) {
        if (url.contains("file://")) {
            return url;
        }
        return "file://" + url;
    }

    public static String formatOs(String url) {
        return url.replace("file://", "");
    }

    public static boolean checkExist(String url) {
        url = formatOs(url);
        Path path = Paths.get(url);
        return Files.exists(path);
    }

    public static void writeToFile(byte[] bytes, String file) throws IOException {
        File output = new File(file);
        writeToFile(bytes, output);
    }

    public static void writeToFile(byte[] bytes, File fileOut) throws IOException {
        DataOutputStream dout = new DataOutputStream(new FileOutputStream(fileOut));
        dout.write(bytes);
    }
}
