package ec.edu.utpl.datalab.codelogs.spyder.libs.common;

import java.io.IOException;
import java.net.Socket;

/**
 * * Created by rfcardenas
 */
public class Network {
    public static boolean isAvaiblePort(int port) {
        try (Socket ignored = new Socket("localhost", port)) {
            return false;
        } catch (IOException ignored) {
            return true;
        }
    }
}
