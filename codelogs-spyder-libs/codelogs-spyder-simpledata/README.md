# CODELOGS SIMPLEDATA

Este modulo proporciona utilidad a nivel de Persitencia
es un TOP sobre ORMLITE, para minimizar la cantidad de 
codigo al momento de crear el nivel de datos


Uso:

```java
1) Crear una interfaz ejmp 

public interface RepositoryTest extends SimpleDataRepository<EntityTest,Integer> {
     @QueryID(CustomQuery.QUERYBYNAME)
     EntityTest selectByName(@QueryParam(EntityTest.NAME_COL) String value);
}

```

Por defecto si solo se requiere CRUD, no es necesario añadir el custom provider
en caso de tener consultas personalizadas utilizar:

```java
RexSimpleData rexSimpleData = RexSimpleData.builder()
                .addServiceRepository(ServiceRepository.builder(connection,WorkSession.class,Integer.class)
                        .addProvider(SharedQuery.findUnSynclast)
                        .addProvider(SharedQuery.findUnSyncEntries)
                        .addProvider(SessionQueries.findAllSessionUnsyncFrom)
                        .generationType(GenerationType.CREATE_TABLES_IFNOTEXIST)
                        .build())
                .build();
        
this.fileCodeRepositoryDataRepo =  rexSimpleData.createRepository(FileCodeRepository.class);

```

Esto crea las tablas de la BD, la implementación de los metodos en la interfaz

OJO: Aun falta por desarrollar mas cosas, para el proyecto cumple su funcionalidad.