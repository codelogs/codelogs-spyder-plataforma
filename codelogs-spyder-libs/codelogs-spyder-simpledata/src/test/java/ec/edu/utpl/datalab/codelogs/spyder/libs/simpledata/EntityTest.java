package ec.edu.utpl.datalab.codelogs.spyder.libs.simpledata;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;

/**
 * * Created by rfcardenas
 */
public class EntityTest {

    public static final String NAME_COL = "name";

    @DatabaseField(dataType = DataType.INTEGER, generatedId = true)
    private int id;

    @DatabaseField
    private int value;

    @DatabaseField
    private String name;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int hashCode() {
        return id;
    }
}
