package ec.edu.utpl.datalab.codelogs.spyder.libs.simpledata;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.QueryBuilder;
import ec.edu.utpl.datalab.codelogs.spyder.libs.simpledata.queryset.ComplexQuery;

/**
 * * Created by rfcardenas
 */
public class CustomQuery {
    public static final String QUERYBYNAME = "@name";

    public static final QueryWrapper findAllSessionUnsyncFrom =
        new QueryWrapper((ComplexQuery) (val, daos) -> {
            Dao<EntityTest, Integer> dao = daos.loadService(EntityTest.class);
            QueryBuilder<EntityTest, Integer> sesqlb = dao.queryBuilder();
            return sesqlb.
                where().eq(EntityTest.NAME_COL, val.get(EntityTest.NAME_COL))
                .query();
        }, QUERYBYNAME);
}
