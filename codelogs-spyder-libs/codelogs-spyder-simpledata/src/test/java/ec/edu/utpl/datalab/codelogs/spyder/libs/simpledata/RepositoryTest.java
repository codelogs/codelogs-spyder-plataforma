package ec.edu.utpl.datalab.codelogs.spyder.libs.simpledata;

import ec.edu.utpl.datalab.codelogs.spyder.libs.simpledata.annotations.QueryID;
import ec.edu.utpl.datalab.codelogs.spyder.libs.simpledata.annotations.QueryParam;

/**
 * * Created by rfcardenas
 */
public interface RepositoryTest extends SimpleDataRepository<EntityTest, Integer> {
    @QueryID(CustomQuery.QUERYBYNAME)
    EntityTest selectByName(@QueryParam(EntityTest.NAME_COL) String value);
}
