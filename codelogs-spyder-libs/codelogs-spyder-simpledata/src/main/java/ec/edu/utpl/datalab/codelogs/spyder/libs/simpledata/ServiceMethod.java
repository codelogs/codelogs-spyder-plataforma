/*
 * The MIT License (MIT)
 * =========================================================================================================
 *
 * Copyright (c) 10/10/16 12:30 Modulo  Proyecto rexcode-simpledata  Autor [rfcardenas]
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY
 * , WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ec.edu.utpl.datalab.codelogs.spyder.libs.simpledata;


import ec.edu.utpl.datalab.codelogs.spyder.libs.simpledata.annotations.OBSERVABLE;
import ec.edu.utpl.datalab.codelogs.spyder.libs.simpledata.annotations.QueryID;
import ec.edu.utpl.datalab.codelogs.spyder.libs.simpledata.queryset.Query;
import ec.edu.utpl.datalab.codelogs.spyder.libs.simpledata.queryset.QueryRunner;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.Optional;

/**
 * * Created by rfcardenas
 */
public class ServiceMethod {
    private final String queryID;
    private final Query query;
    private final boolean observable;

    private ServiceMethod(ServiceMethodBuilder serviceMethodBuilder) {
        this.queryID = serviceMethodBuilder.queryId;
        this.observable = serviceMethodBuilder.observable;
        this.query = serviceMethodBuilder.query;
    }

    public static ServiceMethodBuilder builder(Method method, ServiceRepository repository) {
        return new ServiceMethodBuilder(method, repository);
    }

    public String getQueryID() {
        return queryID;
    }

    public Query getQuery() {
        return query;
    }

    public boolean isObservable() {
        return observable;
    }

    public Object accept(QueryRunner queryRunner) throws Exception {
        return query.accept(queryRunner);
    }

    static class ServiceMethodBuilder {
        private Method method;
        private String queryId;
        private boolean observable = false;
        private Query query;
        private ServiceRepository<Object, Object> repository;

        public ServiceMethodBuilder(Method method, ServiceRepository repository) {
            this.method = method;
            this.repository = repository;
        }

        // 0
        private void resolveMethod() {
            for (Annotation annotation : method.getAnnotations()) {
                parseAnnotations(annotation);
            }
            resolveQuery();
        }

        // 1
        public void parseAnnotations(Annotation annotation) {
            if (annotation instanceof QueryID) {
                this.queryId = ((QueryID) annotation).value();
                if (queryId == null)
                    throw new RexServiceException("Imposible determinar el ID");
            }
            if (annotation instanceof OBSERVABLE) {
                this.observable = true;
            }
            resolveQuery();
        }

        //2
        public void resolveQuery() {
            Optional<Query> provider = repository.
                queries.stream()
                .filter(service -> service.get_id().equals(queryId))
                .map(QueryWrapper::get_query)
                .findFirst();
            if (!provider.isPresent()) {
                throw new RexServiceException("No hay provedor para " + queryId + " en el servicio " + this.getClass());
            }
            this.query = provider.get();
        }

        public ServiceMethod build() {
            resolveMethod();
            return new ServiceMethod(this);
        }
    }
}
