/*
 * The MIT License (MIT)
 * =========================================================================================================
 *
 * Copyright (c) 10/10/16 12:30 Modulo  Proyecto rexcode-simpledata  Autor [rfcardenas]
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY
 * , WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ec.edu.utpl.datalab.codelogs.spyder.libs.simpledata;

import ec.edu.utpl.datalab.codelogs.spyder.libs.simpledata.queryset.SimpleQuery;

import java.util.HashSet;
import java.util.Set;

/**
 * * Created by rfcardenas
 */
public final class BaseProvider {
    public static final String CREATE = "create";
    public static final String UPDATE = "update";
    public static final String SELECT_ID = "selectId";
    public static final String SELECT_ALL = "selectAll";
    public static final String DELETE = "delete";
    public static final String ID_EXITE = "id_Existe";
    private static final Set<QueryWrapper> queries = new HashSet<>();

    public BaseProvider() {
        if (queries.isEmpty())
            init();
    }

    public static <T, K> SimpleQuery q1() {
        return (val, dao1) -> dao1.createIfNotExists(val.get("data"));
    }

    public static <T, K> SimpleQuery q2() {
        return (val, dao1) -> dao1.createOrUpdate(val.get("data"));
    }

    public static <T, K> SimpleQuery q3() {
        return (val, dao1) -> dao1.queryForId((K) val.get("id"));
    }

    public static <T, K> SimpleQuery q4() {
        return (val, dao1) -> dao1.queryForAll();
    }

    public static <T, K> SimpleQuery q5() {
        return (val, dao1) -> dao1.delete(val.get("data"));
    }

    public static <T, K> SimpleQuery q6() {
        return (val, dao1) -> dao1.idExists(val.get("data"));
    }

    public void init() {
        queries.add(new QueryWrapper(q1(), CREATE));
        queries.add(new QueryWrapper(q2(), UPDATE));
        queries.add(new QueryWrapper(q3(), SELECT_ID));
        queries.add(new QueryWrapper(q4(), SELECT_ALL));
        queries.add(new QueryWrapper(q5(), DELETE));
        queries.add(new QueryWrapper(q6(), ID_EXITE));
    }

    public Set<QueryWrapper> getQueries() {
        return queries;
    }
}
