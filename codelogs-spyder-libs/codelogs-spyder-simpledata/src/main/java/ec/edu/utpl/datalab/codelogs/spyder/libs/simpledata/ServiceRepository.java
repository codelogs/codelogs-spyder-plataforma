/*
 * The MIT License (MIT)
 * =========================================================================================================
 *
 * Copyright (c) 10/10/16 12:30 Modulo  Proyecto rexcode-simpledata  Autor [rfcardenas]
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY
 * , WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ec.edu.utpl.datalab.codelogs.spyder.libs.simpledata;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.table.TableUtils;
import ec.edu.utpl.datalab.codelogs.spyder.libs.simpledata.annotations.QueryParam;
import ec.edu.utpl.datalab.codelogs.spyder.libs.simpledata.annotations.QueryProvider;
import ec.edu.utpl.datalab.codelogs.spyder.libs.simpledata.queryset.Query;
import rx.subjects.PublishSubject;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * * Created by rfcardenas
 */
public final class ServiceRepository<T, ID> {
    protected final Set<QueryWrapper> queries;
    private final Map<Method, ServiceMethod> cache = new HashMap();
    private final Connection connection;
    private final Dao dao;
    private final Class target;
    private final PublishSubject subject = PublishSubject.create();

    public ServiceRepository(ServiceBuilder serviceBuilder) {
        this.connection = serviceBuilder.connection;
        this.dao = serviceBuilder.dao;
        this.target = serviceBuilder.target;
        this.queries = serviceBuilder.querySet;
    }

    public static <T, ID> ServiceBuilder<T, ID> builder(Connection connection, Class<T> target, Class<ID> id) {
        return new ServiceBuilder<>(connection, target);
    }

    private Map<String, Object> parametros(Method method, Object... args) {
        HashMap<String, Object> argumentos = new HashMap<>();
        for (Parameter parameter : method.getParameters()) {
            QueryParam q = parameter.getAnnotation(QueryParam.class);
            int argIndex = Integer.parseInt(parameter.getName().replaceAll("arg", ""));
            argumentos.put(q.value(), args[argIndex]);
        }
        return argumentos;
    }

    protected ServiceMethod loadServiceMethod(Method method) {
        ServiceMethod serviceMethod = null;
        synchronized (this) {
            serviceMethod = cache.get(method);
            if (serviceMethod == null) {
                serviceMethod = ServiceMethod.builder(method, this)
                    .build();
                cache.put(method, serviceMethod);
            }
        }
        return serviceMethod;
    }

    protected Dao<T, ID> loadServiceDao() {
        return dao;
    }

    protected Class getTarget() {
        return target;
    }

    public static class ServiceBuilder<T, ID> {
        private Connection connection;
        private GenerationType generationType;
        private Dao<T, ID> dao;
        private Set<QueryWrapper> querySet = new HashSet<>();
        private Class target;

        public ServiceBuilder(Connection connection, Class target) {
            this.connection = connection;
            this.target = target;
        }

        public ServiceBuilder<T, ID> generationType(GenerationType type) {
            this.generationType = type;
            return this;
        }

        public ServiceBuilder<T, ID> addProvider(QueryWrapper queryWrapper) {
            querySet.add(queryWrapper);
            return this;
        }

        public ServiceRepository addProvider(Class provider) {
            return null;
        }

        private void buildStep() {
            BaseProvider baseProvider = new BaseProvider();
            querySet.addAll(baseProvider.getQueries());
            try {
                this.dao = DaoManager.createDao(connection.connectionSource, target);
                this.dao.setObjectCache(false);
                if (generationType == GenerationType.CREATE_TABLES) {
                    TableUtils.createTable(connection.connectionSource, target);
                }
                if (generationType == GenerationType.CREATE_TABLES_IFNOTEXIST) {
                    TableUtils.createTableIfNotExists(connection.connectionSource, target);
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        public ServiceRepository<T, ID> build() {
            buildStep();
            return new ServiceRepository(this);
        }
    }

    class ReaderProviders {
        Set<Query> querySet = new HashSet<>();

        public Set<Query> process(Class clazz) throws IllegalAccessException {
            for (Field field : clazz.getFields()) {
                inspectFiled(field);
            }
            return querySet;
        }

        private void inspectFiled(Field field) throws IllegalAccessException {
            QueryProvider qp = field.getAnnotation(QueryProvider.class);
            if (qp != null && field.get(null) instanceof Query) {
                querySet.add((Query) field.get(null));
            }
        }

        private String procesAnnotation(Annotation annotation) {
            String value = null;
            if (annotation instanceof QueryProvider) {
                value = ((QueryProvider) annotation).value();
            }
            return value;
        }
    }

}
