/*
 * The MIT License (MIT)
 * =========================================================================================================
 *
 * Copyright (c) 10/10/16 12:30 Modulo  Proyecto rexcode-simpledata  Autor [rfcardenas]
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY
 * , WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ec.edu.utpl.datalab.codelogs.spyder.libs.simpledata;


import com.j256.ormlite.jdbc.JdbcConnectionSource;
import com.j256.ormlite.support.ConnectionSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.sql.SQLException;

/**
 * * Created by rfcardenas
 */
public final class Connection {
    private static final Logger log = LoggerFactory.getLogger(Connection.class);

    private final String dbname;
    protected ConnectionSource connectionSource;
    private String prefix = "";

    private Connection(String dbname) {
        System.out.println(dbname);
        this.prefix = "jdbc:sqlite:";
        this.dbname = dbname;
        try {
            Class.forName("org.sqlite.JDBC");
            this.connectionSource = new JdbcConnectionSource(prefix.concat(dbname));
        } catch (SQLException e) {
            log.error("Imposible crear conexión a la bd {}",dbname);
            throw new RexServiceException("Imposible crear la conexion a la bd " + dbname);
        } catch (ClassNotFoundException e) {
            log.error("Clase no encontrada ",e);
        }
    }

    public static Connection newConnection(String dbname) {
        return new Connection(dbname);
    }

    public boolean isConnects() {
        if (connectionSource == null) {
            return false;
        }
        return true;
    }

    public void closeConnection() {
        try {
            this.connectionSource.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}
