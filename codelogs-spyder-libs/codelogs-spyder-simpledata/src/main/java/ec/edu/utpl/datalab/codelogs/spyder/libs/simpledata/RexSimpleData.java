/*
 * The MIT License (MIT)
 * =========================================================================================================
 *
 * Copyright (c) 10/10/16 12:30 Modulo  Proyecto rexcode-simpledata  Autor [rfcardenas]
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY
 * , WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ec.edu.utpl.datalab.codelogs.spyder.libs.simpledata;


import com.j256.ormlite.dao.Dao;
import ec.edu.utpl.datalab.codelogs.spyder.libs.simpledata.annotations.QueryParam;
import ec.edu.utpl.datalab.codelogs.spyder.libs.simpledata.queryset.QueryRunner;

import java.lang.reflect.*;
import java.util.*;
import java.util.stream.Collectors;

/**
 * * Created by rfcardenas
 */
public final class RexSimpleData {
    private final Map<String, ServiceRepository> serviceCache = new HashMap<>();
    private final Set<ServiceRepository> serviceRepositorySet;
    private final Service service;

    private RexSimpleData(RexSimpleDataBuilder builder) {
        this.serviceRepositorySet = builder.serviceRepositorySet;
        this.service = new Service();
    }

    public static RexSimpleDataBuilder builder() {
        return new RexSimpleDataBuilder();
    }

    public <R extends SimpleDataRepository> R createRepository(Class<R> repository) {
        String identidad = identidad(repository);
        ServiceRepository serviceRepository = loadService(identidad);
        // daoMap.put(repository,serviceRepository.loadServiceDao());

        return (R) Proxy.newProxyInstance(repository.getClassLoader(), new Class<?>[]{repository},
            (InvocationHandler) (proxy, method, args) -> {
                if (method.getDeclaringClass() == Object.class) {
                    return method.invoke(this, args);
                }
                ServiceRepository s = loadService(identidad);
                ServiceMethod serviceMethod = s.loadServiceMethod(method);
                QueryRunner queryRunner = new DefaultQueryRunner(parametros(method, args), s.loadServiceDao(), service);
                Object result = serviceMethod.accept(queryRunner);
                return result;
            });

    }

    private Map<String, Object> parametros(Method method, Object... args) {
        HashMap<String, Object> argumentos = new HashMap<>();
        for (Parameter parameter : method.getParameters()) {
            QueryParam q = parameter.getAnnotation(QueryParam.class);
            int argIndex = Integer.parseInt(parameter.getName().replaceAll("arg", ""));
            argumentos.put(q.value(), args[argIndex]);
        }
        return argumentos;
    }

    private ServiceRepository loadService(String key) {
        synchronized (this) {
            if (!serviceCache.containsKey(key)) {
                //System.out.println("Servicio cargado desde x busqueda");
                Optional<ServiceRepository> s = serviceRepositorySet.stream().filter(serviceRepository -> serviceRepository.getTarget().getName().equals(key))
                    .findFirst();
                if (s.isPresent()) {
                    serviceCache.put(s.get().getTarget().getName(), s.get());
                } else {
                    throw new RexServiceException("Ningun provedor para  " + key);
                }
            }
        }
        return serviceCache.get(key);
    }

    private String identidad(Class r) {
        if (r == null)
            return null;
        Type[] genericInterfaces = r.getGenericInterfaces();
        for (Type genericInterface : genericInterfaces) {
            if (genericInterface instanceof ParameterizedType) {
                Type[] genericTypes = ((ParameterizedType) genericInterface).getActualTypeArguments();
                return genericTypes[0].getTypeName();
            }
        }
        //intento 2
        return searchTargetFrom(r);
    }

    public String searchTargetFrom(Class aClass) {
        String identidad = null;
        for (Class ele : aClass.getInterfaces()) {
            identidad = identidad(ele);
            if (ele.isInstance(SimpleDataRepository.class) && identidad == null) {
                searchTargetFrom(ele);
            } else {
                break;
            }
        }

        return identidad;
    }

    public static final class RexSimpleDataBuilder {
        private Set<ServiceRepository> serviceRepositorySet = new HashSet<>();

        public RexSimpleDataBuilder addServiceRepository(ServiceRepository serviceRepository) {
            serviceRepositorySet.add(serviceRepository);
            return this;
        }

        public RexSimpleData build() {
            return new RexSimpleData(this);
        }

    }

    public class Service {
        private final Map<Class, Dao> daoMap;

        public Service() {
            this.daoMap = serviceRepositorySet.stream()
                .collect(Collectors.toMap(ServiceRepository::getTarget, ServiceRepository::loadServiceDao));
        }

        public <T, K> Dao<T, K> loadService(Class<T> target) {
            Dao<T, K> dao = daoMap.get(target);
            if (dao == null) {
                throw new RexServiceException("Imposible encontrar un provedor para la entidad " + target.getName());
            }
            return dao;
        }

    }
}
