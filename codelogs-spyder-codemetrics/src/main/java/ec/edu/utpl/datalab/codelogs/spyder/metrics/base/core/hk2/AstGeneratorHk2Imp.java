package ec.edu.utpl.datalab.codelogs.spyder.metrics.base.core.hk2;

import org.glassfish.hk2.api.AnnotationLiteral;

/**
 * * Created by rfcardenas
 */
public class AstGeneratorHk2Imp extends AnnotationLiteral<AstGenerator> implements AstGenerator {
    private String lang;

    public AstGeneratorHk2Imp(String lang) {
        this.lang = lang;
    }

    @Override
    public String value() {
        return lang;
    }

}
