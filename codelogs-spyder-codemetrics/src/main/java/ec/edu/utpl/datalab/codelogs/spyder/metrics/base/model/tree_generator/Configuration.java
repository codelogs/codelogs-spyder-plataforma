/*
 * The MIT License (MIT)
 * =========================================================================================================
 *
 * Copyright (c) 10/10/16 12:30 Modulo  Proyecto rexcode-metrics-service  Autor [rfcardenas]
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY
 * , WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ec.edu.utpl.datalab.codelogs.spyder.metrics.base.model.tree_generator;

import org.jvnet.hk2.annotations.Service;

/**
 * * Created by rfcardenas
 */
@Service
public class Configuration {
    String path_src = "src"; // -> usado como punto de creacion para el spaceRoot
    String path_test = "test"; //-> usado para la creacion del space test

    public Configuration() {
    }

    public Configuration(String path_src, String path_test) {
        this.path_src = path_src;
        this.path_test = path_test;
    }

    public String getPath_src() {
        return path_src;
    }

    public void setPath_src(String path_src) {
        this.path_src = path_src;
    }

    public String getPath_test() {
        return path_test;
    }

    public void setPath_test(String path_test) {
        this.path_test = path_test;
    }
}
