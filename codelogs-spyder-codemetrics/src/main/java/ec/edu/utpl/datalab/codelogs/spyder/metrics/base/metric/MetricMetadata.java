package ec.edu.utpl.datalab.codelogs.spyder.metrics.base.metric;


import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.model.tree.Node;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.model.tree.NodeKink;

import java.util.List;
import java.util.Optional;
import java.util.Set;

/**
 * * Created by rfcardenas
 */
public interface MetricMetadata {
    String getUuid();

    String getName();

    String getHelp();

    String getAbrv();

    String getCreator();

    String getCategory();

    Optional<ErrorHandler> errorHandler();

    List<String> getKeyWord();

    NodeKink getNodeKinkTarget();

    Class<Node> getNodeTarget();

    Set<Class<? extends MetricProvider>> getDepedencies();
}
