/*
 * The MIT License (MIT)
 * =========================================================================================================
 *
 * Copyright (c) 10/10/16 12:30 Modulo  Proyecto rexcode-metrics-service  Autor [rfcardenas]
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY
 * , WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ec.edu.utpl.datalab.codelogs.spyder.metrics.base.core;


import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.metric.MetricProvider;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.metric.ModuleMetric;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * **************** Representa un vertice del grafo ***********************
 * * Created by rfcardenas
 */
public final class MetricVerticle implements Comparable<MetricVerticle> {
    private final String providerName;
    private final ModuleMetric specification;
    private boolean isEnable = true;

    /**
     * @param provider
     */
    public MetricVerticle(MetricProvider provider) {
        Objects.requireNonNull(provider);
        this.providerName = provider.getClass().getName();
        this.specification = provider.specification();

    }

    public ModuleMetric getSpecification() {
        return specification;
    }

    public String getProviderId() {
        return providerName;
    }

    public boolean isEnable() {
        return isEnable;
    }

    /**
     * crea un provedor de especficaciones (utilizado por el instalador)
     *
     * @param specificacion
     * @return
     */
    public Set<MetricProvider> getProviders() {
        Set<MetricProvider> providerSet = new HashSet<>();
        MetricProvider provider = null;
        for (Class<? extends MetricProvider> aClass : specification.getDepedencies()) {
            MetricVerticle metricVerticle = null;
            try {
                provider = aClass.newInstance();
            } catch (NullPointerException | InstantiationException | IllegalAccessException e) {
                e.printStackTrace();
            }
            if (provider != null) providerSet.add(provider);
        }
        return providerSet;
    }

    public boolean isSameProvider(Class<? extends MetricProvider> provider) {
        return provider.getName().equals(providerName);
    }


    @Override
    public int compareTo(MetricVerticle o) {
        return o.getProviderId().compareTo(o.getProviderId());
    }

    @Override
    public String toString() {
        return providerName;
    }


    public long hashProvider() {
        return providerName.hashCode();
    }
}
