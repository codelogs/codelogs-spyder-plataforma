package ec.edu.utpl.datalab.codelogs.spyder.metrics.base.metric;


import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.domain.Measure;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.holders.ResultHolder;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.model.tree.elements.NodeProject;

/**
 * * Created by rfcardenas
 */
public class test {
    public static void main(String[] args) {
        ActionCached<NodeProject> actionCached = new ActionCached<NodeProject>() {
            @Override
            public Measure execute(NodeProject node, ResultHolder cache) throws MeasureBadException {
                return null;
            }
        };
        DefaultFormulaExecutor defaultFormulaExecutor = new DefaultFormulaExecutor(null, null);
        defaultFormulaExecutor.execute(actionCached);
    }
}
