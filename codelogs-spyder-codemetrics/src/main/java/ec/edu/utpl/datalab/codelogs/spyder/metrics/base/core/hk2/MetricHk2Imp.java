package ec.edu.utpl.datalab.codelogs.spyder.metrics.base.core.hk2;

import org.glassfish.hk2.api.AnnotationLiteral;

/**
 * * Created by rfcardenas
 */
public class MetricHk2Imp extends AnnotationLiteral<Metric> implements Metric {
    private String lang;

    @Override
    public String value() {
        return lang;
    }
}
