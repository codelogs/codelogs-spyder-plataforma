/*
 * The MIT License (MIT)
 * =========================================================================================================
 *
 * Copyright (c) 10/10/16 12:30 Modulo  Proyecto rexcode-metrics-service  Autor [rfcardenas]
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY
 * , WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ec.edu.utpl.datalab.codelogs.spyder.metrics.base.domain;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

/**
 * * Created by rfcardenas
 */
public class Metric {
    public final String name;
    public final String help_user;
    public final String abbreviation;
    public final String creator;
    public final String category;
    public final List<String> key_words;

    private Metric(Builder builder) {
        this.name = builder.name;
        this.help_user = builder.help_user;
        this.abbreviation = builder.abbreviation;
        this.creator = builder.creator;
        this.key_words = Collections.unmodifiableList(builder.keyWords);
        this.category = "default";
        System.out.println("---------------------------------");
        System.out.println(key_words);
    }

    public static Builder builder(String name) {
        return new Builder(name);
    }

    public static class Builder {
        private String name = "";
        private String help_user = "";
        private String abbreviation = "";
        private String creator = "";
        private List<String> keyWords = new ArrayList<>();
        private String languaje = "";

        public Builder(String name) {
            this.name = name;
        }

        public Builder name(String name) {
            this.name = name;
            return this;
        }

        public Builder user_help(String help) {
            this.help_user = help;
            return this;
        }

        public Builder abbreviation(String abbreviation) {
            this.abbreviation = abbreviation;
            return this;
        }

        public Builder keyWord(String keyWord) {
            System.out.println("ADD KEYWORKD");
            keyWords.add(Objects.requireNonNull(keyWord, "no se aceptan palabras clave nulas"));
            return this;
        }

        public Builder creator(String creator) {
            this.creator = creator;
            return this;
        }

        public Metric build() {
            return new Metric(this);
        }
    }

}
