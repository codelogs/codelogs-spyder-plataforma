/*
 * The MIT License (MIT)
 * =========================================================================================================
 *
 * Copyright (c) 10/10/16 12:30 Modulo  Proyecto rexcode-metrics-service  Autor [rfcardenas]
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY
 * , WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ec.edu.utpl.datalab.codelogs.spyder.metrics.base.core;

import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.model.tree_generator.GeneratorFragment;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.model.tree_generator.GeneratorProject;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.model.tree_generator.IGeneratorFragment;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.model.tree_generator.IGeneratorProject;
import org.glassfish.hk2.api.ServiceLocator;
import org.glassfish.hk2.api.ServiceLocatorFactory;
import org.jvnet.hk2.annotations.Service;

/**
 * * Created by rfcardenas
 */
@Service
public final class Generators {
    private ServiceLocator SERVICES = ServiceLocatorFactory.getInstance().create("core_services");

    public Generators() {
    }

    public IGeneratorProject generatorProject() {
        return SERVICES.getService(GeneratorProject.class);
    }

    public IGeneratorFragment generatorFragment() {
        return SERVICES.getService(GeneratorFragment.class);
    }
}
