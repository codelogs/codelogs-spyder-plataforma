/*
 * The MIT License (MIT)
 * =========================================================================================================
 *
 * Copyright (c) 10/10/16 12:30 Modulo  Proyecto rexcode-metrics-service  Autor [rfcardenas]
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY
 * , WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ec.edu.utpl.datalab.codelogs.spyder.metrics.base.model.tree_traverser.spliterators;


import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.model.tree.Node;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.function.Consumer;

/**
 * * Created by rfcardenas
 */
public class BreadFirstTreeSpliterator extends TreeSpliterator {
    private Deque<Node> result = new ArrayDeque<>();
    private Deque<Node> deque = new ArrayDeque<>();


    public BreadFirstTreeSpliterator(Node node) {
        super(node);
        buildList(node);
    }

    @Override
    public void buildList(Node node) {
        deque.addFirst(node);
        while (!deque.isEmpty()) {
            Node node1 = deque.pollFirst();
            node1.getChildren().forEach(deque::add);
            result.add(node1);
        }
    }

    @Override
    public boolean tryAdvance(Consumer<? super Node> action) {
        if (result.isEmpty()) return false;
        Node e = result.poll();
        action.accept(e);
        return !result.isEmpty();
    }

    @Override
    public long estimateSize() {
        return 0;
    }
}
