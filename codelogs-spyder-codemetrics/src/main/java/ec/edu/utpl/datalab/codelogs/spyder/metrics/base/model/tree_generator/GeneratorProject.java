/*
 * The MIT License (MIT)
 * =========================================================================================================
 *
 * Copyright (c) 10/10/16 12:30 Modulo  Proyecto rexcode-metrics-service  Autor [rfcardenas]
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY
 * , WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ec.edu.utpl.datalab.codelogs.spyder.metrics.base.model.tree_generator;

import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.holders.DefaultTreeHolder;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.holders.TreeHolderEditable;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.model.tree.elements.NodeFragmentRoot;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.model.tree.elements.NodeProject;
import org.apache.commons.vfs2.FileObject;
import org.apache.commons.vfs2.FileSystemException;
import org.glassfish.hk2.api.DescriptorVisibility;
import org.glassfish.hk2.api.Visibility;
import org.jvnet.hk2.annotations.Service;

import javax.inject.Inject;
import java.util.Optional;

/**
 * * Created by rfcardenas
 */
@Service
@Visibility(DescriptorVisibility.LOCAL)
public class GeneratorProject implements IGeneratorProject {
    private Configuration configuration;
    private IGeneratorFragmentRoot generatorFragmentRoot;
    private SubProjectSrategyLocator locator;
    private TreeHolderEditable treeHolderEditable;

    @Inject
    public GeneratorProject(Configuration configuration, GeneratorFragmentRoot generatorFragmentRoot, DefaultTreeHolder projectRepository) {
        this.configuration = configuration;
        this.generatorFragmentRoot = generatorFragmentRoot;
        this.treeHolderEditable = projectRepository;
    }

    public Optional<NodeProject> create(FileObject reference_root_project) {
        NodeProject nodeProject = null;
        try {
            FileObject src_project = reference_root_project.resolveFile(configuration.getPath_src());
            if (src_project != null) {
                nodeProject = new NodeProject(reference_root_project);
                Optional<NodeFragmentRoot> spaceFragmentResult = generatorFragmentRoot.create(src_project);
                spaceFragmentResult.ifPresent(nodeProject::setRootFragment);
            } else {
                System.out.println("No se ha encontrado los directoriores de codigo para el proyecto");
            }
        } catch (FileSystemException e) {
            e.printStackTrace();
        }
        if (nodeProject != null) treeHolderEditable.save(reference_root_project, nodeProject);
        return Optional.ofNullable(nodeProject);
    }

}
