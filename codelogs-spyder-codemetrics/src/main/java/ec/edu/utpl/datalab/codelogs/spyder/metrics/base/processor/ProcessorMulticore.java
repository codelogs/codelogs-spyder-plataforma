package ec.edu.utpl.datalab.codelogs.spyder.metrics.base.processor;

import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.core.MetricVerticle;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.domain.Measure;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.metric.ActionCached;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.metric.DefaultFormulaExecutor;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.metric.FormulaExecutable;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.metric.FormulaExecutor;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.model.tree.Node;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.model.tree_traverser.spliterators.DeepFirstTreeSpliterator;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.holders.*;
import org.jgrapht.graph.DefaultEdge;
import org.jgrapht.graph.UnmodifiableDirectedGraph;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import rx.Observable;
import rx.subjects.PublishSubject;

import javax.inject.Inject;
import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.RecursiveTask;
import java.util.stream.Collectors;

/**
 * * Created by rfcardenas
 */
public class ProcessorMulticore implements Processor {
    private final Logger log = LoggerFactory.getLogger(ProcessorMulticore.class);

    private final PublishSubject<ProcessorResult> oSubject = PublishSubject.create();
    private final UnmodifiableDirectedGraph<MetricVerticle, DefaultEdge> dagMeasures;
    private final MetricHolder metricHolder;
    private final ResultHolderEditable resultholder;
    private final ForkJoinPool executor;

    @Inject
    public ProcessorMulticore(MetricHolderEditable metricHolder, ResultHolderEditable resultHolder) {
        this.metricHolder = metricHolder;
        this.dagMeasures = metricHolder.asGraph();
        this.executor = new ForkJoinPool(4);
        this.resultholder = resultHolder;
    }

    public void process(Node node, MetricVerticle verticle) {

        if (verticle == null || node == null) {
            System.out.println("RETURN");
            return;
        }

        if (verticle.getSpecification().getNodeKinkTarget().getLevel() <= node.nodeType().getLevel()) {
            if (verticle.getSpecification().getNodeTarget().equals(node.getClass())) {
                try {
                    UnitWork unitWork = new UnitWork(node, verticle);
                    executor.invoke(unitWork);
                } catch (Exception e) {
                    verticle.getSpecification().errorHandler().ifPresent(errorHandler -> {
                        errorHandler.handle(e);
                    });
                }
            }

        } else {
            System.out.println("El objetino no es lacanz {}" + verticle.getSpecification().getName());
        }
    }

    @Override
    public void process(Node tree) {
        double x1 = System.currentTimeMillis();
        Collection<MetricVerticle> verticles = metricHolder.getAll();
        List<MetricVerticle> stackMetrics = new ArrayList<>();
        verticles.forEach(vertxMod -> {
            dagMeasures.edgesOf(vertxMod).stream()
                .filter(depEspec -> !dagMeasures.getEdgeTarget(depEspec).equals(vertxMod))
                .forEach(defaultEdge -> stackMetrics.add(dagMeasures.getEdgeTarget(defaultEdge)));
            stackMetrics.add(vertxMod);
        });
        double x2 = System.currentTimeMillis();

        DeepFirstTreeSpliterator deepFirstTreeSpliterator = DeepFirstTreeSpliterator.create(tree);
        deepFirstTreeSpliterator.forEachRemaining(node -> {
            //  System.out.println("NODE " + node.nodeType());
            Class nodx = node.getClass();
            for (MetricVerticle stackMetric : stackMetrics) {
                Class metx = stackMetric.getSpecification().getNodeTarget();
                // System.out.println("            METRIC " + stackMetric.getSpecification().getName() + " " + stackMetric.getSpecification().getNodeTarget() + " PI " + stackMetric.getProviderId());
                if (nodx.equals(metx)) {
                    //    System.out.println(" Nx  " + nodx + "tarx " + metx);
                    process(node, stackMetric);
                }

            }
        });

        /** while(!stackMetrics.isEmpty()){
         MetricVerticle metricVerticle = stackMetrics.pop();
         DeepFirstTreeSpliterator treeTypeSpliterator =  DeepFirstTreeSpliterator.create(tree);
         treeTypeSpliterator.forEachRemaining(node -> process(node, metricVerticle));
         process(tree, metricVerticle);
         }
         ProcessorResult processorResult = new ProcessorResult();
         processorResult.setEndStatus(ProcessorResult.EndStatus.SUCCESS);
         oSubject.onNext(new ProcessorResult());**/
        deepFirstTreeSpliterator.reset();

        ProcessorResult processorResult = new ProcessorResult();
        PublicResult publicResult = new DefaultPublicResult(resultholder, metricHolder);


        processorResult.setResultHolder(publicResult);
        processorResult.setTime(x2 - x1);
        processorResult.setTotalNodos((int) deepFirstTreeSpliterator.estimateSize());
        oSubject.onNext(processorResult);
    }

    @Override
    public Observable<ProcessorResult> observable() {
        return oSubject.asObservable();
    }

    private class UnitWork extends RecursiveTask<Optional<Measure>> {
        private Node node;
        private MetricVerticle metricVerticle;
        private DefaultResultHolder.DefaultCacheProcess cacheLocal;

        public UnitWork(Node node, MetricVerticle metricVerticle) {
            this.node = node;
            this.metricVerticle = metricVerticle;
        }

        @Override
        protected Optional<Measure> compute() {
            //System.out.println("Ejecutando " + metricVerticle.getProviderId());
            //System.out.println("ON " + node.getElementID());

            FormulaExecutor formulaExecutor = new DefaultFormulaExecutor(resultholder, node);
            Optional<Measure> measure = null;
            try {
                Set<UnitWork> works = split();
                if (hasDepedencies()) {
                    for (UnitWork depedency : works) {
                        depedency.fork();
                    }
                    for (UnitWork depedency : works) {
                        depedency.join();
                    }
                }
                FormulaExecutable formulaExecutable = metricVerticle.getSpecification().formula();
                if (formulaExecutable instanceof ActionCached) {
                    for (UnitWork work : works) {
                        Optional<Measure> result = work.get();
                        if (result.isPresent()) {
                        }
                    }
                }
                measure = formulaExecutable.accept(formulaExecutor);
                if (!measure.isPresent()) {
                    System.out.println(metricVerticle.getProviderId());
                    log.error("Measure BAD ");
                } else {
                    resultholder.save(node, metricVerticle, measure.get());
                }
                //   System.out.println("RETURN " + measure + " " + tree.getName() + " Metric " + metricVerticle.getSpecification().getName());
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }

            return measure;
        }

        private boolean hasDepedencies() {
            return dagMeasures.edgesOf(metricVerticle).stream()
                .filter(depEspec -> !dagMeasures.getEdgeTarget(depEspec).equals(metricVerticle))
                .count() > 0;
        }

        private Set<UnitWork> split() {
            return dagMeasures.edgesOf(metricVerticle).stream()
                .filter(depEspec -> !dagMeasures.getEdgeTarget(depEspec).equals(metricVerticle))
                .map(edge -> createTaskDep(dagMeasures.getEdgeTarget(edge)))
                .collect(Collectors.toSet());

        }

        private UnitWork createTaskDep(MetricVerticle deped) {
            UnitWork newTask = new UnitWork(node, deped);
            return newTask;
        }

        private int keyGenerator(MetricVerticle metricVerticle, Node node) {
            int e = node.getElementID().hashCode();
            int id = metricVerticle.getProviderId().hashCode();
            return algoritm(e, id);
        }

        int algoritm(int k1, int k2) {
            return ((k1 + k2) * (k1 + k2 + 1)) / 2 + k2;
        }
    }
}
