package ec.edu.utpl.datalab.codelogs.spyder.metrics.base.metric;


import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.domain.Measure;

import java.util.Optional;

/**
 * * Created by rfcardenas
 */
public interface FormulaExecutor {
    Optional<Measure> execute(Action action);

    Optional<Measure> execute(ActionCached actionCached);
}
