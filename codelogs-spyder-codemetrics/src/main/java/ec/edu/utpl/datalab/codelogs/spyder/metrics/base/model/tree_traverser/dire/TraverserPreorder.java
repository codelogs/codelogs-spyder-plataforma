/*
 * The MIT License (MIT)
 * =========================================================================================================
 *
 * Copyright (c) 10/10/16 12:30 Modulo  Proyecto rexcode-metrics-service  Autor [rfcardenas]
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY
 * , WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ec.edu.utpl.datalab.codelogs.spyder.metrics.base.model.tree_traverser.dire;


import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.model.tree.Node;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.model.tree.elements.*;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.model.tree_traverser.ElementFunctional;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.model.tree_traverser.IStrategyTraverserFct;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.model.tree_traverser.ITraverser;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.model.tree_traverser.TraverserFunctions;

/**
 * * Created by rfcardenas
 */
public class TraverserPreorder implements ITraverser, IStrategyTraverserFct {
    private TraverserFunctions actions;

    /**
     * Se define el comportamiento para los nodos utilizando forma funcional Program
     *
     * @param actions
     */
    public TraverserPreorder(TraverserFunctions actions) {
        this.actions = actions;
    }

    /**
     * Constructor por defecto para usar como la forma tradicional [no me parece muy bueno -> codigo crece mucho]
     */
    public TraverserPreorder() {
    }


    @Override
    public void traverse(NodeProject node) {
        node.getSubNodeProjectList().forEach(this::traverse);
        this.traverse(node.getRootSpace());
        if (actions != null)
            this.performAction(node, actions.getOnProject());
    }

    @Override
    public void traverse(NodeFragmentRoot node) {
        node.getSubFragments().forEach(this::traverse);
        if (actions != null)
            performAction(node, actions.getOnSpaceRoot());
    }

    @Override
    public void traverse(NodeFragment node) {
        node.getNodeFragmentList().forEach(this::traverse);
        node.getSourceList().forEach(this::traverse);
        if (actions != null)
            performAction(node, actions.getOnSpaceFragment());
    }

    @Override
    public void traverse(NodeFileCode node) {
        node.getNodeAbstractTrees().forEach(this::traverse);
        if (actions != null)
            this.performAction(node, actions.getOnSourceWrapper());
    }

    @Override
    public void traverse(NodeAbstractNode node) {
        node.getClassList().forEach(elementVirtual -> elementVirtual.accept(this));
        if (actions != null)
            this.performAction(node, actions.getOnSourceParsed());
    }

    @Override
    public void traverser(NodeClase node) {
        node.getSubElements().forEach(elementVirtual -> elementVirtual.accept(this));
        if (actions != null)
            this.performAction(node, actions.getOnClass());
    }

    @Override
    public void traverser(NodeMethod node) {
        if (actions != null)
            this.performAction(node, actions.getOnMethod());
    }

    /**
     * Objetivo es permitir extender y ejecutar como el visitor tradicional
     *
     * @param node
     */


    private void performAction(Node node, ElementFunctional functional) {
        if (functional != null) functional.onNode(node);
    }

    public void setActions(TraverserFunctions actions) {
        this.actions = actions;
    }

    @Override
    public TraverserFunctions actions() {
        return (actions == null)
            ? actions = new TraverserFunctions()
            : actions;
    }

    @Override
    public void apply(Node node) {

    }
}
