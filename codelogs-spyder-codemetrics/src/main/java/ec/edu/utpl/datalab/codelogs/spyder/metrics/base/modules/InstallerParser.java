/*
 * The MIT License (MIT)
 * =========================================================================================================
 *
 * Copyright (c) 10/10/16 12:30 Modulo  Proyecto rexcode-metrics-service  Autor [rfcardenas]
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY
 * , WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ec.edu.utpl.datalab.codelogs.spyder.metrics.base.modules;

import com.google.common.collect.HashMultimap;
import org.apache.commons.vfs2.FileObject;
import org.apache.commons.vfs2.FileSystemException;
import org.jvnet.hk2.annotations.Service;

import java.util.HashSet;
import java.util.Set;


/**
 * * Created by rfcardenas
 */
@Service
public class InstallerParser implements RegistreService<ModuleParser> {
    private HashMultimap<String, ModuleParser> parserMap = HashMultimap.create();

    public Set<ModuleParser> parserFor(FileObject fileObject) {

        try {
            if (fileObject.getName().isFile()) {
                String ext = fileObject.getName().getExtension();
                return parserFor(ext);
            }
        } catch (FileSystemException e) {
            e.printStackTrace();
        }
        return new HashSet<>();
    }

    private Set<ModuleParser> parserFor(String ext) {
        return parserMap.get(ext);
    }


    @Override
    public boolean register(ModuleParser module) {
        if (module != null) {
            String ext = module.getSupportSource().getExtension();
            parserMap.put(ext, module);
        }
        return true;
    }
}
