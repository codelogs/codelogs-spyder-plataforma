/*
 * The MIT License (MIT)
 * =========================================================================================================
 *
 * Copyright (c) 10/10/16 12:30 Modulo  Proyecto rexcode-metrics-service  Autor [rfcardenas]
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY
 * , WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ec.edu.utpl.datalab.codelogs.spyder.metrics.base.holders;


import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.core.MetricValidator;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.core.MetricVerticle;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.metric.MetricProvider;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.model.tree.Node;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.model.tree.NodeKink;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.modules.RegistreService;
import org.jgrapht.experimental.dag.DirectedAcyclicGraph;
import org.jgrapht.graph.DefaultEdge;
import org.jgrapht.graph.UnmodifiableDirectedGraph;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.stream.Collectors;

/**
 * * Created by rfcardenas
 */
public class DefaultMetricsHolderEditable implements MetricHolderEditable, RegistreService<MetricProvider> {
    private final Logger log = LoggerFactory.getLogger(DefaultMetricsHolderEditable.class);
    private HashMap<String, MetricVerticle> specificaiones = new HashMap<>();
    private DirectedAcyclicGraph<MetricVerticle, DefaultEdge> dag = new DirectedAcyclicGraph<>(DefaultEdge.class);

    @Override
    public void save(MetricVerticle metricVerticle) {
        specificaiones.put(metricVerticle.getProviderId(), metricVerticle);
    }

    @Override
    public UnmodifiableDirectedGraph<MetricVerticle, DefaultEdge> asGraph() {
        return new UnmodifiableDirectedGraph<>(dag);
    }

    @Override
    public Optional<MetricVerticle> getFrom(long id) {
        return specificaiones.values().stream()
            .filter(vertxMod -> vertxMod.hashProvider() == id)
            .findFirst();
    }

    @Override
    public MetricVerticle getFrom(String provider) {
        return specificaiones.get(provider);
    }

    @Override
    public MetricVerticle getFrom(Class<? extends MetricProvider> provider) {
        String name = provider.getName();
        return getFrom(name);
    }

    @Override
    public List<MetricVerticle> getByTarget(Class<? extends Node> target) {
        return specificaiones.values().stream()
            .filter(vertxMod -> vertxMod.getSpecification().getNodeTarget().equals(target))
            .collect(Collectors.toList());
    }

    @Override
    public Collection<MetricVerticle> getAll() {
        return specificaiones.values();
    }

    @Override
    public List<MetricVerticle> getAllMetricWithTarget(NodeKink nodeKink) {
        return specificaiones.values().stream()
            .filter(vertxMod -> vertxMod.getSpecification().getNodeKinkTarget() == nodeKink)
            .collect(Collectors.toList());
    }

    @Override
    public boolean register(MetricProvider module) {
        System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>><");
        boolean isValid = MetricValidator.validator().isValid(module);
        if (isValid) {
            createDag(module);
        } else {
            log.error("imposible registrar " + module.getClass());
        }
        log.info("Registrando un modulo metricVerticle");
        return false;
    }

    private void createDag(MetricProvider metricProvider) {
        DagBuilder builder = new DagBuilder(dag, metricProvider);
        try {
            builder.createVertices(metricProvider, null);
            builder.printDebugDag();
        } catch (DirectedAcyclicGraph.CycleFoundException e) {
            log.error("Problema al crear el DAG luego de las comprobaciones..");
            e.printStackTrace();
        }
    }

    public UnmodifiableDirectedGraph<MetricVerticle, DefaultEdge> createDagUmt() {
        return new UnmodifiableDirectedGraph<MetricVerticle, DefaultEdge>(dag);
    }

    private class DagBuilder {
        private DirectedAcyclicGraph<MetricVerticle, DefaultEdge> metricDag;
        private MetricProvider orige;

        public DagBuilder(DirectedAcyclicGraph<MetricVerticle, DefaultEdge> metricDag, MetricProvider orige) {
            this.metricDag = metricDag;
            this.orige = orige;
        }

        private void createVertices(MetricProvider nodo, MetricVerticle parent) throws DirectedAcyclicGraph.CycleFoundException {
            MetricVerticle metricVerticle = new MetricVerticle(nodo);
            save(metricVerticle);
            Set<MetricProvider> depedencies = metricVerticle.getProviders();

            metricDag.addVertex(metricVerticle);
            if (parent != null) {
                metricDag.addDagEdge(parent, metricVerticle);
            }
            for (MetricProvider depedency : depedencies) {
                log.info("{} -> {}", depedencies, metricVerticle);
                createVertices(depedency, metricVerticle);
            }

        }

        private void printDebugDag() {
            metricDag.iterator().forEachRemaining(origen -> {
                System.out.println("EDGES DE : " + origen);
                metricDag.edgesOf(origen).stream()
                    .filter(defaultEdge -> !metricDag.getEdgeTarget(defaultEdge).equals(origen))
                    .collect(Collectors.toList())
                    .forEach(System.out::println);
            });
        }
    }
}
