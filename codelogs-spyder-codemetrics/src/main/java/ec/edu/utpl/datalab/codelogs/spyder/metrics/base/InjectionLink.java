/*
 * The MIT License (MIT)
 * =========================================================================================================
 *
 * Copyright (c) 10/10/16 12:30 Modulo  Proyecto rexcode-metrics-service  Autor [rfcardenas]
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY
 * , WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ec.edu.utpl.datalab.codelogs.spyder.metrics.base;

import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.core.Generators;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.core.RootInstallerImp;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.model.tree_generator.*;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.modules.InstallerMetric;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.modules.InstallerParser;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.processor.ProcessorMulticore;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.holders.*;
import org.glassfish.hk2.api.Descriptor;
import org.glassfish.hk2.api.DescriptorVisibility;
import org.glassfish.hk2.utilities.BuilderHelper;

import javax.inject.Singleton;

/**
 * Esta clase proporciona utilidad para  el enlazado de los
 * componentes (para resolver la injeccion de dependencias)
 * proporcionando un unico punto de configuracion para cambiar las implementaciones
 * de los contratos
 * * Created by rfcardenas
 */
public final class InjectionLink {
    /******************* Config Class ******************************/
    public static Descriptor config() {
        return BuilderHelper
            .link(Configuration.class)
            .visibility(DescriptorVisibility.NORMAL)
            .build();
    }

    /******************* Tree Generador *****************************/
    public static Descriptor generatorProject() {
        return BuilderHelper
            .link(GeneratorProject.class)
            .to(IGeneratorProject.class)
            .visibility(DescriptorVisibility.LOCAL)
            .build();
    }

    public static Descriptor generatorFragmentRoot() {
        return BuilderHelper
            .link(GeneratorFragmentRoot.class)
            .to(IGeneratorFragmentRoot.class)
            .visibility(DescriptorVisibility.LOCAL)
            .build();
    }

    public static Descriptor generatorFragment() {
        return BuilderHelper
            .link(GeneratorFragment.class)
            .to(IGeneratorFragment.class)
            .visibility(DescriptorVisibility.LOCAL)
            .build();
    }

    public static Descriptor generatorSource() {
        return BuilderHelper
            .link(GeneratorSource.class)
            .to(IGeneratorSource.class)
            .visibility(DescriptorVisibility.LOCAL)
            .build();
    }

    public static Descriptor generatorObject() {
        return BuilderHelper
            .link(Generators.class)
            .visibility(DescriptorVisibility.LOCAL)
            .build();
    }

    /***************** Procesadores de lenguaje *****************************/
    public static Descriptor installerParser() {
        return BuilderHelper
            .link(InstallerParser.class)
            .visibility(DescriptorVisibility.NORMAL)
            .in(Singleton.class)
            .build();
    }

    public static Descriptor installerMetric() {
        return BuilderHelper
            .link(InstallerMetric.class)
            .visibility(DescriptorVisibility.NORMAL)
            .in(Singleton.class)
            .build();
    }

    public static Descriptor metricHolderEdiable() {
        return BuilderHelper
            .link(DefaultMetricsHolderEditable.class)
            .to(MetricHolderEditable.class)
            .to(MetricHolder.class)
            .visibility(DescriptorVisibility.NORMAL)
            .in(Singleton.class)
            .build();
    }

    public static Descriptor resultHolderEditable() {
        return BuilderHelper
            .link(DefaultResultHolder.class)
            .to(ResultHolderEditable.class)
            .to(ResultHolder.class)
            .visibility(DescriptorVisibility.NORMAL)
            .in(Singleton.class)
            .build();
    }

    public static Descriptor installer() {
        return BuilderHelper
            .link(RootInstallerImp.class)
            .visibility(DescriptorVisibility.NORMAL)
            .in(Singleton.class)
            .build();
    }

    public static Descriptor processor() {
        return BuilderHelper
            .link(ProcessorMulticore.class)
            .visibility(DescriptorVisibility.NORMAL)
            .in(Singleton.class)
            .build();
    }

    public static Descriptor parserRegistry() {
        return BuilderHelper
            .link(InstallerParser.class)
            .visibility(DescriptorVisibility.NORMAL)
            .in(Singleton.class)
            .build();
    }

    public static Descriptor projectRepository() {
        return BuilderHelper
            .link(DefaultTreeHolder.class)
            .visibility(DescriptorVisibility.NORMAL)
            .in(Singleton.class)
            .build();
    }

}
