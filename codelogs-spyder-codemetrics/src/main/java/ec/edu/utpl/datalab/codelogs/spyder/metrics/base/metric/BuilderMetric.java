/*
 * The MIT License (MIT)
 * =========================================================================================================
 *
 * Copyright (c) 10/10/16 12:30 Modulo  Proyecto rexcode-metrics-service  Autor [rfcardenas]
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY
 * , WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ec.edu.utpl.datalab.codelogs.spyder.metrics.base.metric;


import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.core.MapTreekind;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.domain.Metric;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.model.tree.Node;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.model.tree.NodeKink;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.modules.Version;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * * Created by rfcardenas
 */
public final class BuilderMetric {
    public final String UUID;
    public final Metric METRIC;
    public final Class TARGET;
    public final Set<Class<? extends MetricProvider>> DEPEDENCIES;
    public final FormulaExecutable FORMLA;
    public final ErrorHandler ERRORHANDLER;
    public final NodeKink TREE_KIND;

    <T extends Node> BuilderMetric(BuilderSimple _fluent) {
        this.METRIC = _fluent._fluent_l1.metric;
        this.FORMLA = _fluent._fluent_l2.formua;
        this.TARGET = _fluent._fluent_l1.target;
        this.DEPEDENCIES = _fluent._fluent_l2.classDepedencies;
        this.UUID = _fluent._fluent_l1.uuid;
        this.ERRORHANDLER = _fluent._fluent_l2.errorHandler;
        this.TREE_KIND = _fluent._fluent_l1.targetKind;
    }

    <T extends Node> BuilderMetric(BuilderExtend<T> _fluent) {
        this.METRIC = _fluent._fluent_l1.metric;
        this.FORMLA = _fluent._fluent_l2.formua;
        this.TARGET = _fluent._fluent_l1.target;
        this.DEPEDENCIES = _fluent._fluent_l2.classDepedencies;
        this.UUID = _fluent._fluent_l1.uuid;
        this.ERRORHANDLER = _fluent._fluent_l2.errorHandler;
        this.TREE_KIND = _fluent._fluent_l1.targetKind;
    }

    public static <T extends Node> FluentAPICore<T> fluet(Metric info, Class<T> target, String uuid) {
        return new FluentAPICore<T>(target, info, uuid);
    }

    public ModuleMetric build() {
        return new ModuleMetric(this);
    }

    // LEVEl 1
    public static class FluentAPICore<T extends Node> {
        String uuid;
        Class<T> target;
        Metric metric;
        NodeKink targetKind;

        public FluentAPICore(Class<T> target, Metric metric, String uuid) {
            this.target = target;
            this.metric = metric;
            this.uuid = uuid;
            this.targetKind = MapTreekind.targetkindFrom(target);
        }

        public FluentAPISimple<T> metric(Version version) {
            return new FluentAPISimple<T>(version, this);
        }
    }

    //  LEVEL 2
    public static class FluentAPISimple<T extends Node> {
        FluentAPICore<T> _fluent_l1;
        Version version;
        List<MetricProvider> instaDependencies = new ArrayList<>();
        Set<Class<? extends MetricProvider>> classDepedencies = new HashSet<>();
        FormulaExecutable<T> formua;
        ErrorHandler errorHandler;
        boolean onlyCache;

        FluentAPISimple(Version version, FluentAPICore<T> _fluent_l1) {
            this.version = version;
            this._fluent_l1 = _fluent_l1;
        }

        public FluentAPISimple<T> onlyCache(boolean onlyCache) {
            this.onlyCache = onlyCache;
            return this;
        }

        public FluentAPISimple<T> onProblem(ErrorHandler errorHandler) {
            this.errorHandler = errorHandler;
            return this;
        }

        public FluentAPIExtend<T> addDependecy(MetricProvider spect) {
            if (spect.getClass().equals(BuilderMetric.class))
                throw new RuntimeException("No se aceptan depedencias self");
            instaDependencies.add(spect);
            return new FluentAPIExtend<T>(this);
        }

        public FluentAPIExtend<T> addDependecy(Class<? extends MetricProvider> spect) {
            if (spect.getClass().equals(BuilderMetric.class))
                throw new RuntimeException("No se aceptan depedencias self");
            classDepedencies.add(spect);
            return new FluentAPIExtend<T>(this);
        }

        public BuilderSimple<T> formula(FormulaExecutable<T> simpleFormulaExecutable) {
            this.formua = simpleFormulaExecutable;
            return new BuilderSimple<T>(_fluent_l1, this);
        }

    }

    // LEVEL 3
    public static class FluentAPIExtend<T extends Node> {
        FluentAPISimple _fluent_l2;

        FluentAPIExtend(FluentAPISimple _fluent_l2) {
            this._fluent_l2 = _fluent_l2;
        }

        public FluentAPIExtend<T> addDependecy(Class<? extends MetricProvider> spect) {
            _fluent_l2.addDependecy(spect);
            return this;
        }

        public FluentAPIExtend<T> addDependecy(MetricProvider spect) {
            _fluent_l2.addDependecy(spect);
            return this;
        }

        public FluentAPIExtend<T> onProblem(ErrorHandler errorHandler) {
            _fluent_l2.errorHandler = errorHandler;
            return this;
        }

        public BuilderExtend<T> formula(ActionCached<T> action) {
            this._fluent_l2.formula(action);
            return new BuilderExtend<T>(_fluent_l2._fluent_l1, _fluent_l2, this);
        }

    }

    public static class BuilderSimple<T extends Node> {
        FluentAPICore<T> _fluent_l1;
        FluentAPISimple<T> _fluent_l2;

        public BuilderSimple(FluentAPICore<T> _fluent_l1, FluentAPISimple<T> _fluent_l2) {
            this._fluent_l1 = _fluent_l1;
            this._fluent_l2 = _fluent_l2;
        }

        public ModuleMetric ok() {
            BuilderMetric builderMetric = new BuilderMetric(this);
            ModuleMetric moduleMetric = new ModuleMetric(builderMetric);
            return moduleMetric;
        }
    }

    public static class BuilderExtend<T extends Node> {
        FluentAPICore<T> _fluent_l1;
        FluentAPISimple<T> _fluent_l2;
        FluentAPIExtend<T> _fluent_l3;

        public BuilderExtend(FluentAPICore<T> _fluent_l1, FluentAPISimple<T> _fluent_l2, FluentAPIExtend<T> _fluent_l3) {
            this._fluent_l1 = _fluent_l1;
            this._fluent_l2 = _fluent_l2;
            this._fluent_l3 = _fluent_l3;
        }

        public ModuleMetric ok() {
            BuilderMetric builderMetric = new BuilderMetric(this);
            ModuleMetric moduleMetric = new ModuleMetric(builderMetric);
            return moduleMetric;
        }
    }


}

