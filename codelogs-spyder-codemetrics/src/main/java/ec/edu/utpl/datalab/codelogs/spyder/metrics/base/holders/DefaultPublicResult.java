/*
 * The MIT License (MIT)
 * =========================================================================================================
 *
 * Copyright (c) 10/10/16 12:30 Modulo  Proyecto rexcode-metrics-service  Autor [rfcardenas]
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY
 * , WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ec.edu.utpl.datalab.codelogs.spyder.metrics.base.holders;

import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.core.MetricVerticle;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.domain.Measure;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.model.tree.Node;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.model.tree.NodeKink;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * * Created by rfcardenas
 **/
public class DefaultPublicResult implements PublicResult {
    private ResultHolder measureRepository;
    private MetricHolder metricHolder;
    private Set<Node> mesurableNodeSet;
    private Set<MetricVerticle> metricVerticleSet;

    public DefaultPublicResult(ResultHolder measureRepository, MetricHolder metricHolder) {
        this.measureRepository = measureRepository;
        this.metricHolder = metricHolder;
        initStructures();
    }

    private void initStructures() {
        this.mesurableNodeSet = new LinkedHashSet<>();
        this.metricVerticleSet = new LinkedHashSet<>();
        measureRepository.loadAll().stream()
            .map(DefaultResultHolder.CacheEntry::getNode)
            .forEach(mesurableNodeSet::add);

        metricHolder.getAll().forEach(metricVerticleSet::add);
    }


    @Override
    public Node[] getMeasurableElements() {
        return new Node[0];
    }

    @Override
    public MetricVerticle[] loadAllMetrics() {
        MetricVerticle[] metricVerticles = new MetricVerticle[metricVerticleSet.size()];
        return metricVerticleSet.toArray(metricVerticles);
    }


    @Override
    public Measure getMeasureFor(Node tree, MetricVerticle metricVerticle) {
        Optional<Measure> entry = measureRepository.loadFrom(tree, metricVerticle);
        return entry.isPresent() ? entry.get() : new Measure(-1);
    }

    @Override
    public double getAverageFor(MetricVerticle metric, NodeKink level) {
        return 0;
    }

    @Override
    public double getMaxValueFor(MetricVerticle metric, NodeKink level) {
        return 0;
    }

    @Override
    public double getMinValueFor(MetricVerticle metric, NodeKink level) {
        return 0;
    }

    @Override
    public double total(MetricVerticle metric, NodeKink level) {
        return 0;
    }


    @Override
    public List<MetricVerticle> loadAllMetrics(Class target) {
        return metricVerticleSet.stream()
            .filter(x -> x.getSpecification().getNodeTarget().equals(target))
            .collect(Collectors.toList());
    }

    @Override
    public List<Measure> getAllMeasures(MetricVerticle metricVerticle, NodeKink treeKind) {
        List<DefaultResultHolder.CacheEntry> cacheEntries = measureRepository.loadAll().stream()
            .filter(x -> x.metricVerticle.getSpecification().getNodeKinkTarget().equals(treeKind))
            .collect(Collectors.toList());
        return cacheEntries.stream().map(DefaultResultHolder.CacheEntry::getMeasure).collect(Collectors.toList());
    }

    @Override
    public List<Measure> loadAllMeasures(NodeKink treeKind) {
        List<DefaultResultHolder.CacheEntry> cacheEntries = measureRepository.loadAll().stream()
            .filter(x -> x.metricVerticle.getSpecification().getNodeKinkTarget().equals(treeKind))
            .collect(Collectors.toList());
        return cacheEntries.stream().map(DefaultResultHolder.CacheEntry::getMeasure).collect(Collectors.toList());
    }

    @Override
    public Measure loadSingleByName(String name, MetricVerticle metricVerticle) {
        //System.out.println("_________________________LBN__________________________");
        Optional<DefaultResultHolder.CacheEntry> entry = measureRepository.loadAll()
            .stream().filter(cacheEntry -> {
                String namenoded = cacheEntry.getNode().getName();
                MetricVerticle metricVerticle1 = cacheEntry.getMetricVerticle();
                //          System.out.println( namenoded+ " ?1 " + name + " = " + (name.equals(namenoded)) );
                //          System.out.println( metricVerticle1+ " ?2 " + metricVerticle1 + " = " + (metricVerticle1.equals(metricVerticle)) );
                return cacheEntry.getNode().getName().equals(name)
                    && cacheEntry.getMetricVerticle().equals(metricVerticle);
            }).findFirst();
        if (entry.isPresent()) {
            return entry.get().getMeasure();
        }
        return new Measure(-1);
    }

    @Override
    public Measure loadSingleByPath(String pathtree, MetricVerticle metricVerticle) {
        Optional<DefaultResultHolder.CacheEntry> entry = measureRepository.loadAll()
            .stream().filter(cacheEntry -> {
                return cacheEntry.getNode().getElementID().equals(pathtree)
                    && cacheEntry.getMetricVerticle().equals(metricVerticle);
            }).findFirst();
        if (entry.isPresent()) {
            return entry.get().getMeasure();
        }
        return new Measure(-1);
    }

    @Override
    public List<MetricVerticle> loadAllMetrics(NodeKink node) {
        return metricVerticleSet.stream()
            .filter(metricVerticle -> metricVerticle.getSpecification().getNodeKinkTarget().equals(node))
            .collect(Collectors.toList());
    }

}
