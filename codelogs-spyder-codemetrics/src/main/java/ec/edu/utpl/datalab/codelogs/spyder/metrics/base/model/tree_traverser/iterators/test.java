/*
 * The MIT License (MIT)
 * =========================================================================================================
 *
 * Copyright (c) 10/10/16 12:30 Modulo  Proyecto rexcode-metrics-service  Autor [rfcardenas]
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY
 * , WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ec.edu.utpl.datalab.codelogs.spyder.metrics.base.model.tree_traverser.iterators;


import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.model.tree.Node;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.model.tree.elements.NodeClase;

/**
 * * Created by rfcardenas
 */
public class test {
    public static void main(String[] args) {
        Node f = new NodeClase("F");
        Node b = new NodeClase("B");
        Node a = new NodeClase("A");
        Node d = new NodeClase("D");
        Node c = new NodeClase("C");
        Node e = new NodeClase("E");
        Node g = new NodeClase("G");
        Node i = new NodeClase("I");
        Node h = new NodeClase("H");

        f.setAsParentOf(b);
        b.setAsParentOf(a);
        b.setAsParentOf(d);
        d.setAsParentOf(c);
        d.setAsParentOf(e);
        f.setAsParentOf(g);
        g.setAsParentOf(i);
        i.setAsParentOf(h);

        IteratorBreadFirst ix = new IteratorBreadFirst(f);
        while (ix.hasNext()) {
            System.out.println(ix.next().getName());
        }
        System.out.println("----------");

    }
}
