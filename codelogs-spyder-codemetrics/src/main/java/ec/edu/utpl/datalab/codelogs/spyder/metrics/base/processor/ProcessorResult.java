package ec.edu.utpl.datalab.codelogs.spyder.metrics.base.processor;

import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.holders.PublicResult;

/**
 * * Created by rfcardenas
 */
public class ProcessorResult {

    private int totalMetricas;
    private int totalNodos;
    private double time;
    private EndStatus endStatus;
    private PublicResult resultHolder;

    public PublicResult getResultHolder() {
        return resultHolder;
    }

    public void setResultHolder(PublicResult resultHolder) {
        this.resultHolder = resultHolder;
    }

    public int getTotalMetricas() {
        return totalMetricas;
    }

    public void setTotalMetricas(int totalMetricas) {
        this.totalMetricas = totalMetricas;
    }

    public int getTotalNodos() {
        return totalNodos;
    }

    public void setTotalNodos(int totalNodos) {
        this.totalNodos = totalNodos;
    }

    public double getTime() {
        return time;
    }

    public void setTime(double time) {
        this.time = time;
    }

    public EndStatus getEndStatus() {
        return endStatus;
    }

    public void setEndStatus(EndStatus endStatus) {
        this.endStatus = endStatus;
    }

    public enum EndStatus {
        SUCCESS,
        FAIL
    }
}
