/*
 * The MIT License (MIT)
 * =========================================================================================================
 *
 * Copyright (c) 10/10/16 12:30 Modulo  Proyecto rexcode-metrics-service  Autor [rfcardenas]
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY
 * , WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ec.edu.utpl.datalab.codelogs.spyder.metrics.base.model.tree_traverser;


import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.model.tree.Node;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.model.tree.elements.*;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.model.tree_traverser.dire.TraverserPreorder;
import org.apache.commons.lang.StringUtils;

/**
 * * Created by rfcardenas
 */
public class TraverserBag {
    public static ITraverser degubStructure(TypeTraverser traverser) {
        ElementFunctional<NodeProject> project = node -> System.out.println(formatPrint(node));
        ElementFunctional<NodeFragmentRoot> spacert = node -> System.out.println(formatPrint(node));
        ElementFunctional<NodeFragment> spacefr = node -> System.out.println(formatPrint(node));
        ElementFunctional<NodeFileCode> sourcew = node -> System.out.println(formatPrint(node));
        ElementFunctional<NodeAbstractNode> sourcep = node -> System.out.println(formatPrint(node));
        ElementFunctional<NodeClase> clase = node -> System.out.println(formatPrint(node));
        ElementFunctional<NodeMethod> method = node -> System.out.println(formatPrint(node));

        TraverserFunctions behanvior = new TraverserFunctions();
        behanvior.onProject(project);
        behanvior.onSpaceRoot(spacert);
        behanvior.onSpaceFragment(spacefr);
        behanvior.onSourceWrapper(sourcew);
        behanvior.onSourceParser(sourcep);
        behanvior.onClass(clase);
        behanvior.onMethod(method);
        ITraverser xt = createTraverser(TypeTraverser.PREORDER, behanvior);
        return xt;
    }

    static String formatPrint(Node node) {
        String separador = StringUtils.repeat("-", node.getDeep());
        String icon = "*";
        switch (node.nodeType()) {
            case PROJECT:
                icon = "+";
                break;
            case SPACE_ROOT:
                icon = "*";
                break;
            case SPACE_FRAGMENT:
                icon = "[]";
                break;
            case SOURCE:
                icon = "{}";
                break;
            case CLASS:
                icon = ">";
                break;
            case METHOD:
                icon = ">>>";
                break;
        }

        return icon + separador + node.getElementID() + " -> " + node.nodeType() + " deep " + node.getDeep();
    }

    public static ITraverser createTraverser(TypeTraverser typeTraverser, TraverserFunctions traverserFunctions) {
        ITraverser xt = null;
        switch (typeTraverser) {
            case PREORDER:
                xt = new TraverserPreorder(traverserFunctions);
                break;
            case POSTORDER:
                break;
        }
        return xt;
    }

}
