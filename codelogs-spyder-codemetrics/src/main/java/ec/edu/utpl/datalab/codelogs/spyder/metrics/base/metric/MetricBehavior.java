package ec.edu.utpl.datalab.codelogs.spyder.metrics.base.metric;

/**
 * * Created by rfcardenas
 */
public interface MetricBehavior {
    FormulaExecutable formula();
}
