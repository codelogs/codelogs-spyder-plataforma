/*
 * The MIT License (MIT)
 * =========================================================================================================
 *
 * Copyright (c) 10/10/16 12:30 Modulo  Proyecto rexcode-metrics-service  Autor [rfcardenas]
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY
 * , WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.utpl.datalab.codelogs.spyder.metrics.base.ide_support.netbeans;


import org.apache.commons.vfs2.FileObject;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.*;
import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 * @author ronald
 */
public class NetbeansPatchCompile {

    public static boolean patch(FileObject fo, String path) {
        InputStream stream = null;
        try {
            DocumentBuilderFactory docFac = DocumentBuilderFactory.newInstance();
            DocumentBuilder documentBuilder = docFac.newDocumentBuilder();
            stream = fo.getContent().getInputStream();
            Document document = documentBuilder.parse(stream);
            Element root = document.getDocumentElement();

            XPathFactory xPathfactory = XPathFactory.newInstance();
            XPath xpath = xPathfactory.newXPath();

            XPathExpression expr = xpath.compile("//target[@name=\"-do-compile\"]");

            Node node = (Node) expr.evaluate(document, XPathConstants.NODE);

            if (node != null) {
                System.out.println("Si esta");
            }

            NodeList map = node.getChildNodes();
            Node old = null;
            for (int i = 0; i < map.getLength(); i++) {
                if (map.item(i).getNodeType() == Node.ELEMENT_NODE) {
                    Element e = (Element) map.item(i);
                    String ex = e.toString();
                    if (ex.contains("j2seproject3:javac")) {
                        old = map.item(i);
                    }
                    if (ex.contains("record")) {

                        node.removeChild(map.item(i));
                    }
                }
            }
            Element target = document.createElement("j2seproject3:javac");
            target.setAttribute("gensrcdir", "${build.generated.sources.dir}");

            Element recordS = document.createElement("record");
            recordS.setAttribute("name", path);
            recordS.setAttribute("action", "start");

            Element recordE = document.createElement("record");
            recordE.setAttribute("name", path);
            recordE.setAttribute("action", "stop");

            node.appendChild(recordS);
            node.appendChild(target);
            node.appendChild(recordE);

            node.removeChild(old);

            DOMSource source = new DOMSource(document);

            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();

            transformer.setOutputProperty(OutputKeys.INDENT, "yes");

            StreamResult result = new StreamResult(fo.getName().toString());
            transformer.transform(source, result);


        } catch (ParserConfigurationException | XPathExpressionException | IOException | SAXException | TransformerException ex) {
            Logger.getLogger(NetbeansPatchCompile.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (stream != null) {
                try {
                    stream.close();
                } catch (IOException ex) {
                    Logger.getLogger(NetbeansPatchCompile.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

        return false;
    }

}
