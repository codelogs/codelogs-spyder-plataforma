/*
 * The MIT License (MIT)
 * =========================================================================================================
 *
 * Copyright (c) 10/10/16 12:30 Modulo  Proyecto rexcode-metrics-service  Autor [rfcardenas]
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY
 * , WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ec.edu.utpl.datalab.codelogs.spyder.metrics.base.holders;

import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.model.tree.Node;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.model.tree_traverser.spliterators.BreadFirstTreeSpliterator;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.model.tree_traverser.spliterators_type.TreeTypeSpliterator;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.model.tree.elements.*;
import org.apache.commons.vfs2.FileObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static java.util.stream.StreamSupport.stream;

/**
 * * Created by rfcardenas
 */
public class DefaultTreeHolder implements TreeHolderEditable {
    private HashMap<Long, NodeProject> projectHashMap = new HashMap<>();
    private WorkSpace workSpace = new WorkSpace();

    @Override
    public Optional<Node> findElementById(long id_element) {
        BreadFirstTreeSpliterator breadFirstTreeSpliterator = new BreadFirstTreeSpliterator(workSpace);
        return stream(breadFirstTreeSpliterator, false)
            .filter(element -> element.getIdentifier() == id_element)
            .findFirst();
    }

    @Override
    public Optional<NodeProject> findProjectById(long id_project) {
        TreeTypeSpliterator<NodeProject> treeTypeSpliterator = TreeTypeSpliterator.create(workSpace, NodeProject.class);
        return stream(treeTypeSpliterator, false)
            .filter(project -> project.getIdentifier() == id_project)
            .findFirst();
    }

    @Override
    public Optional<NodeProject> findProjectPathFile(FileObject fileObject) {
        TreeTypeSpliterator<NodeProject> treeTypeSpliterator = TreeTypeSpliterator.create(workSpace, NodeProject.class);
        return stream(treeTypeSpliterator, false)
            .filter(project -> project.getFilePath().equals(fileObject.getName().getPath()))
            .findFirst();
    }

    @Override
    public List<NodeProject> findAllProjects() {
        return workSpace.getOpenNodeProjectList();
    }

    @Override
    public List<NodeFragment> findAllFragmentsFrom(long elementContainer) {
        List<NodeFragment> nodeFragmentList = new ArrayList<>();
        Optional<Node> result = findElementById(elementContainer);
        if (result.isPresent()) {
            TreeTypeSpliterator<NodeFragment> ts = TreeTypeSpliterator.create(result.get(), NodeFragment.class);
            stream(ts, false)
                .forEach(nodeFragmentList::add);
        }
        return nodeFragmentList;
    }

    @Override
    public List<NodeFileCode> findAllFilesCodeFrom(long elementContainer) {
        List<NodeFileCode> nodeFileCodes = new ArrayList<>();
        Optional<Node> result = findElementById(elementContainer);
        if (result.isPresent()) {
            TreeTypeSpliterator<NodeFileCode> ts = TreeTypeSpliterator.create(result.get(), NodeFileCode.class);
            stream(ts, false)
                .forEach(nodeFileCodes::add);
        }
        return nodeFileCodes;
    }

    @Override
    public List<NodeAbstractNode> findAllAstFrom(long elementContainer) {
        List<NodeAbstractNode> nodeAbstractTrees = new ArrayList<>();
        Optional<Node> result = findElementById(elementContainer);
        if (result.isPresent()) {
            TreeTypeSpliterator<NodeAbstractNode> ts = TreeTypeSpliterator.create(result.get(), NodeAbstractNode.class);
            stream(ts, false)
                .forEach(nodeAbstractTrees::add);
        }
        return nodeAbstractTrees;
    }

    @Override
    public List<NodeClase> findAllClasesFrom(long elementContainer) {
        List<NodeClase> nodeClaseList = new ArrayList<>();
        Optional<Node> result = findElementById(elementContainer);
        if (result.isPresent()) {
            TreeTypeSpliterator<NodeClase> ts = TreeTypeSpliterator.create(result.get(), NodeClase.class);
            stream(ts, false)
                .forEach(nodeClaseList::add);
        }
        return nodeClaseList;
    }

    @Override
    public List<NodeMethod> findAllMethodsFrom(long elementContainer) {
        List<NodeMethod> nodeMethodList = new ArrayList<>();
        Optional<Node> result = findElementById(elementContainer);
        if (result.isPresent()) {
            TreeTypeSpliterator<NodeMethod> ts = TreeTypeSpliterator.create(result.get(), NodeMethod.class);
            stream(ts, false)
                .forEach(nodeMethodList::add);
        }
        return nodeMethodList;
    }

    @Override
    public List<NodeFragment> getAllFragments() {
        TreeTypeSpliterator<NodeFragment> typeSpliterator = TreeTypeSpliterator.create(workSpace, NodeFragment.class);
        List<NodeFragment> nodeFragmentList = StreamSupport.stream(typeSpliterator, false)
            .collect(Collectors.toList());
        return nodeFragmentList;
    }

    @Override
    public List<NodeAbstractNode> getAllFiles() {
        TreeTypeSpliterator<NodeAbstractNode> typeSpliterator = TreeTypeSpliterator.create(workSpace, NodeAbstractNode.class);
        List<NodeAbstractNode> nodeAbstractTreeList = StreamSupport.stream(typeSpliterator, false)
            .collect(Collectors.toList());
        return nodeAbstractTreeList;
    }

    @Override
    public List<NodeClase> getAllClases() {
        TreeTypeSpliterator<NodeClase> typeSpliterator = TreeTypeSpliterator.create(workSpace, NodeClase.class);
        List<NodeClase> nodeClaseList = StreamSupport.stream(typeSpliterator, false)
            .collect(Collectors.toList());
        return nodeClaseList;
    }

    @Override
    public List<NodeMethod> getAllMethods() {
        TreeTypeSpliterator<NodeMethod> typeSpliterator = TreeTypeSpliterator.create(workSpace, NodeMethod.class);
        List<NodeMethod> nodeMethodList = StreamSupport.stream(typeSpliterator, false)
            .collect(Collectors.toList());
        return nodeMethodList;
    }

    @Override
    public void save(FileObject reference, NodeProject nodeProject) {
        long id = nodeProject.getIdentifier();
        projectHashMap.put(id, nodeProject);
        workSpace.addProjectToWorkSpace(nodeProject);
    }
}
