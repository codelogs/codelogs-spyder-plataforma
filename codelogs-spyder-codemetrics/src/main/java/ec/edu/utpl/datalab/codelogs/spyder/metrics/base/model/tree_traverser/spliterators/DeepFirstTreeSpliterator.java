package ec.edu.utpl.datalab.codelogs.spyder.metrics.base.model.tree_traverser.spliterators;


import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.model.tree.Node;

import java.util.*;
import java.util.function.Consumer;

/**
 * * Created by rfcardenas
 */
public class DeepFirstTreeSpliterator extends TreeSpliterator {
    private Deque<Node> deque = new ArrayDeque<>();
    private List<Node> list = new ArrayList<>();
    private int index = 0;

    private DeepFirstTreeSpliterator(Node node) {
        super(node);
        long t1 = System.currentTimeMillis();
        buildList(node);
        long t2 = System.currentTimeMillis();

        System.out.println("TIME " + (t2 - t1));
        this.deque = sort(list);
    }

    public static DeepFirstTreeSpliterator create(Node origen) {
        return new DeepFirstTreeSpliterator(origen);
    }

    @Override
    public void buildList(Node node) {

        for (int i = 0; i < node.getChildren().size(); i++) {
            Node ie = node.getChildren().get(i);
            if (!ie.getChildren().isEmpty()) {
                buildList(ie);
            }
        }

        list.add(node);
    }

    @Override
    public void reset() {
        index = 0;
    }

    public Deque<Node> sort(List<Node> nodes) {
        Collections.sort(list, (o1, o2) -> {
            Integer l1 = o1.nodeType().getLevel();
            Integer l2 = o2.nodeType().getLevel();
            return l2.compareTo(l1);
        });
        return new ArrayDeque<>(list);
    }

    @Override
    public boolean tryAdvance(Consumer action) {
        if (index < list.size()) {
            action.accept(list.get(index));
        }
        index++;
        return index <= list.size();
    }

    @Override
    public long estimateSize() {
        return list.size();
    }
}
