/*
 * The MIT License (MIT)
 * =========================================================================================================
 *
 * Copyright (c) 10/10/16 12:30 Modulo  Proyecto rexcode-metrics-service  Autor [rfcardenas]
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY
 * , WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ec.edu.utpl.datalab.codelogs.spyder.metrics.base.model.tree_traverser;


import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.model.tree.Node;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.model.tree.elements.*;

/**
 * * Created by rfcardenas
 */
public final class TraverserFunctions implements ITraverserActions, ITraverser {
    private ElementFunctional<NodeProject> onProject;
    private ElementFunctional<NodeFragmentRoot> onSpaceRoot;
    private ElementFunctional<NodeFragment> onSpaceFragment;
    private ElementFunctional<NodeFileCode> onSourceWrapper;
    private ElementFunctional<NodeAbstractNode> onSourceParsed;
    private ElementFunctional<NodeClase> onClass;
    private ElementFunctional<NodeMethod> onMethod;

    @Override
    public void onProject(ElementFunctional<NodeProject> action) {
        this.onProject = action;
    }

    @Override
    public void onSpaceRoot(ElementFunctional<NodeFragmentRoot> action) {
        this.onSpaceRoot = action;
    }

    @Override
    public void onSpaceFragment(ElementFunctional<NodeFragment> action) {
        this.onSpaceFragment = action;
    }

    @Override
    public void onSourceWrapper(ElementFunctional<NodeFileCode> action) {
        this.onSourceWrapper = action;
    }

    @Override
    public void onSourceParser(ElementFunctional<NodeAbstractNode> action) {
        this.onSourceParsed = action;
    }

    @Override
    public void onClass(ElementFunctional<NodeClase> action) {
        this.onClass = action;
    }

    @Override
    public void onMethod(ElementFunctional<NodeMethod> action) {
        this.onMethod = action;
    }

    public ElementFunctional<NodeProject> getOnProject() {
        return onProject;
    }

    public ElementFunctional<NodeFragmentRoot> getOnSpaceRoot() {
        return onSpaceRoot;
    }

    public ElementFunctional<NodeFragment> getOnSpaceFragment() {
        return onSpaceFragment;
    }

    public ElementFunctional<NodeFileCode> getOnSourceWrapper() {
        return onSourceWrapper;
    }

    public ElementFunctional<NodeAbstractNode> getOnSourceParsed() {
        return onSourceParsed;
    }

    public ElementFunctional<NodeClase> getOnClass() {
        return onClass;
    }

    public ElementFunctional<NodeMethod> getOnMethod() {
        return onMethod;
    }

    protected void performAction(Node node, ElementFunctional action) {
        if (action != null) action.onNode(node);
    }

    @Override
    public void traverse(NodeProject node) {
        if (onProject != null) performAction(node, onProject);
    }

    @Override
    public void traverse(NodeFragmentRoot node) {

    }

    @Override
    public void traverse(NodeFragment node) {
        if (onSpaceFragment != null) onSpaceFragment.onNode(node);
    }

    @Override
    public void traverse(NodeFileCode node) {
        if (onSourceWrapper != null) onSourceWrapper.onNode(node);
    }

    @Override
    public void traverse(NodeAbstractNode node) {
        if (onSourceParsed != null) onSourceParsed.onNode(node);
    }

    @Override
    public void traverser(NodeClase node) {
        if (onClass != null) onClass.onNode(node);
    }

    @Override
    public void traverser(NodeMethod node) {

    }
}
