/*
 * The MIT License (MIT)
 * =========================================================================================================
 *
 * Copyright (c) 10/10/16 12:30 Modulo  Proyecto rexcode-metrics-service  Autor [rfcardenas]
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY
 * , WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ec.edu.utpl.datalab.codelogs.spyder.metrics.base;


import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.core.Generators;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.core.RootInstallerImp;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.core.hk2.AstGeneratorHk2Imp;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.core.hk2.MetricHk2Imp;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.holders.*;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.metric.MetricProvider;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.model.tree_generator.IGeneratorProject;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.modules.ProviderParser;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.processor.Processor;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.processor.ProcessorMulticore;
import org.glassfish.hk2.api.DynamicConfiguration;
import org.glassfish.hk2.api.DynamicConfigurationService;
import org.glassfish.hk2.api.ServiceLocator;
import org.glassfish.hk2.api.ServiceLocatorFactory;
import org.glassfish.hk2.bootstrap.HK2Populator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.List;

/**
 * * Created by rfcardenas
 */
public class Plataforma {
    private final Logger log = LoggerFactory.getLogger(Plataforma.class);

    private ServiceLocator SERVICES = ServiceLocatorFactory.getInstance().create("core_services");
    private Generators generators;

    public Plataforma() {
        initFrameRx();
        initLink();
    }

    private void initLink() {
        System.out.println("********************************************");
        System.out.println("******************CORE¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨");
        System.out.println("Static code");
        DynamicConfigurationService dcs = SERVICES.getService(DynamicConfigurationService.class);
        DynamicConfiguration config = dcs.createDynamicConfiguration();
        // enlazar componentes internos
        config.bind(InjectionLink.config());
        config.bind(InjectionLink.installer());
        config.bind(InjectionLink.installerMetric());
        config.bind(InjectionLink.installerParser());
        config.bind(InjectionLink.processor());
        config.bind(InjectionLink.projectRepository());
        config.bind(InjectionLink.metricHolderEdiable());
        config.bind(InjectionLink.resultHolderEditable());
        // Enalzados de degenradores
        config.bind(InjectionLink.generatorProject());
        config.bind(InjectionLink.generatorFragmentRoot());
        config.bind(InjectionLink.generatorFragment());
        config.bind(InjectionLink.generatorSource());
        config.bind(InjectionLink.generatorObject());
        config.bind(InjectionLink.parserRegistry());
        config.commit();
        /**
         * Descubir servicios externos
         */

        try {
            log.info("Populate services");
            HK2Populator.populate(SERVICES);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public ServiceLocator createServiceLocator(String name) {
        ServiceLocator serviceLocator = ServiceLocatorFactory.getInstance().create(name, SERVICES);
        return serviceLocator;
    }

    public void initFrameRx() {

    }

    public IGeneratorProject generadores() {
        return SERVICES.getService(IGeneratorProject.class);
    }

    public void findAndInstallModules() {

        System.out.println("Find ");
        List<ProviderParser> providerParsers = SERVICES.getAllServices(ProviderParser.class);
        List<MetricProvider> metricProviders = SERVICES.getAllServices(MetricProvider.class);
        System.out.println(providerParsers.size());
        log.info("Instalando generadores AST");
        for (ProviderParser providerParser : providerParsers) {
            log.info("Instalando parser " + providerParser.getClass());
            try {
                installer().install(providerParser);
            } catch (Exception e) {
                log.warn("Imposible registrar parser {}", providerParser.getClass().getName());
            }
        }
        log.info("Instalando Metricas");
        for (MetricProvider metricProvider : metricProviders) {
            log.info("Instalando metrica " + metricProvider.getClass());
            try {
                installer().install(metricProvider);
            } catch (Exception e) {
                log.warn("Imposible registrar metrica " + metricProvider.getClass());
            }
        }
    }

    public void findAndInstallGeneratorASTModules(AstGeneratorHk2Imp... astDescriptors) {
        List<ProviderParser> providerParsers = SERVICES.getAllServices(ProviderParser.class, astDescriptors);
        System.out.println(providerParsers.size());
        log.info("Instalando generadores AST");
        for (ProviderParser providerParser : providerParsers) {
            log.info("Instalando parser " + providerParser.getClass());
            try {
                installer().install(providerParser);
            } catch (Exception e) {
                log.warn("Imposible registrar parser {}", providerParser.getClass().getName());
            }
        }
    }

    public void findAndInstallMetricsModules(MetricHk2Imp... metricDescriptors) {
        List<MetricProvider> providerParsers = SERVICES.getAllServices(MetricProvider.class, metricDescriptors);
        System.out.println(providerParsers.size());
        log.info("Instalando Metricas ");
        for (MetricProvider providerParser : providerParsers) {
            log.info("Instalando metrica " + providerParser.getClass());
            try {
                installer().install(providerParser);
            } catch (Exception e) {
                log.warn("Imposible registrar parser {}", providerParser.getClass().getName());
            }
        }
    }


    public RootInstallerImp installer() {
        return SERVICES.getService(RootInstallerImp.class);
    }

    public Processor processor() {
        return SERVICES.getService(ProcessorMulticore.class);
    }

    public MetricHolder metricRepository() {
        return SERVICES.getService(DefaultMetricsHolderEditable.class);
    }

    public TreeHoder projectRepository() {
        return SERVICES.getService(DefaultTreeHolder.class);
    }

    public DefaultResultHolder measureHolder() {
        return SERVICES.getService(DefaultResultHolder.class);
    }

}
