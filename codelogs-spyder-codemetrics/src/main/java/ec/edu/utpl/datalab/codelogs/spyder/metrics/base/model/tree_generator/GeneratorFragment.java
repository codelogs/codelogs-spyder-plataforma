/*
 * The MIT License (MIT)
 * =========================================================================================================
 *
 * Copyright (c) 10/10/16 12:30 Modulo  Proyecto rexcode-metrics-service  Autor [rfcardenas]
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY
 * , WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ec.edu.utpl.datalab.codelogs.spyder.metrics.base.model.tree_generator;

import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.model.tree.elements.NodeFileCode;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.model.tree.elements.NodeFragment;
import org.apache.commons.vfs2.FileObject;
import org.apache.commons.vfs2.FileSystemException;
import org.glassfish.hk2.api.DescriptorVisibility;
import org.glassfish.hk2.api.Visibility;
import org.jvnet.hk2.annotations.Service;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * * Created by rfcardenas
 */
@Service
@Visibility(DescriptorVisibility.LOCAL)
public class GeneratorFragment implements IGeneratorFragment {

    private GeneratorSource generatorSource;

    @Inject
    public GeneratorFragment(GeneratorSource generatorSource) {
        this.generatorSource = generatorSource;
    }

    @Override
    public Optional<NodeFragment> create(FileObject directory) {
        NodeFragment nodeFragment = null;
        if (ReferencesValidator.isValidFolder(directory)) {
            nodeFragment = new NodeFragment(directory);
            List<NodeFileCode> result = generatorSource.create(nodeFragment);
            nodeFragment.addSourceElement(result);
        }
        return Optional.ofNullable(nodeFragment);
    }

    @Override
    public Optional<NodeFragment> createFragmentRecursive(FileObject fileObject) {
        Optional<NodeFragment> resultCreate = create(fileObject);
        if (resultCreate.isPresent()) {
            try {
                for (FileObject object : fileObject.getChildren()) {
                    Optional<NodeFragment> subFragmentResult = createFragmentRecursive(object);
                    subFragmentResult.ifPresent(resultCreate.get()::addSpaceFragment);
                }
            } catch (FileSystemException e) {
                e.printStackTrace();
            }
        }
        return resultCreate;
    }

    @Override
    public List<NodeFragment> createFragmentRecursive(FileObject[] references) {
        List<NodeFragment> listResult = new ArrayList<>();
        for (FileObject reference : references) {
            Optional<NodeFragment> subFragmentResult = createFragmentRecursive(reference);
            subFragmentResult.ifPresent(listResult::add);
        }
        return listResult;
    }


}
