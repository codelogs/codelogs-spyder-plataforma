/*
 * The MIT License (MIT)
 * =========================================================================================================
 *
 * Copyright (c) 10/10/16 12:30 Modulo  Proyecto rexcode-metrics-service  Autor [rfcardenas]
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY
 * , WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ec.edu.utpl.datalab.codelogs.spyder.metrics.base.model.tree_generator;

import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.model.tree.elements.NodeAbstractNode;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.model.tree.elements.NodeFileCode;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.model.tree.elements.NodeFragment;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.modules.InstallerParser;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.modules.ModuleParser;
import org.apache.commons.vfs2.FileObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

/**
 * * Created by rfcardenas
 */

public class GeneratorSource implements IGeneratorSource {
    private final Logger LOG = LoggerFactory.getLogger(GeneratorSource.class);
    private InstallerParser coreModuleService;

    @Inject
    public GeneratorSource(InstallerParser coreModuleService) {
        this.coreModuleService = coreModuleService;
    }

    public Optional<NodeFileCode> create(FileObject sourceFile) {
        NodeFileCode nodeFileCode = null;
        Set<ModuleParser> handlers = coreModuleService.parserFor(sourceFile);
        if (handlers.size() > 0) {
            nodeFileCode = new NodeFileCode(sourceFile);
            for (ModuleParser handler : handlers) {
                NodeAbstractNode ast = null;
                try {
                    ast = handler.parser(sourceFile);
                    if (ast != null)
                        nodeFileCode.setAsParentOf(ast);
                } catch (Exception e) {
                    LOG.error("Imposible parsear archivo " + sourceFile + " Con " + handler);
                }
            }
        } else {
            LOG.info("Ningun manejador para el fichero " + sourceFile);
        }

        return Optional.ofNullable(nodeFileCode);
    }

    @Override
    public List<NodeFileCode> create(FileObject[] sourceFiles) {
        List<NodeFileCode> wrapperList = new ArrayList<>();
        for (FileObject sourceFile : sourceFiles) {
            Optional<NodeFileCode> result = create(sourceFile);
            if (result.isPresent()) {
                wrapperList.add(result.get());
            }
        }
        return wrapperList;
    }

    @Override
    public List<NodeFileCode> create(NodeFragment nodeFragment) {
        return create(nodeFragment.getAllFiles());
    }
}
