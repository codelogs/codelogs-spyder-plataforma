/*
 * The MIT License (MIT)
 * =========================================================================================================
 *
 * Copyright (c) 10/10/16 12:30 Modulo  Proyecto rexcode-metrics-service  Autor [rfcardenas]
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY
 * , WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ec.edu.utpl.datalab.codelogs.spyder.metrics.base.metric;


import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.domain.Metric;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.domain.MetricType;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.model.tree.Node;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.model.tree.NodeKink;

import java.util.*;

/**
 * * Created by rfcardenas
 */
public class ModuleMetric implements MetricMetadata, MetricBehavior {

    private String uuid;
    private Metric metric;
    private Class<Node> target;
    private MetricType metricType;
    private Set<Class<? extends MetricProvider>> classDepedencies = new HashSet<>();
    private FormulaExecutable formulaExecutable;
    private Optional<ErrorHandler> errorHandler;
    private NodeKink nodeKinkTarget;

    public ModuleMetric(BuilderMetric builder) {
        this.uuid = builder.UUID;
        this.target = builder.TARGET;
        this.classDepedencies = builder.DEPEDENCIES;
        this.formulaExecutable = builder.FORMLA;
        this.metric = builder.METRIC;
        this.errorHandler = Optional.ofNullable(builder.ERRORHANDLER);
        this.nodeKinkTarget = builder.TREE_KIND;
    }

    @Override
    public String getName() {
        return metric.name == null ? "unknow" : metric.name;
    }

    @Override
    public String getHelp() {
        return metric.help_user == null ? "unknow" : metric.help_user;
    }

    @Override
    public String getAbrv() {
        return metric.abbreviation == null ? "unknow" : metric.abbreviation;
    }

    @Override
    public String getCreator() {
        return metric.creator == null ? "unknow" : metric.creator;
    }

    @Override
    public String getCategory() {
        return metric.category == null ? "unknow" : metric.category;
    }

    @Override
    public Optional<ErrorHandler> errorHandler() {
        return errorHandler;
    }

    public NodeKink getNodeKinkTarget() {
        return nodeKinkTarget == null ? NodeKink.NOCHECK : nodeKinkTarget;
    }

    @Override
    public Class<Node> getNodeTarget() {
        return target;
    }

    @Override
    public List<String> getKeyWord() {
        return metric.key_words == null ? Collections.emptyList() : metric.key_words;
    }

    @Override
    public Set<Class<? extends MetricProvider>> getDepedencies() {
        return classDepedencies;
    }


    @Override
    public FormulaExecutable formula() {
        return formulaExecutable;
    }

    @Override
    public String getUuid() {
        return uuid;
    }
}
