/*
 * The MIT License (MIT)
 * =========================================================================================================
 *
 * Copyright (c) 10/10/16 12:30 Modulo  Proyecto rexcode-metrics-service  Autor [rfcardenas]
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY
 * , WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ec.edu.utpl.datalab.codelogs.spyder.metrics.base.holders;


import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.model.tree.Node;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.model.tree.elements.*;
import org.apache.commons.vfs2.FileObject;

import java.util.List;
import java.util.Optional;

/**
 * * Created by rfcardenas
 */
public interface TreeHoder {
    Optional<Node> findElementById(long id_element);

    Optional<NodeProject> findProjectById(long id_project);

    Optional<NodeProject> findProjectPathFile(FileObject fileObject);

    List<NodeProject> findAllProjects();

    List<NodeFragment> findAllFragmentsFrom(long elementContainer);

    List<NodeFileCode> findAllFilesCodeFrom(long elementContainer);

    List<NodeAbstractNode> findAllAstFrom(long elementContainer);

    List<NodeClase> findAllClasesFrom(long elementContainer);

    List<NodeMethod> findAllMethodsFrom(long elementContainer);

    List<NodeFragment> getAllFragments();

    List<NodeAbstractNode> getAllFiles();

    List<NodeClase> getAllClases();

    List<NodeMethod> getAllMethods();
}
