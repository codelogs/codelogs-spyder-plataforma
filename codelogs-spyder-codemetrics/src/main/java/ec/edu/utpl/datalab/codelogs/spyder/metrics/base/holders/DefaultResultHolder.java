/*
 * The MIT License (MIT)
 * =========================================================================================================
 *
 * Copyright (c) 10/10/16 12:30 Modulo  Proyecto rexcode-metrics-service  Autor [rfcardenas]
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY
 * , WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ec.edu.utpl.datalab.codelogs.spyder.metrics.base.holders;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.core.MetricVerticle;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.domain.Measure;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.metric.MetricProvider;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.model.tree.Node;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.model.tree.NodeKink;
import org.jvnet.hk2.annotations.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * * Created by rfcardenas
 */
@Service
public final class DefaultResultHolder implements ResultHolderEditable {

    Cache<Long, CacheEntry> cache;

    public DefaultResultHolder() {
        this.cache = CacheBuilder.newBuilder()
            .expireAfterAccess(30, TimeUnit.MINUTES)
            .build();
    }

    public void save(Node node, MetricVerticle metricVerticle, Measure measure) {
        if (measure != null && node != null && metricVerticle != null) {
            CacheEntry cacheEntry = new CacheEntry(node, metricVerticle, measure);
            long index = generateIndex(node, metricVerticle);
            cache.put(index, cacheEntry);
        }
    }

    @Override
    public Collection<CacheEntry> loadAll() {
        return cache.asMap().values();
    }

    @Override
    public List<CacheEntry> loadAllFrom(NodeKink kind) {
        List<CacheEntry> r = cache.asMap().values().stream()
            .filter(cacheEntry -> cacheEntry.getNode().nodeType() == kind)
            .collect(Collectors.toList());
        return r;
    }

    public List<CacheEntry> loadAllFrom(Node nodeTarget) {
        List<CacheEntry> r = cache.asMap().values().stream()
            .filter(cacheEntry -> cacheEntry.node.equals(nodeTarget))
            .collect(Collectors.toList());
        return r;
    }

    public List<CacheEntry> loadAllFrom(MetricVerticle metricVerticle) {
        return cache.asMap().values().stream()
            .filter(cacheEntry -> cacheEntry.metricVerticle.equals(metricVerticle))
            .collect(Collectors.toList());
    }

    @Override
    public Optional<Measure> loadFrom(Node node, MetricVerticle metricVerticle) {
        Measure measure = null;
        long rebuildIndex = generateIndex(node, metricVerticle);
        CacheEntry cacheEntry = cache.getIfPresent(rebuildIndex);
        if (cacheEntry != null) {
            measure = cacheEntry.measure;
        } else {
            System.out.println("FALLO CACHE " + node.getName() + " Con la metrica " + metricVerticle.getSpecification().getName());
        }
        return Optional.ofNullable(measure);
    }

    public List<CacheEntry> getAllMetrics() {
        return new ArrayList<>(cache.asMap().values());
    }

    public Optional<CacheEntry> getMeasure(long id_metric, long id_node) {
        return cache.asMap().values()
            .stream()
            .filter(cacheEntry -> cacheEntry.node.getElementID().equals(id_node) && cacheEntry.getMetricVerticle().getProviderId().equals(id_metric))
            .findFirst();
    }

    @Override
    public List<CacheEntry> loadAllFrom(long id_metric, String nodeType) {
        return cache.asMap().values()
            .stream().filter(cacheEntry -> {
                return cacheEntry.node.nodeType().toString().equals(nodeType)
                    && cacheEntry.metricVerticle.hashProvider() == id_metric;
            }).collect(Collectors.toList());
    }


    @Override
    public Optional<Measure> load(Node node, Class<? extends MetricProvider> provider) {
        Measure measure = null;
        long providerid = provider.getName().hashCode();
        long nodeide = node.getElementID().hashCode();
        long regenerate = generateIndex(providerid, nodeide);
        CacheEntry tmp = cache.getIfPresent(regenerate);
        if (tmp != null) {
            return Optional.ofNullable(tmp.getMeasure());
        }
        return Optional.empty();
    }

    @Override
    public List<Measure> load(NodeKink nodeKink, Class<? extends MetricProvider> provider) {
        String providerid = provider.getName();
        return cache.asMap().values()
            .stream().filter(cacheEntry -> cacheEntry.node.nodeType().equals(nodeKink) && cacheEntry.getMetricVerticle().getProviderId().equals(providerid)).map(CacheEntry::getMeasure)
            .collect(Collectors.toList());
    }

    private long generateIndex(Node node, MetricVerticle metricVerticle) {
        long k1 = node.getElementID().hashCode();
        long k2 = metricVerticle.getProviderId().hashCode();
        return ((k1 + k2) * (k1 + k2 + 1)) / 2 + k2;
    }

    private long generateIndex(long id_metric, long id_node) {
        long k1 = id_node;
        long k2 = id_metric;
        return ((k1 + k2) * (k1 + k2 + 1)) / 2 + k2;
    }

    /**
     * solo parte del nucleo
     */
    public static abstract class CacheProcess {

        private int _level = 0;

        public CacheProcess(int _level) {
            this._level = _level;
        }

        public abstract Optional<Measure> getMeasureFrom(Class<? extends MetricProvider> provider);

        public abstract void saveMeasure(Measure measure, MetricVerticle metricVerticle);
    }

    public static class DefaultCacheProcess extends CacheProcess {

        private ConcurrentHashMap<String, Measure> cache;
        private CacheReadOnly ro;

        public DefaultCacheProcess(int level) {
            super(level);
            this.cache = new ConcurrentHashMap<>();
        }

        @Override
        public Optional<Measure> getMeasureFrom(Class<? extends MetricProvider> provider) {
            String providerID = provider.getName();
            return Optional.ofNullable(cache.get(providerID));
        }

        @Override
        public void saveMeasure(Measure measure, MetricVerticle metricVerticle) {
            String providerID = metricVerticle.getProviderId();
            this.cache.put(providerID, measure);
        }

        public CacheReadOnly getReadOnly() {
            return (ro == null)
                ? ro = new CacheReadOnly()
                : ro;
        }

        @Override
        public String toString() {
            return cache.toString();
        }

        public class CacheReadOnly {

            public Optional<Measure> getMeasureFrom(Class<? extends MetricProvider> provider) {
                return getMeasureFrom(provider);
            }
        }
    }

    public class CacheEntry {

        public Node node;
        public MetricVerticle metricVerticle;
        public Measure measure;

        private CacheEntry(Node node, MetricVerticle metricVerticle, Measure measure) {
            this.node = node;
            this.metricVerticle = metricVerticle;
            this.measure = measure;
        }

        public Node getNode() {
            return node;
        }

        public void setNode(Node node) {
            this.node = node;
        }

        public MetricVerticle getMetricVerticle() {
            return metricVerticle;
        }

        public void setMetricVerticle(MetricVerticle metricVerticle) {
            this.metricVerticle = metricVerticle;
        }

        public Measure getMeasure() {
            return measure;
        }

        public void setMeasure(Measure measure) {
            this.measure = measure;
        }

    }
}
