/*
 * The MIT License (MIT)
 * =========================================================================================================
 *
 * Copyright (c) 10/10/16 12:30 Modulo  Proyecto rexcode-metrics-service  Autor [rfcardenas]
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY
 * , WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ec.edu.utpl.datalab.codelogs.spyder.metrics.base.core;


import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.holders.DefaultMetricsHolderEditable;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.metric.MetricProvider;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.modules.InstallerParser;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.modules.ModuleParser;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.modules.ProviderParser;
import org.jvnet.hk2.annotations.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;

/**
 * * Created by rfcardenas
 */

@Service
public class RootInstallerImp implements RootInstaller {
    private final Logger log = LoggerFactory.getLogger(RootInstallerImp.class);
    private DefaultMetricsHolderEditable metricInstallerPoint;
    private InstallerParser installerParser;

    @Inject
    public RootInstallerImp(DefaultMetricsHolderEditable metricInstallerPoint, InstallerParser installerParser) {
        this.metricInstallerPoint = metricInstallerPoint;
        this.installerParser = installerParser;
    }

    @Override
    public void install(MetricProvider metricProvider) {
        metricInstallerPoint.register(metricProvider);
    }

    @Override
    public void install(ProviderParser providerParser) {
        ModuleParser parser = providerParser.provide();
        installerParser.register(parser);
    }

}
