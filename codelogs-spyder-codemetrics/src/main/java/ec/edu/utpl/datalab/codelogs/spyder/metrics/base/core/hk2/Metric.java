package ec.edu.utpl.datalab.codelogs.spyder.metrics.base.core.hk2;

import javax.inject.Qualifier;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * * Created by rfcardenas
 */
@Qualifier
@Retention(RUNTIME)
@Target({TYPE})
public @interface Metric {
    /**
     * Lenguaje de programacion extension
     *
     * @return
     */
    String value();
}
