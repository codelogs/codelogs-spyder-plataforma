/*
 * The MIT License (MIT)
 * =========================================================================================================
 *
 * Copyright (c) 10/10/16 12:30 Modulo  Proyecto rexcode-metrics-service  Autor [rfcardenas]
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY
 * , WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ec.edu.utpl.datalab.codelogs.spyder.metrics.base.model.tree.elements;


import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.model.tree.Node;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.model.tree.NodeKink;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.model.tree_traverser.ITraverser;
import org.apache.commons.vfs2.FileObject;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * * Created by rfcardenas
 */
public class NodeFileCode extends Node {
    private Set<NodeAbstractNode> nodeAbstractTrees;
    private FileObject referenceFile;

    public NodeFileCode(FileObject referenceFile) {
        super(referenceFile.getName().getBaseName());
        this.referenceFile = referenceFile;
    }

    public void addRepresentation(NodeAbstractNode nodeAbstractTree) {
        getNodeAbstractTrees().add(nodeAbstractTree);
        setAsParentOf(nodeAbstractTree);
    }

    public Set<NodeAbstractNode> getNodeAbstractTrees() {
        return (nodeAbstractTrees == null)
            ? nodeAbstractTrees = new HashSet<>()
            : nodeAbstractTrees;
    }

    public FileObject getReferenceFile() {
        return referenceFile;
    }

    public String getFilePath() {
        return referenceFile.getName().getPath();
    }

    public <T extends NodeAbstractNode> List<T> getAstHoldersList(Class<T> type) {
        return getNodeAbstractTrees().stream()
            .filter(type::isInstance)
            .map(type::cast)
            .collect(Collectors.toList());
    }

    public <T extends NodeAbstractNode> Optional<T> getAstHolderUnique(Class<T> type) {
        return getNodeAbstractTrees().stream()
            .filter(type::isInstance)
            .map(type::cast)
            .findFirst();
    }

    @Override
    public NodeKink nodeType() {
        return NodeKink.SOURCE_WRAPPER;
    }

    @Override
    public void accept(ITraverser traverser) {
        traverser.traverse(this);
    }

    @Override
    public void clear() {
        nodeAbstractTrees.forEach(Node::clear);
    }
}
