/*
 * The MIT License (MIT)
 * =========================================================================================================
 *
 * Copyright (c) 10/10/16 12:30 Modulo  Proyecto rexcode-metrics-service  Autor [rfcardenas]
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY
 * , WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ec.edu.utpl.datalab.codelogs.spyder.metrics.base.model.tree_traverser;


import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.model.tree.Node;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.model.tree.elements.*;

/**
 * * Created by rfcardenas
 */
public class VMVisitor implements ITraverser {
    TraverserFunctions traverserFunctions;

    public VMVisitor(TraverserFunctions traverserFunctions) {
        this.traverserFunctions = traverserFunctions;
    }

    @Override
    public void traverse(NodeProject node) {
        if (traverserFunctions != null) executeAction(node, traverserFunctions.getOnProject());
    }

    @Override
    public void traverse(NodeFragmentRoot node) {
        if (traverserFunctions != null) executeAction(node, traverserFunctions.getOnSpaceRoot());
    }

    @Override
    public void traverse(NodeFragment node) {
        if (traverserFunctions != null) executeAction(node, traverserFunctions.getOnSpaceFragment());
    }

    @Override
    public void traverse(NodeFileCode node) {
        if (traverserFunctions != null) executeAction(node, traverserFunctions.getOnSourceWrapper());
    }

    @Override
    public void traverse(NodeAbstractNode node) {
        if (traverserFunctions != null) executeAction(node, traverserFunctions.getOnSourceParsed());
    }

    @Override
    public void traverser(NodeClase node) {
        if (traverserFunctions != null) executeAction(node, traverserFunctions.getOnClass());
    }

    @Override
    public void traverser(NodeMethod node) {
        if (traverserFunctions != null) executeAction(node, traverserFunctions.getOnMethod());
    }

    private void executeAction(Node node, ElementFunctional elementFunctional) {
        if (elementFunctional != null) elementFunctional.onNode(node);
    }
}
