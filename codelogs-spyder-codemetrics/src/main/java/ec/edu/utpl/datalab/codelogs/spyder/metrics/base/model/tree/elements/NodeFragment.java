/*
 * The MIT License (MIT)
 * =========================================================================================================
 *
 * Copyright (c) 10/10/16 12:30 Modulo  Proyecto rexcode-metrics-service  Autor [rfcardenas]
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY
 * , WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ec.edu.utpl.datalab.codelogs.spyder.metrics.base.model.tree.elements;


import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.model.tree.NodeKink;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.model.tree.NodeMeasurable;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.model.tree_traverser.ITraverser;
import ec.edu.utpl.datalab.codelogs.spyder.libs.filesystem.search.FilterSet;
import ec.edu.utpl.datalab.codelogs.spyder.libs.filesystem.search.Level;
import ec.edu.utpl.datalab.codelogs.spyder.libs.filesystem.search.SearchFileSystem;
import org.apache.commons.vfs2.FileObject;

import java.util.ArrayList;
import java.util.List;

/**
 * * Created by rfcardenas
 */
public class NodeFragment extends NodeMeasurable {
    private FileObject reference;
    private List<NodeFragment> nodeFragmentList;
    private List<NodeFileCode> nodeFileCodeElements;

    public NodeFragment(FileObject reference) {
        super.setName(reference.getName().getBaseName());
        this.reference = reference;
    }

    public FileObject[] getAllFiles() {
        return SearchFileSystem.newSearch(Level.CURRENT)
            .withFilter(FilterSet.filterOnlyFiles())
            .build()
            .executeFinder(reference);
    }

    public void addSpaceFragment(NodeFragment nodeFragment) {
        getNodeFragmentList().add(nodeFragment);
        setAsParentOf(nodeFragment);
    }

    public void addSourceElement(NodeFileCode nodeFileCode) {
        getSourceList().add(nodeFileCode);
        setAsParentOf(nodeFileCode);
    }

    public void addSpaceFragment(List<NodeFragment> nodeFragmentList) {
        nodeFragmentList.forEach(this::addSpaceFragment);
    }

    public void addSourceElement(List<NodeFileCode> nodeFileCodeElements) {
        nodeFileCodeElements.forEach(this::addSourceElement);
    }

    public List<NodeFragment> getNodeFragmentList() {
        return (nodeFragmentList == null)
            ? nodeFragmentList = new ArrayList<>()
            : nodeFragmentList;
    }

    public List<NodeFileCode> getSourceList() {
        return (nodeFileCodeElements == null)
            ? nodeFileCodeElements = new ArrayList<>()
            : nodeFileCodeElements;
    }

    public FileObject getReferenceFileSystem() {
        return reference;
    }

    @Override
    public NodeKink nodeType() {
        return NodeKink.SPACE_FRAGMENT;
    }

    @Override
    public void clear() {

    }

    @Override
    public void accept(ITraverser traverser) {
        traverser.traverse(this);
    }
}
