package ec.edu.utpl.datalab.codelogs.spyder.metrics.base.processor;

import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.model.tree.Node;
import rx.Observable;

/**
 * * Created by rfcardenas
 */
public interface Processor {
    /**
     * Procesador para un nodo x
     *
     * @param node
     */
    void process(Node node);

    Observable<ProcessorResult> observable();
}
