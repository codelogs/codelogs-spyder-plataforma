package ec.edu.utpl.datalab.codelogs.spyder.metrics.base.metric;


import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.domain.Measure;

import java.util.Optional;

/**
 * * Created by rfcardenas
 */
public interface FormulaExecutable<T> {
    Optional<Measure> accept(FormulaExecutor formulaExecutor);
}
