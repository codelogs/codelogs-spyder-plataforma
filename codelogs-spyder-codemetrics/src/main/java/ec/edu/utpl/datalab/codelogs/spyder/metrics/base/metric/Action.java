package ec.edu.utpl.datalab.codelogs.spyder.metrics.base.metric;


import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.domain.Measure;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.model.tree.Node;

import java.util.Optional;

/**
 * * Created by rfcardenas
 */
public interface Action<T extends Node> extends FormulaExecutable<T> {
    @Override
    default Optional<Measure> accept(FormulaExecutor formulaExecutor) {
        return formulaExecutor.execute(this);
    }

    Measure execute(T node) throws MeasureBadException;
}
