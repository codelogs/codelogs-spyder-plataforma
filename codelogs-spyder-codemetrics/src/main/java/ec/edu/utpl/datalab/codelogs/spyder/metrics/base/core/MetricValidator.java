/*
 * The MIT License (MIT)
 * =========================================================================================================
 *
 * Copyright (c) 10/10/16 12:30 Modulo  Proyecto rexcode-metrics-service  Autor [rfcardenas]
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY
 * , WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ec.edu.utpl.datalab.codelogs.spyder.metrics.base.core;

import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.metric.MetricProvider;
import org.jgrapht.DirectedGraph;
import org.jgrapht.alg.CycleDetector;
import org.jgrapht.graph.DefaultDirectedGraph;
import org.jgrapht.graph.DefaultEdge;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Set;


/**
 * proporciona un objeto de vida corta Valida las depedencias -> REV especificacion
 * explora toda las depedencias de un vertice
 * * Created by rfcardenas
 */
public final class MetricValidator {
    private final Logger log = LoggerFactory.getLogger(MetricValidator.class);
    private DirectedGraph<Class<? extends MetricProvider>, DefaultEdge> cylDetector = new DefaultDirectedGraph<>(DefaultEdge.class);
    private CycleDetector cycleDetector = new CycleDetector(cylDetector);

    private MetricValidator() {
    }

    public static MetricValidator validator() {
        return new MetricValidator();
    }

    /**
     * metodo de acceso a la clase outer para determiar si es el provedor es valido
     *
     * @param metricProvider
     * @return
     */
    public boolean isValid(MetricProvider metricProvider) {

        // si la especificacion retorna null -> no se puede satisfacer la depedencia y los valores por los
        // de los que depedenden nun se calculara [por esta razon automaticamente se indica que esta metricVerticle no sirve]
        if (metricProvider.specification() == null) {
            log.debug("El provedor de especificacion bad return %s", metricProvider.getClass());
            return false;
        }
        // tratar de validar la metricVerticle  si no se dispara ningun problemde ciclo esta bienq
        try {
            validateDepedencies(metricProvider, null, cylDetector);
            log.debug("FIN test {}", metricProvider.getClass().toString());
            return true;
        } catch (VertxBadDefException e) {
            log.error(e.toString());
            return false;
        } catch (InstantiationException | IllegalAccessException e) {
            log.error(e.toString());
            return false;
        }
    }

    /**
     * metodo recurso explora depedencias en busca de problemas [en caso de un ciclo throw CycleFoundException]
     *
     * @param nodo
     * @param parent
     * @param cylDetector
     */
    private void validateDepedencies(MetricProvider nodo, MetricProvider parent, DirectedGraph<Class<? extends MetricProvider>, DefaultEdge> cylDetector) throws VertxBadDefException, IllegalAccessException, InstantiationException {

        if (cycleDetector.detectCycles()) {
            throw new VertxBadDefException(String.format("Depedencia Recursiva \n >>> Revise %s ", cycleDetector.findCycles()));
        }
        Class<? extends MetricProvider> aClassRef = nodo.getClass(); // obtener class
        cylDetector.addVertex(aClassRef);      // añadir la clase al grafo temporal
        if (parent != null) {                       // ejecutar solo si existe un padre en esta iteracion
            Class<? extends MetricProvider> aParentClass = parent.getClass(); // obtener la clase del padre y crear una arista entre el padre y el hijo
            cylDetector.addEdge(aParentClass, aClassRef); // si no se dispara CycleFoundException las depedencias estan correctas
        }

        Set<Class<? extends MetricProvider>> a = nodo.specification().getDepedencies(); // se obtinen las depedencias del nodo
        for (Class<? extends MetricProvider> depedencia : a) {                            // por cada depdencia realiar la validacion
            MetricProvider metricProvider = depedencia.newInstance();
            validateDepedencies(metricProvider, nodo, cylDetector);
        }
    }
}
