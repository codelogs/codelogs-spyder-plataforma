package ec.edu.utpl.datalab.codelogs.spyder.metrics.base.metric;


import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.domain.Measure;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.holders.ResultHolder;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.model.tree.Node;

import java.util.Optional;

/**
 * * Created by rfcardenas
 */
public class DefaultFormulaExecutor implements FormulaExecutor {
    private ResultHolder resultHolder;
    private Node node;

    public DefaultFormulaExecutor(ResultHolder resultHolder, Node node) {
        this.resultHolder = resultHolder;
        this.node = node;
    }

    @Override
    public Optional<Measure> execute(Action action) {

        Measure measure = null;
        try {
            measure = action.execute(node);
        } catch (MeasureBadException e) {
            e.printStackTrace();
        }
        return Optional.ofNullable(measure);
    }

    @Override
    public Optional<Measure> execute(ActionCached action) {
        Measure measure = null;
        try {
            measure = action.execute(node, resultHolder);
        } catch (MeasureBadException e) {
            e.printStackTrace();
        }
        return Optional.ofNullable(measure);
    }
}
