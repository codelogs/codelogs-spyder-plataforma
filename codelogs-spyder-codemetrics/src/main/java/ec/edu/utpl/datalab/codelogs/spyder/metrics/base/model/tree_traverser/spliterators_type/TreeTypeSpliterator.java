/*
 * The MIT License (MIT)
 * =========================================================================================================
 *
 * Copyright (c) 10/10/16 12:30 Modulo  Proyecto rexcode-metrics-service  Autor [rfcardenas]
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY
 * , WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ec.edu.utpl.datalab.codelogs.spyder.metrics.base.model.tree_traverser.spliterators_type;


import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.model.tree.Node;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.Objects;
import java.util.Spliterator;
import java.util.function.Consumer;

/**
 * * Created by rfcardenas
 */
public class TreeTypeSpliterator<T extends Node> implements Spliterator<T> {
    private Node origen;
    private Class<T> target;
    private Deque<T> deque = new ArrayDeque<>();


    public TreeTypeSpliterator(Node origen, Class<T> target) {
        this.origen = Objects.requireNonNull(origen, " Se requiere de un nodo origen");
        this.target = Objects.requireNonNull(target, "Este tipo de iterator requiere la clase filtro");
        buildList(origen);
    }

    public static <T extends Node> TreeTypeSpliterator<T> create(Node origen, Class<T> target) {
        return new TreeTypeSpliterator<T>(origen, target);
    }

    public void buildList(Node e) {
        e.getChildren().stream()
            .filter(target::isInstance)
            .map(target::cast)
            .forEach(deque::addFirst);
        e.getChildren().forEach(this::buildList);
    }

    @Override
    public boolean tryAdvance(Consumer<? super T> action) {
        if (deque.isEmpty()) return false;
        Node e = deque.getLast();
        action.accept((T) (e));
        deque.removeLast();
        return (!deque.isEmpty());
    }

    @Override
    public Spliterator<T> trySplit() {
        return null;
    }

    @Override
    public long estimateSize() {
        return deque.size();
    }

    @Override
    public int characteristics() {
        return ORDERED | NONNULL;
    }
}
