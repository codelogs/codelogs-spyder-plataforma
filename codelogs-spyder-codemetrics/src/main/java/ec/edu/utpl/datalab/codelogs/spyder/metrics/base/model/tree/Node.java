/*
 * The MIT License (MIT)
 * =========================================================================================================
 *
 * Copyright (c) 10/10/16 12:30 Modulo  Proyecto rexcode-metrics-service  Autor [rfcardenas]
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY
 * , WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ec.edu.utpl.datalab.codelogs.spyder.metrics.base.model.tree;


import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.model.tree_traverser.ITraverser;

import java.util.ArrayList;
import java.util.List;
import java.util.StringJoiner;

/**
 * * Created by rfcardenas
 */
public abstract class Node {
    private List<Node> nodeList;
    private Node parent = null;
    private String name = "Unknow-name";
    private String elementID = "unknow-nh";

    public Node() {
    }

    public Node(String name) {
        this.name = name;
        this.elementID = name;
    }

    public Node(Node parent, String name) {
        this.parent = parent;
        this.name = name;
    }

    public void setAsParentOf(Node node) {
        if (node != null) {
            node.setParent(this);
            if (nodeList == null)
                nodeList = new ArrayList<>();
            nodeList.add(node);
        }
    }

    public void unSetAsParentOf(Node node) {
        getChildren().remove(node);
    }

    public void setAsParentOf(List<? extends Node> children) {
        children.forEach(this::setAsParentOf);
    }

    public void setInheritedName(String elementID) {
        this.elementID = elementID;
    }

    public Node getParent() {
        return parent;
    }

    public void setParent(Node parent) {
        if (this.parent != null) {
            this.parent.unSetAsParentOf(this);
        }
        this.parent = parent;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getIdentifier() {
        return getElementID().hashCode();
    }

    public final String getElementID() {
        if (parent != null) {
            if (!parent.nodeType().equals(NodeKink.SPACE_ROOT)) {
                StringJoiner joiner = new StringJoiner(">");
                joiner.add(parent.getElementID());
                joiner.add(this.name);
                this.elementID = joiner.toString();
                return elementID;
            }
        }
        return name;
    }

    public int getDeep() {
        return (parent == null) ? 0 : parent.getDeep() + 1;
    }

    public List<Node> getChildren() {
        return (nodeList == null)
            ? new ArrayList<>()
            : nodeList;
    }

    public abstract NodeKink nodeType();

    public abstract void clear();

    public abstract void accept(ITraverser traverser);


    @Override
    public String toString() {
        return String.format("Nodo %s", getName());
    }


}
