# CODELOGS CODEMETRICS

Modulo base para el desarrollo y extracción de métricas de código
las metricas son implementadas siguiendo el siguiente esquema

por ejemplo para calcular el indice de mantenilidad en java, se puede desarrollar
una métrica de la siguiente forma;

El primer metodo es un builder se requiere :
 - UUID del Servidor,  (Metricas definidas a nivel de WEBAPP)
 - Dependencias sobre otras ya existentes
 - El nodo a alcanzar, en nuestro ejemplo un fichero java
 - la formula, en este caso implementado en la misma clase

```java
@Service
@Metric("java")
public class FormulaMantenibilityIndexFile implements MetricProvider {
    @Override
    public ModuleMetric specification() {
        return BuilderMetric.fluet(DefinicionMantenibilidad.INDEX_MANTEN, JSource.class,"X")
                .metric(SharedData.versionMetric)
                .addDependecy(FormulaCycloFile.class)
                .addDependecy(FormulaLocFile.class)
                .addDependecy(FormulaHalsteadVFile.class)
                .formula(this::formula)
                .ok();
    }

    public Measure formula(JSource node, ResultHolder resultHolder)   {
        double total  = 0;

        Optional<Measure> cacheCl = resultHolder.load(node,FormulaCycloFile.class);
        Optional<Measure> cachelc = resultHolder.load(node,FormulaLocFile.class);
        Optional<Measure> cacheHv = resultHolder.load(node,FormulaHalsteadVFile.class);
        if(cacheCl.isPresent() && cachelc.isPresent() && cacheHv.isPresent()){
            double cyclo = cacheCl.get().getResult();
            double halvol = cacheHv.get().getResult();
            double loc = cachelc.get().getResult();
            double expr2 = (171-5.2 *   Math.log(halvol)-0.23*(cyclo)-16.2*Math.log(loc))*100/171;
            total = Math.max(0,expr2);
        }else{
            System.err.print("Err Im");
        }
        total = (total == Double.POSITIVE_INFINITY)? 100:total;
        total = (total == Double.NEGATIVE_INFINITY)? 0:total;
        total = (total == Double.NaN)? 100:total;

        return new Measure(total);
    }
}
```