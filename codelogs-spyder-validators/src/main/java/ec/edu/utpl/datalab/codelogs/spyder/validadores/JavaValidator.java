package ec.edu.utpl.datalab.codelogs.spyder.validadores;

import com.github.javaparser.JavaParser;
import com.github.javaparser.ast.CompilationUnit;
import ec.edu.utpl.datalab.codelogs.spyder.core.validator.SourceValidator;
import ec.edu.utpl.datalab.codelogs.spyder.core.validator.Validator;
import org.apache.commons.vfs2.FileObject;
import org.jvnet.hk2.annotations.Service;

import java.io.InputStream;

/**
 * * Created by rfcardenas
 */
@Service
@Validator("java")
public class JavaValidator implements SourceValidator {
    @Override
    public boolean isValid(FileObject fileObject) throws Exception {
        boolean valid = true;
        InputStream content = fileObject.getContent().getInputStream();
        try {
            CompilationUnit javaParser = JavaParser.parse(content);
        } catch (Exception e) {
            valid = false;
        } finally {
            content.close();
        }
        return valid;
    }
}
