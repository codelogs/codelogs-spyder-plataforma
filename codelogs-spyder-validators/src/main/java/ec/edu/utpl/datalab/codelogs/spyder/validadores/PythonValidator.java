package ec.edu.utpl.datalab.codelogs.spyder.validadores;

import ec.edu.utpl.datalab.codelogs.spyder.core.validator.SourceValidator;
import ec.edu.utpl.datalab.codelogs.spyder.core.validator.Validator;
import org.apache.commons.vfs2.FileObject;
import org.jvnet.hk2.annotations.Service;

/**
 * * Created by rfcardenas
 */
@Service
@Validator("py")
public class PythonValidator implements SourceValidator {
    @Override
    public boolean isValid(FileObject fileObject) throws Exception {
        return true;
    }
}
