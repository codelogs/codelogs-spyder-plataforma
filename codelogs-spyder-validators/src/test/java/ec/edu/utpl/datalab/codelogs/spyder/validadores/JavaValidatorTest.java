package ec.edu.utpl.datalab.codelogs.spyder.validadores;

import ec.edu.utpl.datalab.codelogs.spyder.libs.filesystem.search.FilterSet;
import ec.edu.utpl.datalab.codelogs.spyder.libs.filesystem.search.SearchFileSystem;
import org.apache.commons.vfs2.FileObject;
import org.junit.Assert;
import org.junit.Test;

import java.util.Optional;

/**
 * * Created by rfcardenas
 */
public class JavaValidatorTest {
    @Test
    public void isValid() throws Exception {
        Optional<FileObject> fileObject = ResourceLoader.loadApp();
        if (fileObject.isPresent()) {
            FileObject app = fileObject.get();
            SearchFileSystem searchFileSystem = SearchFileSystem.newSearch(5)
                .withFilter(FilterSet.filterOnlyFiles())
                .withFilter(FilterSet.filterByName("HelloUTPL.java"))
                .build();

            FileObject[] results = searchFileSystem.executeFinder(app);
            for (FileObject result : results) {
                JavaValidator javaValidator = new JavaValidator();
                boolean valid = javaValidator.isValid(result);
                System.out.println("El resultado esperado es [false] -> la varaible tiene un valor de : " + valid);
                Assert.assertFalse(valid);
            }
        }

    }
}
