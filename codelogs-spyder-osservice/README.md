# CODELOGS MONITOR

Este es un pequeño programa como un servicio a nivel del OS
proporciona interpreta ordenes de los clientes por ejemplo:

```
public class SystemCloseProject implements SystemCommandProvider {
    .....

    @Inject
    public SystemCloseProject(DefualtRouter defualtRouter) {
        this.defualtRouter = defualtRouter;
    }

    @Override
    public SystemCommand command() {
        Option portOption = Option.builder(MonitorCommand.CMD_CLOSE_PROJECT)
                .hasArg()
                .desc("Finaliza el trabajo (monitoreo de un proyecto)")
                .build();
        return SystemCommand.builder(portOption)
                .setAction(this::execute)
                .build();
    }
    public void execute(Map<String,String> args)
    {
        ....
    }
}
```

Cuando el estudiante inicia o termina de programar algo se ejecuta 
la orden descrita anteriomente, para extender las ordenes se tiene que implementar en este nivel