/*
 * The MIT License (MIT)
 * =========================================================================================================
 *
 * Copyright (c) 10/10/16 12:30 Modulo  Proyecto rexcode-monitor-service  Autor [rfcardenas]
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY
 * , WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ec.edu.utpl.datalab.codelogs.spyder.service.core;

import org.apache.commons.io.FileUtils;
import org.apache.commons.vfs2.FileObject;
import org.apache.commons.vfs2.FileSystemException;
import org.apache.commons.vfs2.VFS;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

/**
 * Memento cache es un componente de la clase proyecto context
 * * Created by rfcardenas
 */
public class DefaultMemoryBlock implements MemoryBlock {
    private static final Logger LOG = LoggerFactory.getLogger(DefaultMemoryBlock.class);
    private final Map<String, DefaultMemoryCell> backMemory = new HashMap<>();
    private final FileObject folderTemp;

    private DefaultMemoryBlock(Builder builder) {
        this.folderTemp = builder.fileObject;
    }

    public static Builder builder() {
        return new Builder();
    }

    /**
     * @param sourceCode
     * @return
     */
    @Override
    public boolean createOrSyncMemory(FileObject sourceCode) {
        String pathCode = sourceCode.getName().getPath();
        DefaultMemoryCell defaultMemoryCell = backMemory.get(pathCode);
        try {
            if (!folderTemp.exists()) {
                folderTemp.createFolder();
            }
            if (defaultMemoryCell == null) {
                defaultMemoryCell = new DefaultMemoryCell(sourceCode, folderTemp);
                backMemory.put(pathCode, defaultMemoryCell);
                LOG.info("Creando memento para {}", sourceCode.getName().getBaseName());
                return true;
            } else {
                defaultMemoryCell.sync();
                LOG.info("Update memento {}", sourceCode.getName().getBaseName());
                return true;
            }

        } catch (FileSystemException e) {
            e.printStackTrace();
        }
        return false;
    }

    /**
     * Recupera una memoria para un archivo
     *
     * @param sourceCode
     * @return
     */
    @Override
    public DefaultMemoryCell recoverMemoryFrom(FileObject sourceCode) {
        return recoverMemoryFrom(sourceCode.getName().getPath());
    }

    /**
     * Recupera una memoria para un path
     *
     * @param sourceCode
     * @return NUll posible
     */
    @Override
    public DefaultMemoryCell recoverMemoryFrom(String sourceCode) {
        return backMemory.get(sourceCode);
    }

    /**
     * Elimina la memoria de un archivo
     *
     * @param sourceCode
     * @return
     */
    @Override
    public boolean removeMemoryof(FileObject sourceCode) {
        return removeMemoryof(sourceCode.getName().getPath());
    }

    /**
     * Elimna la memoria de un archivo
     *
     * @param sourceCode
     * @return
     */
    @Override
    public boolean removeMemoryof(String sourceCode) {
        DefaultMemoryCell memory = recoverMemoryFrom(sourceCode);
        if (memory != null) {
            memory.destroyMemory();
            backMemory.remove(sourceCode);
        }
        return false;
    }

    /**
     * Determina la existencia de una memoria
     *
     * @param fileObject
     * @return
     */
    @Override
    public boolean existMemoryOf(FileObject fileObject) {
        return existMemoryOf(fileObject.getName().getPath());
    }

    /**
     * Determina la existencia de una memoria
     *
     * @param path
     * @return
     */
    @Override
    public boolean existMemoryOf(String path) {
        return backMemory.get(path) != null;
    }

    @Override
    public void destroy() throws FileSystemException {
        LOG.info("Destruyendo memoria.....");
        if (folderTemp.exists()) {
            for (FileObject fileObject : folderTemp.getChildren()) {
                fileObject.delete();
            }
            backMemory.clear();
        }
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        destroy();
        //Se ejectua cuando se asegura que no existen mas referencias y el objecto sera destruido
        LOG.info("Destruyendo la memory folder.... " + folderTemp.getName());
        folderTemp.delete();
    }

    /**
     * Inner builder memory estructure
     */
    public static class Builder {
        private String pathTemporal = null;
        private int defaultCache = 10;
        private FileObject fileObject;

        public Builder temporalPath(String path) {
            this.pathTemporal = path;
            return this;
        }

        public Builder size(int cacheDefaultSize) {
            this.defaultCache = cacheDefaultSize;
            return this;
        }

        public MemoryBlock build() {
            if (defaultCache < 5 || defaultCache > 1000) {
                defaultCache = 10;
            }
            try {
                if (pathTemporal == null) {
                    pathTemporal = FileUtils.getTempDirectoryPath() + File.separatorChar + "rxcode";
                } else {
                    pathTemporal = FileUtils.getTempDirectoryPath() + File.separatorChar + "rxcode" + File.separatorChar + pathTemporal;
                }
                fileObject = VFS.getManager().resolveFile(pathTemporal);
                if (!fileObject.exists()) {
                    fileObject.createFolder();
                    this.pathTemporal = fileObject.getName().getPath();
                }
                return new DefaultMemoryBlock(this);
            } catch (FileSystemException e) {
                e.printStackTrace();
            }
            return null;
        }
    }
}
