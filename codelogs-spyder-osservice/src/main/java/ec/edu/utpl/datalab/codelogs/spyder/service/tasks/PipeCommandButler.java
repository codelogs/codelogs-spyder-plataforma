/*
 * The MIT License (MIT)
 * =========================================================================================================
 *
 * Copyright (c) 10/10/16 12:30 Modulo  Proyecto rexcode-monitor-service  Autor [rfcardenas]
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY
 * , WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ec.edu.utpl.datalab.codelogs.spyder.service.tasks;

import ec.edu.utpl.datalab.codelogs.spyder.core.util.pipes.AbstractPipe;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * * Created by rfcardenas
 */
public class PipeCommandButler extends AbstractPipe<String, String> implements Runnable {
    // &&  private ProcListener io;
    private static final Logger log = LoggerFactory.getLogger(PipeCommandButler.class);

    public PipeCommandButler() {
        /**    try {
         this.io = new DefaultRmiServer(MonitorCommand._rmireg,9090);
         } catch (RemoteException e) {
         e.printStackTrace();
         }
         io.asObservable()
         .filter(remoteData -> remoteData.getRemoteDataCategory()== RemoteDataCategory.COMMAND)
         .map(RemoteData::getData)
         .subscribe(this);**/
    }

    @Override
    public void onCompleted() {
        log.info("End ");
    }

    @Override
    public void onError(Throwable e) {

    }

    @Override
    public void onNext(String s) {
        log.info("CMD:{}", s);
        notify(s);
    }

    @Override
    public void run() {
        log.info("CMD service start");
    }
}
