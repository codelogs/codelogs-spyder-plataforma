/*
 * The MIT License (MIT)
 * =========================================================================================================
 *
 * Copyright (c) 10/10/16 12:30 Modulo  Proyecto rexcode-monitor-service  Autor [rfcardenas]
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY
 * , WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ec.edu.utpl.datalab.codelogs.spyder.service.core.analyzer;

import ec.edu.utpl.datalab.codelogs.spyder.core.models.*;
import ec.edu.utpl.datalab.codelogs.spyder.core.persistencia.RepositoryService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;

/**
 * * Created by rfcardenas
 */
public class DefaultSaverOnDisk extends AbstractSaver {
    private static final Logger log = LoggerFactory.getLogger(DefaultSaverOnDisk.class);
    private RepositoryService repositoryService;

    @Inject
    public DefaultSaverOnDisk(RepositoryService repositoryService) {
        this.repositoryService = repositoryService;
    }

    @Override
    public void function(Project project) {
        log.info("Save Project ..");
        Project inDb = repositoryService.projectRepository().findProjectByPath(project.getPath());
        if (!project.isValidRepresentation() && inDb == null) {
            Project p = repositoryService.projectRepository().save(project);
            project.setId(p.getId());
        }
        if (inDb != null) {
            project.setId(inDb.getId());
        }
    }

    @Override
    public void function(WorkSession session) {
        log.info("Save session ..");
        WorkSession db = repositoryService.worksessionRepository().save(session);
        session.setId(db.getId());
    }

    @Override
    public void function(HeartBeatCode heartBeatCode) {
        log.info("Save pulse ..");
        if (!heartBeatCode.isValidRepresentation()) {
            FileCode fileCode = repositoryService.fileCodeRepository().findByPath(heartBeatCode.getPathFile());
            heartBeatCode.setFileCode(fileCode);
            repositoryService.pulseCodeRepository().save(heartBeatCode);
        }
    }

    @Override
    public void function(FileCode fileCode) {
        log.info("Save file ..");
        FileCode inDb = repositoryService.fileCodeRepository().findByPath(fileCode.getPathFile());
        if (!fileCode.isValidRepresentation() && inDb == null) {
            repositoryService.fileCodeRepository().save(fileCode);
        }
    }

    @Override
    public void function(HeartBeatBuildRun data) {
        log.info("Save compiledata");
        repositoryService.runCompileRepository().save(data);
    }
}
