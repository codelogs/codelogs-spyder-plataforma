/*
 * The MIT License (MIT)
 * =========================================================================================================
 *
 * Copyright (c) 10/10/16 12:30 Modulo  Proyecto rexcode-monitor-service  Autor [rfcardenas]
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY
 * , WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ec.edu.utpl.datalab.codelogs.spyder.service.comandos;

/**
 * constantes comandos
 * * Created by rfcardenas
 */
public class MonitorCommand {
    public static final String COMMAND = "command";
    public static final String VALUE = "value";
    public static final String _rmireg = "rexcode";
    public static final String _apikey = "apikey";
    public static final String _openProject = "open";
    public static final String _openProject_sh = "o";
    public static final String _closeProject = "close";
    public static final String _closeProject_sh = "c";
    public static final String CMD_COLLECT_DATA = "collect";
    public static final String _shutdown = "shutdown";
    public static final String _fileEventUpdate = "file_event_update";
    public static final String _argument = "argument";


    /**
     * EVENT COMMANDS
     */

    public static final String CMD_EVENT_FILE = "event";
    public static final String CMD_EVENT_TYPE = "type";
    public static final String EVENT_VALUE_UPDATE = "update";
    public static final String EVENT_VALUE_CREATE = "create";
    public static final String EVENT_VALUE_DELETE = "delete";

    /**
     * PROJECT_COMANDOS
     */
    public static final String CMD_OPEN_PROJECT = "open";
    public static final String CMD_CLOSE_PROJECT = "close";
    public static final String IDE_ARGUMENT = "ide";
    public static final String IDE_DEFAULT = "CodeLogs";

    /**
     * Monitor ciclo de vida
     */

    public static final String CMD_SHUTDOWN_MONITOR = "shutdown";
    public static final String CMD_SYNC = "sync";


    /**
     * Compilation Behavior
     */
    public static final String CMD_BUILD_BEHAVIOR = "build";
    public static final String CMD_RUN_BEHAVIOR = "run";
}
