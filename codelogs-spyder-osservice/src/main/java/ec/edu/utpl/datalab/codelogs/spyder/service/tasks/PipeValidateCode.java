package ec.edu.utpl.datalab.codelogs.spyder.service.tasks;

import ec.edu.utpl.datalab.codelogs.spyder.core.util.pipes.AbstractPipe;
import ec.edu.utpl.datalab.codelogs.spyder.core.validator.SourceValidator;
import ec.edu.utpl.datalab.codelogs.spyder.core.validator.Validator;
import ec.edu.utpl.datalab.codelogs.spyder.core.validator.ValidatorImp;
import ec.edu.utpl.datalab.codelogs.spyder.libs.filesystem.code_event2.packs.Event;
import org.apache.commons.vfs2.FileObject;
import org.glassfish.hk2.api.ServiceLocator;
import org.glassfish.hk2.utilities.ServiceLocatorUtilities;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;

/**
 * * Created by rfcardenas
 */
public class PipeValidateCode extends AbstractPipe<Event, Event> {
    private static final Logger log = LoggerFactory.getLogger(PipeFileSystemEvent.class);
    private static ServiceLocator SERVICES = ServiceLocatorUtilities.createAndPopulateServiceLocator();

    @Inject
    public PipeValidateCode(PipeFileSystemEvent pipeFileSystemEvent) {
        pipeFileSystemEvent.notifyEvents()
            .subscribe(this);
    }

    public void validate(Event event) {
        FileObject fileObject = event.getFileObject();
        try {
            if (fileObject.isFile()) {
                String ext = fileObject.getName().getExtension();
                SourceValidator validator = SERVICES.getService(SourceValidator.class, new ValidatorImp(ext));
                if (validator != null) {
                    if (validator.isValid(fileObject)) {
                        notify(event);
                    } else {
                        log.warn("El archivo {} esta mal escrito, no se ejecutaran acciones de analisis", fileObject.getName().getBaseName());
                    }
                } else {
                    log.warn("Ningun validador para extension {}. se notifica los cambios sin validar la estructura", ext);
                    notify(event);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private String getAnnotation(SourceValidator sourceValidator) {
        Validator validator = sourceValidator.getClass().getAnnotation(Validator.class);
        if (validator != null) {
            return validator.value();
        }
        return null;
    }

    @Override
    public void onCompleted() {
        super.onCompleted();
    }

    @Override
    public void onError(Throwable e) {
        super.onError(e);
    }

    @Override
    public void onNext(Event event) {
        validate(event);
    }
}
