/*
 * The MIT License (MIT)
 * =========================================================================================================
 *
 * Copyright (c) 10/10/16 12:30 Modulo  Proyecto rexcode-monitor-service  Autor [rfcardenas]
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY
 * , WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package ec.edu.utpl.datalab.codelogs.spyder.service.core.analyzer;

import ec.edu.utpl.datalab.codelogs.spyder.core.models.FileCode;
import ec.edu.utpl.datalab.codelogs.spyder.core.models.HeartBeatBuildRun;
import ec.edu.utpl.datalab.codelogs.spyder.core.models.HeartBeatCode;
import ec.edu.utpl.datalab.codelogs.spyder.core.models.Project;
import ec.edu.utpl.datalab.codelogs.spyder.libs.common.FileUtils;
import org.apache.commons.vfs2.FileObject;
import org.apache.commons.vfs2.FileSystemException;
import org.apache.commons.vfs2.VFS;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

/**
 * * Created by rfcardenas
 */
public final class DefualtRouter implements Router {

    private static final Logger log = LoggerFactory.getLogger(DefualtRouter.class);
    private final Map<String, DefaultRouterProjectData> contextMap;
    private final AbstractSaver saver;
    private final CacheWorkSession cacheWorkSession;

    @Inject
    public DefualtRouter(CacheWorkSession cacheWorkSession, AbstractSaver saver) {
        this.cacheWorkSession = cacheWorkSession;
        this.contextMap = new HashMap<>();
        this.saver = saver;
    }

    /**
     * Enruta los datos para un proyecto, se recomienda utilizar
     * el formato universal file://ruta/cas | file://c:ruta
     * @param path
     * @return
     */
    private Optional<DefaultRouterProjectData> getContextFromPathChildren(String path) {
        contextMap.entrySet().stream().forEach(stringDefaultRouterProjectDataEntry -> {
            System.out.println("KEY -> " + stringDefaultRouterProjectDataEntry.getKey());
            System.out.println("TARGET -> " + path);
        });
        return contextMap.entrySet().stream()
            .filter(row -> path.contains(row.getKey()) || path.equals(row.getKey()))
            .map(Map.Entry::getValue)
            .findFirst();
    }

    @Override
    public Optional<DefaultRouterProjectData> getContext(FileObject fileObject) {
        // casi imposible que suceda pero... x si aca
        Optional<DefaultRouterProjectData> result = getContextFromPathChildren(fileObject);
        if (!result.isPresent()) {
            log.error("Contexto no encontrado ..");
        }
        return result;
    }

    private Optional<DefaultRouterProjectData> getContextFromPathChildren(FileObject file) {
        String path = file.getName().getFriendlyURI();
        return getContextFromPathChildren(path);
    }

    private Optional<DefaultRouterProjectData> getContextFromProject(Project project) {
        String path = project.getPath();
        return getContextFromPathChildren(path);
    }

    @Override
    public void ruouteFile(FileCode code) {
        Optional<DefaultRouterProjectData> ruta = getContextFromPathChildren(code.getFileObject());
        if (ruta.isPresent()) {
            ruta.get().processEntitySave(code);
        } else {
            log.error("No se puede encontrar la ruta para persistir Filecode {}", code.getName());
        }
    }

    @Override
    public void routePulse(HeartBeatCode pulse) {
        Optional<DefaultRouterProjectData> ruta = getContextFromPathChildren(pulse.getPathFile());
        if (ruta.isPresent()) {
            ruta.get().processEntitySave(pulse);
        } else {
            log.error("No se puede encontrar la ruta para persistir Pulso {}", pulse.getPathFile());
        }
    }

    @Override
    public void routeCompileRunData(HeartBeatBuildRun data) {
        Optional<DefaultRouterProjectData> ruta = getContextFromPathChildren(data.getPathContext());
        if (ruta.isPresent()) {
            ruta.get().processEntitySave(data);
        } else {
            log.error("No se puede encontrar la ruta para persistir CompilationRunResult {}", data.getPathContext());
        }
    }

    //Crea una ruta por donde se envian todos eventos que tengan un path relativo al del proyecto
    public void createRouteFor(Project project) {
        log.info("Intentado crear ruta para {}", project.getName());
        Optional<DefaultRouterProjectData> result = getContextFromProject(project);
        if (result.isPresent()) {
            log.warn("Ya existe una ruta para los datos del proyecto {}", project.getName());
        } else {
            saver.process(project);
            DefaultRouterProjectData defaultRouterProjectData = new DefaultRouterProjectData(project, cacheWorkSession, saver);
            defaultRouterProjectData.createWorkSession(project);
            //es ejectua acciones de persitencia
            defaultRouterProjectData.connetProcessorSaver(saver);
            contextMap.put(project.getPath(), defaultRouterProjectData);
            log.info("Project guardado...");
        }
    }

    @Override
    public void removeRoute(String project) {
        try {
            log.info("Eliminado ruta...");
            project = FileUtils.formatUniversal(project);
            FileObject fileObject = VFS.getManager().resolveFile(project);
            log.info("Eliminado ruta... 2");
            if (fileObject.exists()) {
                DefaultRouterProjectData result = contextMap.remove(project);
                if (result != null) {
                    try {
                        // en caso de que algun cliente mantenga una referencia, limipiar
                        // los datos y la memoria tanto en disco como en RAM
                        log.warn("LLAMADO DE LIMPIEZA FORZADA");
                        cacheWorkSession.remove(project);
                    } catch (Throwable throwable) {
                        throwable.printStackTrace();
                    }
                } else {
                    log.warn("La ruta {} no esta disponible para su eliminacion", project);
                }
            } else {
                log.warn("la ruta no fue encontrada en disco...");
            }
        } catch (FileSystemException ex) {
            log.error("Error remove route {}", ex.getMessage());
            ex.printStackTrace();
        }
    }
}
