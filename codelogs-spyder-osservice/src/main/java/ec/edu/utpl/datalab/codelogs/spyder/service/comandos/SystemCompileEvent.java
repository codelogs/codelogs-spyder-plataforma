package ec.edu.utpl.datalab.codelogs.spyder.service.comandos;

import ec.edu.utpl.datalab.codelogs.spyder.core.mappers.MinerMap;
import ec.edu.utpl.datalab.codelogs.spyder.core.models.HeartBeatBuildRun;
import ec.edu.utpl.datalab.codelogs.spyder.core.synchronization.ModuloSynchronization;
import ec.edu.utpl.datalab.codelogs.spyder.core.synchronization.STRATEGY_SYNC;
import ec.edu.utpl.datalab.codelogs.spyder.core.util.JsonMapper;
import ec.edu.utpl.datalab.codelogs.spyder.libs.transfers.HeartBeatBuildRunPack;
import ec.edu.utpl.datalab.codelogs.spyder.service.core.analyzer.DefualtRouter;
import ec.edu.utpl.datalab.codelogs.spyder.libs.terminal.SystemCommand;
import ec.edu.utpl.datalab.codelogs.spyder.libs.terminal.SystemCommandProvider;
import org.apache.commons.cli.Option;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.util.Map;
import java.util.Optional;

/**
 * * Created by rfcardenas
 */
public class SystemCompileEvent implements SystemCommandProvider {
    private static final Logger log = LoggerFactory.getLogger(SystemCompileEvent.class);

    private DefualtRouter router;
    private ModuloSynchronization synchronization;

    @Inject
    public SystemCompileEvent(DefualtRouter router, ModuloSynchronization synchronization) {
        this.router = router;
        this.synchronization = synchronization;
    }

    @Override
    public SystemCommand command() {

        Option updateOption = Option.builder(MonitorCommand.CMD_BUILD_BEHAVIOR)
            .hasArg()
            .desc("Compilation behavior")
            .build();

        return SystemCommand.builder(updateOption)
            .setAction(this::execute)
            .build();
    }

    public void execute(Map<String, String> args) {
        log.info("Ejecutando comando con argumentos {} , {}", args.get(MonitorCommand.CMD_BUILD_BEHAVIOR), args.get(MonitorCommand.CMD_EVENT_TYPE));
        String data = args.get(MonitorCommand.CMD_BUILD_BEHAVIOR);

        Optional<HeartBeatBuildRunPack> jsonResult = JsonMapper.read(HeartBeatBuildRunPack.class, data);
        if (jsonResult.isPresent()) {
            HeartBeatBuildRun buildRun = MinerMap.compilationResultMapper.map(jsonResult.get());
            router.routeCompileRunData(buildRun);
            synchronization.onNext(STRATEGY_SYNC.LAST_CHANGES);

        }else{
            log.error("Imposible procesar evento de las fases build/run, JSON no reconocido");
        }
    }
}
