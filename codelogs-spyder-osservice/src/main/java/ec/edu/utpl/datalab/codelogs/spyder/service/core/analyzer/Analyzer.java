/*
 * The MIT License (MIT)
 * =========================================================================================================
 *
 * Copyright (c) 10/10/16 12:30 Modulo  Proyecto rexcode-monitor-service  Autor [rfcardenas]
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY
 * , WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ec.edu.utpl.datalab.codelogs.spyder.service.core.analyzer;


import org.apache.commons.vfs2.FileObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import rx.Observable;
import rx.subjects.PublishSubject;

/**
 * Plantilla para analizar un pulso, esta compuesta por la memoria de codigo
 * esta memoria es un directorio temporal en el directio {tmp - os }/rexcode/uuid
 * * Created by rfcardenas
 */
public abstract class Analyzer<AbstractPack> {
    private Logger log = LoggerFactory.getLogger(Analyzer.class);
    private PublishSubject<AbstractPack> subject = PublishSubject.create();


    /**
     * publica los resultados para cualquiera destino (los componentes) deberan subscribirse
     * al analizador para recibir los resultados de los analisis de forma reactiva
     *
     * @param data
     */
    public final void publish(AbstractPack data) {
        //siguiente fase implemnetarlo con rxjava
        subject.onNext(data);
        subject.onCompleted();
    }

    /**
     * retorna un obse del los resultados (AbstractPack)
     *
     * @return
     */
    public Observable<AbstractPack> observable() {
        return subject.asObservable();
    }

    /**
     * Metodo publico para la ejecucion del analizador, los clientes tienen que hacer
     * uso de este metodo para la ejecucion de los analisis
     *
     * @param fileObject // referencia a un fichero de texto
     */
    public void runAnalyzer(FileObject fileObject) {
        try {
            analyze(fileObject);
        } catch (RuntimeException e) {
            log.error("problema al ejectuar el analizador {}", e.getMessage());
        }
    }

    /**
     * Metodo abstracto para analizar el codigo, el codigo
     *
     * @param file
     */
    protected abstract void analyze(FileObject file) throws RuntimeException;
}
