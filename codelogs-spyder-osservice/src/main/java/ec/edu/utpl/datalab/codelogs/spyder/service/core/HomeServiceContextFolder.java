package ec.edu.utpl.datalab.codelogs.spyder.service.core;

import ec.edu.utpl.datalab.codelogs.spyder.core.env.BadArgumentException;
import ec.edu.utpl.datalab.codelogs.spyder.core.env.EnvironmentProperties;
import ec.edu.utpl.datalab.codelogs.spyder.core.env.PropertiesDefault;
import ec.edu.utpl.datalab.codelogs.spyder.core.filesystem.EnvironmentAppNotFound;
import org.apache.commons.vfs2.FileObject;
import org.apache.commons.vfs2.FileSystemException;
import org.apache.commons.vfs2.VFS;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Clase facilita trabajo ejecucion area
 * * Created by rfcardenas
 */
public class HomeServiceContextFolder implements HomeService {
    private static final Logger log = LoggerFactory.getLogger(HomeServiceContextFolder.class);
    private FileObject homeDirectory = null;
    private EnvironmentProperties properties;

    /**
     * Utiliza una referencia a un directorio
     * @throws EnvironmentAppNotFound
     */
    public HomeServiceContextFolder(EnvironmentProperties properties) throws EnvironmentAppNotFound {
        this.properties = properties;
        if(!properties.hasProperty(PropertiesDefault.SPYDER_HOME)){
            throw new BadArgumentException("Se requiere de Spyder home, directorio donde esta ejecutandose el proyecto");
        }
        String path = properties.getProperty(PropertiesDefault.SPYDER_HOME);
        try {
            initHome(path);
        } catch (FileSystemException e) {
            throw new BadArgumentException(e);
        }
    }
    private final void initHome(String home) throws FileSystemException {

        homeDirectory = VFS.getManager().resolveFile(home);
        FileObject bin = homeDirectory.resolveFile("bin");
        FileObject configs = homeDirectory.resolveFile("config");
        FileObject libs = homeDirectory.resolveFile("lib");

        if(!bin.exists() || !configs.exists() || !libs.exists()){
            log.info("bin presente {}" ,bin.exists());
            log.info("configs presente {}" ,configs.exists());
            log.info("libs presente {}",libs.exists());
            throw new BadArgumentException(String.format("Carpeta no encontrada"));
        }
    }


    /**
     * Retorna el archivo de configuración
     * @return
     * @throws FileSystemException
     */
    public FileObject getConfigFile() throws Exception {
        return homeDirectory.resolveFile(PropertiesDefault.map.get(PropertiesDefault.SPYDER_HOME_CONFIG_FILE).toString());
    }

    /**
     * Retorna archivo de sopporte de lenguajes
     * @return
     * @throws FileSystemException
     */
    public FileObject getSupportLangsFile() throws Exception {
        return homeDirectory.resolveFile(PropertiesDefault.map.get(PropertiesDefault.SPYDER_HOME_CONFIG_FILE_SUPPORT).toString());
    }

    @Override
    public FileObject getFixPathStore() throws Exception {
        FileObject fileObject =  homeDirectory
            .resolveFile(PropertiesDefault.map.get(PropertiesDefault.SPYDER_HOME_DB_PATH).toString());

        if(!fileObject.exists()){
            fileObject.createFile();
            log.warn("El archivo no existe");
        }
        return fileObject;
    }

    /**
     * Retorna el path de la bd
     * @return
     * @throws FileSystemException
     */
    public FileObject getPathDb() throws FileSystemException {
        FileObject fileObject =  homeDirectory.resolveFile(PropertiesDefault.map.get(PropertiesDefault.SPYDER_HOME_DB_PATH).toString());
        if(!fileObject.exists()){
            fileObject.createFile();
        }
        return fileObject;
    }



}
