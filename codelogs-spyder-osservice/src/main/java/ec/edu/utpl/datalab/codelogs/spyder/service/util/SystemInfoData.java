package ec.edu.utpl.datalab.codelogs.spyder.service.util;

import ec.edu.utpl.datalab.codelogs.spyder.libs.common.StringUtils;
import oshi.SystemInfo;
import oshi.hardware.HardwareAbstractionLayer;
import oshi.hardware.NetworkIF;
import oshi.software.os.OperatingSystem;
import oshi.software.os.OperatingSystemVersion;

import java.util.Optional;
import java.util.StringJoiner;

/**
 * * Created by rfcardenas
 */
public class SystemInfoData {
    private static SystemInfo system = new SystemInfo();

    public static String getIpInfo() {
        StringJoiner panat = new StringJoiner(",");
        Optional<HardwareAbstractionLayer> data = getHardwarelayer();
        if (data.isPresent()) {
            NetworkIF[] networkIF = data.get().getNetworkIFs();
            for (NetworkIF net : networkIF) {
                String item = String.format("{'mac':'%s','name':'%s','ip4':'%s',ip6:'%s'}", net.getMacaddr(), net.getName(), StringUtils.stringFrom(net.getIPv4addr()), StringUtils.stringFrom(net.getIPv6addr()));
                panat.add(item);
            }
        }
        return panat.toString();
    }

    public static String getMacInfo() {
        int hd = 1;
        StringJoiner panat = new StringJoiner(",");
        Optional<HardwareAbstractionLayer> data = getHardwarelayer();
        if (data.isPresent()) {
            NetworkIF[] networkIF = data.get().getNetworkIFs();
            for (NetworkIF net : networkIF) {
                String item = String.format("{mac:'%s',name:'%s'}", net.getMacaddr(), net.getName());
                panat.add(item);
            }
        }
        return panat.toString();
    }

    public static long getTotalRam() {
        long rawav = 0;
        Optional<HardwareAbstractionLayer> data = getHardwarelayer();
        if (data.isPresent()) {
            rawav = data.get().getMemory().getTotal();
        }
        return rawav;
    }

    public static long getRamAvaible() {
        long rawav = 0;
        Optional<HardwareAbstractionLayer> data = getHardwarelayer();
        if (data.isPresent()) {
            rawav = data.get().getMemory().getAvailable();
        }
        return rawav;
    }

    public static long getSystemUptime() {
        long timemachineuptime = 0;
        Optional<HardwareAbstractionLayer> data = getHardwarelayer();
        if (data.isPresent()) {
            timemachineuptime = data.get().getProcessor().getSystemUptime();
        }
        return timemachineuptime;
    }

    public static String getMachineName() {
        String username = System.getProperty("user.name");
        return username.isEmpty() ? "unknow" : username;
    }

    public static int getCoresCpu() {
        int hd = 1;
        Optional<HardwareAbstractionLayer> data = getHardwarelayer();
        if (data.isPresent()) {
            hd = data.get().getProcessor().getPhysicalProcessorCount();
        }
        return hd;
    }

    public static String getArchitecture() {
        String hd = "";
        Optional<HardwareAbstractionLayer> data = getHardwarelayer();
        if (data.isPresent()) {
            hd = data.get().getProcessor().getName() + "/" + data.get().getProcessor().getVendor();
        }
        return hd;
    }

    public static String getOsname() {
        String osname = "";
        Optional<OperatingSystem> data = getOslayer();
        if (data.isPresent()) {
            OperatingSystemVersion version = data.get().getVersion();
            osname = data.get().getFamily() + "/" + version.getCodeName() + "/" + version.getVersion() + " comp " + version.getBuildNumber();
        }
        return osname;
    }

    private static Optional<OperatingSystem> getOslayer() {
        return Optional.ofNullable(system.getOperatingSystem());
    }

    private static Optional<HardwareAbstractionLayer> getHardwarelayer() {
        return Optional.ofNullable(system.getHardware());
    }
}
