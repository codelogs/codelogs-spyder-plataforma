/*
 * The MIT License (MIT)
 * =========================================================================================================
 *
 * Copyright (c) 10/10/16 12:30 Modulo  Proyecto rexcode-monitor-service  Autor [rfcardenas]
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY
 * , WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ec.edu.utpl.datalab.codelogs.spyder.service.core;

import ec.edu.utpl.datalab.codelogs.spyder.core.models.SourceCode;
import org.apache.commons.vfs2.FileObject;
import org.glassfish.hk2.utilities.ServiceLocatorUtilities;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.*;

/**
 * Clase representa un mapa de los lenguajes de programacion registrados
 * ya sean estos localizados dentro del json , classpath o añadidos  de forma
 * * Created by rfcardenas
 */
public final class CodeLangMap {
    private static final Logger log = LoggerFactory.getLogger(CodeLangMap.class);

    private final Map<String, SourceCode> map;

    public CodeLangMap(Builder builder) {
        map = builder.map;
    }

    public static Builder builder() {
        return new Builder();
    }

    /**
     * Retorna la definicion de un fichero de codigo analizando la extension de este
     *
     * @param fileObject
     * @return
     */
    public Optional<SourceCode> getDefinitionOf(FileObject fileObject) {
        return getDefinitionOf(fileObject.getName().getExtension());
    }

    /**
     * Retorna la definicion de un fichero de codigo tomando como referencia la extension
     *
     * @param ext
     * @return
     */
    public Optional<SourceCode> getDefinitionOf(String ext) {
        Optional<SourceCode> codeOptional = map.entrySet().stream()
            .filter(e -> e.getKey().equals(ext))
            .map(Map.Entry::getValue)
            .findFirst();
        if (!codeOptional.isPresent()) {
            log.warn("No se pudo localizar la definición  para la extesión {} en el mapa", ext);
        }
        return codeOptional;
    }

    public Set<SourceCode> getCodeLangSet() {
        return new HashSet<>(map.values());
    }

    public static class Builder {
        private Set<SourceCode> sourceCodeSet = new HashSet<>();
        private Map<String, SourceCode> map = new HashMap<>();

        public Builder populateWith(File jsonProvider) {
            List<SourceCode> sources = CodeLangReader.sourceCodeList(jsonProvider);
            sourceCodeSet.addAll(sources);
            return this;
        }

        public Builder populateWith(SourceCode... sourceCodes) {
            Collections.addAll(sourceCodeSet, sourceCodes);
            return this;
        }

        public Builder populateWithAutoDiscoverService() {
            List<SourceCode> sources = ServiceLocatorUtilities.createAndPopulateServiceLocator()
                .getAllServices(SourceCode.class);
            sourceCodeSet.addAll(sources);
            return this;
        }

        public CodeLangMap buildMap() {
            if (sourceCodeSet.size() == 0) {
                log.warn("Ninguna definicion de lengauje se ha cargado  al mapa ....");
            }
            sourceCodeSet.forEach(sourceCode -> map.put(sourceCode.getExt(), sourceCode));
            return new CodeLangMap(this);
        }
    }
}
