/*
 * The MIT License (MIT)
 * =========================================================================================================
 *
 * Copyright (c) 10/10/16 12:30 Modulo  Proyecto rexcode-monitor-service  Autor [rfcardenas]
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY
 * , WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ec.edu.utpl.datalab.codelogs.spyder.service.core.analyzer;


import ec.edu.utpl.datalab.codelogs.spyder.service.core.MemoryBlock;
import ec.edu.utpl.datalab.codelogs.spyder.service.core.MemoryCell;
import ec.edu.utpl.datalab.codelogs.spyder.service.core.PulseAnalyticPack;
import ec.edu.utpl.datalab.codelogs.spyder.service.util.CodeDiff;
import org.apache.commons.io.FileUtils;
import org.apache.commons.vfs2.FileObject;
import org.joda.time.Interval;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.LinkedList;
import java.util.List;

/**
 * Analizador de los pulsos es encargado de extraer datos como direfferencia de codigo
 * el proceso de differencia de codigo es similar al empleado en github, se analiza un historico
 * de archivos para determinr los cambios y esfuerzo (diff_levenshtein) como metrica
 * * Created by rfcardenas
 */
public class PulseAnalyzer extends Analyzer<PulseAnalyticPack> {
    private final MemoryBlock defaultMemoryBlock;
    private Logger log = LoggerFactory.getLogger(PulseAnalyzer.class);
    private CodeDiff codeDiff = new CodeDiff(); // CodeDiff google porvider

    public PulseAnalyzer(MemoryBlock defaultMemoryBlock) {
        this.defaultMemoryBlock = defaultMemoryBlock;
    }

    /**
     * inspeccion un archivo de codigo
     *
     * @param object -> referencia fileobject
     * @throws IOException
     */
    private void inspect(FileObject object) throws IOException {
        String path = object.getName().getPath(); // path del fichero
        MemoryCell defaultMemoryCell = defaultMemoryBlock.recoverMemoryFrom(path); /// se recupera la memoria del archivo
        if (defaultMemoryCell != null) { // si  existe una memoria se puede ejecutar este tipo de analisis
            File fileCode = defaultMemoryCell.getSourceFile();
            File memCode = defaultMemoryCell.getMemoryFile();
            String str1 = FileUtils.readFileToString(memCode, Charset.defaultCharset());
            String str2 = FileUtils.readFileToString(fileCode, Charset.defaultCharset());
            LinkedList<CodeDiff.Diff> diffLinkedList = codeDiff.diff_main(str1, str2, true);
            int linesAdd = countAddCode(diffLinkedList);
            int linesDel = countDeleteCode(diffLinkedList);
            long diffLevenshtein = codeDiff.diff_levenshtein(diffLinkedList);
            long delta_time = calculateDetal(fileCode, memCode);
            PulseAnalyticPack resul = new PulseAnalyticPack(object, linesAdd, linesDel, diffLevenshtein, delta_time);
            publish(resul);
            log.info("\n" + resul.toString());
        } else {
            log.warn("Memoria no encontrada para el archivo {} ", object.getName().getBaseName());
        }
    }

    /**
     * Calcula el tiempo delta de edicion del archivo, para esto se utiliza los intervalos
     * de fecha de creacion de los archivos
     *
     * @param file
     * @param mem
     * @return
     * @throws IOException
     */
    private long calculateDetal(File file, File mem) throws IOException {
        try {
            Long timemem = mem.lastModified();
            Long timefile = file.lastModified();
            Interval interval = new Interval(timemem, timefile);
            long timediff = interval.toDuration().getStandardSeconds();
            return timediff;
        } catch (RuntimeException e) {
            log.error("Imposible calcular Diff");
        }
        return 0;
    }

    /**
     * Contador de lineas de codigo añadidas
     *
     * @param diffs
     * @return
     */
    private int countAddCode(List<CodeDiff.Diff> diffs) {
        return diffs.stream().filter(diff -> diff.operation == CodeDiff.Operation.INSERT)
            .mapToInt(this::countFromDiff)
            .sum();
    }

    /**
     * Funcion cuenta las lineas de codigo eliminadas, comparando la memoria del codigo
     * con el estado actual
     *
     * @param diffs
     * @return
     */
    private int countDeleteCode(List<CodeDiff.Diff> diffs) {
        return diffs.stream().filter(diff -> diff.operation == CodeDiff.Operation.DELETE)
            .mapToInt(this::countFromDiff)
            .sum();
    }

    /**
     * Funcion cuenta el numero de diferencias encontradas
     *
     * @param diff
     * @return
     */
    private int countFromDiff(CodeDiff.Diff diff) {
        try {
            return sliceCounter(diff.text);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return 0;
    }

    private int sliceCounter(String codeslice) throws IOException {
        byte[] c = new byte[1024];
        int count = 0;
        int readChars = 0;
        InputStream stream = new ByteArrayInputStream(codeslice.getBytes(StandardCharsets.UTF_8));
        try {
            while ((readChars = stream.read(c)) != -1) {
                for (int i = 0; i < readChars; ++i) {
                    if (c[i] == '\n') {
                        ++count;
                    }
                }
            }
        } finally {
            stream.close();
        }
        return count;
    }

    /**
     * Implementacion del metodo abstracto, el cual contiene la logica y las
     * llamadas a los metodos necesarios para este tipo de analisis
     *
     * @param file
     */
    @Override
    public void analyze(FileObject file) {
        try {
            inspect(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
