/*
 * The MIT License (MIT)
 * =========================================================================================================
 *
 * Copyright (c) 10/10/16 12:30 Modulo  Proyecto rexcode-monitor-service  Autor [rfcardenas]
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY
 * , WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package ec.edu.utpl.datalab.codelogs.spyder.service.tasks;

import ec.edu.utpl.datalab.codelogs.spyder.core.env.EnvironmentProperties;
import ec.edu.utpl.datalab.codelogs.spyder.core.env.PropertiesDefault;
import ec.edu.utpl.datalab.codelogs.spyder.core.models.Project;
import ec.edu.utpl.datalab.codelogs.spyder.libs.filesystem.code_event2.EventSystem;
import ec.edu.utpl.datalab.codelogs.spyder.libs.filesystem.code_event2.file.EventFileSystem;
import ec.edu.utpl.datalab.codelogs.spyder.libs.filesystem.code_event2.packs.Event;
import ec.edu.utpl.datalab.codelogs.spyder.libs.filesystem.code_event2.time.TimeManager;
import ec.edu.utpl.datalab.codelogs.spyder.libs.filesystem.search.FilterSet;
import ec.edu.utpl.datalab.codelogs.spyder.libs.filesystem.search.SearchFileSystem;
import ec.edu.utpl.datalab.codelogs.spyder.service.core.CodeLangMap;
import ec.edu.utpl.datalab.codelogs.spyder.service.core.analyzer.CacheWorkSession;
import org.apache.commons.vfs2.FileObject;
import org.apache.commons.vfs2.FileSystemException;
import org.apache.commons.vfs2.VFS;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import rx.Observable;

import javax.inject.Inject;
import java.util.Set;
import java.util.concurrent.TimeUnit;

/**
 * * Created by rfcardenas
 */
public class PipeFileSystemEvent extends AbstractService {

    private static final Logger log = LoggerFactory.getLogger(PipeFileSystemEvent.class);

    private CodeLangMap codeLangMap;
    private EventSystem eventSystem;
    private EventFileSystem eventFileSystem;
    private CacheWorkSession cacheWorkSession;
    private EnvironmentProperties environmentProperties;

    @Inject
    public PipeFileSystemEvent(
        EventFileSystem eventFileSystem,
        CacheWorkSession cacheWorkSession,
        EnvironmentProperties environmentProperties)

    {
        this.cacheWorkSession = cacheWorkSession;
        this.eventFileSystem = eventFileSystem;
        this.environmentProperties = environmentProperties;
        init();
    }

    private void init() {
        cacheWorkSession.newSessionNotify()
            .subscribe(workSession -> enableEvents(workSession.getProject()));
        cacheWorkSession.closeSessionNotify()
            .subscribe(workSession -> disableEvents(workSession.getProject()));

        this.eventSystem = EventSystem.builder()
            .withSourceEvents(eventFileSystem)
            .withTimerManager(new TimeManager(5, TimeUnit.MINUTES))
            .build();


    }

    private void enableEvents(Project project) {
        log.info("Activando eventos para {}", project.getName());
        try {
            FileObject fileObject = VFS.getManager().resolveFile(project.getPath());
            log.info("Buscando directorio permitido {} tipo carpeta {}", fileObject.getName(), fileObject.isFolder());
            selectWatch(fileObject);
        } catch (Exception e) {
            e.printStackTrace();
            log.info("Error  " + e.getMessage());
        }
    }

    /**
     * Selecciona la carpeta del proyecto a minitorear, el orden preconfigurado,
     * si encuentra una entonces se moniterea esta y se sale del programa
     *
     * @param fileObject
     */
    private void selectWatch(FileObject fileObject) throws FileSystemException {
        boolean runFinally = true;
        log.info("Select watch");
        if (!fileObject.exists() && !fileObject.isFolder()) {
            log.info("Proyecto no valido existe: {} & es carpeta : {}", fileObject.exists(), fileObject.isFolder());
            return;
        }
        Set<String> targets = environmentProperties.getAsSetWithDefaults(PropertiesDefault.MONITOR_SCAN_SOURCE, String.class, "src");
        log.info("Objetivos disponibles {}", targets.size());
        for (String target : targets) {
            log.info("Buscando objetivo {}", target);
            /**
             * Buscador con una profunidad de tres niveles tomando como
             * referencia el proyecto pasado como parametro
             */
            FileObject[] a = SearchFileSystem.newSearch(2)
                .withFilter(FilterSet.filterOnlyFolders())
                .withFilter(FilterSet.filterByName(target))
                .build().executeFinder(fileObject);
            if (a.length > 0) {
                log.info("Carpeta de codigo encontrada -[{}]", target);
                eventSystem.watch(a[0]);
                runFinally = false;
            }
            break;
        }
        if (runFinally) {
            log.warn("No se encontro ninguna carpeta, escuchando eventos para todos los niveles en el proyecto");
            eventSystem.watch(fileObject);
        }

    }

    private void disableEvents(Project project) {
        log.info("Desactivando eventos para {}", project.getName());
        try {
            FileObject fileObject = VFS.getManager().resolveFile(project.getPath());
            Set<String> targets = environmentProperties.getAsSetWithDefaults(PropertiesDefault.MONITOR_SCAN_SOURCE, String.class, "src");
            log.info("Objetivos disponibles {}", targets.size());
            for (String target : targets) {
                log.info("Buscando objetivo {}", target);
                /**
                 * Buscador con una profunidad de tres niveles tomando como
                 * referencia el proyecto pasado como parametro
                 */
                FileObject[] a = SearchFileSystem.newSearch(2)
                    .withFilter(FilterSet.filterOnlyFolders())
                    .withFilter(FilterSet.filterByName(target))
                    .build().executeFinder(fileObject);
                if (a.length > 0) {
                    log.info("Carpeta de codigo encontrada -[{}]", target);
                    eventSystem.stopWatch(a[0].getName().getFriendlyURI());
                }
            }
        } catch (FileSystemException ex) {
            log.error("Error sistema de archivos ", ex.getMessage());
            ex.printStackTrace();
        }

    }

    @Override
    public void start() {
        eventSystem.start();
    }

    public Observable<Event> notifyEvents() {
        return eventSystem.observable();
    }
}
