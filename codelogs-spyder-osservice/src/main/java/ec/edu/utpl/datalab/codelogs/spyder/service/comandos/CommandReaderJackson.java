package ec.edu.utpl.datalab.codelogs.spyder.service.comandos;

import ec.edu.utpl.datalab.codelogs.spyder.core.util.JsonMapper;
import ec.edu.utpl.datalab.codelogs.spyder.libs.terminal.Command;
import ec.edu.utpl.datalab.codelogs.spyder.libs.terminal.CommandEntry;
import ec.edu.utpl.datalab.codelogs.spyder.libs.terminal.SystemTerminal;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Consumer;

/**
 * * Created by rfcardenas
 */
public class CommandReaderJackson implements Consumer<String> {
    private static final Logger log = LoggerFactory.getLogger(CommandReaderJackson.class);

    private static final String THREAD_NAME_PATTERN = "%s-%d";
    private SystemTerminal systemTerminal;
    private ExecutorService threadPoolExecutor;

    @Inject
    public CommandReaderJackson(SystemTerminal systemTerminal) {
        this.systemTerminal = systemTerminal;
        this.threadPoolExecutor = Executors.newFixedThreadPool(2, new ThreadFactory() {
            private final AtomicInteger counter = new AtomicInteger();

            @Override
            public Thread newThread(Runnable r) {
                final String threadName = String.format(THREAD_NAME_PATTERN, "Processor", counter.incrementAndGet());
                return new Thread(r, threadName);
            }
        });
    }

    @Override
    public void accept(String commandstrg) {
        log.info(commandstrg);
        Optional<Command> command = JsonMapper.read(Command.class,commandstrg);
        if(command.isPresent()){
            String[] commands = new String[command.get().getCommandEntries().size()];

            List<CommandEntry> commEntry = command.get().getCommandEntries();
            for (int i = 0; i < commEntry.size(); i++) {
                CommandEntry entry = commEntry.get(i);
                String instruction = entry.getInstruction();
                String value = entry.getArgument();
                commands[i] = "-" + instruction + " " + value;
            }

            threadPoolExecutor.execute(() -> {
                systemTerminal.process(commands);
            });
        }else{
            log.error("Error no se pudo mapear el comando");
        }

    }
}
