/*
 * The MIT License (MIT)
 * =========================================================================================================
 *
 * Copyright (c) 10/10/16 12:30 Modulo  Proyecto rexcode-monitor-service  Autor [rfcardenas]
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY
 * , WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ec.edu.utpl.datalab.codelogs.spyder.service.comandos;

import ec.edu.utpl.datalab.codelogs.spyder.core.models.Project;
import ec.edu.utpl.datalab.codelogs.spyder.service.core.analyzer.DefualtRouter;
import ec.edu.utpl.datalab.codelogs.spyder.libs.terminal.SystemCommand;
import ec.edu.utpl.datalab.codelogs.spyder.libs.terminal.SystemCommandProvider;
import org.apache.commons.cli.Option;
import org.apache.commons.vfs2.FileObject;
import org.apache.commons.vfs2.FileSystemException;
import org.apache.commons.vfs2.VFS;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import rx.Observable;
import rx.subjects.PublishSubject;

import javax.inject.Inject;
import java.time.ZonedDateTime;
import java.util.Map;

/**
 * Abre un proyecto
 * * Created by rfcardenas
 */
public class SystemOpenProject implements SystemCommandProvider {
    private static final Logger log = LoggerFactory.getLogger(SystemOpenProject.class);
    private final DefualtRouter defualtRouter;
    private PublishSubject<FileObject> subject = PublishSubject.create();

    /**
     * Requiere router data ,
     *
     * @param defualtRouter
     */
    @Inject
    public SystemOpenProject(DefualtRouter defualtRouter) {
        this.defualtRouter = defualtRouter;

    }

    @Override
    public SystemCommand command() {
        Option openOption = Option.builder(MonitorCommand.CMD_OPEN_PROJECT)
            .hasArg()
            .desc("Abre un proyecto")
            .build();
        Option apiKeyOption = Option.builder(MonitorCommand.IDE_ARGUMENT)
            .hasArg()
            .desc("IDE del proyecto")
            .build();
        return SystemCommand.builder(openOption)
            .addOptions(apiKeyOption)
            .setAction(this::execute)
            .build();
    }

    public void execute(Map<String, String> args) {
        log.info("Ejecutando comando con argumentos {}", args.get(MonitorCommand.CMD_OPEN_PROJECT));
        String projectPath = args.get(MonitorCommand.CMD_OPEN_PROJECT);
        String ide = (args.get(MonitorCommand.IDE_ARGUMENT) == null) ? MonitorCommand.IDE_DEFAULT : args.get(MonitorCommand.IDE_ARGUMENT);

        if (isValidPath(projectPath)) {
            try {
                FileObject object = VFS.getManager().resolveFile(projectPath);
                Project project = new Project();
                project.setName(object.getName().getBaseName());
                project.setCreateDate(ZonedDateTime.now());
                project.setPath(object.getName().getFriendlyURI());
                project.setIde(ide);
                defualtRouter.createRouteFor(project);
                subject.onNext(object);
            } catch (FileSystemException e) {
                log.error("Error al resolver el archivo causa {}",e);
            }
        } else {
            log.error("Agumento no valido {} ", args);
        }
    }

    public boolean isValidPath(String path) {
        try {
            FileObject object = VFS.getManager().resolveFile(path);
            return object.isFolder() && object.exists();
        } catch (FileSystemException e) {
            log.warn("El proyecto no es una referencia valida {}",path,e);
        }
        return false;
    }

    public Observable<FileObject> observable() {
        return subject;
    }

}
