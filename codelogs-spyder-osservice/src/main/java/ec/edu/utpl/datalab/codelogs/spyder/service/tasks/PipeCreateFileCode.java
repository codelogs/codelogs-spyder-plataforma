/*
 * The MIT License (MIT)
 * =========================================================================================================
 *
 * Copyright (c) 10/10/16 12:30 Modulo  Proyecto rexcode-monitor-service  Autor [rfcardenas]
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY
 * , WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ec.edu.utpl.datalab.codelogs.spyder.service.tasks;

import ec.edu.utpl.datalab.codelogs.spyder.core.models.FileCode;
import ec.edu.utpl.datalab.codelogs.spyder.core.util.pipes.AbstractPipe;
import ec.edu.utpl.datalab.codelogs.spyder.libs.filesystem.code_event2.packs.EventType;
import ec.edu.utpl.datalab.codelogs.spyder.service.core.analyzer.DefualtRouter;
import org.apache.commons.vfs2.FileObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;

/**
 * * Created by rfcardenas
 */
public class PipeCreateFileCode extends AbstractPipe<FileObject, FileCode> {
    private static final Logger LOG = LoggerFactory.getLogger(PipeCreateFileCode.class);

    private PipeValidateCode validateCode;
    private DefualtRouter defualtRouter;

    @Inject
    public PipeCreateFileCode(DefualtRouter defualtRouter, PipeValidateCode validateCode) {
        this.defualtRouter = defualtRouter;
        this.validateCode = validateCode;
        init();
    }

    private void init() {
        validateCode.observable()
            .filter(event -> event.isOfType(EventType.FILE_CREATE, EventType.FILE_UPDATE))
            .subscribe(event -> createFileInDb(event.getFileObject()));
    }

    public void createFileInDb(FileObject fileObject) {
        FileCode fileCode = new FileCode();
        fileCode.setPathFile(fileObject.getName().getFriendlyURI());
        fileCode.setAvailable(true);
        fileCode.setExtension(fileObject.getName().getExtension());
        fileCode.setName(fileObject.getName().getBaseName());
        fileCode.setAvailable(true);
        fileCode.setSync(false);
        fileCode.setFileObject(fileObject);
        defualtRouter.ruouteFile(fileCode);
        notify(fileCode);
        //});
    }

    @Override
    public void onCompleted() {
        LOG.info("End Sub");
    }

    @Override
    public void onError(Throwable e) {
        LOG.error(e.getMessage());
    }

    @Override
    public void onNext(FileObject fileObject) {
        createFileInDb(fileObject);
    }
}
