package ec.edu.utpl.datalab.codelogs.spyder.service.comandos;

import ec.edu.utpl.datalab.codelogs.spyder.libs.filesystem.code_event2.file.EventFileSystem;
import ec.edu.utpl.datalab.codelogs.spyder.libs.filesystem.code_event2.packs.Event;
import ec.edu.utpl.datalab.codelogs.spyder.libs.filesystem.code_event2.packs.EventType;
import ec.edu.utpl.datalab.codelogs.spyder.libs.terminal.SystemCommand;
import ec.edu.utpl.datalab.codelogs.spyder.libs.terminal.SystemCommandProvider;
import org.apache.commons.cli.Option;
import org.apache.commons.vfs2.FileObject;
import org.apache.commons.vfs2.FileSystemException;
import org.apache.commons.vfs2.VFS;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import rx.Observable;
import rx.subjects.PublishSubject;

import java.util.Map;

/**
 * * Created by rfcardenas
 */

public class SystemFileEvent implements SystemCommandProvider, EventFileSystem {
    private static final Logger log = LoggerFactory.getLogger(SystemFileEvent.class);
    private static final PublishSubject<Event> subject = PublishSubject.create();



    public SystemFileEvent() {
    }

    @Override
    public SystemCommand command() {
        Option updateOption = Option.builder(MonitorCommand.CMD_EVENT_FILE)
            .hasArg()
            .desc("monitorea el proyecto")
            .build();
        Option argument = Option.builder(MonitorCommand.CMD_EVENT_TYPE)
            .hasArg()
            .desc("Argumento path")
            .build();
        return SystemCommand.builder(updateOption)
            .addOptions(argument)
            .setAction(this::execute)
            .build();
    }

    public void execute(Map<String, String> args) {
        log.info("Ejecutando comando con argumentos {} , {}", args.get(MonitorCommand.CMD_EVENT_FILE), args.get(MonitorCommand.CMD_EVENT_TYPE));
        String eventPath = args.get(MonitorCommand.CMD_EVENT_FILE);
        String eventType = args.get(MonitorCommand.CMD_EVENT_TYPE);

        EventType enumEvent = EventType.FILE_UPDATE;
        if (eventType.equals(MonitorCommand.EVENT_VALUE_UPDATE)) {
            enumEvent = EventType.FILE_UPDATE;
        } else if (eventType.equals(MonitorCommand.EVENT_VALUE_DELETE)) {
            enumEvent = EventType.FILE_DELETE;
        } else if (eventType.equals(MonitorCommand.EVENT_VALUE_CREATE)) {
            enumEvent = EventType.FILE_CREATE;
        }

        try {
            FileObject fileObject = VFS.getManager().resolveFile(eventPath);
            Event event = new Event(fileObject, enumEvent);
            log.info("Notificando evento de tipo {}", event.toString());
            subject.onNext(event);
        } catch (FileSystemException e) {
            e.printStackTrace();
        }
    }


    @Override
    public Observable<Event> observable() {
        return subject;
    }

    @Override
    public void start() {
        // por defecto nada los eventos son externos
    }

    @Override
    public void stop() {
        /**
         * los eventos son externos
         */

    }

    @Override
    public void followEvents(FileObject fileObject) {
        // los eventos son externo
    }

    @Override
    public void unfollowEvents(FileObject fileObject) {
        // los eventos son externo
    }
}
