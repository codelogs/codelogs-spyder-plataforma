package ec.edu.utpl.datalab.codelogs.spyder.service.core;

import ec.edu.utpl.datalab.codelogs.spyder.core.env.EnvironmentProperties;
import ec.edu.utpl.datalab.codelogs.spyder.core.env.PropertiesDefault;
import ec.edu.utpl.datalab.codelogs.spyder.core.filesystem.EnvironmentAppNotFound;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * * Created by rfcardenas
 */
public class DefaultEnvironmentResolver implements EnvironmentResolver {
    private static final Logger log = LoggerFactory.getLogger(DefaultEnvironmentResolver.class);
    private EnvironmentProperties environmentProperties;
    private HomeService homeService;


    public DefaultEnvironmentResolver(EnvironmentProperties environmentProperties) {
        this.environmentProperties = environmentProperties;
    }

    @Override
    public HomeService service() {
        if(environmentProperties.hasProperty(PropertiesDefault.SPYDER_HOME)){
            try {
                return new HomeServiceContextFolder(environmentProperties);
            } catch (EnvironmentAppNotFound appNotFound) {
                log.error("No se pudo cargar homeservice con un path como argumento");
            }
        }
        log.info("Cargando configuraciones desde classloader");

        return new HomeServiceContextClassPath();
    }
}
