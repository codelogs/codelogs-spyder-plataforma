package ec.edu.utpl.datalab.codelogs.spyder.service.tasks;

import ec.edu.utpl.datalab.codelogs.spyder.core.synchronization.ModuloSynchronization;
import ec.edu.utpl.datalab.codelogs.spyder.core.synchronization.STRATEGY_SYNC;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;

/**
 * * Created by rfcardenas
 */
public class PipeSyncPulse extends AbstractService {
    private static final Logger log = LoggerFactory.getLogger(PipeSyncPulse.class);

    private ModuloSynchronization moduloSynchronization;

    @Inject
    public PipeSyncPulse(ModuloSynchronization moduloSynchronization) {
        this.moduloSynchronization = moduloSynchronization;
    }

    @Override
    public void start() {
        moduloSynchronization.onNext(STRATEGY_SYNC.LAST_CHANGES);
    }
}
