/*
 * The MIT License (MIT)
 * =========================================================================================================
 *
 * Copyright (c) 10/10/16 12:30 Modulo  Proyecto rexcode-monitor-service  Autor [rfcardenas]
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY
 * , WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ec.edu.utpl.datalab.codelogs.spyder.service.core;

import java.io.File;

/**
 * * Created by rfcardenas
 */
public interface MemoryCell {
    /**
     * Syncroniza la memoria de un codigo  (ARchivo de codigo)
     * Dos comportamientos posibles en un solo metodo
     * - Crear si no existe
     * - Actualizar si existe
     */
    void sync();

    /**
     * Retorna el archivo de codigo
     * Este archivo contiene la memoria del codigo antes de un cambio
     *
     * @return
     */
    File getSourceFile();

    /**
     * Determina si el archivo en disco esta presente
     * Es decir si la memorya en disco esta presente
     *
     * @return
     */
    boolean memoryFileExit();

    void destroyMemory();

    File getMemoryFile();

    /**
     * Destriye la memorya
     * Este metodo destruye los arhcivos fisicos creados en HD
     */

}
