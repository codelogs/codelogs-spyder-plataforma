package ec.edu.utpl.datalab.codelogs.spyder.service.comandos;

import com.google.inject.Singleton;
import ec.edu.utpl.datalab.codelogs.spyder.core.synchronization.ModuloSynchronization;
import ec.edu.utpl.datalab.codelogs.spyder.core.synchronization.STRATEGY_SYNC;
import ec.edu.utpl.datalab.codelogs.spyder.libs.terminal.SystemCommand;
import ec.edu.utpl.datalab.codelogs.spyder.libs.terminal.SystemCommandProvider;
import org.apache.commons.cli.Option;

import javax.inject.Inject;
import java.util.Map;

/**
 * * Created by rfcardenas
 */
@Singleton
public class SystemSync implements SystemCommandProvider {
    private ModuloSynchronization moduloSynchronization;

    @Inject
    public SystemSync(ModuloSynchronization moduloSynchronization) {
        this.moduloSynchronization = moduloSynchronization;
    }

    @Override
    public SystemCommand command() {
        Option openOption = Option.builder(MonitorCommand.CMD_SYNC)
            .hasArg()
            .desc("Abre un proyecto")
            .build();

        return SystemCommand.builder(openOption)
            .setAction(this::execute)
            .build();
    }

    public void execute(Map<String, String> args) {
        moduloSynchronization.onNext(STRATEGY_SYNC.FULL_CHANGES);
    }
}
