/*
 * The MIT License (MIT)
 * =========================================================================================================
 *
 * Copyright (c) 10/10/16 12:30 Modulo  Proyecto rexcode-monitor-service  Autor [rfcardenas]
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY
 * , WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ec.edu.utpl.datalab.codelogs.spyder.service.core.analyzer;

import ec.edu.utpl.datalab.codelogs.spyder.core.models.FileCode;
import ec.edu.utpl.datalab.codelogs.spyder.core.models.HeartBeatBuildRun;
import ec.edu.utpl.datalab.codelogs.spyder.core.models.HeartBeatCode;
import ec.edu.utpl.datalab.codelogs.spyder.core.models.Project;
import org.apache.commons.vfs2.FileObject;

import java.util.Optional;

/**
 * * Created by rfcardenas
 */
public interface Router {
    /**
     * Retorna el contexto para un evento de archivo
     *
     * @param fileObject
     * @return
     */
    Optional<DefaultRouterProjectData> getContext(FileObject fileObject);

    /**
     * Enruta la entidad FileCode
     *
     * @param code
     */
    void ruouteFile(FileCode code);

    /**
     * Enruta la entidad pulse code
     *
     * @param pulse
     */
    void routePulse(HeartBeatCode pulse);

    /**
     * Entura la entidad compole
     */
    void routeCompileRunData(HeartBeatBuildRun data);
    /**
     * crea la ruta para un proyecto
     *
     * @param project
     */
    void createRouteFor(Project project);

    /**
     * Elimina la ruta para un proyecto
     *
     * @param project
     */
    void removeRoute(String project);
}
