package ec.edu.utpl.datalab.codelogs.spyder.service.core;

import ec.edu.utpl.datalab.codelogs.spyder.core.env.PropertiesDefault;
import org.apache.commons.vfs2.FileObject;
import org.apache.commons.vfs2.NameScope;
import org.apache.commons.vfs2.VFS;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * * Created by rfcardenas
 */
public class HomeServiceContextClassPath implements HomeService {
    private static final Logger log = LoggerFactory.getLogger(DefaultEnvironmentResolver.class);

    private ClassLoader  classLoader;


    public HomeServiceContextClassPath(ClassLoader classLoader ) {
        this.classLoader = classLoader;
    }


    public HomeServiceContextClassPath() {
        classLoader = getClass().getClassLoader();
    }

    @Override
    public FileObject getConfigFile() throws Exception {
        String file = classLoader.getResource("monitor_daemon.properties").getFile();
        FileObject object = VFS.getManager().resolveFile(file);
        return object;
    }

    @Override
    public FileObject getSupportLangsFile() throws Exception {
        String file = classLoader.getResource("monitor_support.json").getFile();
        FileObject object = VFS.getManager().resolveFile(file);
        return object;
    }

    @Override
    public FileObject getFixPathStore() throws Exception {
        String temporalPath = System.getProperty("java.io.tmpdir");
        String path = PropertiesDefault.SPYDER_HOME_DB_PATH;
        FileObject temporal = VFS.getManager().resolveFile(temporalPath);
        FileObject pathDb = temporal.resolveFile(path, NameScope.DESCENDENT_OR_SELF);
        if(!pathDb.exists()){
            pathDb.createFile();
            log.warn("path no existe, pasando object como url absoluta");
        }
        return pathDb;
    }
}
