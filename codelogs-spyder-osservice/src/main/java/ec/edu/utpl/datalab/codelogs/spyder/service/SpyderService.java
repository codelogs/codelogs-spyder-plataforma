/*
 * The MIT License (MIT)
 * =========================================================================================================
 *
 * Copyright (c) 10/10/16 12:30 Modulo  Proyecto rexcode-monitor-service  Autor [rfcardenas]
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY
 * , WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ec.edu.utpl.datalab.codelogs.spyder.service;

import com.google.inject.Guice;
import com.google.inject.Injector;
import ec.edu.utpl.datalab.codelogs.spyder.core.Application;
import ec.edu.utpl.datalab.codelogs.spyder.core.ApplicationDaemon;
import ec.edu.utpl.datalab.codelogs.spyder.core.DeamonFactory;
import ec.edu.utpl.datalab.codelogs.spyder.core.DefaultApplicationManager;
import ec.edu.utpl.datalab.codelogs.spyder.core.env.*;
import ec.edu.utpl.datalab.codelogs.spyder.core.os.ws.WebSocketProcessChannel;
import ec.edu.utpl.datalab.codelogs.spyder.core.persistencia.DefaultRepository;
import ec.edu.utpl.datalab.codelogs.spyder.core.synchronization.ModuloSynchronization;
import ec.edu.utpl.datalab.codelogs.spyder.core.synchronization.STRATEGY_SYNC;
import ec.edu.utpl.datalab.codelogs.spyder.service.comandos.CommandReaderJackson;
import ec.edu.utpl.datalab.codelogs.spyder.service.comandos.ConfigurePipe;
import ec.edu.utpl.datalab.codelogs.spyder.service.core.DefaultEnvironmentResolver;
import ec.edu.utpl.datalab.codelogs.spyder.service.core.EnvironmentCheckerBasic;
import ec.edu.utpl.datalab.codelogs.spyder.service.core.EnvironmentResolver;
import ec.edu.utpl.datalab.codelogs.spyder.service.tasks.PipeCreateFileCode;
import ec.edu.utpl.datalab.codelogs.spyder.service.tasks.PipeFileSystemEvent;
import org.apache.commons.vfs2.FileObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.InputStream;
import java.util.concurrent.Executors;

/**
 * * Created by rfcardenas
 */
public class SpyderService extends ApplicationDaemon {
    private static final Logger log = LoggerFactory.getLogger(SpyderService.class);

    public static final InputStream INPUT_STREAM = System.in;

    public static void main(String[] args) throws Exception {
        try {
            //soluciona el contexto de ejucion, home de spyder
            EnvironmentResolver resolver = new DefaultEnvironmentResolver(new ArgsEnvironmentProperties(args));
            FileObject file = resolver.service().getConfigFile();
            EnvironmentProperties loader = new FileEnvironmentPropertiesImpl(file, PropertiesDefault.map);
            // Nivel 1 Defaults, luego lo que esta en el archivo
            if (args.length > 0) {
                loader.overrideWith(new ArgsEnvironmentProperties(args));
            }
            loader.putValue(PropertiesDefault.SPYDER_HOME_DB_PATH,resolver.service().getFixPathStore().getName().getPath());
            loader.putValue(PropertiesDefault.SPYDER_HOME_CONFIG_FILE_SUPPORT,resolver.service().getSupportLangsFile().getName().getPath());
            EnvironmentDebug.debug(loader);
            EnvironmentCkecker environmentCkecker = new EnvironmentCheckerBasic();
            environmentCkecker.check(loader);
            Integer port = loader.getAsInt(PropertiesDefault.MONITOR_PROC_CMD_PORT, 8484);
            Application application = new DeamonFactory()
                .withProcessChannel(new WebSocketProcessChannel(port))
                .withProperties(loader)
                .build(new SpyderService());

            DefaultApplicationManager defaultApplicationManager = new DefaultApplicationManager();
            defaultApplicationManager.start(application);
        }catch (Exception e){
            log.error("Error en spyder detectado  ",e);
        }
    }

    @Override
    protected void configure() {
        Thread.currentThread().setName("RxDaemon");
        super.configure();
    }

    @Override
    public void onStart() {
        DefaultRepository repositoryService = new DefaultRepository(getEnvironmentProperties());
        // crea el injector con las dependencias
        Injector inject = Guice.createInjector(new InjectionService(repositoryService, getEnvironmentProperties()));
        inject.getInstance(PipeCreateFileCode.class);
        inject.getInstance(ConfigurePipe.class).joinPipes();
        inject.getInstance(ModuloSynchronization.class).start();
        // configura el preprocesador de comandos
        setCmdProcessor(inject.getInstance(CommandReaderJackson.class));
        /**
         * Notifica todos los eventos al modulo de sync
         */
        inject.getInstance(PipeFileSystemEvent.class).notifyEvents()
            .subscribe(event -> {
                inject.getInstance(ModuloSynchronization.class).onNext(STRATEGY_SYNC.LAST_CHANGES);
            });
        Executors.newFixedThreadPool(1).execute(inject.getInstance(PipeFileSystemEvent.class));
    }

    @Override
    public void onStop() {
    }

}
