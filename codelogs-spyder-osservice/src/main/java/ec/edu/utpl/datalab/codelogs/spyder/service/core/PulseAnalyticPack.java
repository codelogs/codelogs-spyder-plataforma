/*
 * The MIT License (MIT)
 * =========================================================================================================
 *
 * Copyright (c) 10/10/16 12:30 Modulo  Proyecto rexcode-monitor-service  Autor [rfcardenas]
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY
 * , WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ec.edu.utpl.datalab.codelogs.spyder.service.core;

import org.apache.commons.vfs2.FileObject;

/**
 * Representa una paquete de analisis, este paquete es enviado al servidor
 * Contiene
 * lineas_add --> Lineas añadidas en el pulso
 * lineas_del --> Cantidad de lineas elimnadas tras el pulso
 * levenshtein --> el esfuerza de cambios
 * delta_time_edit --> el tiempo que le tomo al programador efectuar el cmabio
 * * Created by rfcardenas
 */
public class PulseAnalyticPack extends AbstractPack {
    public final int lineas_add;
    public final int lineas_del;
    public final long levenshtein;
    public final long delta_time_edit;

    /**
     * @param codefile
     * @param lineas_add
     * @param lineas_del
     * @param levenshtein
     * @param delta_time_edit
     */
    public PulseAnalyticPack(FileObject codefile, int lineas_add, int lineas_del, long levenshtein, long delta_time_edit) {
        super(codefile);
        this.lineas_add = lineas_add;
        this.lineas_del = lineas_del;
        this.levenshtein = levenshtein;
        this.delta_time_edit = delta_time_edit;
    }

    @Override
    public String toString() {
        return "{lines_add: " + lineas_add + "," +
            "lines_del: " + lineas_del + "," +
            "levenshtein: " + levenshtein + "," +
            "delta_time_edit: " + delta_time_edit + "}";
    }
}
