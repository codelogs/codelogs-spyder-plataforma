/*
 * The MIT License (MIT)
 * =========================================================================================================
 *
 * Copyright (c) 10/10/16 12:30 Modulo  Proyecto rexcode-monitor-service  Autor [rfcardenas]
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY
 * , WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ec.edu.utpl.datalab.codelogs.spyder.service.tasks;

import ec.edu.utpl.datalab.codelogs.spyder.libs.filesystem.code_event2.packs.Event;
import ec.edu.utpl.datalab.codelogs.spyder.libs.filesystem.code_event2.packs.EventType;
import ec.edu.utpl.datalab.codelogs.spyder.service.core.MemoryBlock;
import ec.edu.utpl.datalab.codelogs.spyder.service.core.analyzer.DefaultRouterProjectData;
import ec.edu.utpl.datalab.codelogs.spyder.service.core.analyzer.DefualtRouter;
import ec.edu.utpl.datalab.codelogs.spyder.service.core.analyzer.Router;
import org.apache.commons.vfs2.FileObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.util.Optional;

/**
 * * Created by rfcardenas
 */
public class PipeClearMemory {
    private static final Logger log = LoggerFactory.getLogger(PipeCommandButler.class);
    private PipeFileSystemEvent pipeFileSystemEvent;
    private Router defualtRouter;

    @Inject
    public PipeClearMemory(PipeFileSystemEvent pipeFileSystemEvent, DefualtRouter defualtRouter) {
        this.pipeFileSystemEvent = pipeFileSystemEvent;
        this.defualtRouter = defualtRouter;

        pipeFileSystemEvent.notifyEvents()
            .filter(event -> event.isOfType(EventType.TIME_END))
            .map(Event::getFileObject)
            .subscribe(this::handleTimer, Throwable::printStackTrace);
    }

    private void handleTimer(FileObject fileObject) {
        Optional<DefaultRouterProjectData> context = defualtRouter.getContext(fileObject);
        if (context.isPresent()) {
            log.info("Task clear defaultMemoryBlock run...");
            MemoryBlock memoryBlock = context.get().getDefaultMemoryBlock();
            memoryBlock.removeMemoryof(fileObject);
        }
    }
}
