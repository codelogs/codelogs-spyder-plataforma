/*
 * The MIT License (MIT)
 * =========================================================================================================
 *
 * Copyright (c) 10/10/16 12:30 Modulo  Proyecto rexcode-monitor-service  Autor [rfcardenas]
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY
 * , WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ec.edu.utpl.datalab.codelogs.spyder.service.tasks;

import ec.edu.utpl.datalab.codelogs.spyder.core.models.FileCode;
import ec.edu.utpl.datalab.codelogs.spyder.core.models.HeartBeatCode;
import ec.edu.utpl.datalab.codelogs.spyder.core.util.pipes.AbstractPipe;
import ec.edu.utpl.datalab.codelogs.spyder.libs.filesystem.code_event2.packs.EventType;
import ec.edu.utpl.datalab.codelogs.spyder.service.core.MemoryBlock;
import ec.edu.utpl.datalab.codelogs.spyder.service.core.PulseAnalyticPack;
import ec.edu.utpl.datalab.codelogs.spyder.service.core.analyzer.DefaultRouterProjectData;
import ec.edu.utpl.datalab.codelogs.spyder.service.core.analyzer.DefualtRouter;
import ec.edu.utpl.datalab.codelogs.spyder.service.core.analyzer.PulseAnalyzer;
import org.apache.commons.vfs2.FileObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.sql.Date;
import java.time.Instant;
import java.util.Optional;
import java.util.UUID;

/**
 * * Created by rfcardenas
 */
public class PipeCreatePulseCode extends AbstractPipe<FileCode, HeartBeatCode> {
    private static final Logger log = LoggerFactory.getLogger(PipeCreatePulseCode.class);
    private final int minimCambios = 5;
    private PulseAnalyzer pulseAnalyzer;
    private DefualtRouter defualtRouter;

    @Inject
    public PipeCreatePulseCode(DefualtRouter defualtRouter, PipeValidateCode pipeValidateCode) {
        this.defualtRouter = defualtRouter;
        pipeValidateCode.observable()
            .filter(event -> event.isOfType(EventType.FILE_UPDATE))
            .subscribe(event -> createPulseCode(event.getFileObject()));
        pipeValidateCode.observable()
            .filter(event -> event.isOfType(EventType.TIME_END))
            .subscribe(event -> createPulseTime(event.getFileObject()));
    }

    private void createPulseTime(FileObject reference) {
        log.info("Creando pulso de tiempo sin contexto");
        HeartBeatCode heartBeatCode = new HeartBeatCode();
        heartBeatCode.setTypeBeat(reference.getName().getExtension());
        defualtRouter.routePulse(heartBeatCode);
        notify(heartBeatCode);
    }

    private void createPulseCode(FileObject reference) {
        Optional<DefaultRouterProjectData> context = defualtRouter.getContext(reference);
        if (context.isPresent()) {
            MemoryBlock defaultMemoryBlock = context.get().getDefaultMemoryBlock();
            PulseAnalyzer pulseAnalyzer = new PulseAnalyzer(defaultMemoryBlock);
            pulseAnalyzer.observable()
                .subscribe(this::slinkPulseAnalyzer);
            pulseAnalyzer.analyze(reference);
            defaultMemoryBlock.createOrSyncMemory(reference);
        }
    }

    public void slinkPulseAnalyzer(PulseAnalyticPack pulseAnalyticPack) {
        // En este punto solo se crea el pulse el encargado de llegar los otros campos es el director
        // campos  como : si el dato esta sync o no, y la session a la que pertenece este pulso
        HeartBeatCode heartBeatCode = new HeartBeatCode();
        heartBeatCode.setPathFile(pulseAnalyticPack.getPathCode());
        heartBeatCode.setLinesAdd(pulseAnalyticPack.lineas_add);
        heartBeatCode.setLinesDel(pulseAnalyticPack.lineas_del);
        heartBeatCode.setTimeFixed(pulseAnalyticPack.delta_time_edit);
        heartBeatCode.setLevenshtein(pulseAnalyticPack.levenshtein);
        heartBeatCode.setUuid(UUID.randomUUID().toString());
        heartBeatCode.setCaptureDate(Date.from(Instant.now()));
        heartBeatCode.setTypeBeat(pulseAnalyticPack.getTypeFile());
        heartBeatCode.setLang(pulseAnalyticPack.getTypeFile());
        if (pulseAnalyticPack.levenshtein > minimCambios) {
            defualtRouter.routePulse(heartBeatCode);
            notify(heartBeatCode);
        } else {
            log.warn("Cambios no detectados configuracion actual levenshtein {} ", minimCambios);
        }

    }

    @Override
    public void onCompleted() {
        log.info("Completo");
    }

    @Override
    public void onError(Throwable e) {
        e.printStackTrace();
    }

    @Override
    public void onNext(FileCode fileCode) {

    }
}
