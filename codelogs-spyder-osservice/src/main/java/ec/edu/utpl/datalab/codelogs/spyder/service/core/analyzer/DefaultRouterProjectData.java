/*
 * The MIT License (MIT)
 * =========================================================================================================
 *
 * Copyright (c) 10/10/16 12:30 Modulo  Proyecto rexcode-monitor-service  Autor [rfcardenas]
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY
 * , WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ec.edu.utpl.datalab.codelogs.spyder.service.core.analyzer;

import ec.edu.utpl.datalab.codelogs.spyder.libs.common.StringUtils;
import ec.edu.utpl.datalab.codelogs.spyder.core.models.*;
import ec.edu.utpl.datalab.codelogs.spyder.service.core.DefaultMemoryBlock;
import ec.edu.utpl.datalab.codelogs.spyder.service.core.MemoryBlock;
import ec.edu.utpl.datalab.codelogs.spyder.service.util.SystemInfoData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import rx.Subscription;
import rx.subjects.PublishSubject;

import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

/**
 * Router relacionado los paquetes de entrada segun su contexto
 * por ejemplo cuando se modifica un archivo llega un FileCode
 * pero se desconoce del contexto como ?  A que proyecto pertenece ese archivo
 * puesto que el monitor FileSystemTemporal Lib no proporciona ese tipo de informacion
 * en esta clase se relaciona los datos a que proyecto o session pertenecen
 * <p>
 * si el fichecho entrada (PATH) contiene alguna entrada del mapa
 * entonses marcarlo con los datos de contexto alamacenaddos en el mapa
 * <p>
 * <p>
 * * Created by rfcardenas
 */
public final class DefaultRouterProjectData implements RouterProjectData {
    private static final Logger log = LoggerFactory.getLogger(DefaultRouterProjectData.class);
    private final AbstractSaver saver;
    private final CacheWorkSession cacheWorkSession;
    private final PublishSubject<EntityVisitable> subject;
    private final MemoryBlock defaultMemoryBlock;
    private final Set<Subscription> currentProcessors;
    private WorkSession workSession;

    /**
     * @param project          // el proyecto
     * @param cacheWorkSession // una referencia a la cache de sessiones
     * @param saverOnDisk      // el procesador de guardado de informacion
     */
    public DefaultRouterProjectData(Project project, CacheWorkSession cacheWorkSession, AbstractSaver saverOnDisk) {
        this.saver = saverOnDisk;
        this.cacheWorkSession = cacheWorkSession;
        currentProcessors = new HashSet<>();
        this.subject = PublishSubject.create();
        this.defaultMemoryBlock = DefaultMemoryBlock.builder().temporalPath(UUID.randomUUID().toString()).build();
    }

    /**
     * Crea una session para el proyecto (El modelo propuesto toma como session)
     * las operaciones de apertura de un programa
     * * las operaciones de apertura de un programa
     *
     * @param project
     * @return
     */
    @Override
    public WorkSession createWorkSession(Project project) {
        System.out.println(project);
        this.workSession = new WorkSession();
        workSession.setSessionStart(ZonedDateTime.now());
        workSession.setProject(project);
        workSession.setTerminalName(StringUtils.fixLimit(SystemInfoData.getMachineName(), 100));
        workSession.setTerminalArchitecture(SystemInfoData.getArchitecture());
        workSession.setTerminalCores(SystemInfoData.getCoresCpu());
        workSession.setTerminalUptime(SystemInfoData.getSystemUptime());
        workSession.setTerminalOs(StringUtils.fixLimit(SystemInfoData.getOsname(), 100));
        workSession.setTerminalRamAv(SystemInfoData.getRamAvaible());
        workSession.setTerminalRam(SystemInfoData.getTotalRam());
        workSession.setTerminalMac(StringUtils.fixLimit(SystemInfoData.getMacInfo(), 200));
        workSession.setTerminalIp(StringUtils.fixLimit(SystemInfoData.getIpInfo(), 200));
        workSession.setEditor(project.getIde());
        // minimize las depedencias sobre el repositorio, esta llamada es sincrona no se
        // es decir cuando se crea el objeto se tiene ya el proyecto inicializado....
        // los demas campos se actualizan por el proc)
        processEntitySave(workSession);

        cacheWorkSession.save(workSession);
        workSession.applyFunction(saver);
        return workSession;
    }

    @Override
    public void connetProcessorSaver(rx.Observer<EntityVisitable> processor) {
        Subscription subscription = this.subject.asObservable()
            .subscribe(processor);
        currentProcessors.add(subscription);
    }

    @Override
    public void processEntitySave(EntityVisitable data) {
        data.applyFunction(new PostProcess());
        subject.onNext(data);
    }

    @Override
    public MemoryBlock getDefaultMemoryBlock() {
        return defaultMemoryBlock;
    }

    public WorkSession getCurrentSession() {
        return workSession;
    }

    @Override
    public Project getProject() {
        return workSession.getProject();
    }

    @Override
    public void unsubcribeAllProcessors() {
        currentProcessors.stream()
            .filter(Subscription::isUnsubscribed)
            .forEach(Subscription::unsubscribe);

    }


    /**
     * Innerclass que se encarga de rellendar la data con los datos fantes para el siguiente encargado de
     * procesamiento
     */
    private class PostProcess implements EntityVisitor {
        @Override
        public void function(Project project) {
        }

        @Override
        public void function(WorkSession session) {
        }

        @Override
        public void function(HeartBeatCode heartBeatCode) {
            log.info("Post process entity data bind pulsecode");
            heartBeatCode.setWorkSession(workSession);
        }

        @Override
        public void function(FileCode fileCode) {
            log.info("Post process entity data bind filecode");
            fileCode.setProject(getProject());
        }

        @Override
        public void function(HeartBeatBuildRun data) {
            log.info("Post process entity data bind worksession");
            data.setWorkSession(workSession);
        }
    }

}
