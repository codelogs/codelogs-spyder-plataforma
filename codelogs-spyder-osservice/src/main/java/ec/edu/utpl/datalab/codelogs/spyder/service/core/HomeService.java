package ec.edu.utpl.datalab.codelogs.spyder.service.core;

import org.apache.commons.vfs2.FileObject;
import org.apache.commons.vfs2.FileSystemException;

/**
 * * Created by rfcardenas
 */
public interface HomeService {
    FileObject getConfigFile() throws Exception;

    /**
     * Retorna archivo de sopporte de lenguajes
     * @return
     * @throws FileSystemException
     */
    FileObject getSupportLangsFile() throws Exception;



    FileObject getFixPathStore() throws Exception;
}
