/*
 * The MIT License (MIT)
 * =========================================================================================================
 *
 * Copyright (c) 10/10/16 12:30 Modulo  Proyecto rexcode-monitor-service  Autor [rfcardenas]
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY
 * , WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ec.edu.utpl.datalab.codelogs.spyder.service.core.analyzer;

import ec.edu.utpl.datalab.codelogs.spyder.core.models.EntityVisitable;
import ec.edu.utpl.datalab.codelogs.spyder.core.models.EntityVisitor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import rx.Observer;

/**
 * clase abstracta para procesar el almancenamiento de datos
 * * Created by rfcardenas
 */
public abstract class AbstractSaver implements Observer<EntityVisitable>, EntityVisitor {
    private static final Logger log = LoggerFactory.getLogger(AbstractSaver.class);

    @Override
    public final void onCompleted() {
        log.info("La subscripcion finalizo correctamente...");
    }

    @Override
    public final void onError(Throwable e) {
        e.printStackTrace();
    }

    @Override
    public final synchronized void onNext(EntityVisitable data) {
        process(data);
    }

    /**
     * Procesa los datos para su almacenamiento,
     *
     * @param data
     */
    public void process(EntityVisitable data) {

        try {
            data.applyFunction(this);
        } catch (Exception e) {
            System.err.print(e.getMessage());
            log.error("Ocurrio un problema al tratar de guardar los datos {} repo", e.getMessage());
            e.printStackTrace();
        }
    }

}
