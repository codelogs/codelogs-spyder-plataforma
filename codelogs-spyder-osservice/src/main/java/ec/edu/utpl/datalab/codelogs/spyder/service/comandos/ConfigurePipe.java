/*
 * The MIT License (MIT)
 * =========================================================================================================
 *
 * Copyright (c) 10/10/16 12:30 Modulo  Proyecto rexcode-monitor-service  Autor [rfcardenas]
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY
 * , WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ec.edu.utpl.datalab.codelogs.spyder.service.comandos;


import ec.edu.utpl.datalab.codelogs.spyder.service.core.analyzer.AbstractSaver;
import ec.edu.utpl.datalab.codelogs.spyder.service.tasks.PipeClearMemory;
import ec.edu.utpl.datalab.codelogs.spyder.service.tasks.PipeCommandButler;
import ec.edu.utpl.datalab.codelogs.spyder.service.tasks.PipeCreatePulseCode;
import ec.edu.utpl.datalab.codelogs.spyder.service.tasks.PipeSyncPulse;
import ec.edu.utpl.datalab.codelogs.spyder.libs.terminal.SystemTerminal;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

/**
 * Enlaza las tareas criticas
 * * Created by rfcardenas
 */
public class ConfigurePipe {
    private static final Logger log = LoggerFactory.getLogger(ConfigurePipe.class);

    @Inject
    private PipeCommandButler pipeCommandButler;
    @Inject
    private SystemTerminal systemTerminal;
    @Inject
    private PipeCreatePulseCode pipeCreatePulseCode;
    @Inject
    private AbstractSaver abstractSaver; //--> Saver network (disparador para sync)
    @Inject
    private PipeClearMemory pipeClearMemory;
    @Inject
    private PipeSyncPulse pipeSyncPulse;

    @PostConstruct
    public void joinPipes() {
        /**
         * Intancias injectadas por el contructor son autoconfiguras e injectadas
         * cuando se llama esta clase
         */
        log.info("Join pipes [] {}", pipeCommandButler.getClass());
        log.info("Join pipes [] {}", systemTerminal.getClass());
        log.info("Join pipes [] {}", pipeCreatePulseCode.getClass());
        log.info("Join pipes [] {}", abstractSaver.getClass());
        log.info("Join pipes [] {}", pipeClearMemory.getClass());

        pipeCommandButler.observable()
            .subscribe(s -> systemTerminal.process(new String[]{s}));
    }


}
