/*
 * The MIT License (MIT)
 * =========================================================================================================
 *
 * Copyright (c) 10/10/16 12:30 Modulo  Proyecto rexcode-monitor-service  Autor [rfcardenas]
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY
 * , WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ec.edu.utpl.datalab.codelogs.spyder.service.core.analyzer;

import ec.edu.utpl.datalab.codelogs.spyder.core.models.Project;
import ec.edu.utpl.datalab.codelogs.spyder.core.models.WorkSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import rx.Observable;
import rx.subjects.PublishSubject;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

/**
 * * Created by rfcardenas
 */
public final class CacheWorkSession {
    private static final Logger log = LoggerFactory.getLogger(CacheWorkSession.class);
    private final PublishSubject<WorkSession> newSession;
    private final PublishSubject<WorkSession> closeSession;

    private final Map<String, WorkSession> workSessionMap;

    public CacheWorkSession() {
        this.workSessionMap = new HashMap<>();
        this.newSession = PublishSubject.create();
        this.closeSession = PublishSubject.create();
    }

    protected void save(WorkSession workSession) {
        log.info("Guardando session en cache map");
        if (workSession != null) {
            String path = workSession.getProject().getPath();
            workSessionMap.put(path, workSession);
            newSession.onNext(workSession);
        } else {
            log.warn("No se pudo crear la sess");
        }
    }

    protected void remove(WorkSession workSession) {
        remove(workSession.getProject().getPath());
    }

    protected void remove(Project project) {
        remove(project.getPath());
    }

    protected void remove(String pathProject) {
        log.warn(pathProject);
        WorkSession workSession = workSessionMap.remove(pathProject);
        if (workSession != null) {
            log.info("Se elimino la session para el proyecto {}", pathProject);
            closeSession.onNext(workSession);
        } else {
            log.error("la session no fue encontrada");
        }
    }

    public Optional<WorkSession> loadWorkSession(String pathChild) {
        return workSessionMap.entrySet().stream()
            .filter(row -> pathChild.contains(row.getKey()) || pathChild.equals(row.getKey()))
            .map(Map.Entry::getValue)
            .findFirst();
    }

    public Optional<Project> loadProject(String pathChild) {
        Optional<WorkSession> session = loadWorkSession(pathChild);
        return session.isPresent()
            ? Optional.ofNullable(session.get().getProject())
            : Optional.empty();
    }

    public Observable<WorkSession> newSessionNotify() {
        return newSession.asObservable();
    }

    public Observable<WorkSession> closeSessionNotify() {
        return closeSession.asObservable();
    }
}
