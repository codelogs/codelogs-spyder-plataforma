/*
 * The MIT License (MIT)
 * =========================================================================================================
 *
 * Copyright (c) 10/10/16 12:30 Modulo  Proyecto rexcode-monitor-service  Autor [rfcardenas]
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY
 * , WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ec.edu.utpl.datalab.codelogs.spyder.service;

import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.google.inject.Singleton;
import com.google.inject.internal.SingletonScope;
import com.google.inject.name.Names;
import ec.edu.utpl.datalab.codelogs.spyder.core.env.EnvironmentProperties;
import ec.edu.utpl.datalab.codelogs.spyder.core.env.PropertiesDefault;
import ec.edu.utpl.datalab.codelogs.spyder.core.models.SourceCode;
import ec.edu.utpl.datalab.codelogs.spyder.core.persistencia.RepositoryService;
import ec.edu.utpl.datalab.codelogs.spyder.core.synchronization.ModuloSynchronization;
import ec.edu.utpl.datalab.codelogs.spyder.libs.filesystem.code_event2.file.EventFileSystem;
import ec.edu.utpl.datalab.codelogs.spyder.libs.filesystem.code_event2.file.EventSysteminherited;
import ec.edu.utpl.datalab.codelogs.spyder.sync.ModuleSyncBeta;
import ec.edu.utpl.datalab.codelogs.spyder.service.comandos.*;
import ec.edu.utpl.datalab.codelogs.spyder.service.core.CodeLangMap;
import ec.edu.utpl.datalab.codelogs.spyder.service.core.DefaultMemoryBlock;
import ec.edu.utpl.datalab.codelogs.spyder.service.core.MemoryBlock;
import ec.edu.utpl.datalab.codelogs.spyder.service.core.analyzer.AbstractSaver;
import ec.edu.utpl.datalab.codelogs.spyder.service.core.analyzer.CacheWorkSession;
import ec.edu.utpl.datalab.codelogs.spyder.service.core.analyzer.DefaultSaverOnDisk;
import ec.edu.utpl.datalab.codelogs.spyder.service.core.analyzer.DefualtRouter;
import ec.edu.utpl.datalab.codelogs.spyder.service.tasks.*;
import ec.edu.utpl.datalab.codelogs.spyder.libs.terminal.SystemCommandProvider;
import ec.edu.utpl.datalab.codelogs.spyder.libs.terminal.SystemTerminal;
import ec.edu.utpl.datalab.codelogs.spyder.libs.terminal.TerminalBuilder;
import org.apache.commons.vfs2.FileObject;
import org.apache.commons.vfs2.VFS;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.UUID;

/**
 * * Created by rfcardenas
 */
public class InjectionService extends AbstractModule {
    private static final Logger log = LoggerFactory.getLogger(InjectionService.class);

    private RepositoryService repositoryService;
    private EnvironmentProperties environmentProperties;

    public InjectionService(RepositoryService repositoryService, EnvironmentProperties environment) {
        this.repositoryService = repositoryService;
        this.environmentProperties = environment;
    }

    @Override
    protected void configure() {
        bind(SystemCommandProvider.class).annotatedWith(Names.named("cmdfs")).to(SystemFileEvent.class).in(new SingletonScope());
        bind(SystemCommandProvider.class).annotatedWith(Names.named("cmdopen")).to(SystemCloseProject.class);
        bind(SystemCommandProvider.class).annotatedWith(Names.named("cmdclose")).to(SystemOpenProject.class);
        bind(SystemCommandProvider.class).annotatedWith(Names.named("cmdshut")).to(SystemShutdown.class);
        bind(SystemCommandProvider.class).annotatedWith(Names.named("cmddata")).to(SystemCollectDataMetrics.class);
        bind(SystemCommandProvider.class).annotatedWith(Names.named("cmdbuild")).to(SystemCompileEvent.class);
        bind(SystemCommandProvider.class).annotatedWith(Names.named("cmdRun")).to(SystemRuntimeEvent.class);
        bind(AbstractSaver.class).to(DefaultSaverOnDisk.class).in(new SingletonScope());
        bind(DefualtRouter.class).in(new SingletonScope());
        bind(CommandReaderJackson.class).in(new SingletonScope());
        bind(PipeCreateFileCode.class).in(new SingletonScope());
        bind(PipeCreatePulseCode.class).in(new SingletonScope());
        bind(PipeFileSystemEvent.class).in(new SingletonScope());
        bind(PipeValidateCode.class).in(new SingletonScope());
        bind(CacheWorkSession.class).in(new SingletonScope());
        bind(ConfigurePipe.class);
        bind(PipeCommandButler.class).in(new SingletonScope());

        bind(ModuloSynchronization.class).to(ModuleSyncBeta.class).in(new SingletonScope());
    }

    @Provides
    public CodeLangMap codeLangMap() {
        CodeLangMap.Builder codeLangMap = CodeLangMap.builder();

        try {
            log.info("Cargado archivo " + environmentProperties().getProperty(PropertiesDefault.SPYDER_HOME_CONFIG_FILE_SUPPORT));
            FileObject file = VFS.getManager().resolveFile(environmentProperties.getProperty(PropertiesDefault.SPYDER_HOME_CONFIG_FILE_SUPPORT));
            codeLangMap.populateWith(new File(file.getName().getPath()));
        } catch (Exception e) {
            log.error("No se pudo cargar archivo de soporte {}",e.getMessage());
        }
        return codeLangMap
            .populateWith(new SourceCode("java", "java", "java"))
            .buildMap();
    }

    @Provides
    public MemoryBlock memory() {
        return DefaultMemoryBlock.builder().temporalPath(UUID.randomUUID().toString()).build();
    }

    @Provides
    public RepositoryService repositoryService() {
        return repositoryService;
    }

    @Provides
    public EnvironmentProperties environmentProperties() {
        return environmentProperties;
    }

    /**
     * @param codeLangMap
     * @param systemFileEvent
     * @return
     */
    @Provides
    @Singleton
    public EventFileSystem eventFileSystem(CodeLangMap codeLangMap, SystemFileEvent systemFileEvent) {
        EventFileSystem eventFileSystem;
        boolean inheritedEventSystemEnable = environmentProperties.getAsBolean(PropertiesDefault.MONITOR_ENABLE_EVENTSYSTEM_INHERITED, true);
        if (inheritedEventSystemEnable) {
            log.info("Event System heredado");
            eventFileSystem = EventSysteminherited
                .builder()
                .addSupport(codeLangMap.getCodeLangSet())
                .build();
        } else {
            log.info("Event System externo");
            eventFileSystem = systemFileEvent;
        }
        return eventFileSystem;
    }

    @Provides
    @Singleton
    public SystemTerminal terminal(
        SystemCloseProject systemCloseProject,
        SystemOpenProject systemOpenProject,
        SystemShutdown systemShutdown,
        SystemCollectDataMetrics systemCollectDataMetrics,
        SystemFileEvent systemFileEvent,
        SystemSync systemSync,
        SystemCompileEvent systemCompileEvent,
        SystemRuntimeEvent systemRuntimeEvent
    ) {
        return TerminalBuilder.instance()
            .addCommand(systemCloseProject)
            .addCommand(systemOpenProject)
            .addCommand(systemShutdown)
            .addCommand(systemCollectDataMetrics)
            .addCommand(systemFileEvent)
            .addCommand(systemSync)
            .addCommand(systemCompileEvent)
            .addCommand(systemRuntimeEvent)
            .commit();
    }

}
