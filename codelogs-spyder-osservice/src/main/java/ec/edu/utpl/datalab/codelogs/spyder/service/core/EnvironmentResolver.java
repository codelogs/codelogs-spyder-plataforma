package ec.edu.utpl.datalab.codelogs.spyder.service.core;

/**
 * * Created by rfcardenas
 */
public interface EnvironmentResolver  {
    HomeService service();
}
