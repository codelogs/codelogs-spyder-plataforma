package ec.edu.utpl.datalab.codelogs.spyder.service.core;

import ec.edu.utpl.datalab.codelogs.spyder.core.env.EnvironmentCkecker;
import ec.edu.utpl.datalab.codelogs.spyder.core.env.EnvironmentProperties;
import ec.edu.utpl.datalab.codelogs.spyder.core.env.PropertiesDefault;
import ec.edu.utpl.datalab.codelogs.spyder.core.filesystem.EnvironmentAppNotFound;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * * Created by rfcardenas
 */
public class EnvironmentCheckerBasic implements EnvironmentCkecker {

    private static final Logger log = LoggerFactory.getLogger(EnvironmentCheckerBasic.class);

    @Override
    public void check(EnvironmentProperties properties) throws EnvironmentAppNotFound {
        String[] checklist = new String[]{
            PropertiesDefault.SYNC_APIKEY,
            PropertiesDefault.SYNC_SERVER,
            PropertiesDefault.MONITOR_ENABLE_EVENTSYSTEM_INHERITED
        };
        for (String property : checklist) {
            if(!properties.hasProperty(property)){
                log.error("la propidad {} no esta configurada",property);
                throw new EnvironmentAppNotFound(String.format("la propiedad %s no es configurada",property));
            }
        }
    }
}
