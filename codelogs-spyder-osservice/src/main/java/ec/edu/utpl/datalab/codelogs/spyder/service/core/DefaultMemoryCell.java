/*
 * The MIT License (MIT)
 * =========================================================================================================
 *
 * Copyright (c) 10/10/16 12:30 Modulo  Proyecto rexcode-monitor-service  Autor [rfcardenas]
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY
 * , WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ec.edu.utpl.datalab.codelogs.spyder.service.core;


import org.apache.commons.io.FileUtils;
import org.apache.commons.vfs2.FileContent;
import org.apache.commons.vfs2.FileObject;
import org.apache.commons.vfs2.FileSystemException;
import org.apache.commons.vfs2.FileUtil;

import java.io.File;
import java.io.IOException;
import java.util.UUID;

/**
 * Representa una entrada para la memoria
 * * Created by rfcardenas
 */
public class DefaultMemoryCell implements MemoryCell {
    // mementos histo
    private FileObject pathMemento = null;
    private FileObject sourceCodePath;
    private FileObject tmpDir;

    /**
     * Requiere el codigo fuenete original y el directorio temporal de memoria
     *
     * @param sourceCode
     * @param tmpDir
     */

    public DefaultMemoryCell(FileObject sourceCode, FileObject tmpDir) {
        this.sourceCodePath = sourceCode;
        this.tmpDir = tmpDir;
        sync();
    }

    /**
     * Actualiza o crea la mempria
     */
    @Override
    public void sync() {
        try {
            if (pathMemento == null || !memoryFileExit()) {
                createPathFileMemory();
            } else {
                updateMemento();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Actualiza la memoria
     *
     * @throws IOException
     */
    private void updateMemento() throws IOException {
        File code = getSourceFile();
        File newCopy = new File(pathMemento.getName().getPath());
        FileUtils.copyFile(code, newCopy);
    }

    /**
     * Retorna la direccion del
     *
     * @return
     */
    @Override
    public File getSourceFile() {
        return new File(sourceCodePath.getName().getPath());
    }

    /**
     * Retorna un valor boolean para determinar
     * si el el archivo en disco existe
     *
     * @return
     */
    @Override
    public boolean memoryFileExit() {
        try {
            return pathMemento.exists();
        } catch (FileSystemException e) {
            e.printStackTrace();
        }
        return false;
    }

    /**
     * Retorna el archivo memento
     *
     * @return
     */
    @Override
    public File getMemoryFile() {
        return new File(pathMemento.getName().getPath());
    }

    /**
     * Destruye la memoria
     */
    @Override
    public void destroyMemory() {
        try {
            if (pathMemento.exists()) {
                pathMemento.delete();
            }
        } catch (FileSystemException e) {
            e.printStackTrace();
        }
    }

    /**
     * Crea la memoria
     *
     * @throws IOException
     */
    private void createPathFileMemory() throws IOException {
        if (tmpDir.isReadable() && tmpDir.exists()) {
            String UUID_MEMORY = UUID.randomUUID().toString();
            FileObject memoryFile = tmpDir.resolveFile(UUID_MEMORY + ".rxmemory");
            memoryFile.createFile();
            FileContent fileContent = memoryFile.getContent();
            FileUtil.writeContent(sourceCodePath, fileContent.getOutputStream());
            fileContent.close();
            this.pathMemento = memoryFile;
        }
    }
}
