/*
 * The MIT License (MIT)
 * =========================================================================================================
 *
 * Copyright (c) 10/10/16 12:30 Modulo  Proyecto rexcode-monitor-service  Autor [rfcardenas]
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY
 * , WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ec.edu.utpl.datalab.codelogs.spyder.service.core.analyzer;

import ec.edu.utpl.datalab.codelogs.spyder.core.models.EntityVisitable;
import ec.edu.utpl.datalab.codelogs.spyder.core.models.Project;
import ec.edu.utpl.datalab.codelogs.spyder.core.models.WorkSession;
import ec.edu.utpl.datalab.codelogs.spyder.service.core.MemoryBlock;

/**
 * * Created by rfcardenas
 */
public interface RouterProjectData {
    /**
     * Crea una session de trabajo para el nuevo proyecto de entrda
     *
     * @param project
     * @return
     */
    WorkSession createWorkSession(Project project);

    /**
     * Procesamiento de datos (Domain objects) utilizando patron visitor para
     * facilitar las cosas
     *
     * @param data
     */
    void processEntitySave(EntityVisitable data);

    /**
     * Rertorna el bloque de memoria asociado al proyecto
     *
     * @return
     */
    MemoryBlock getDefaultMemoryBlock();

    /**
     * Retorna el proyecto al cual esta enrutando datos
     *
     * @return
     */
    Project getProject();

    /**
     * Conecta el router con una fuente de datos
     *
     * @param processor
     */
    void connetProcessorSaver(rx.Observer<EntityVisitable> processor);


    /**
     * se elimna de la lista observers
     */
    void unsubcribeAllProcessors();
}
