/*
 * The MIT License (MIT)
 * =========================================================================================================
 *
 * Copyright (c) 10/10/16 12:30 Modulo  Proyecto rexcode-monitor-service  Autor [rfcardenas]
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY
 * , WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ec.edu.utpl.datalab.codelogs.spyder.service.comandos;

import ec.edu.utpl.datalab.codelogs.spyder.service.core.analyzer.DefualtRouter;
import ec.edu.utpl.datalab.codelogs.spyder.libs.terminal.SystemCommand;
import ec.edu.utpl.datalab.codelogs.spyder.libs.terminal.SystemCommandProvider;
import org.apache.commons.cli.Option;
import org.apache.commons.vfs2.FileObject;
import org.apache.commons.vfs2.FileSystemException;
import org.apache.commons.vfs2.VFS;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.util.Map;

/**
 * Comando cierra la session de trabajo
 * * Created by rfcardenas
 */
public class SystemCloseProject implements SystemCommandProvider {
    private static final Logger log = LoggerFactory.getLogger(SystemOpenProject.class);

    private DefualtRouter defualtRouter;

    @Inject
    public SystemCloseProject(DefualtRouter defualtRouter) {
        this.defualtRouter = defualtRouter;
    }

    @Override
    public SystemCommand command() {
        Option portOption = Option.builder(MonitorCommand.CMD_CLOSE_PROJECT)
            .hasArg()
            .desc("Finaliza el trabajo (monitoreo de un proyecto)")
            .build();
        return SystemCommand.builder(portOption)
            .setAction(this::execute)
            .build();
    }

    public void execute(Map<String, String> args) {
        String pathProject = args.get(MonitorCommand.CMD_CLOSE_PROJECT);
        log.info("Close command invk {}", pathProject);
        try {

            FileObject projectObject = VFS.getManager().resolveFile(pathProject);
            defualtRouter.removeRoute(projectObject.getName().getFriendlyURI());
        } catch (FileSystemException ex) {
            log.error("Error close task  {}", ex.getMessage());
            ex.printStackTrace();
        }
    }
}
