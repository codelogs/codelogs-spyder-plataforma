/*
 * The MIT License (MIT)
 * =========================================================================================================
 *
 * Copyright (c) 10/10/16 12:30 Modulo  Proyecto rexcode-monitor-service  Autor [rfcardenas]
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY
 * , WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ec.edu.utpl.datalab.codelogs.spyder.service.comandos;

import ec.edu.utpl.datalab.codelogs.spyder.core.synchronization.ModuloSynchronization;
import ec.edu.utpl.datalab.codelogs.spyder.core.synchronization.RestService;
import ec.edu.utpl.datalab.codelogs.spyder.core.util.ReaderJson;
import ec.edu.utpl.datalab.codelogs.spyder.libs.transfers.CodeAnalysisPack;
import ec.edu.utpl.datalab.codelogs.spyder.libs.terminal.SystemCommand;
import ec.edu.utpl.datalab.codelogs.spyder.libs.terminal.SystemCommandProvider;
import org.apache.commons.cli.Option;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.util.Map;
import java.util.Optional;

/**
 * Abre un proyecto
 * * Created by rfcardenas
 */
public class SystemCollectDataMetrics implements SystemCommandProvider {
    private static final Logger log = LoggerFactory.getLogger(SystemCollectDataMetrics.class);
    private ModuloSynchronization syncronization;

    @Inject
    public SystemCollectDataMetrics(ModuloSynchronization syncronization) {
        this.syncronization = syncronization;
    }

    /**
     * Requiere router data estructure,
     */


    @Override
    public SystemCommand command() {
        Option openOption = Option.builder(MonitorCommand.CMD_COLLECT_DATA)
            .hasArg()
            .desc("monitorea el proyecto")
            .build();
        return SystemCommand.builder(openOption)
            .setAction(this::execute)
            .build();
    }

    public void execute(Map<String, String> args) {
        log.info("Ejecutando comando con argumentos ---> {}", args.get(MonitorCommand.CMD_COLLECT_DATA));
        String data = args.get(MonitorCommand.CMD_COLLECT_DATA);
        if (data != null) {
            Optional<CodeAnalysisPack> result = ReaderJson.read(CodeAnalysisPack.class, data);
            result.ifPresent(this::sendMeasures);
        }else{
            log.warn("No se encontraron los datos...");
        }
    }

    private void sendMeasures(CodeAnalysisPack projectDataAnalysis) {
        log.info("Enviando datos al servidor .......");

        if (projectDataAnalysis.getPathContext() != null) {
            RestService restService = syncronization.restService();
            if (restService != null) {
                restService.create(projectDataAnalysis).subscribe(data -> {
                    log.info(data.getUuid());
                });
            } else {
                log.error("Error...");
            }
        }
    }
}
