package ec.edu.utpl.datalab.codelogs.spyder.service;

import org.junit.Ignore;
import org.junit.Test;

/**
 * * Created by rfcardenas
 */
@Ignore
public class InjectionServiceTest {
    @Test
    public void configure() throws Exception {

    }

    @Test
    public void codeLangMap() throws Exception {
        InjectionService injectionService = new InjectionService(null,null);
        System.out.println(injectionService.codeLangMap().getCodeLangSet().size());
    }

    @Test
    public void memory() throws Exception {

    }

    @Test
    public void repositoryService() throws Exception {

    }

    @Test
    public void environmentProperties() throws Exception {

    }

    @Test
    public void eventFileSystem() throws Exception {

    }

    @Test
    public void terminal() throws Exception {

    }

}
