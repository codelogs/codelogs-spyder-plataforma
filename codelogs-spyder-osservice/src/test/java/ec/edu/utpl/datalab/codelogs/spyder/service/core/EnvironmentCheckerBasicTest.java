package ec.edu.utpl.datalab.codelogs.spyder.service.core;

import ec.edu.utpl.datalab.codelogs.spyder.core.env.ArgsEnvironmentProperties;
import ec.edu.utpl.datalab.codelogs.spyder.core.env.EnvironmentCkecker;
import ec.edu.utpl.datalab.codelogs.spyder.core.env.EnvironmentProperties;
import ec.edu.utpl.datalab.codelogs.spyder.core.filesystem.EnvironmentAppNotFound;
import org.junit.Ignore;
import org.junit.Test;

/**
 * * Created by rfcardenas
 */
@Ignore
public class EnvironmentCheckerBasicTest {
    /**
     * Se espera  un excetion de tipo EnvironmentAppNotFound
     * @throws Exception
     */
    @Test(expected=EnvironmentAppNotFound.class)
    public void isConfig() throws Exception {
        EnvironmentProperties properties = new ArgsEnvironmentProperties(new String[]{"a=a"});
        EnvironmentCkecker environmentCkecker = new EnvironmentCheckerBasic();
        environmentCkecker.check(properties);
    }
}
