# Estado
[![N|Solid](https://cldup.com/dTxpPi9lDf.thumb.png)](https://nodesource.com/products/nsolid)
[![Gitter](https://img.shields.io/gitter/room/nwjs/nw.js.svg)](https://gitter.im/codelogs-fragent/Lobby?utm_source=share-link&utm_medium=link&utm_campaign=share-link)
[![Packagist](https://img.shields.io/packagist/l/doctrine/orm.svg)](https://opensource.org/licenses/MIT)
[![coverage](https://gitlab.com/codelogs/codelogs-spyder-plataforma/badges/master/build.svg)](https://gitlab.com/codelogs/codelogs-spyder-plataforma/commits/master)

# CODELOGS CODIGO BASE 

[![N|Solid](https://cldup.com/dTxpPi9lDf.thumb.png)](https://nodesource.com/products/nsolid)
### Repositorio.

Proyectos que forman el núcleo de las aplicaciones, este repositorio contiene
una colección de programas y librerías desarrolladas útiles para el desarrollo CODELOGS, 

Este proyecto se complementa con codelogs-mvn-tools, para capturar problemas
de compilación y ejecución.
Este repo presenta la solución al principal objetivo del trabajo de titulación,
recolectar datos y métricas de las actividades de programación de quienes inician en
la programación.

### Utilidad
Este proyecto es la base para el desarrollo de nuevos plugins, es utilizado para captar la actividad del estudiante de forma independiente del IDE, 
el programa trabajo como un servidor pequeño utilizando websockets localmente
para conectar distintos IDEs, editores y captar datos de actividad. 

También se tiene programas  analizar la calidad del código, aunque las métricas implementadas son pocas, son las básicas identificadas en este Trabajo; sin embargo se puede utilizar herramientas mas profesionales y utilizar este medio para enviarlas a los servidores de la UTPL. 

#### ¿Que puedes encontrar en este repositorio?

- Librerías a nivel de Sistema de archivos
- Librerías a nivel de persistencia
- Código base de codelogs
- Estructuras de IA
- Utilidades
- Shells
- Programs para calcular métricas de código. 
- Java Métrics

#### Tecnologia

- Maven 3.3.x
- Java
- Rxjava

### Construir las shell

1) clona el proycto git clone ....
2) mvn compile 
3) mvn package

esto genera la distribución .zip en el directorio codelogs-spyder-dist el cual tiene la siguiente estructura : 

``` TREE DIST
.
├── bin
│   ├── shell.bat
│   ├── shell.sh
│   └── spring-shell.log
├── config
│   ├── Config.yml
│   ├── log4j2.xml
│   ├── log4j2.xml.dev
│   ├── logback.xml
│   ├── monitor_daemon.properties
│   └── monitor_support.json
├── db
│   └── spyder.db
├── lib
│   ├── adapter-rxjava-2.1.0.jar
│   ├── aopalliance-1.0.jar
│   ├── aopalliance-repackaged-2.5.0-b05.jar
...................
...................
```
### Uso

1) Inicia la shell (./shell.sh o shell.bat si estas en windows)
2) inicia el servicio con el comando "--iniciar"
 -- Tokens , puertos, servidores etc. son los parámetros de configuración
3) abre una sesión de trabajo utilizando open --path x/y/zproject 

esto lanza un monitor de tiempo, y de actividad escuchando el sistema de archivos
ante los cambios en el código, en cada cambio se aplica algoritmos de diferencia métricas de código, datos que son enviados al servidor.
