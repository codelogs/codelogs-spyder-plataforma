package ec.edu.utpl.datalab.codelogs.spyder.shell.monitor;

import ec.edu.utpl.datalab.codelogs.spyder.core.env.PropertiesDefault;
import ec.edu.utpl.datalab.codelogs.spyder.core.os.ws.AutoConnectionSocket;
import ec.edu.utpl.datalab.codelogs.spyder.libs.filesystem.search.FilterSet;
import ec.edu.utpl.datalab.codelogs.spyder.libs.filesystem.search.SearchFileSystem;
import org.apache.commons.vfs2.FileObject;
import org.apache.commons.vfs2.VFS;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.shell.core.CommandMarker;
import org.springframework.shell.core.annotation.CliCommand;
import org.springframework.stereotype.Component;

/**
 * * Created by rfcardenas
 */
@Component
public class ShellFixData implements CommandMarker {
    private static final Logger log = LoggerFactory.getLogger(ShellOpenProject.class);
    private AutoConnectionSocket autoConnectionSocket;

    public ShellFixData(AutoConnectionSocket autoConnectionSocket) {
        this.autoConnectionSocket = autoConnectionSocket;
    }

    @CliCommand(value = "fixdata", help = "Los datos se han vuelto corruptos, los claves otorgadas por el " +
        "servidor no se encuentra, Atención este comando eliminara tu bd local, por el momento " +
        "esta solución es temporal")
    public void fixData() throws Exception {
        log.info("Este comando es temporal y se utiliza en raras ocaciones");
        String home  = System.getProperty(PropertiesDefault.SPYDER_HOME);
        FileObject folder = VFS.getManager().resolveFile(home);

        SearchFileSystem search = SearchFileSystem.newSearch(3)
            .withFilter(FilterSet.filterOnlyFiles())
            .withFilter(FilterSet.filterByExtension("db"))
            .build();
        FileObject[] targets = search.executeFinder(folder);
        if(targets.length<=0){
            log.info("no hay datos en el contexto del proyecto");
        }else{
            for (FileObject target : targets) {
                if(target.isWriteable()){
                    target.delete();
                    log.info("Eliminado base local de {} ",target);
                }else{
                    log.warn("No se puede eliminar {} ",target);
                }
            }
        }

        log.warn("Se apagara el servidor al que se esta conectado");
        if(autoConnectionSocket.isConnected()){
            new ShellShutDownMonitor(autoConnectionSocket);
        }

    }
}

