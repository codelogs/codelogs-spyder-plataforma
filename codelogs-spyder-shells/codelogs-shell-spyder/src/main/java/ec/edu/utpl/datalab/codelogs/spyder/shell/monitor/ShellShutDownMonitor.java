package ec.edu.utpl.datalab.codelogs.spyder.shell.monitor;

import ec.edu.utpl.datalab.codelogs.spyder.core.os.ws.AutoConnectionSocket;
import ec.edu.utpl.datalab.codelogs.spyder.core.util.JsonMapper;
import ec.edu.utpl.datalab.codelogs.spyder.service.comandos.MonitorCommand;
import ec.edu.utpl.datalab.codelogs.spyder.libs.terminal.Command;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.shell.core.CommandMarker;
import org.springframework.shell.core.annotation.CliCommand;
import org.springframework.stereotype.Component;

import javax.inject.Inject;

/**
 * * Created by rfcardenas
 */
@Component
public class ShellShutDownMonitor implements CommandMarker {
    private static final Logger log = LoggerFactory.getLogger(ShellShutDownMonitor.class);
    private AutoConnectionSocket autoConnectionSocket;

    @Inject
    public ShellShutDownMonitor(AutoConnectionSocket autoConnectionSocket) {
        this.autoConnectionSocket = autoConnectionSocket;
    }

    @CliCommand(value = "shutdown",
        help = "Apaga el servidor, los recursos que esta consumiendo son liberados.")
    public void kill() throws Exception {

        /**JsonArray command = Json.array().asArray()
            .add(Json.object()
                .add(MonitorCommand.COMMAND, MonitorCommand.CMD_SHUTDOWN_MONITOR)
                .add(MonitorCommand.VALUE, ""));**/

        Command command =  Command.createCommand(MonitorCommand.CMD_SHUTDOWN_MONITOR,"").build();

        autoConnectionSocket.send(JsonMapper.writte(command));

    }
}
