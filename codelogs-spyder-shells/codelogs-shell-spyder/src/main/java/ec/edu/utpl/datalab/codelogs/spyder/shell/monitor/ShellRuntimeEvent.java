package ec.edu.utpl.datalab.codelogs.spyder.shell.monitor;

import ec.edu.utpl.datalab.codelogs.spyder.core.os.ws.AutoConnectionSocket;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.shell.core.CommandMarker;
import org.springframework.shell.core.annotation.CliCommand;
import org.springframework.shell.core.annotation.CliOption;

import javax.inject.Inject;

/**
 * * Created by rfcardenas
 */
public class ShellRuntimeEvent implements CommandMarker {
    private static final Logger log = LoggerFactory.getLogger(ShellRuntimeEvent.class);


    private AutoConnectionSocket autoConnectionSocket;

    @Inject
    public ShellRuntimeEvent(AutoConnectionSocket autoConnectionSocket) {
        this.autoConnectionSocket = autoConnectionSocket;
    }

    @CliCommand(
        value = "event.runtime",
        help = "Registra los datos de compilación/Ejecución de un proyecto" +
            "por ejemplo problemas encontrados")
    public void connectws(
        @CliOption(key = "port", unspecifiedDefaultValue = "8484", mandatory = false, help = "port service monitor") int port,
        @CliOption(key = "force", unspecifiedDefaultValue = "normal", mandatory = false, specifiedDefaultValue = "normal", help = "forza la conexion") ShellConnect.Mode mode) throws Exception {

    }
}
