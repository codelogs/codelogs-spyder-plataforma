package ec.edu.utpl.datalab.codelogs.spyder.shell.monitor;

import ec.edu.utpl.datalab.codelogs.spyder.core.os.ws.AutoConnectionSocket;
import ec.edu.utpl.datalab.codelogs.spyder.core.util.JsonMapper;
import ec.edu.utpl.datalab.codelogs.spyder.service.comandos.MonitorCommand;
import ec.edu.utpl.datalab.codelogs.spyder.libs.terminal.Command;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.shell.core.CommandMarker;
import org.springframework.shell.core.annotation.CliCommand;
import org.springframework.shell.core.annotation.CliOption;
import org.springframework.stereotype.Component;

import javax.inject.Inject;

/**
 * * Created by rfcardenas
 */
@Component
public class ShellCollectMetrics implements CommandMarker {
    private static final Logger log = LoggerFactory.getLogger(ShellCloseProject.class);

    private AutoConnectionSocket autoConnectionSocket;

    @Inject
    public ShellCollectMetrics(AutoConnectionSocket autoConnectionSocket) {
        this.autoConnectionSocket = autoConnectionSocket;
    }

    @CliCommand(value = "event.metrics", help = "Comando para registrar métricas de código, ya sean " +
        "estas calculadas en herramientas externas o especificas como Sonar")
    public void sendMetrics(
        @CliOption(key = "data", mandatory = true, help = "Paquete de metricas recolectadas") String metrics) throws Exception {

        Command command = Command.createCommand(MonitorCommand.CMD_COLLECT_DATA, metrics).build();

        /**JsonArray command = Json.array().asArray()
            .add(Json.object()
                .add(MonitorCommand.COMMAND, MonitorCommand.CMD_COLLECT_DATA)
                .add(MonitorCommand.VALUE, metrics));**/

        autoConnectionSocket.send(JsonMapper.writte(command));
    }
}
