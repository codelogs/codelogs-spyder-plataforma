/**
 * Este paquete contiene comandios opcionales
 * para ponerlos en funcionamientio anote con @Compornet
 * <p>
 * Ejemplo :
 *
 * @Component public class ProcCloseProject implements CommandMarker {
 * <p>
 * Tome en cuenta que solo puede existir un comando bajo el mismo nombre
 * deshabilite aquellos que quiera reemplazar quitando la anotacion
 * * Created by rfcardenas
 */
package ec.edu.utpl.datalab.codelogs.spyder.shell.monitor.optional;
