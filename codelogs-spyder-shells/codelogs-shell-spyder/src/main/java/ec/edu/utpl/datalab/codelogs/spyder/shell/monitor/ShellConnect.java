/*
 * The MIT License (MIT)
 * =========================================================================================================
 *
 * Copyright (c) 10/10/16 15:18 Modulo  Proyecto shell-monitor  Autor [rfcardenas]
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY
 * , WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ec.edu.utpl.datalab.codelogs.spyder.shell.monitor;

import ec.edu.utpl.datalab.codelogs.spyder.core.os.ws.AutoConnectionSocket;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.shell.core.CommandMarker;
import org.springframework.shell.core.annotation.CliCommand;
import org.springframework.shell.core.annotation.CliOption;
import org.springframework.stereotype.Component;

import javax.inject.Inject;

/**
 * * Created by rfcardenas
 */
@Component
public class ShellConnect implements CommandMarker {
    private static final Logger log = LoggerFactory.getLogger(ShellConnect.class);


    private AutoConnectionSocket autoConnectionSocket;

    @Inject
    public ShellConnect(AutoConnectionSocket autoConnectionSocket) {
        this.autoConnectionSocket = autoConnectionSocket;
    }

    @CliCommand(value = "conectar", help = "Se conecta a un proceso existen, puerto por defecto 8484")
    public void connectws(
        @CliOption(key = "port", unspecifiedDefaultValue = "8484", mandatory = false, help = "port service monitor") int port,
        @CliOption(key = "force", unspecifiedDefaultValue = "normal", mandatory = false, specifiedDefaultValue = "normal", help = "forza la conexion") Mode mode) throws Exception {
            log.info("Intentando conexión con el puerto {}",port);
        switch (mode) {
            case force:
                autoConnectionSocket.forceConnectionMonitor(port);
                break;
            case normal: {
                if (port != 8484) {
                    autoConnectionSocket.forceConnectionMonitor(port);
                } else {
                    autoConnectionSocket.connect();
                }
                break;
            }
        }
    }

    enum Mode {
        force("forzar"),
        normal("normal");

        private String type;

        private Mode(String type) {
            this.type = type;
        }

        public String getType() {
            return type;
        }
    }
}
