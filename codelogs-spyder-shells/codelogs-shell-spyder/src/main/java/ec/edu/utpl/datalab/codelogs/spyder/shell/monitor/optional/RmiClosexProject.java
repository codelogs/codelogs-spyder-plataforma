/*
 * The MIT License (MIT)
 * =========================================================================================================
 *
 * Copyright (c) 10/10/16 12:30 Modulo  Proyecto shell-monitor  Autor [rfcardenas]
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY
 * , WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ec.edu.utpl.datalab.codelogs.spyder.shell.monitor.optional;

import ec.edu.utpl.datalab.codelogs.spyder.core.os.rmi.ExecutorRemote;
import ec.edu.utpl.datalab.codelogs.spyder.service.comandos.MonitorCommand;
import org.springframework.shell.core.CommandMarker;
import org.springframework.shell.core.annotation.CliCommand;
import org.springframework.shell.core.annotation.CliOption;

import java.net.MalformedURLException;
import java.nio.file.FileSystemException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;

/**
 * * Created by rfcardenas
 */
public class RmiClosexProject implements CommandMarker {
    @CliCommand(value = "Close RMI", help = "Crea una nueva session para un proyecto RMI version")
    public String translate(
        @CliOption(key = "path", mandatory = true, help = "path del proyecto") String path,
        @CliOption(key = "port", mandatory = true, help = "puerto de servicio") Integer port
    ) throws FileSystemException {
        /** FileObject fileObject = VFS.getManager().resolveFile(MonitorProc._PROC);
         if(!fileObject.exists()){
         System.out.println("El proceso no esta corriendo");
         }else{**/
        String cmd = String.format("-%s %s", MonitorCommand._closeProject, path);
        String lkp = String.format("rmi://localhost:%d/%s", port, MonitorCommand._rmireg);
        System.out.println("Ejecutando lkp " + lkp + " =>> " + cmd);
        try {
            ExecutorRemote remote = (ExecutorRemote) Naming.lookup(lkp);
            remote.runCommand(cmd);
        } catch (RuntimeException | NotBoundException | RemoteException | MalformedURLException e) {
            e.printStackTrace();
        }

        return ">>End";
    }
}
