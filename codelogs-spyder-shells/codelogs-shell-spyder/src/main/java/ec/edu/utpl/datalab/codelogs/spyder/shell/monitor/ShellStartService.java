package ec.edu.utpl.datalab.codelogs.spyder.shell.monitor;

import ec.edu.utpl.datalab.codelogs.spyder.core.env.EnvironmentProperties;
import ec.edu.utpl.datalab.codelogs.spyder.core.env.EnvironmentPropertiesImpl;
import ec.edu.utpl.datalab.codelogs.spyder.core.env.PropertiesDefault;
import ec.edu.utpl.datalab.codelogs.spyder.core.os.ws.AutoConnectionSocket;
import ec.edu.utpl.datalab.codelogs.spyder.libs.common.Network;
import ec.edu.utpl.datalab.codelogs.spyder.libs.filesystem.search.FilterSet;
import ec.edu.utpl.datalab.codelogs.spyder.libs.filesystem.search.SearchFileSystem;
import org.apache.commons.vfs2.FileObject;
import org.apache.commons.vfs2.VFS;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.shell.core.CommandMarker;
import org.springframework.shell.core.annotation.CliCommand;
import org.springframework.shell.core.annotation.CliOption;
import org.springframework.stereotype.Component;
import org.zeroturnaround.exec.ProcessExecutor;
import org.zeroturnaround.exec.ProcessResult;

import javax.inject.Inject;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;
import java.util.concurrent.TimeoutException;

import static ec.edu.utpl.datalab.codelogs.spyder.libs.filesystem.search.SearchFileSystem.newSearch;

/**
 * * Created by rfcardenas
 */
@Component
public class ShellStartService implements CommandMarker {
    private static final Logger log = LoggerFactory.getLogger(ShellStartService.class);
    private static Thread thread = null;

    private AutoConnectionSocket autoConnectionSocket;

    /**
     * las propiedades de ambiente solo requiere el ambiente como propierad cargada HOME
     * @see  PropertiesDefault
     * @param autoConnectionSocket
     */
    @Inject
    public ShellStartService(AutoConnectionSocket autoConnectionSocket) {
        this.autoConnectionSocket = autoConnectionSocket;
    }

    @CliCommand(value = "iniciar", help = "Inicia el programa spyder como un servicio en segundo plano")
    public void iniciarSpyder(
            @CliOption(key = "puerto",
                    mandatory = false,
                    help = "puerto de conexion para el monitor") Integer port,
            @CliOption(key = "token",
                    mandatory = false,
                    help = "Token spyder para sincronizar datos") String token,
            @CliOption(key = "servidor",
                    mandatory = false,
                    help = "Dirección del servidor para sincronizar datos") String server,
            @CliOption(key = "filesystem",
                    mandatory = false,
                    help = "Monitor de sistem de archivos activado, si opción es false no se escuchara nada a nivel de FS") Boolean filesystem,
            @CliOption(key = "debug",
                    mandatory = false,
                    help = "Depuración activida") Boolean debug,
            @CliOption(key = "home",
                    mandatory = false,
                    help = "ruta de la carpeta por defecto, si no se proporciona se busca en home del usuario") String home) throws Exception {

        //carga el argumento proporcionado, si no existe se asigna el valor "." por defecto
        String homePath = System.getProperty(PropertiesDefault.SPYDER_HOME);
        FileObject folder = VFS.getManager().resolveFile((homePath==null)?".":homePath);

        System.out.println("--------- " + folder.getName().getPath() );
        System.out.println("--------- " + folder.getName().getPath());
        log.info("Iniciando spyder en el puerto {} home {}", port,folder.getName().getPath());

        EnvironmentProperties properties = new EnvironmentPropertiesImpl();
        if(server != null) properties.putValue(PropertiesDefault.SYNC_SERVER, server);
        if(token != null) properties.putValue(PropertiesDefault.SYNC_APIKEY, token);
        if(debug != null) properties.putValue(PropertiesDefault.MONITOR_DEBUG,debug);
        if(filesystem != null) properties.putValue(PropertiesDefault.MONITOR_ENABLE_EVENTSYSTEM_INHERITED,filesystem);
        properties.putValue(PropertiesDefault.SPYDER_HOME,homePath);


        Runnable procRunnable = new Runnable() {
            @Override
            public void run() {
                try {
                    SearchFileSystem modulo =
                        newSearch(3)
                        .withFilter(FilterSet.filterFileNameContain("codelogs-spyder-osservice"))
                        .build();
                    FileObject[] jars = modulo.executeFinder(folder);

                    SearchFileSystem logconfig = SearchFileSystem
                        .newSearch(3)
                        .withFilter(FilterSet.filterFileNameContain("log4j2.xml"))
                        .build();

                    FileObject[] loggins = logconfig.executeFinder(folder);

                    if(jars.length==0) throw new RuntimeException("No se pudo cargar el modulo jar spyder");

                    String proc = jars[0].getName().getPath();
                    ArrayList<String> command = new ArrayList<String>();
                    command.add("java");
                    command.add("-Xmx48m");
                    command.add("-XX:MaxMetaspaceSize=48m");
                    command.add("-XX:MaxMetaspaceSize=48m");
                    command.add("-Dlog4j.configurationFile="+((loggins.length>0)?loggins[0].getName().getPath():""));
                    //command.add("-Djava.util.logging.config.file=asd");
                    command.add("-jar");
                    command.add(proc);
                    for (Map.Entry<Object, Object> prop : properties.asSet()) {
                        command.add(String.format("%s=%s", prop.getKey(),prop.getValue()));
                        log.info("Ejecturando spyder con argumentos {}={}",prop.getKey(), prop.getValue());
                    }
                    log.info("lanzando comando {}",command);
                    ProcessResult out = new ProcessExecutor().command(command)
                        .redirectOutput(System.out)
                        .redirectError(System.err)
                        .readOutput(false)
                        .execute();
                } catch (IOException | InterruptedException | TimeoutException ex) {
                    log.error("Imposible ejecutar thread spyder ",ex);
                }
            }
        };

        thread = new Thread(procRunnable);
        thread.setName("Spyder Thread");
        thread.setUncaughtExceptionHandler((Thread t, Throwable e) -> {
            log.error("Error Thread {} causa {}", e);
        });

        thread.setPriority(Thread.MIN_PRIORITY);
        thread.setDaemon(true);
        thread.start();

    }
    @CliCommand(value = "leventado", help = "determinar si hay una instancia de spyder corriendo")
    public void isRunning(
            @CliOption(key = "puerto",
                    unspecifiedDefaultValue = PropertiesDefault.DEFAULT_PORT + "",
                    mandatory = false,
                    help = "puerto de conexion para el monitor") int port
    ){
        boolean disponible = Network.isAvaiblePort(port);
        if(disponible == false)
            System.out.println("El puerto no es disponible, posiblemente spyder este corriendo  " + port);
        else
            System.out.println("Ningun monitor activo");
    }

}
