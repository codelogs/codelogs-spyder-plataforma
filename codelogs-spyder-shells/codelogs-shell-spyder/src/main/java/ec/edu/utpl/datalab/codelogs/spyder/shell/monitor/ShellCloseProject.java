/*
 * The MIT License (MIT)
 * =========================================================================================================
 *
 * Copyright (c) 10/10/16 12:30 Modulo  Proyecto shell-monitor  Autor [rfcardenas]
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY
 * , WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ec.edu.utpl.datalab.codelogs.spyder.shell.monitor;

import ec.edu.utpl.datalab.codelogs.spyder.core.os.ws.AutoConnectionSocket;
import ec.edu.utpl.datalab.codelogs.spyder.core.util.JsonMapper;
import ec.edu.utpl.datalab.codelogs.spyder.service.comandos.MonitorCommand;
import ec.edu.utpl.datalab.codelogs.spyder.libs.terminal.Command;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.shell.core.CommandMarker;
import org.springframework.shell.core.annotation.CliCommand;
import org.springframework.shell.core.annotation.CliOption;
import org.springframework.stereotype.Component;

import javax.inject.Inject;

/**
 * * Created by rfcardenas
 */
@Component
public class ShellCloseProject implements CommandMarker {
    private static final Logger log = LoggerFactory.getLogger(ShellCloseProject.class);

    private AutoConnectionSocket autoConnectionSocket;

    @Inject
    public ShellCloseProject(AutoConnectionSocket autoConnectionSocket) {
        this.autoConnectionSocket = autoConnectionSocket;
    }

    @CliCommand(
        value = "close",
        help = "Termina la sesión de trabajo para un proyecto, el proyecto es cerrado" +
            "y se deja de seguir los eventos")
    public void close(
        @CliOption(key = "path", mandatory = true, help = "path del proyecto") String path) throws Exception {

        Command command = Command.createCommand(MonitorCommand.CMD_CLOSE_PROJECT, path).build();
        /**JsonArray command = Json.array().asArray()
            .add(Json.object()
                .add(MonitorCommand.COMMAND, MonitorCommand.CMD_CLOSE_PROJECT)
                .add(MonitorCommand.VALUE, path));**/

        autoConnectionSocket.send(JsonMapper.writte(command));
    }
}
