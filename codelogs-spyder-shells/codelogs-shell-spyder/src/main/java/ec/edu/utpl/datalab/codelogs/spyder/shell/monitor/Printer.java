package ec.edu.utpl.datalab.codelogs.spyder.shell.monitor;

import java.io.IOException;

/**
 * * Created by rfcardenas
 */
public class Printer {
    public static void print(String data) {
        try {
            System.out.write(data.getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
