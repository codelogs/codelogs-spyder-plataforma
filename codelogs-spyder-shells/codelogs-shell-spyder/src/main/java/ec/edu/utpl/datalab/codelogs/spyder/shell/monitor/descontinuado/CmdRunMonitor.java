/*
 * The MIT License (MIT)
 * =========================================================================================================
 *
 * Copyright (c) 10/10/16 12:30 Modulo  Proyecto shell-monitor  Autor [rfcardenas]
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY
 * , WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ec.edu.utpl.datalab.codelogs.spyder.shell.monitor.descontinuado;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.shell.core.CommandMarker;
import org.springframework.shell.core.annotation.CliCommand;
import org.springframework.shell.core.annotation.CliOption;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * * Created by rfcardenas
 */
public class CmdRunMonitor implements CommandMarker {
    private static final Logger log = LoggerFactory.getLogger(CmdRunMonitor.class);

    @CliCommand(value = "launch_server", help = "Crea un monitor de trabajo")
    public String close(
        @CliOption(key = "port", mandatory = true, help = "port service") int port,
        @CliOption(key = "ram", mandatory = false, help = "maxmemory in mega") int rammax
    ) throws Exception {
        Process builder = Runtime.getRuntime().exec("java -jar -Xmx90m -XX:MaxMetaspaceSize=40m /lib/shell-monitor-1.0-SNAPSHOT.jar");
        InputStream inputStream = inputStream = builder.getInputStream();
        String line;
        BufferedReader input
            = new BufferedReader(new InputStreamReader(inputStream));
        log.info("Process esta vivo " + builder.isAlive());
        while ((line = input.readLine()) != null) {
            System.out.println(line);
        }
        input.close();

        return "imposible ejecutar";
    }
}
