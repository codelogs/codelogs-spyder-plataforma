package ec.edu.utpl.datalab.codelogs.spyder.shell.monitor;

import ec.edu.utpl.datalab.codelogs.spyder.core.os.ws.AutoConnectionSocket;
import ec.edu.utpl.datalab.codelogs.spyder.core.util.JsonMapper;
import ec.edu.utpl.datalab.codelogs.spyder.service.comandos.MonitorCommand;
import ec.edu.utpl.datalab.codelogs.spyder.libs.terminal.Command;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.shell.core.CommandMarker;
import org.springframework.shell.core.annotation.CliCommand;
import org.springframework.shell.core.annotation.CliOption;
import org.springframework.stereotype.Component;

import javax.inject.Inject;

/**
 * * Created by rfcardenas
 */
@Component
public class ShellEventFile implements CommandMarker {
    private static final Logger log = LoggerFactory.getLogger(ShellConnect.class);
    private AutoConnectionSocket autoConnectionSocket;

    @Inject
    public ShellEventFile(AutoConnectionSocket autoConnectionSocket) {
        this.autoConnectionSocket = autoConnectionSocket;
    }

    @CliCommand(value = "event.file",
        help = "Registra evento de código, cualquier tipo de modificación Actualización, Edición, Eliminación de código"
    )
    public void eventFile(
        @CliOption(
            key = "path", mandatory = true,
            help = "ruta del proyecto, el directorio - path raiz que sirve de contexto para enrutar los datos") String path,
        @CliOption(
            key = "type",
            mandatory = true,
            help = "Tipo de evento, CRUD")
            String type
    ) throws Exception {

        Command command = Command.createCommand(MonitorCommand.CMD_EVENT_FILE,path)
            .addCommand(MonitorCommand.CMD_EVENT_TYPE,type)
            .build();

        autoConnectionSocket.send(JsonMapper.writte(command));
    }
}
