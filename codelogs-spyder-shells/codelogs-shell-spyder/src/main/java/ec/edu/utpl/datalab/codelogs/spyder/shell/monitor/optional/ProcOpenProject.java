/*
 * The MIT License (MIT)
 * =========================================================================================================
 *
 * Copyright (c) 10/10/16 12:30 Modulo  Proyecto shell-monitor  Autor [rfcardenas]
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY
 * , WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ec.edu.utpl.datalab.codelogs.spyder.shell.monitor.optional;

import ec.edu.utpl.datalab.codelogs.spyder.core.filesystem.FileSystemTemporal;
import ec.edu.utpl.datalab.codelogs.spyder.shell.monitor.Util;
import org.apache.commons.vfs2.FileObject;
import org.apache.commons.vfs2.FileSystemException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.shell.core.CommandMarker;
import org.springframework.shell.core.annotation.CliCommand;
import org.springframework.shell.core.annotation.CliOption;

/**
 * * Created by rfcardenas
 */
public class ProcOpenProject implements CommandMarker {
    private static final Logger log = LoggerFactory.getLogger(ProcOpenProject.class);

    @CliCommand(value = "open", help = "Inicia una sesión de trabajo")
    public String open(
        @CliOption(key = "path", mandatory = true, help = "path del proyecto") String path,
        @CliOption(key = "apikey", mandatory = true, help = "API-key del servicio") String apikey) throws FileSystemException, InterruptedException {

        FileObject procFile = FileSystemTemporal.getProcFile();
        if (!procFile.exists()) {
            log.error("El proceso no esta corriendo.....");
            return "Imposible enviar orden";
        }
        int maxAttemps = 5;
        for (int i = 0; i < maxAttemps; i++) {
            log.info("raw cmd..");
            if (procFile.isWriteable()) {
                if (Util.writeCommandOpen(procFile, path)) {
                    break;
                }
            }
            Thread.sleep(1000);
        }
        /** FileObject fileObject = VFS.getManager().resolveFile(MonitorProc._PROC);
         if(!fileObject.exists()){
         System.out.println("El proceso no esta corriendo");
         }else{
         String cmd = String.format("-%s %s -%s %s", MonitorCommand._openProject,path,MonitorCommand._apikey,apikey);
         MonitorProc.main(new String[]{cmd});
         }**/
        return "Monitoreando projecto";
    }
}
