package ec.edu.utpl.datalab.codelogs.spyder.shell.monitor;

/**
 * * Created by rfcardenas
 */
public abstract class AbstractLinkProcess {

    public final void send(String command) {
        sendCommand(command);
    }

    abstract boolean sendCommand(String command);

    abstract boolean finish();
}
