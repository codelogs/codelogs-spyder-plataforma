/*
 * The MIT License (MIT)
 * =========================================================================================================
 *
 * Copyright (c) 10/10/16 12:30 Modulo  Proyecto shell-monitor  Autor [rfcardenas]
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY
 * , WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ec.edu.utpl.datalab.codelogs.spyder.shell.monitor;

import ec.edu.utpl.datalab.codelogs.spyder.core.os.ws.AutoConnectionSocket;
import ec.edu.utpl.datalab.codelogs.spyder.core.util.JsonMapper;
import ec.edu.utpl.datalab.codelogs.spyder.service.comandos.MonitorCommand;
import ec.edu.utpl.datalab.codelogs.spyder.libs.terminal.Command;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.shell.core.CommandMarker;
import org.springframework.shell.core.annotation.CliCommand;
import org.springframework.shell.core.annotation.CliOption;
import org.springframework.stereotype.Component;

import javax.inject.Inject;

/**
 * * Created by rfcardenas
 */
@Component
public class ShellOpenProject implements CommandMarker {
    private static final Logger log = LoggerFactory.getLogger(ShellOpenProject.class);

    private AutoConnectionSocket autoConnectionSocket;

    @Inject
    public ShellOpenProject(AutoConnectionSocket autoConnectionSocket) {
        this.autoConnectionSocket = autoConnectionSocket;
    }

    @CliCommand(value = "open", help = "Inicia una sesión de trabajo")
    public void open(
        @CliOption(key = "path", mandatory = true, help = "path del proyecto") String path,
        @CliOption(key = "ide", mandatory = false, unspecifiedDefaultValue = MonitorCommand.IDE_DEFAULT, help = "Editor utlizado") String ide
    ) throws Exception {

        /**JsonArray command = Json.array().asArray()
            .add(Json.object()
                .add(MonitorCommand.COMMAND, MonitorCommand.CMD_OPEN_PROJECT)
                .add(MonitorCommand.VALUE, path))
            .add(Json.object()
                .add(MonitorCommand.COMMAND, MonitorCommand.IDE_ARGUMENT)
                .add(MonitorCommand.VALUE, ide));**/

        Command command = Command.createCommand(MonitorCommand.CMD_OPEN_PROJECT,path)
            .addCommand(MonitorCommand.IDE_ARGUMENT,ide)
            .build();


        autoConnectionSocket.send(JsonMapper.writte(command));
    }


}
