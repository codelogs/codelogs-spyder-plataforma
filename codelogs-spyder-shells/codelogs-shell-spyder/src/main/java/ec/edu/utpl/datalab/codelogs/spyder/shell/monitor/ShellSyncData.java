package ec.edu.utpl.datalab.codelogs.spyder.shell.monitor;

import ec.edu.utpl.datalab.codelogs.spyder.core.os.ws.AutoConnectionSocket;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.shell.core.CommandMarker;
import org.springframework.shell.core.annotation.CliCommand;
import org.springframework.shell.core.annotation.CliOption;
import org.springframework.stereotype.Component;

import javax.inject.Inject;

/**
 * * Created by rfcardenas
 */
@Component
public class ShellSyncData implements CommandMarker {
    private static final Logger log = LoggerFactory.getLogger(ShellCloseProject.class);

    private AutoConnectionSocket autoConnectionSocket;

    @Inject
    public ShellSyncData(AutoConnectionSocket autoConnectionSocket) {
        this.autoConnectionSocket = autoConnectionSocket;
    }

    @CliCommand(value = "sync", help = "Sincronizar los datos con el server")
    public void sendMetrics(
        @CliOption(key = "data", mandatory = true, help = "Data") String data
    ) throws Exception {
        //autoConnectionSocket.send(data);
    }
}
