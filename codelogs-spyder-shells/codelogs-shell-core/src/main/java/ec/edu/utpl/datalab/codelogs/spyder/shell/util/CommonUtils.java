/*
 * The MIT License (MIT)
 * =========================================================================================================
 *
 * Copyright (c) 10/10/16 12:30 Modulo  Proyecto shell-core  Autor [rfcardenas]
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY
 * , WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ec.edu.utpl.datalab.codelogs.spyder.shell.util;

import java.io.IOException;
import java.io.Reader;
import java.text.DateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.StringJoiner;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;


public final class CommonUtils {

    public static final String NOT_AVAILABLE = "N/A";
    private static final String EMAIL_VALIDATION_REGEX = "^[_A-Za-z0-9-+]+(\\.[_A-Za-z0-9-+]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9-]+)*(\\.[A-Za-z]{2,})$";


    private CommonUtils() {
        throw new AssertionError();
    }


    public static String padRight(String inputString, int size, char paddingChar) {

        final String stringToPad;

        if (inputString == null) {
            stringToPad = "";
        } else {
            stringToPad = inputString;
        }

        StringBuilder padded = new StringBuilder(stringToPad);
        while (padded.length() < size) {
            padded.append(paddingChar);
        }
        return padded.toString();
    }


    public static String padRight(String string, int size) {
        return padRight(string, size, ' ');
    }


    public static String collectionToCommaDelimitedString(Collection<String> list) {
        StringJoiner stringJoiner = new StringJoiner(",");
        list.forEach(stringJoiner::add);
        return stringJoiner.toString();
    }


    public static void closeReader(Reader reader) {
        if (reader != null) {
            try {
                reader.close();
            } catch (IOException e) {
                throw new IllegalStateException("Encountered problem closing Reader.", e);
            }
        }
    }


    public static boolean isValidEmail(String emailAddress) {
        return emailAddress.matches(EMAIL_VALIDATION_REGEX);
    }


    public static String maskPassword(String password) {
        int lengthOfPassword = password.length();
        StringBuilder stringBuilder = new StringBuilder(lengthOfPassword);

        for (int i = 0; i < lengthOfPassword; i++) {
            stringBuilder.append('*');
        }

        return stringBuilder.toString();
    }


    public static String getLocalTime(Date date, TimeZone timeZone) {
        if (date == null) {
            return CommonUtils.NOT_AVAILABLE;
        }


        final DateFormat dateFormat = DateFormat.getDateInstance();
        dateFormat.setTimeZone(timeZone);
        return dateFormat.format(date);
    }


    public static String getTimeZoneNameWithOffset(TimeZone timeZone) {

        long hours = TimeUnit.MILLISECONDS.toHours(timeZone.getRawOffset());
        long minutes = TimeUnit.MILLISECONDS.toMinutes(timeZone.getRawOffset()) - TimeUnit.HOURS.toMinutes(hours);

        return String.format("%s (UTC %d:%02d)", timeZone.getDisplayName(), hours, minutes);

    }
}
