/*
 * The MIT License (MIT)
 * =========================================================================================================
 *
 * Copyright (c) 10/10/16 12:30 Modulo  Proyecto shell-core  Autor [rfcardenas]
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY
 * , WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ec.edu.utpl.datalab.codelogs.spyder.shell.util;


/**
 * Various utility methods when dealing with shell commands.
 *
 * @author Eric Bottard
 */
public class Assertions {

    private Assertions() {

    }

    /**
     * Accepts 2*N arguments, even ones being names and odd ones being values for those names. Asserts that exactly only
     * one value is non null (or non-false, Boolean.FALSE being treated as false), or throws an exception with a
     * descriptive message otherwise.
     *
     * @return the index of the "pair" that was not {@code null}
     * @throws IllegalArgumentException if more than one argument is non null
     * @throws IllegalStateException    if the method is called with wrong values (e.g. non even number of args)
     */
    public static int exactlyOneOf(Object... namesAndValues) {
        int index = atMostOneOf(namesAndValues);
        return index;
    }

    /**
     * Accepts 2*N arguments, even ones being names and odd ones being values for those names. Asserts that at most one
     * value is non null (or non-false, Boolean.FALSE being treated as false), or throws an exception with a descriptive
     * message otherwise.
     *
     * @return the index of the "pair" that was not {@code null}, or -1 if none was set
     * @throws IllegalArgumentException if more than one argument is non null
     * @throws IllegalStateException    if the method is called with wrong values (e.g. non even number of args)
     */
    public static int atMostOneOf(Object... namesAndValues) {
        int index = -1;
        for (int i = 0; i < namesAndValues.length / 2; i++) {
            if (namesAndValues[i * 2 + 1] != null && namesAndValues[i * 2 + 1] != Boolean.FALSE) {
                if (index != -1) {
                    throw new IllegalArgumentException(String.format("You can't specify both '%s' and '%s'",
                        namesAndValues[index * 2], namesAndValues[i * 2]));
                } else {
                    index = i;
                }
            }
        }
        return index;
    }

    /**
     * Returns the names (even values) out of the given array.
     */
    private static String collectNames(Object[] namesAndValues) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < namesAndValues.length; i += 2) {
            sb.append('\'').append(namesAndValues[i]).append("', ");
        }
        return sb.substring(0, sb.length() - 2); // chop last ", "
    }
}
