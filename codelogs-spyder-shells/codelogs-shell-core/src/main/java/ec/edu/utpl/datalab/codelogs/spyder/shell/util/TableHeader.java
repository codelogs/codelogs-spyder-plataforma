/*
 * The MIT License (MIT)
 * =========================================================================================================
 *
 * Copyright (c) 10/10/16 12:30 Modulo  Proyecto shell-core  Autor [rfcardenas]
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY
 * , WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ec.edu.utpl.datalab.codelogs.spyder.shell.util;


public class TableHeader {

    private int maxWidth = -1;

    private int width = 0;

    private String name;

    /**
     * Constructor that initializes the table header with the provided header name and the with of the table header.
     *
     * @param name
     * @param width
     */
    public TableHeader(String name, int width) {

        super();
        this.width = width;
        this.name = name;

    }

    /**
     * Constructor that initializes the table header with the provided header name. The with of the table header is
     * calculated and assigned based on the provided header name.
     *
     * @param name
     */
    public TableHeader(String name) {
        super();
        this.name = name;

        if (name == null) {
            this.width = 0;
        } else {
            this.width = name.length();
        }

    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    /**
     * Updated the width for this particular column, but only if the value of the passed-in width is higher than the
     * value of the pre-existing width.
     *
     * @param width
     */
    public void updateWidth(int width) {
        if (this.width < width) {
            if (this.maxWidth > 0 && this.maxWidth < width) {
                this.width = this.maxWidth;
            } else {
                this.width = width;
            }
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getMaxWidth() {
        return maxWidth;
    }

    /**
     * Defaults to -1 indicating to ignore the property.
     *
     * @param maxWidth If negative or zero this property will be ignored.
     */
    public void setMaxWidth(int maxWidth) {
        this.maxWidth = maxWidth;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + maxWidth;
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + width;
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        TableHeader other = (TableHeader) obj;
        if (maxWidth != other.maxWidth)
            return false;
        if (name == null) {
            if (other.name != null)
                return false;
        } else if (!name.equals(other.name))
            return false;
        if (width != other.width)
            return false;
        return true;
    }


}
