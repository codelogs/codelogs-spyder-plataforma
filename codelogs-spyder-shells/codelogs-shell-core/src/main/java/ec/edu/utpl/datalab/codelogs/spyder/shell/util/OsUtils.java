package ec.edu.utpl.datalab.codelogs.spyder.shell.util;

import static jline.internal.Configuration.getOsName;

/**
 * Created by rfcardenas
 */
public class OsUtils {
    public static boolean isWindows() {
        return getOsName().startsWith("Windows");
    }

    public static boolean isLinux() {
        return getOsName().startsWith("Linux");
    }
}
