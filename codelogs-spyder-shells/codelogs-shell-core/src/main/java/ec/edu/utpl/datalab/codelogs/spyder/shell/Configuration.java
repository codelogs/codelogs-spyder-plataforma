/*
 * The MIT License (MIT)
 * =========================================================================================================
 *
 * Copyright (c) 10/10/16 12:30 Modulo  Proyecto shell-core  Autor [rfcardenas]
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY
 * , WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ec.edu.utpl.datalab.codelogs.spyder.shell;

import ec.edu.utpl.datalab.codelogs.spyder.shell.util.CommonUtils;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import java.lang.annotation.Target;
import java.util.Date;
import java.util.TimeZone;


@Component
public class Configuration {

    private Target target;

    private TimeZone clientTimeZone;


    public Configuration() {
        clientTimeZone = TimeZone.getDefault();
    }


    public Target getTarget() {
        return target;
    }


    public void setTarget(Target target) {
        Assert.notNull(target, "The provided target must not be null.");
        this.target = target;
    }


    public TimeZone getClientTimeZone() {
        return clientTimeZone;
    }


    public void setClientTimeZone(TimeZone clientTimeZone) {
        Assert.notNull(clientTimeZone, "The provided timeZone must not be null.");
        this.clientTimeZone = clientTimeZone;
    }


    public String getLocalTime(Date date) {
        return CommonUtils.getLocalTime(date, this.clientTimeZone);
    }

}
