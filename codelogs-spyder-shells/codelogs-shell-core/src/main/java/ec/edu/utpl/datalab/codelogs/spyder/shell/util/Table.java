/*
 * The MIT License (MIT)
 * =========================================================================================================
 *
 * Copyright (c) 10/10/16 12:30 Modulo  Proyecto shell-core  Autor [rfcardenas]
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY
 * , WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ec.edu.utpl.datalab.codelogs.spyder.shell.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class Table {

    private final Map<Integer, TableHeader> headers = new TreeMap<Integer, TableHeader>();

    private volatile List<TableRow> rows = new ArrayList<TableRow>(0);

    public List<TableRow> getRows() {
        return rows;
    }

    public Map<Integer, TableHeader> getHeaders() {
        return headers;
    }

    public Table addHeader(Integer columnIndex, TableHeader tableHeader) {
        this.headers.put(columnIndex, tableHeader);
        return this;
    }

    /**
     * Add a new empty row to the table.
     *
     * @return the newly created row, which can be then be populated
     */
    public TableRow newRow() {
        TableRow row = new TableRow();
        rows.add(row);
        return row;
    }

    public Table addRow(String... values) {

        final TableRow row = new TableRow();

        int column = 1;

        for (String value : values) {
            row.addValue(column, value);
            column++;
        }

        rows.add(row);

        return this;
    }

    public void calculateColumnWidths() {
        for (Map.Entry<Integer, TableHeader> headerEntry : headers.entrySet()) {
            final Integer headerEntryKey = headerEntry.getKey();
            for (TableRow tableRow : rows) {
                if (tableRow.getValue(headerEntryKey) != null) {
                    headerEntry.getValue().updateWidth(tableRow.getValue(headerEntryKey).length());
                }
            }
        }
    }

    @Override
    public String toString() {
        return UiUtils.renderTextTable(this);
    }

    @Override
    public int hashCode() {
        calculateColumnWidths();
        final int prime = 31;
        int result = 1;
        result = prime * result + ((headers == null) ? 0 : headers.hashCode());
        result = prime * result + ((rows == null) ? 0 : rows.hashCode());
        return result;
    }


    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;

        Table other = (Table) obj;
        this.calculateColumnWidths();
        other.calculateColumnWidths();
        if (headers == null) {
            if (other.headers != null)
                return false;
        } else if (!headers.equals(other.headers))
            return false;
        if (rows == null) {
            if (other.rows != null)
                return false;
        } else if (!rows.equals(other.rows))
            return false;
        return true;
    }
}
