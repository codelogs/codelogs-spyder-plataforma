/*
 * The MIT License (MIT)
 * =========================================================================================================
 *
 * Copyright (c) 10/10/16 12:30 Modulo  Proyecto shell-core  Autor [rfcardenas]
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY
 * , WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ec.edu.utpl.datalab.codelogs.spyder.shell.util;

import com.google.common.base.Splitter;
import org.apache.commons.lang3.StringUtils;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public final class UiUtils {

    public static final String HORIZONTAL_LINE = "-------------------------------------------------------------------------------\n";

    public static final int COLUMN_1 = 1;

    public static final int COLUMN_2 = 2;

    public static final int COLUMN_3 = 3;

    public static final int COLUMN_4 = 4;

    public static final int COLUMN_5 = 5;

    public static final int COLUMN_6 = 6;


    private UiUtils() {
        throw new AssertionError();
    }


    public static String renderMapDataAsTable(List<Map<String, Object>> data, List<String> columns) {

        Table table = new Table();

        int col = 0;
        for (String colName : columns) {
            col++;
            table.getHeaders().put(col, new TableHeader(colName));
            if (col >= 6) {
                break;
            }
        }

        for (Map<String, Object> dataRow : data) {

            TableRow tableRow = new TableRow();

            for (int i = 0; i < col; i++) {
                String value = dataRow.get(columns.get(i)).toString();
                table.getHeaders().get(i + 1).updateWidth(value.length());
                tableRow.addValue(i + 1, value);
            }

            table.getRows().add(tableRow);
        }

        return renderTextTable(table);
    }

    public static String renderParameterInfoDataAsTable(Map<String, String> parameters, boolean withHeader,
                                                        int lastColumnMaxWidth) {
        final Table table = new Table();

        table.getHeaders().put(COLUMN_1, new TableHeader("Parameter"));

        final TableHeader tableHeader2 = new TableHeader("Value (Configured or Default)");
        tableHeader2.setMaxWidth(lastColumnMaxWidth);
        table.getHeaders().put(COLUMN_2, tableHeader2);

        for (Entry<String, String> entry : parameters.entrySet()) {

            final TableRow tableRow = new TableRow();

            table.getHeaders().get(COLUMN_1).updateWidth(entry.getKey().length());
            tableRow.addValue(COLUMN_1, entry.getKey());

            int width = entry.getValue() != null ? entry.getValue().length() : 0;

            table.getHeaders().get(COLUMN_2).updateWidth(width);
            tableRow.addValue(COLUMN_2, entry.getValue());

            table.getRows().add(tableRow);
        }

        return renderTextTable(table, withHeader);
    }


    public static String renderParameterInfoDataAsTable(Map<String, String> parameters) {
        return renderParameterInfoDataAsTable(parameters, true, -1);
    }

    public static String renderTextTable(Table table) {
        return renderTextTable(table, true);
    }


    public static String renderTextTable(Table table, boolean withHeader) {

        table.calculateColumnWidths();

        final String padding = "  ";
        final String headerBorder = getHeaderBorder(table.getHeaders());
        final StringBuilder textTable = new StringBuilder();

        if (withHeader) {
            final StringBuilder headerline = new StringBuilder();
            for (TableHeader header : table.getHeaders().values()) {

                if (header.getName().length() > header.getWidth()) {
                    Iterable<String> chunks = Splitter.fixedLength(header.getWidth()).split(header.getName());
                    int length = headerline.length();
                    boolean first = true;
                    for (String chunk : chunks) {
                        final String lineToAppend;
                        if (first) {
                            lineToAppend = padding + CommonUtils.padRight(chunk, header.getWidth());
                        } else {
                            lineToAppend = StringUtils.leftPad("", length) + padding
                                + CommonUtils.padRight(chunk, header.getWidth());
                        }
                        first = false;
                        headerline.append(lineToAppend);
                        headerline.append("\n");
                    }
                    headerline.deleteCharAt(headerline.lastIndexOf("\n"));
                } else {
                    String lineToAppend = padding + CommonUtils.padRight(header.getName(), header.getWidth());
                    headerline.append(lineToAppend);
                }
            }
            textTable.append(StringUtils.trim(headerline.toString()));
            textTable.append("\n");
        }

        textTable.append(headerBorder);

        for (TableRow row : table.getRows()) {
            StringBuilder rowLine = new StringBuilder();
            for (Entry<Integer, TableHeader> entry : table.getHeaders().entrySet()) {
                String value = row.getValue(entry.getKey());
                if (null != value && (value.length() > entry.getValue().getWidth())) {
                    Iterable<String> chunks = Splitter.fixedLength(entry.getValue().getWidth()).split(value);
                    int length = rowLine.length();
                    boolean first = true;
                    for (String chunk : chunks) {
                        final String lineToAppend;
                        if (first) {
                            lineToAppend = padding + CommonUtils.padRight(chunk, entry.getValue().getWidth());
                        } else {
                            lineToAppend = StringUtils.leftPad("", length) + padding
                                + CommonUtils.padRight(chunk, entry.getValue().getWidth());
                        }
                        first = false;
                        rowLine.append(lineToAppend);
                        rowLine.append("\n");
                    }
                    rowLine.deleteCharAt(rowLine.lastIndexOf("\n"));
                } else {
                    String lineToAppend = padding + CommonUtils.padRight(value, entry.getValue().getWidth());
                    rowLine.append(lineToAppend);
                }
            }
            textTable.append(StringUtils.trim(rowLine.toString()));
            textTable.append("\n");
        }

        if (!withHeader) {
            textTable.append(headerBorder);
        }

        return textTable.toString();
    }


    public static String getHeaderBorder(Map<Integer, TableHeader> headers) {

        final StringBuilder headerBorder = new StringBuilder();

        for (TableHeader header : headers.values()) {
            headerBorder.append(CommonUtils.padRight("  ", header.getWidth() + 2, '-'));
        }
        headerBorder.append("\n");

        return headerBorder.toString();
    }
}
