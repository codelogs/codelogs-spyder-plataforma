/*
 * The MIT License (MIT)
 * =========================================================================================================
 *
 * Copyright (c) 10/10/16 12:30 Modulo  Proyecto shell-core  Autor [rfcardenas]
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY
 * , WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ec.edu.utpl.datalab.codelogs.spyder.shell;

import ec.edu.utpl.datalab.codelogs.spyder.shell.util.UiUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.shell.plugin.BannerProvider;
import org.springframework.shell.support.util.FileUtils;
import org.springframework.stereotype.Component;


@Component
@Order(Ordered.HIGHEST_PRECEDENCE)
public class RxShellBannerProvider implements BannerProvider {

    private static final String WELCOME = "Welcome to the REXcode  shell. For assistance hit TAB or type \"help\".";
    @Autowired
    private Configuration configuration;

    @Override
    public String getProviderName() {
        return "REX Shell";
    }

    @Override
    public String getBanner() {


        StringBuilder banner = new StringBuilder();
        banner.append(FileUtils.readBanner(RxShellBannerProvider.class, "/banner.txt"));

        banner.append("\n" + UiUtils.HORIZONTAL_LINE)
            .append("\nConectate al monitor, para ejecutar acciones")
            .append("\n" + UiUtils.HORIZONTAL_LINE);

        return banner.toString();
    }

    /**
     * Returns the version information as found in the manifest file (set during release).
     */
    @Override
    public String getVersion() {
        Package pkg = RxShellBannerProvider.class.getPackage();
        String version = null;
        if (pkg != null) {
            version = pkg.getImplementationVersion();
        }
        return (version != null ? version : "Version 1.0");
    }

    @Override
    public String getWelcomeMessage() {
        return WELCOME;
    }

}
