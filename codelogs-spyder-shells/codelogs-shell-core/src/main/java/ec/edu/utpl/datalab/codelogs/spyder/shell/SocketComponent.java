package ec.edu.utpl.datalab.codelogs.spyder.shell;

import com.google.inject.Singleton;
import ec.edu.utpl.datalab.codelogs.spyder.core.os.ws.AutoConnectionSocket;
import org.springframework.stereotype.Component;

/**
 * Solucion temporal @Bean no trabaj
 * * Created by rfcardenas
 */
@Component
@Singleton
public class SocketComponent extends AutoConnectionSocket {

    public SocketComponent() {
        super(8484);
        try {
            connectMonitor();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void connectMonitor() throws Exception {
        System.out.println("Ejecutando Socket shell ");
        super.connectMonitor();
    }

}
