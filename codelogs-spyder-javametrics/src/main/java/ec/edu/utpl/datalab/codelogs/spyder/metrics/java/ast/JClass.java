/*
 * The MIT License (MIT)
 * =========================================================================================================
 *
 * Copyright (c) 10/10/16 12:30 Modulo  Proyecto rexcode-metrics-service-modules  Autor [rfcardenas]
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY
 * , WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ec.edu.utpl.datalab.codelogs.spyder.metrics.java.ast;


import com.github.javaparser.ast.body.ClassOrInterfaceDeclaration;
import com.github.javaparser.ast.visitor.VoidVisitorAdapter;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.model.tree.elements.NodeClase;

import java.util.ArrayList;
import java.util.List;

/**
 * * Created by rfcardenas
 */
public class JClass extends NodeClase implements JVisitable, AstVisitable {
    private ClassOrInterfaceDeclaration ast;
    private List<JNodeMethod> jMethodList = new ArrayList<>();
    private List<JClass> jinnerClass = new ArrayList<>();

    public JClass(String nameNode, ClassOrInterfaceDeclaration ast) {
        super(nameNode);
        this.ast = ast;
    }

    public void addJmethod(JNodeMethod method) {
        addSubElement(method);
        this.jMethodList.add(method);
    }

    public void addInnerClass(JClass jClass) {
        addSubElement(jClass);
        jinnerClass.add(jClass);
    }

    public ClassOrInterfaceDeclaration getAst() {
        return ast;
    }

    public List<JNodeMethod> getjMethodList() {
        return jMethodList;
    }

    public List<JClass> getInnersClass() {
        return jinnerClass;
    }

    @Override
    public void accept(IJTraverser traverser) {
        traverser.visit(this);
    }

    @Override
    public void accept(VoidVisitorAdapter visitor) {
        ast.accept(visitor, null);
    }


    @Override
    public void clear() {
        this.ast = null;
    }
}
