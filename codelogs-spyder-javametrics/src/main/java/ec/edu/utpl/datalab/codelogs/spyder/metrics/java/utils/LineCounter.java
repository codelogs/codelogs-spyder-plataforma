/*
 * The MIT License (MIT)
 * =========================================================================================================
 *
 * Copyright (c) 10/10/16 12:30 Modulo  Proyecto rexcode-metrics-service-modules  Autor [rfcardenas]
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY
 * , WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ec.edu.utpl.datalab.codelogs.spyder.metrics.java.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * * Created by rfcardenas
 */
public class LineCounter {

    public static int getCommentFor(String srcFile) {
        Pattern p = Pattern.compile("^[\\s]*?\\n", Pattern.MULTILINE);
        //Pattern p = Pattern.compile("(?m)^[\\s]*$");
        Matcher m = p.matcher(srcFile);
        int scloc = 0;
        while (m.find()) {
            scloc += 1;
        }
        return scloc;
    }

    public static int getNumberLinesFromString(String string) {
        if (string == null || string.isEmpty()) {
            return 0;
        }
        int lines = 1;
        int pos = 0;
        while ((pos = string.indexOf("\n", pos) + 1) != 0) {
            lines++;
        }
        return lines;
    }
}
