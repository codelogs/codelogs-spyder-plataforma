package ec.edu.utpl.datalab.codelogs.spyder.metrics.java.metrics.formulas.fragment;

import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.core.hk2.Metric;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.domain.Measure;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.java.ast.JSource;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.java.metrics.definiciones.DefinicionesSourceLines;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.java.metrics.formulas.SharedData;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.java.utils.LineCounter;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.metric.Action;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.metric.BuilderMetric;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.metric.MetricProvider;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.metric.ModuleMetric;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.model.tree.elements.NodeFragment;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.model.tree_traverser.spliterators_type.TreeTypeSpliterator;
import org.jvnet.hk2.annotations.Service;

import java.util.stream.StreamSupport;

/**
 * * Created by rfcardenas
 */
@Service
@Metric("java")
public class FormulaLocFragment implements MetricProvider {
    @Override
    public ModuleMetric specification() {
        return BuilderMetric.fluet(DefinicionesSourceLines.LINES_CODE, NodeFragment.class, "X")
            .metric(SharedData.versionMetric)
            .formula((Action<NodeFragment>) this::formula)
            .ok();
    }

    public Measure formula(NodeFragment fragment) {
        int totalLines = 0;
        TreeTypeSpliterator<JSource> typeSpliterator = TreeTypeSpliterator.create(fragment, JSource.class);
        totalLines = StreamSupport.stream(typeSpliterator, false)
            .filter(cu -> {
                return cu.getCompilationUnit() != null;
            })
            .mapToInt(cu -> {
                return LineCounter.getNumberLinesFromString(cu.getCompilationUnit().toString());
            }).sum();
        System.out.println("--------------------------> fragment " + totalLines);
        return new Measure(totalLines);
    }
}
