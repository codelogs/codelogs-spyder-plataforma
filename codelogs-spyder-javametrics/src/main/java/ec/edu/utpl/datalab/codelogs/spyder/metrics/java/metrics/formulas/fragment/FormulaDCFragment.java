/*
 * The MIT License (MIT)
 * =========================================================================================================
 *
 * Copyright (c) 10/10/16 12:30 Modulo  Proyecto rexcode-metrics-service-modules  Autor [rfcardenas]
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY
 * , WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ec.edu.utpl.datalab.codelogs.spyder.metrics.java.metrics.formulas.fragment;

import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.core.hk2.Metric;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.domain.Measure;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.holders.ResultHolder;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.java.metrics.definiciones.DefinicionesSourceLines;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.java.metrics.formulas.SharedData;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.metric.BuilderMetric;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.metric.MetricProvider;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.metric.ModuleMetric;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.model.tree.elements.NodeFragment;
import org.jvnet.hk2.annotations.Service;

import java.util.Optional;

/**
 * * Created by rfcardenas
 */
@Service
@Metric("java")
public class FormulaDCFragment implements MetricProvider {
    @Override
    public ModuleMetric specification() {
        return BuilderMetric.fluet(DefinicionesSourceLines.DENSIDAD_COMMENT, NodeFragment.class, "1e82b11e-1a91-4bf6-bfdd-87de61a3b708")
            .metric(SharedData.versionMetric)
            .addDependecy(FormulaCLocFragment.class)
            .addDependecy(FormulaLocFragment.class)
            .formula(this::formula)
            .ok();
    }

    public Measure formula(NodeFragment nodeProject, ResultHolder defaultCacheProcess) {
        double radio = 0;
        Optional<Measure> mresultLoc = defaultCacheProcess.load(nodeProject, FormulaCLocFragment.class);
        Optional<Measure> mresultCloc = defaultCacheProcess.load(nodeProject, FormulaLocFragment.class);
        if (mresultCloc.isPresent() && mresultLoc.isPresent()) {
            radio = (mresultCloc.get().getResult() * 100) / mresultLoc.get().getResult();
        }
        return new Measure(radio);
    }
}
