package ec.edu.utpl.datalab.codelogs.spyder.metrics.java.metrics.formulas.file;


import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.core.hk2.Metric;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.domain.Measure;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.java.ast.JSource;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.java.metrics.definiciones.DefinicionesSourceLines;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.java.metrics.formulas.SharedData;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.java.utils.LineCounter;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.metric.Action;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.metric.BuilderMetric;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.metric.MetricProvider;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.metric.ModuleMetric;
import org.jvnet.hk2.annotations.Service;

/**
 * * Created by rfcardenas
 */
@Service
@Metric("java")
public class FormulaLocFile implements MetricProvider {

    @Override
    public ModuleMetric specification() {
        return BuilderMetric.fluet(DefinicionesSourceLines.LINES_CODE, JSource.class, "x")
            .metric(SharedData.versionMetric)
            .formula((Action<JSource>) this::formula)
            .ok();
    }

    public Measure formula(JSource source) {
        int totalLines = 0;
        totalLines = LineCounter.getNumberLinesFromString(source.getCompilationUnit().toString());
        return new Measure(totalLines);
    }
}
