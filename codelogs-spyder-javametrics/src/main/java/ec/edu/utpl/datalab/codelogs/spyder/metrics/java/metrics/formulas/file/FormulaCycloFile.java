package ec.edu.utpl.datalab.codelogs.spyder.metrics.java.metrics.formulas.file;


import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.core.hk2.Metric;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.domain.Measure;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.java.ast.JSource;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.java.metrics.definiciones.DefinitionesComplex;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.java.metrics.formulas.SharedData;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.java.metrics.visitors.CycloVisitor;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.metric.Action;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.metric.BuilderMetric;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.metric.MetricProvider;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.metric.ModuleMetric;
import org.jvnet.hk2.annotations.Service;

/**
 * * Created by rfcardenas
 */
@Service
@Metric("java")
public class FormulaCycloFile implements MetricProvider {
    @Override
    public ModuleMetric specification() {
        return BuilderMetric.fluet(DefinitionesComplex.cyclomaticComplex, JSource.class, "X")
            .metric(SharedData.versionMetric)
            .formula((Action<JSource>) this::formula)
            .ok();
    }

    public Measure formula(JSource jSource) {
        int value = 0;
        CycloVisitor cycloVisitor = new CycloVisitor();
        jSource.accept(cycloVisitor);
        value = cycloVisitor.getCyclo();
        return new Measure(value);
    }
}
