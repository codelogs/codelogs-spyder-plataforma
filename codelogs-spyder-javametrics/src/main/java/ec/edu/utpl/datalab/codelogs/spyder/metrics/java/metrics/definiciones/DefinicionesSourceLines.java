/*
 * The MIT License (MIT)
 * =========================================================================================================
 *
 * Copyright (c) 10/10/16 12:30 Modulo  Proyecto rexcode-metrics-service-modules  Autor [rfcardenas]
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY
 * , WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ec.edu.utpl.datalab.codelogs.spyder.metrics.java.metrics.definiciones;


import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.domain.Metric;

/**
 * * Created by rfcardenas
 */
public class DefinicionesSourceLines {
    public static Metric LINES_CODE = Metric.builder("Total de lineas de codigo")
        .abbreviation("LOC")
        .keyWord("loc file")
        .creator("UTPL")
        .user_help("Calcula el valor total de codigo en el nodo")
        .build();
    public static Metric SOURCE_LINES = Metric.builder("Total codigo fuente")
        .abbreviation("SLOC")
        .keyWord("sloc file")
        .creator("UTPL")
        .user_help("Calcula la cantidad de codigo correspondiente  enfocado al trabajo")
        .build();


    public static Metric COMMENT_LINES = Metric.builder("Total Comentarios")
        .abbreviation("CLOC")
        .keyWord("Cloc file, lineas comentadas por archivo")
        .creator("UTPL")
        .user_help("Calcula la cantidad de condigo comentado")
        .build();
    public static Metric DENSIDAD_COMMENT = Metric.builder("Densidad Comentarios")
        .abbreviation("DCM")
        .keyWord("Densidad de comentarios")
        .creator("UTPL")
        .user_help("Calcula el porcentaje de comentarios encontrados en el nodo")
        .build();
}
