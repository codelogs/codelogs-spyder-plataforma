package ec.edu.utpl.datalab.codelogs.spyder.metrics.java.metrics.formulas.clase;


import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.core.hk2.Metric;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.domain.Measure;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.java.ast.JClass;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.java.metrics.definiciones.DefinitionesComplex;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.java.metrics.formulas.SharedData;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.java.metrics.visitors.CycloVisitor;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.metric.Action;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.metric.BuilderMetric;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.metric.MetricProvider;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.metric.ModuleMetric;
import org.jvnet.hk2.annotations.Service;

/**
 * * Created by rfcardenas
 */

@Service
@Metric("java")
public class FormulaCycloClass implements MetricProvider {
    @Override
    public ModuleMetric specification() {
        return BuilderMetric.fluet(DefinitionesComplex.cyclomaticComplex, JClass.class, "64f860b9-ceb6-491e-bce6-1c4a29c0e645")
            .metric(SharedData.versionMetric)
            .formula((Action<JClass>) this::formula)
            .ok();
    }

    public Measure formula(JClass jclass) {
        int value = 0;
        CycloVisitor cycloVisitor = new CycloVisitor();
        jclass.accept(cycloVisitor);
        value = cycloVisitor.getCyclo();
        return new Measure(value);
    }
}
