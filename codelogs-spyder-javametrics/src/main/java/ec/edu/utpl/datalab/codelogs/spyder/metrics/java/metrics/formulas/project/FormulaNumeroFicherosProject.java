package ec.edu.utpl.datalab.codelogs.spyder.metrics.java.metrics.formulas.project;

import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.core.hk2.Metric;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.domain.Measure;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.java.ast.JSource;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.java.metrics.definiciones.DefinicionSize;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.java.metrics.formulas.SharedData;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.metric.Action;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.metric.BuilderMetric;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.metric.MetricProvider;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.metric.ModuleMetric;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.model.tree.elements.NodeProject;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.model.tree_traverser.spliterators_type.TreeTypeSpliterator;
import org.jvnet.hk2.annotations.Service;

import java.util.stream.StreamSupport;

/**
 * * Created by rfcardenas
 */
@Service
@Metric("java")
public class FormulaNumeroFicherosProject implements MetricProvider {
    @Override
    public ModuleMetric specification() {
        return BuilderMetric.fluet(DefinicionSize.NUMERO_FICHEROS, NodeProject.class, "fe128235-4ac8-430a-956d-e34dc402366f")
            .metric(SharedData.versionMetric)
            .formula((Action<NodeProject>) this::formula)
            .ok();
    }

    public Measure formula(NodeProject nodeProject) {
        int totalfiles = 0;
        TreeTypeSpliterator<JSource> typeSpliterator = TreeTypeSpliterator.create(nodeProject, JSource.class);
        totalfiles = StreamSupport.stream(typeSpliterator, false)
            .mapToInt(JSource -> 1).sum();
        return new Measure(totalfiles);
    }
}
