/*
 * The MIT License (MIT)
 * =========================================================================================================
 *
 * Copyright (c) 10/10/16 12:30 Modulo  Proyecto rexcode-metrics-service-modules  Autor [rfcardenas]
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY
 * , WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ec.edu.utpl.datalab.codelogs.spyder.metrics.java.metrics.formulas.fragment;

import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.core.hk2.Metric;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.domain.Measure;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.java.ast.JNodeMethod;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.java.metrics.definiciones.DefinicionSize;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.java.metrics.formulas.SharedData;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.metric.Action;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.metric.BuilderMetric;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.metric.MetricProvider;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.metric.ModuleMetric;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.model.tree.elements.NodeFragment;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.model.tree_traverser.spliterators_type.TreeTypeSpliterator;
import org.jvnet.hk2.annotations.Service;

import java.util.stream.StreamSupport;

/**
 * * Created by rfcardenas
 */
@Service
@Metric("java")
public class FormulaNumMethodFragment implements MetricProvider {
    @Override
    public ModuleMetric specification() {
        return BuilderMetric.fluet(DefinicionSize.NUMERO_METODOS, NodeFragment.class, "666909f6-39fe-400b-a0fd-27c5da008c2b")
            .metric(SharedData.versionMetric)
            .formula((Action<NodeFragment>) this::formula)
            .ok();
    }

    public Measure formula(NodeFragment node) {
        int totalMethod = 0;
        TreeTypeSpliterator<JNodeMethod> typeSpliterator = TreeTypeSpliterator.create(node, JNodeMethod.class);
        totalMethod = StreamSupport.stream(typeSpliterator, false)
            .mapToInt(jmethod -> 1).sum();
        return new Measure(totalMethod);
    }
}
