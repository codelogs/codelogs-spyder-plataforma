/*
 * The MIT License (MIT)
 * =========================================================================================================
 *
 * Copyright (c) 10/10/16 12:30 Modulo  Proyecto rexcode-metrics-service-modules  Autor [rfcardenas]
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY
 * , WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ec.edu.utpl.datalab.codelogs.spyder.metrics.java.metrics.visitors;

import com.github.javaparser.ast.expr.CastExpr;
import com.github.javaparser.ast.stmt.*;
import com.github.javaparser.ast.visitor.VoidVisitorAdapter;

public class CycloVisitor extends VoidVisitorAdapter {
    private int cyclo = 0;

    public CycloVisitor() {
        super();
    }

    @Override
    public void visit(BreakStmt n, Object arg) {
        super.visit(n, arg);
        cyclo++;
    }

    @Override
    public void visit(CastExpr n, Object arg) {
        super.visit(n, arg);
        cyclo++;
    }

    @Override
    public void visit(CatchClause n, Object arg) {
        super.visit(n, arg);
    }

    @Override
    public void visit(ForeachStmt n, Object arg) {
        super.visit(n, arg);
        cyclo++;
    }

    @Override
    public void visit(ForStmt n, Object arg) {
        super.visit(n, arg);
        cyclo++;
    }

    @Override
    public void visit(IfStmt n, Object arg) {
        super.visit(n, arg);
        cyclo++;
    }

    @Override
    public void visit(SwitchEntryStmt n, Object arg) {
        super.visit(n, arg);
        cyclo++;
    }

    @Override
    public void visit(SwitchStmt n, Object arg) {
        super.visit(n, arg);
        cyclo++;
    }

    @Override
    public void visit(ThrowStmt n, Object arg) {
        super.visit(n, arg);
        cyclo++;
    }

    @Override
    public void visit(TryStmt n, Object arg) {
        super.visit(n, arg);
        cyclo++;
    }

    @Override
    public void visit(WhileStmt n, Object arg) {
        super.visit(n, arg);
        cyclo++;
    }

    @Override
    public void visit(DoStmt n, Object arg) {
        super.visit(n, arg);
        cyclo++;
    }

    @Override
    public void visit(ReturnStmt n, Object arg) {
        super.visit(n, arg);
        cyclo++;
    }

    public int getCyclo() {
        return cyclo;
    }
}
