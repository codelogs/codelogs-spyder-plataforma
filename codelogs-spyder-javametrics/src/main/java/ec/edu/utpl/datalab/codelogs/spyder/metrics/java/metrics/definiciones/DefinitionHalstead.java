/*
 * The MIT License (MIT)
 * =========================================================================================================
 *
 * Copyright (c) 10/10/16 12:30 Modulo  Proyecto rexcode-metrics-service-modules  Autor [rfcardenas]
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY
 * , WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ec.edu.utpl.datalab.codelogs.spyder.metrics.java.metrics.definiciones;


import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.domain.Metric;

/**
 * * Created by rfcardenas
 */
public class DefinitionHalstead {
    public static Metric HALSTEAD_EFFORT = Metric.builder("Halstead EFFORT")
        .abbreviation("CC")
        .keyWord("Esfuerzo Halstead")
        .creator("UTPL")
        .user_help("Calcula la complejidad ciclomatica")
        .build();

    public static Metric HALSTED_VOL = Metric.builder("Halstead Volumen")
        .abbreviation("VL")
        .keyWord("Tamaño")
        .creator("UTPL")
        .user_help("Es otra estimacion del tamaño del programa")
        .build();
    public static Metric HALSTED_TIME = Metric.builder("Halstead Tiempo")
        .abbreviation("T")
        .keyWord("Tiempo")
        .creator("UTPL")
        .user_help("Tiempo estimado de implementacion")
        .build();
    public static Metric HALSTEA_DIF = Metric.builder("Halstead Dificultaa")
        .abbreviation("HD")
        .keyWord("Tiempo")
        .creator("UTPL")
        .user_help("Tiempo estimado de implementacion")
        .build();
    public static Metric HALSTEA_BUGS = Metric.builder("Halstead Bugs")
        .abbreviation("HB")
        .keyWord("Tiempo")
        .creator("UTPL")
        .user_help("Tiempo estimado de implementacion")
        .build();

    public static Metric HALSTEA_VOCABULARY = Metric.builder("Halstead Vocabulary")
        .abbreviation("HV")
        .keyWord("Halstead Vocabulary")
        .creator("UTPL")
        .user_help("Tiempo estimado de implementacion")
        .build();
}
