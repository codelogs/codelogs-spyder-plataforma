package ec.edu.utpl.datalab.codelogs.spyder.metrics.java.metrics.formulas.project;

import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.core.hk2.Metric;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.domain.Measure;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.holders.ResultHolder;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.java.metrics.definiciones.DefinicionMantenibilidad;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.java.metrics.formulas.SharedData;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.java.metrics.formulas.file.FormulaMantenibilityIndexFile;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.metric.ActionCached;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.metric.BuilderMetric;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.metric.MetricProvider;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.metric.ModuleMetric;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.model.tree.NodeKink;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.model.tree.elements.NodeProject;
import org.jvnet.hk2.annotations.Service;

import java.util.List;

/**
 * * Created by rfcardenas
 */
@Service
@Metric("java")
public class FormulaAvgMantenibilityIndexProject implements MetricProvider {
    @Override
    public ModuleMetric specification() {
        return BuilderMetric.fluet(DefinicionMantenibilidad.INDEX_MANTEN_AVG, NodeProject.class, "8bfc6914-cee6-4dcb-9aab-45da574fcde3")
            .metric(SharedData.versionMetric)
            .formula((ActionCached<NodeProject>) this::formula)
            .ok();
    }

    public Measure formula(NodeProject nodeProject, ResultHolder resultHolder) {
        double total = 0;
        List<Measure> measures = resultHolder.load(NodeKink.SOURCE, FormulaMantenibilityIndexFile.class);
        System.out.println("LA " + measures.size());
        double valid = measures.stream().filter(measure -> measure.getResult() > 0)
            .count();

        total = measures.stream()
            .filter(measure -> !Double.isNaN(measure.getResult()))
            .filter(measure -> !Double.isInfinite(measure.getResult()))
            .filter(measure -> measure.getResult() > 0)
            .mapToDouble(Measure::getResult)
            .average().getAsDouble();

        total = (total == Double.POSITIVE_INFINITY) ? 100 : total;
        total = (total == Double.NEGATIVE_INFINITY) ? 0 : total;
        total = (total == Double.NaN) ? 100 : total;
        return new Measure(total);
    }
}
