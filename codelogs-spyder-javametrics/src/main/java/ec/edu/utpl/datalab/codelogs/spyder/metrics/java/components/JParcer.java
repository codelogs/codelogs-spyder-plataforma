/*
 * The MIT License (MIT)
 * =========================================================================================================
 *
 * Copyright (c) 10/10/16 12:30 Modulo  Proyecto rexcode-metrics-service-modules  Autor [rfcardenas]
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY
 * , WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ec.edu.utpl.datalab.codelogs.spyder.metrics.java.components;


import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.body.ClassOrInterfaceDeclaration;
import com.github.javaparser.ast.body.MethodDeclaration;
import com.github.javaparser.ast.visitor.VoidVisitorAdapter;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.core.hk2.AstGenerator;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.java.ast.JClass;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.java.ast.JNodeMethod;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.java.ast.JSource;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.model.tree.elements.NodeAbstractNode;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.modules.ModuleParser;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.modules.ProviderParser;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.modules.Version;
import ec.edu.utpl.datalab.codelogs.spyder.libs.filesystem.code_event2.support.FileSupport;
import org.apache.commons.vfs2.FileObject;
import org.jvnet.hk2.annotations.Service;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;


/**
 * * Created by rfcardenas
 */
@Service
@AstGenerator("java")
public class JParcer implements ProviderParser {


    @Override
    public ModuleParser provide() {
        return new ModuleParser(new Version(1, 2, 1), this::parser, new FileSupport("java", "java"));
    }

    public NodeAbstractNode parser(FileObject source) throws Exception {
        File file = new File(source.getName().getPath());
        CompilationUnit cunit = null;
        cunit = com.github.javaparser.JavaParser.parse(file, "UTF-8", true);

        JSource jfile = new JSource(source, cunit);
        NodeResolver nr = new NodeResolver(jfile);
        jfile.accept(nr);
        return jfile;
    }

    public Version version() {
        return new Version(1, 2, 1);
    }

    class NodeResolver extends VoidVisitorAdapter {
        HashMap<String, JClass> hashMap = new HashMap<>();
        private JSource source;

        public NodeResolver(JSource source) {
            this.source = source;
        }


        @Override
        public void visit(ClassOrInterfaceDeclaration ast, Object arg) {
            if (!ast.isInterface()) {
                resolveClass(ast);
            }
            super.visit(ast, arg);
        }


        private void resolveClass(ClassOrInterfaceDeclaration node) {
            if (node.getParentNode() instanceof CompilationUnit)
                resolveOuter(node);
            else if (node.getParentNode() instanceof ClassOrInterfaceDeclaration) {
                resolveInner(node);
            }
        }

        private void resolveOuter(ClassOrInterfaceDeclaration node) {
            String nameOuter = node.getName();
            JClass jouter = new JClass(nameOuter, node);

            hashMap.put(nameOuter, jouter);
            resolverMethods(jouter);
            source.addJClass(jouter);
        }

        private void resolveInner(ClassOrInterfaceDeclaration node) {
            ClassOrInterfaceDeclaration outer = (ClassOrInterfaceDeclaration) node.getParentNode();
            String nameOuter = outer.getName();
            String nameInner = node.getName();

            JClass jinner = new JClass(nameInner, node);
            JClass jouter = hashMap.get(nameOuter);
            if (jouter == null) {
                jouter = new JClass(nameOuter, outer);
                hashMap.put(nameOuter, jouter);
            }
            resolverMethods(jinner);
            jouter.addInnerClass(jinner);
        }


        private void resolverMethods(JClass jClass) {
            List<MethodDeclaration> md = jClass.getAst().getMembers()
                .stream()
                .filter(MethodDeclaration.class::isInstance)
                .map(MethodDeclaration.class::cast)
                .collect(Collectors.toList());
            for (MethodDeclaration methodDeclaration : md) {
                String name = methodDeclaration.getDeclarationAsString(false, false);
                JNodeMethod jmethod = new JNodeMethod(name, methodDeclaration);
                jClass.addJmethod(jmethod);
            }

        }
    }
}

