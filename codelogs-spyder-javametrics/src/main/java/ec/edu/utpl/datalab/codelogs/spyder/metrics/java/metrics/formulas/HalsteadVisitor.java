/*
 * The MIT License (MIT)
 * =========================================================================================================
 *
 * Copyright (c) 10/10/16 12:30 Modulo  Proyecto rexcode-metrics-service-modules  Autor [rfcardenas]
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY
 * , WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ec.edu.utpl.datalab.codelogs.spyder.metrics.java.metrics.formulas;

import com.github.javaparser.ast.Node;
import com.github.javaparser.ast.body.ClassOrInterfaceDeclaration;
import com.github.javaparser.ast.body.VariableDeclarator;
import com.github.javaparser.ast.expr.*;
import com.github.javaparser.ast.visitor.VoidVisitorAdapter;

import java.util.HashSet;
import java.util.Set;

/**
 * * Created by rfcardenas
 */
public class HalsteadVisitor extends VoidVisitorAdapter {
    private final Set<String> operators = new HashSet<String>(32);
    private final Set<String> operands = new HashSet<String>(32);
    private int numOperands = 0;
    private int numOperators = 0;
    private boolean inCompileTimeConstant = false;


    @Override
    public void visit(VariableDeclarator n, Object arg) {
        super.visit(n, arg);
        String var = String.valueOf(n.getId());
        registerOperand(var);
    }


    private int getNumDistinctOperands() {
        return operands.size();
    }

    private int getNumDistinctOperators() {
        return operators.size();
    }

    public int getLength() {
        return numOperands + numOperators;
    }

    public int getVocabulary() {
        return operands.size() + operators.size();
    }

    public double getDifficulty() {
        final int N2 = numOperands;
        final int n1 = getNumDistinctOperators();
        final int n2 = getNumDistinctOperands();
        return n2 == 0 ? 0.0 : ((double) n1 / 2.0) * ((double) N2 / (double) n2);
    }

    public double getVolume() {
        System.out.println("DEBUG --------------------------");
        System.out.println("LONGITUD " + getLength());
        System.out.println("VACB " + getVocabulary());
        final double vocabulary = (double) getVocabulary();
        System.out.println("DEBUG --------------------------");
        return (double) getLength() * Math.log(vocabulary) / Math.log(2.0);
    }

    public double getEffort() {
        return getDifficulty() * getVolume();
    }

    public double getBugs() {
        final double effort = getEffort();
        return Math.pow(effort, 2.0 / 3.0) / 3000.0;
    }

    @Override
    public void visit(StringLiteralExpr n, Object arg) {
        super.visit(n, arg);
        String exp = n.getValue();
        registerOperand(exp);
    }


    @Override
    public void visit(BinaryExpr n, Object arg) {
        super.visit(n, arg);
        registerSign(n.getOperator());
    }

    @Override
    public void visit(AssignExpr n, Object arg) {
        super.visit(n, arg);
        registerOperator(n.getOperator().toString());
    }

    @Override
    public void visit(UnaryExpr n, Object arg) {
        super.visit(n, arg);
        String ux = n.getExpr().toString();
        registerOperator(ux);
    }

    @Override
    public void visit(TypeExpr n, Object arg) {
        super.visit(n, arg);
    }


    @Override
    public void visit(MethodCallExpr n, Object arg) {
        super.visit(n, arg);
        Node node = n.getParentNode();
        while (node != null && !node.getClass().equals(ClassOrInterfaceDeclaration.class)) {
            node = node.getParentNode();
        }

        if (node != null) {
            ClassOrInterfaceDeclaration classOrInterfaceDeclaration = (ClassOrInterfaceDeclaration) node;
            StringBuilder stringBuilder = new StringBuilder();
            String methodName = n.getName();
            String classname = classOrInterfaceDeclaration.getName();
            stringBuilder.append(classname);
            stringBuilder.append(".");
            stringBuilder.append(methodName);
            stringBuilder.append("(");
            for (int i = 0; i < n.getTypeArgs().size(); i++) {
                if (i != 0) stringBuilder.append(",");
                stringBuilder.append(n.getTypeArgs().get(i));
            }
            stringBuilder.append(")");
            String m = stringBuilder.toString();
            if (m.isEmpty()) {
                registerOperator(m);
            }
        }

    }

    private void registerSign(BinaryExpr.Operator sign) {
        final String text = sign.name();
        registerOperator(text);
    }

    private void registerOperator(String operator) {
        numOperators++;
        operators.add(operator);
    }

    private void registerOperand(String operand) {
        numOperands++;
        operands.add(operand);
    }
}
