package ec.edu.utpl.datalab.codelogs.spyder.metrics.java.metrics.formulas.fragment;

import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.core.hk2.Metric;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.domain.Measure;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.holders.ResultHolder;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.java.metrics.definiciones.DefinitionesComplex;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.java.metrics.formulas.SharedData;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.java.metrics.formulas.file.FormulaCycloFile;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.metric.ActionCached;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.metric.BuilderMetric;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.metric.MetricProvider;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.metric.ModuleMetric;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.model.tree.NodeKink;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.model.tree.elements.NodeProject;
import org.jvnet.hk2.annotations.Service;

import java.util.List;

/**
 * * Created by rfcardenas
 */
@Service
@Metric("java")
public class FormulaAverageCycloFragment implements MetricProvider {
    @Override
    public ModuleMetric specification() {
        return BuilderMetric.fluet(DefinitionesComplex.cyclomaticComplex, NodeProject.class, "fdc3bb99-8e0c-4319-bad1-672488d94e13")
            .metric(SharedData.versionMetric)
            .formula((ActionCached<NodeProject>) this::formula)
            .ok();
    }

    public Measure formula(NodeProject nodeProject, ResultHolder resultHolder) {
        double total = 0;
        List<Measure> cyclorepo = resultHolder.load(NodeKink.SOURCE, FormulaCycloFile.class);
        total = cyclorepo.stream().mapToDouble(Measure::getResult)
            .average().getAsDouble();
        return new Measure(total);
    }
}
