/*
 * The MIT License (MIT)
 * =========================================================================================================
 *
 * Copyright (c) 10/10/16 12:30 Modulo  Proyecto rexcode-metrics-service-modules  Autor [rfcardenas]
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY
 * , WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ec.edu.utpl.datalab.codelogs.spyder.metrics.java.metrics.formulas;

/**
 * * Created by rfcardenas
 */
public interface KeyWords {
    String ABSTRACT = "abstract";
    String ASSERT = "assert";
    String BOOLEAN = "boolean";
    String BREAK = "break";
    String BYTE = "byte";
    String CASE = "case";
    String CATCH = "catch";
    String CHAR = "char";
    String CLASS = "class";
    String CONST = "const";
    String CONTINUE = "continue";
    String DEFAULT = "default";
    String DO = "do";
    String DOUBLE = "double";
    String ELSE = "else";
    String ENUM = "enum";
    String EXTENDS = "extends";
    String FINAL = "final";
    String FINALLY = "finally";
    String FLOAT = "float";
    String FOR = "for";
    String GOTO = "goto";
    String IF = "if";
    String IMPLEMENTS = "implements";
    String IMPORT = "import";
    String INSTANCEOF = "instanceof";
    String INT = "int";
    String INTERFACE = "interface";
    String LONG = "long";
    String NATIVE = "native";
    String NEW = "new";
    String PACKAGE = "package";
    String PRIVATE = "private";
    String PROTECTED = "protected";
    String PUBLIC = "public";
    String RETURN = "return";
    String SHORT = "short";
    String STATIC = "static";
    String STRICTFP = "strictfp";
    String SUPER = "super";
    String SWITCH = "switch";
    String SYNCHRONIZED = "synchronized";
    String THIS = "this";
    String THROW = "throw";
    String THROWS = "throws";
    String TRANSIENT = "transient";
    String TRY = "try";
    String VOID = "void";
    String VOLATILE = "volatile";
    String WHILE = "while";

    String TRUE = "true";
    String FALSE = "false";
    String NULL = "null";

    String MODULE = "module";
    String REQUIRES = "requires";
    String EXPORTS = "exports";
    String USES = "uses";
    String PROVIDES = "provides";
    String TO = "to";
    String WITH = "with";
}
