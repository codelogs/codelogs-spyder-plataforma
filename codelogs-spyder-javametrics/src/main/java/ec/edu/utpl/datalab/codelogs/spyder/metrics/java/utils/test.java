package ec.edu.utpl.datalab.codelogs.spyder.metrics.java.utils;

import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.Plataforma;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.holders.DefaultResultHolder;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.model.tree.NodeKink;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.model.tree.elements.NodeProject;
import org.apache.commons.vfs2.FileSystemException;
import org.apache.commons.vfs2.VFS;

/**
 * * Created by rfcardenas
 */
public class test {
    public static void main(String[] args) throws FileSystemException {
        String projectPAth = "/home/rfcardenas/Escritorio/codelogs_repositorio_open/codelogs-spyder-complements/codelogs-mvn-demo";
        Plataforma plataforma = new Plataforma();
        plataforma.findAndInstallModules();

        NodeProject javaProject = plataforma.generadores()
                .create(VFS.getManager().resolveFile(projectPAth))
                .get();

        plataforma.processor().process(javaProject);

        plataforma.measureHolder()
                .loadAllFrom(NodeKink.PROJECT).forEach(test::printCache);
    }
    public static void printCache(DefaultResultHolder.CacheEntry cacheEntry){
        System.out.println(cacheEntry.getMeasure());
        System.out.println(cacheEntry.getMetricVerticle());
    }
}
