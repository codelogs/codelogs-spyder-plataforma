package ec.edu.utpl.datalab.codelogs.spyder.metrics.java.metrics.formulas.file;

import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.core.hk2.Metric;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.domain.Measure;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.holders.ResultHolder;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.java.ast.JSource;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.java.metrics.definiciones.DefinicionMantenibilidad;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.java.metrics.formulas.SharedData;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.metric.BuilderMetric;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.metric.MetricProvider;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.metric.ModuleMetric;
import org.jvnet.hk2.annotations.Service;

import java.util.Optional;

/**
 * * Created by rfcardenas
 */
@Service
@Metric("java")
public class FormulaMantenibilityIndexFile implements MetricProvider {
    @Override
    public ModuleMetric specification() {
        return BuilderMetric.fluet(DefinicionMantenibilidad.INDEX_MANTEN, JSource.class, "X")
            .metric(SharedData.versionMetric)
            .addDependecy(FormulaCycloFile.class)
            .addDependecy(FormulaLocFile.class)
            .addDependecy(FormulaHalsteadVFile.class)
            .formula(this::formula)
            .ok();
    }

    public Measure formula(JSource node, ResultHolder resultHolder) {
        double total = 0;

        Optional<Measure> cacheCl = resultHolder.load(node, FormulaCycloFile.class);
        Optional<Measure> cachelc = resultHolder.load(node, FormulaLocFile.class);
        Optional<Measure> cacheHv = resultHolder.load(node, FormulaHalsteadVFile.class);
        if (cacheCl.isPresent() && cachelc.isPresent() && cacheHv.isPresent()) {
            double cyclo = cacheCl.get().getResult();
            double halvol = cacheHv.get().getResult();
            double loc = cachelc.get().getResult();
            double expr2 = (171 - 5.2 * Math.log(halvol) - 0.23 * (cyclo) - 16.2 * Math.log(loc)) * 100 / 171;
            total = Math.max(0, expr2);
        } else {
            System.err.print("Err Im");
        }
        total = (total == Double.POSITIVE_INFINITY) ? 100 : total;
        total = (total == Double.NEGATIVE_INFINITY) ? 0 : total;
        total = (total == Double.NaN) ? 100 : total;

        return new Measure(total);
    }
}
