package ec.edu.utpl.datalab.codelogs.spyder.metrics.java.metrics.formulas.clase;

import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.core.hk2.Metric;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.domain.Measure;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.java.ast.JClass;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.java.metrics.definiciones.DefinitionHalstead;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.java.metrics.formulas.HalsteadVisitor;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.java.metrics.formulas.SharedData;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.metric.*;
import org.jvnet.hk2.annotations.Service;

/**
 * * Created by rfcardenas
 */
@Service
@Metric("java")
public class FormulaHalsteadVolumenClass implements MetricProvider {
    @Override
    public ModuleMetric specification() {
        return BuilderMetric.fluet(DefinitionHalstead.HALSTED_VOL, JClass.class, "X")
            .metric(SharedData.versionMetric)
            .formula((Action<JClass>) this::formula)
            .ok();
    }

    public Measure formula(JClass node) throws MeasureBadException {
        double totalLines = 0;
        HalsteadVisitor halsteadVisitor = new HalsteadVisitor();
        node.accept(halsteadVisitor);
        totalLines = halsteadVisitor.getVolume();
        return new Measure(totalLines);
    }
}
