/*
 * The MIT License (MIT)
 * =========================================================================================================
 *
 * Copyright (c) 10/10/16 12:30 Modulo  Proyecto rexcode-metrics-service-modules  Autor [rfcardenas]
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY
 * , WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ec.edu.utpl.datalab.codelogs.spyder.metrics.java.ast;


import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.visitor.VoidVisitorAdapter;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.model.tree.elements.NodeAbstractNode;
import org.apache.commons.vfs2.FileObject;

import java.util.ArrayList;
import java.util.List;

/**
 * * Created by rfcardenas
 */
public class JSource extends NodeAbstractNode implements JVisitable, AstVisitable {
    private CompilationUnit compilationUnit;
    private List<JClass> jClassList = new ArrayList<>();

    public JSource(FileObject reference, CompilationUnit compilationUnit) {
        super(reference);
        this.compilationUnit = compilationUnit;
    }

    public void addJClass(JClass clase) {
        this.jClassList.add(clase);
        setAsParentOf(clase);
    }

    public List<JClass> getjClassList() {
        return jClassList;
    }

    public CompilationUnit getCompilationUnit() {
        return compilationUnit;
    }

    public void accept(IJTraverser traverser) {
        traverser.visit(this);
    }

    @Override
    public void accept(VoidVisitorAdapter visitor) {
        compilationUnit.accept(visitor, null);
    }

    @Override
    public void clear() {
        this.compilationUnit = null;
    }

}
