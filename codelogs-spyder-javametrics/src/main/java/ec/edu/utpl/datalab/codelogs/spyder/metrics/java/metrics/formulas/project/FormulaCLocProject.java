/*
 * The MIT License (MIT)
 * =========================================================================================================
 *
 * Copyright (c) 10/10/16 12:30 Modulo  Proyecto rexcode-metrics-service-modules  Autor [rfcardenas]
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY
 * , WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ec.edu.utpl.datalab.codelogs.spyder.metrics.java.metrics.formulas.project;

import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.core.hk2.Metric;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.domain.Measure;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.java.ast.JSource;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.java.metrics.definiciones.DefinicionesSourceLines;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.java.metrics.formulas.SharedData;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.java.metrics.visitors.CommentVisitor;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.metric.*;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.model.tree.elements.NodeProject;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.model.tree_traverser.spliterators_type.TreeTypeSpliterator;
import org.jvnet.hk2.annotations.Service;

import java.util.stream.StreamSupport;

/**
 * * Created by rfcardenas
 */
@Service
@Metric("java")
public class FormulaCLocProject implements MetricProvider {
    @Override
    public ModuleMetric specification() {
        return BuilderMetric.fluet(DefinicionesSourceLines.COMMENT_LINES, NodeProject.class, "64f860b9-ceb6-491e-bce6-1c4a29c0e645")
            .metric(SharedData.versionMetric)
            .formula((Action<NodeProject>) this::formula)
            .ok();
    }

    public Measure formula(NodeProject nodeProject) throws MeasureBadException {
        int totalLines = 0;
        TreeTypeSpliterator<JSource> typeSpliterator = TreeTypeSpliterator.create(nodeProject, JSource.class);
        totalLines = StreamSupport.stream(typeSpliterator, false)
            .filter(jSource -> jSource.getCompilationUnit() != null)
            .mapToInt(jSource -> {
                try {
                    CommentVisitor commentVisitor = new CommentVisitor();
                    jSource.accept(commentVisitor);
                    return commentVisitor.getLineComments();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return 0;
            }).sum();
        return new Measure(totalLines);
    }
}
