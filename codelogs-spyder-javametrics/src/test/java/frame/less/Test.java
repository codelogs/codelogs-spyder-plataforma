/*
 * The MIT License (MIT)
 * =========================================================================================================
 *
 * Copyright (c) 10/10/16 12:30 Modulo  Proyecto rexcode-metrics-service-modules  Autor [rfcardenas]
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY
 * , WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package frame.less;

import com.github.javaparser.ParseException;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.Plataforma;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.core.MetricVerticle;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.metric.MeasureBadException;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.model.tree.NodeKink;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.model.tree.elements.NodeProject;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.model.tree_traverser.spliterators.DeepFirstTreeSpliterator;
import org.apache.commons.vfs2.FileObject;
import org.apache.commons.vfs2.VFS;

import java.io.IOException;
import java.util.Optional;

/**
 * * Created by rfcardenas
 */
public class Test {
    public static void main(String[] args) throws IOException, ParseException, MeasureBadException {

        FileObject fileObject = VFS.getManager().resolveFile("/home/rfcardenas/Escritorio/behavior-trees-master");
        Plataforma plataforma = new Plataforma();
        plataforma.findAndInstallModules();
        for (MetricVerticle metricVerticle : plataforma.metricRepository().getAll()) {
            System.out.println("-->");
            System.out.println(metricVerticle);
        }


        Optional<NodeProject> project = plataforma.generadores().create(fileObject);

        DeepFirstTreeSpliterator x2 = DeepFirstTreeSpliterator.create(project.get());
        plataforma.processor().observable().subscribe(processorResult -> {
            x2.forEachRemaining(node -> {
                processorResult.getResultHolder().loadAllMetrics(node.getClass()).stream().filter(metricVerticle -> metricVerticle.getSpecification().getNodeKinkTarget().equals(NodeKink.SOURCE)).forEach(metricVerticle -> {
                    System.out.println(metricVerticle.getSpecification().getName());
                    System.out.println(metricVerticle.getSpecification().getNodeKinkTarget());
                    System.out.println(processorResult.getResultHolder().getMeasureFor(node, metricVerticle));
                });
            });
        });
        DeepFirstTreeSpliterator jMethodTreeTypeSpliterator = DeepFirstTreeSpliterator.create(project.get());
        System.out.println(jMethodTreeTypeSpliterator.estimateSize());
        System.out.println(project.get());
        plataforma.processor().process(project.get());


        /**plataforma.processor().addListenerEnd(metricResult -> {
         System.out.println(metricResult.getAllMetrics().length);
         for (MetricVerticle metricVerticle : metricResult.getAllMetrics()) {
         System.out.println("Valuasdassadassadasdasddde " + metricVerticle);
         System.out.println(metricResult.getAllMeasures(metricVerticle, TreeKind.PROJECT));
         }
         });
         plataforma.processor().process(project.get(),plataforma.metricRepository().getAll());**/
    }
}
