package ec.edu.utpl.datalab.codelogs.spyder.metrics.java.metrics.formulas.file;

import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.domain.Measure;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.java.ast.JSource;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.java.components.JParcer;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.java.utils.ResourceLoader;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.model.tree.elements.NodeAbstractNode;
import ec.edu.utpl.datalab.codelogs.spyder.libs.common.ObjectUtils;
import ec.edu.utpl.datalab.codelogs.spyder.libs.filesystem.search.FilterSet;
import ec.edu.utpl.datalab.codelogs.spyder.libs.filesystem.search.SearchFileSystem;
import org.apache.commons.vfs2.FileObject;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

/**
 * * Created by rfcardenas
 */
public class FormulaCycloFileTest {
    Map<String, Double> valuesExp = new HashMap<>();

    @Test
    public void formula() throws Exception {
        valuesExp.put("HelloUTPL.java", 9.0);
        valuesExp.put("HelloUTPLL2.java", 0.0);

        FormulaCycloFile formulaCycloFile = new FormulaCycloFile();
        Optional<FileObject> app = ResourceLoader.loadApp();
        if (app.isPresent()) {
            FileObject appFolder = app.get();
            SearchFileSystem searchFileSystem = SearchFileSystem.newSearch(5)
                .withFilter(FilterSet.filterByExtension("java"))
                .build();
            FileObject[] codefiles = searchFileSystem.executeFinder(appFolder);
            for (FileObject codefile : codefiles) {
                String codename = codefile.getName().getBaseName();
                JParcer jParcer = new JParcer();
                NodeAbstractNode ast = jParcer.parser(codefile);
                Measure measure = formulaCycloFile.formula(ObjectUtils.as(ast, JSource.class));
                System.out.println(measure.getResult() + " For " + codefile.getName().getBaseName());
                int value1Exp = valuesExp.get(codename).intValue();
                int resultCalc = (int) measure.getResult();
                System.out.println("RESULTADO " + resultCalc + "Esperado " + valuesExp);

            }
        }
    }

}
