package ec.edu.utpl.datalab.codelogs.spyder.metrics.java.utils;

import ec.edu.utpl.datalab.codelogs.spyder.libs.common.StreamUtilis;
import ec.edu.utpl.datalab.codelogs.spyder.libs.filesystem.search.FilterSet;
import ec.edu.utpl.datalab.codelogs.spyder.libs.filesystem.search.SearchFileSystem;
import org.apache.commons.vfs2.FileObject;
import org.junit.Assert;
import org.junit.Test;

import java.util.Optional;

/**
 * * Created by rfcardenas
 */
public class LineCounterTest {
    @Test
    public void getCommentFor() throws Exception {
        Optional<FileObject> app = ResourceLoader.loadApp();
        if (app.isPresent()) {
            FileObject appFolder = app.get();
            SearchFileSystem searchFileSystem = SearchFileSystem.newSearch(5)
                .withFilter(FilterSet.filterByExtension("java"))
                .build();
            FileObject[] codefiles = searchFileSystem.executeFinder(appFolder);
            int total = 0;
            for (FileObject codefile : codefiles) {
                String content = StreamUtilis.copyToStringAndClose(codefile.getContent().getInputStream());
                total += LineCounter.getCommentFor(content);
            }
            Assert.assertEquals(3, total);
        }
    }

    @Test
    public void getNumberLinesFromString() throws Exception {
        Optional<FileObject> app = ResourceLoader.loadApp();
        if (app.isPresent()) {
            FileObject appFolder = app.get();
            SearchFileSystem searchFileSystem = SearchFileSystem.newSearch(5)
                .withFilter(FilterSet.filterByExtension("java"))
                .build();
            FileObject[] codefiles = searchFileSystem.executeFinder(appFolder);
            int total = 0;
            for (FileObject codefile : codefiles) {
                String content = StreamUtilis.copyToStringAndClose(codefile.getContent().getInputStream());
                total += LineCounter.getNumberLinesFromString(content);
            }
            Assert.assertEquals(53, total);
        }
    }

}
