package ec.edu.utpl.datalab.codelogs.spyder.metrics.java.utils;

import ec.edu.utpl.datalab.codelogs.spyder.metrics.base.Plataforma;
import ec.edu.utpl.datalab.codelogs.spyder.metrics.java.components.JParcer;

/**
 * * Created by rfcardenas
 */
public class PlataformaTest {
    public static Plataforma plataforma = new Plataforma();

    static {
        plataforma.installer().install(new JParcer());
    }
}
